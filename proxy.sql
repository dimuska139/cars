-- phpMyAdmin SQL Dump
-- version 4.2.12deb2
-- http://www.phpmyadmin.net
--
-- Хост: localhost
-- Время создания: Дек 28 2015 г., 16:47
-- Версия сервера: 5.5.44-0+deb8u1-log
-- Версия PHP: 5.6.13-0+deb8u1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- База данных: `cars`
--

-- --------------------------------------------------------

--
-- Структура таблицы `proxy`
--

CREATE TABLE IF NOT EXISTS `proxy` (
`id` int(11) NOT NULL,
  `proxy` varchar(15) NOT NULL,
  `proxy_login` varchar(50) NOT NULL,
  `proxy_pass` varchar(50) NOT NULL,
  `http_port` int(6) NOT NULL DEFAULT '80',
  `socks5_port` int(6) NOT NULL,
  `avito_login` varchar(50) NOT NULL,
  `auto_login` varchar(50) NOT NULL,
  `avito_pass` varchar(50) NOT NULL,
  `auto_pass` varchar(50) NOT NULL,
  `createdate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `phone` varchar(20) DEFAULT '',
  `confirmed_avito` tinyint(1) NOT NULL,
  `confirmed_auto` tinyint(1) NOT NULL,
  `user_id` int(11) NOT NULL,
  `status` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `proxy`
--

INSERT INTO `proxy` (`id`, `proxy`, `proxy_login`, `proxy_pass`, `http_port`, `socks5_port`, `avito_login`, `auto_login`, `avito_pass`, `auto_pass`, `createdate`, `phone`, `confirmed_avito`, `confirmed_auto`, `user_id`, `status`) VALUES
(2, '185.104.15.14', 'so_5cars', '6rkWdxglJ', 24531, 24530, '', '', '', '', '2015-11-18 20:35:30', 'test', 0, 0, 0, 0),
(3, '185.80.149.133', 'so_5cars', '6rkWdxglJ', 24532, 24533, '', '', '', '', '2015-11-15 19:28:12', 'test', 0, 0, 0, 0),
(4, '185.80.149.133', 'so_cars10', 'dOkEEk34w', 24532, 24533, '', '', '', '', '2015-11-19 13:47:36', 'dfgdg', 0, 0, 0, 0),
(5, '81.177.182.192', 'so_cars', 'ebd5JCy', 24677, 24688, '', '', '', '', '2015-11-19 20:25:41', 'erre', 0, 0, 0, 0),
(6, '217.106.239.12', 'so_cars', 'm627LL', 24529, 24531, '', '', '', '', '2015-11-20 10:40:24', 'eee', 0, 0, 0, 0),
(7, '217.106.238.16', 'so_cars', 'm627LL', 24529, 24531, '', '', '', '', '2015-11-20 10:43:17', '', 0, 0, 0, 0);

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `proxy`
--
ALTER TABLE `proxy`
 ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `proxy`
--
ALTER TABLE `proxy`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=8;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
