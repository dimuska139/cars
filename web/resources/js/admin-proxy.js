$(document).ready(function(){
    $('.copy-ctrl .copy .copy-button').zclip({
        path:'/resources/js/ZeroClipboard.swf',
        copy: function(){
            return $(this).parent('.copy').parent('.copy-ctrl').find('.text').html()
        },
        beforeCopy:function(){
            $(this).fadeOut();
        },
        afterCopy:function(){
            $(this).fadeIn();
        }
    });
    
    $('.create-proxy-form input').bind('change keyup input click', function(event){
        this.value = this.value.replace(/\s+/g, '');
    });
});