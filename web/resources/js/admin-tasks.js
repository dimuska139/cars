$(document).ready(function(){
    $('.cars-list .line .status').on('change', function(){
        var car_id = $(this).data('id');
        var car_status = $(this).val();
        var prev_status = $(this).data('prev');
        var me = this;
        $.ajax({
            type: "post",
            url: "/ajax/changestatus",
            dataType: "json",
            data: {
                id: car_id,
                status: car_status
            },
            async: false,
            success: function(res){
                if (res.success){
                    $(me).data('prev', car_status);
                    $('.car_'+car_id).removeClass('status_'+prev_status);
                    $('.car_'+car_id).addClass('status_'+car_status);
                }
            }
        });
    });
    
    $('.url_input').on('change input cut copy paste', function(){
        if ($(this).hasClass('autoru_url')){
            var url = "/ajax/changeautourl";
        } else {
            var url = "/ajax/changeavitourl";
        }
        var car_url = $(this).val();
        var car_id = $(this).data('id');
        var me = this;
        $.ajax({
            type: "post",
            url: url,
            dataType: "json",
            data: {
                id: car_id,
                url: car_url
            },
            async: false,
            success: function(res){
                if (!res.success){
                    alert("Ошибка при сохранении");
                }
            }
        });
    });
});