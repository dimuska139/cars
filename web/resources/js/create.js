$(document).ready(function(){
    //$('.page .content').css('height', 'initial');
    var autoru_timer = false;
    var avitoru_timer = false;
    var phone_id = $('#phone_id').val().length!=0?$('#phone_id').val():0;
//    $('#year').parent('.form-line').find('.indicator').show();
    $('#condition').parent('.form-line').find('.indicator').show();
//    $('.body_type').parent('.form-line').find('.indicator').show();
//    $('#color').parent('.form-line').find('.indicator').show();
//    $('.engine_type').parent('.form-line').find('.indicator').show();
//    $('#engine_volume').parent('.form-line').find('.indicator').show();
//    $('.transmission_type').parent('.form-line').find('.indicator').show();
//    $('.drive_type').parent('.form-line').find('.indicator').show();
    $('.rudder').parent('.form-line').find('.indicator').show();
 //   $("#subway").parent('.form-line').find('.indicator').show();
 //   $("#city").parent('.form-line').find('.indicator').show();
//    $('#subway').parent('.form-line').find('.indicator').show();

    $('#inspection-place').parent('.form-line').find('.indicator').show();
    
    function checkNext(){
        if ($('.page-car .indicator:visible').length==13){
            $('.page-car .next').addClass('next-active');
            $('.tab-car .indicator').show();
        } else {
            $('.page-car .next').removeClass('next-active');
            $('.tab-car .indicator').hide();
        }
    }
    
    
  /*  $('#city').on('change', function(){
        if ($(this).val()==0){
            $('#subway').prop('disabled', true);
            $('#subway').css('background-color','#C8C8C8');
            $('#subway').empty();
            $(this).parent('.form-line').find('.indicator').hide();
        } else {
            var me = this;
            var params = {};
            params.city_id = $(this).val();
            $.ajax({
                type: "post",
                url: "/ajax/subwaystations",
                dataType: "json",
                data: params,
                beforeSend: function(){
                },
                complete: function(){
                },
                success: function(response){
                    $(me).parent('.form-line').find('.indicator').show();
                    $('#subway').html('<option value="0">Не указывать</option>');
                    for (var i=0;i<response.subwaystations.length;i++){
                        $('#subway').html($('#subway').html()+'<option value="'+response.subwaystations[i].id+'">'+response.subwaystations[i].name+'</option>');
                    }
                    $('#subway').prop('disabled', false);
                    $('#subway').css('background-color','initial');
                }
            });
            
            
        }
    });*/
    
    $('#year').on('change', function(){
        if ($(this).val()!=0){
            $(this).parent('.form-line').find('.indicator').show();
            $("#year [value='0']"). remove();
            checkNext();
        }
    });
    
    $('#model').on('change', function(){
        if ($(this).val()!=0){
            if ($(this).val()==-1){
                $('#another_model').html('');
                $("#another_model").prop("disabled", false);
                $(this).parent('.form-line').find('.indicator').hide();
                $('#another_model').show();
            } else {
                $('#another_model').hide();
                $("#another_model").prop("disabled", true);
                $(this).parent('.form-line').find('.indicator').show();
            }
            $("#model [value='0']"). remove(); 
            checkNext();
        }
    });
    
    $('#another_model').bind('change keyup input click', function(event){
        this.value = this.value.charAt(0).toUpperCase() + this.value.substr(1);
        if (this.value.charAt(0)==' ')
            this.value = this.value.substr(1);
        if (this.value.length>20)
            this.value = this.value.substr(0, this.value.length-1);
        if ($('#another_model').val().length>0){
            $('#model').parent('.form-line').find('.indicator').show(); 
        } else {
            $('#model').parent('.form-line').find('.indicator').hide();
        }
        checkNext();
    });
    
    $('#engine_volume').on('change', function(){
        if ($(this).val()!=0){
            $(this).parent('.form-line').find('.indicator').show();
            $("#engine_volume [value='0']"). remove();
            checkNext();
        }
    });
    
    $('#brand').on('change', function(){
        var params = new Object();
        params.brand_id = $(this).val();
        if (params.brand_id != 0){
            $("#brand [value='0']"). remove();
            $(this).parent('.form-line').find('.indicator').show();    
            $.ajax({
                type: "post",
                url: "/ajax/models",
                dataType: "json",
                data: params,
                beforeSend: function(){
                },
                complete: function(){
                },
                success: function(response){
                    $('#model').parent('.form-line').find('.indicator').hide();
                    $('#model').html('');
                    $('#model').append('<option value="0"></option>');
                    $.each(response, function(index, model){
                        $('#model').append('<option value="'+model.id+'">'+model.name+'</option>');
                    });
                    $('#model').append('<option value="-1">Другая</option>');
                    $('#another_model').html('');
                    $("#another_model").prop("disabled", true);
                    $('#another_model').hide();
                //    $('#model').parent('.form-line').find('.indicator').fadeIn();
                    checkNext();
                }
            });
            if (!$('#model').parent('div').is(':visible'))
                $('#model').parent('div').show();
        } else {
            $('#model').parent('div').hide();   
        }
    });
    
    $('#fileupload').fileupload({
        dataType: 'json',
        start: function(e) {
            $('.camera_container .progress').html('');        
        },
        done: function (e, data) {
            if (data.result.success){
                $('.tab-photos .indicator').show();
                var number = $('.uploaded-image').length;
                if (number==10)
                    return;
                number+=1;
                if (number==10){
                    $('.camera_container').fadeOut();
                }
                $(".camera_container").before('<div class="preview-container" data-src="'+data.result.url+'" style="background: url('+data.result.url+') no-repeat center; background-size: cover;">'+
                        '<input class="uploaded-image" type="hidden" name="photos_id[]" value="'+data.result.id+'">'+
                        '<div class="number">'+number+'</div>'+
                        '<img data-id="'+data.result.id+'" class="delete" src="/resources/img/delete.png"/>'+
                '</div>');
            /*    $('.uploaded').append('<div class="preview-container" style="background: url('+data.result.url+') no-repeat center; background-size: cover;">'+
                        '<input class="uploaded-image" type="hidden" name="photos_id[]" value="'+data.result.id+'">'+
                        '<div class="number">'+number+'</div>'+
                        '<img data-id="'+data.result.id+'" class="delete" src="/resources/img/close.png"/>'+
                '</div>');*/
                $('.page-photos .pass').hide();
                $('.page-photos .next').fadeIn();
                $('.camera_container .progress').html(''); 
            }
            else {
                $('.camera_container .progress').html('');
                alert("Проверьте расширение загружаемого файла. Можно загружать только фотографии в формате jpg или png");
            }
        },
        progressall: function (e, data) {
            var progress = parseInt(data.loaded / data.total * 100, 10);
            $('.camera_container .progress').html(progress+'%');
        }
    });
    
    $('.body_type .item').on('click', function(){
        $('.body_type .item').removeClass('active');
        $('.body_type .item').each(function(){
            var default_img = $(this).data('img');
            $(this).css('background-image', "url('/resources/img/body/"+default_img+"')");
        });
        var img_name = $(this).data('img');
        var img_name_arr = img_name.split('.');
        $(this).css('background-image', "url('/resources/img/body/"+img_name_arr[0]+'a.'+img_name_arr[1]+"')");
        $(this).addClass('active');
        $('#body_type').val($(this).data('id'));
        $(this).parent('.col').parent('.body_type').parent('.form-line').find('.indicator').show();
        checkNext();
    });
    
    $('.body_type .item').on({
        mouseenter: function () {
            if (!$(this).hasClass('active')){
                var default_img = $(this).data('img');
                var img_name_arr = default_img.split('.');
                $(this).css('background-image', "url('/resources/img/body/"+img_name_arr[0]+'h.'+img_name_arr[1]+"')");
            }
        },
        mouseleave: function () {
            if (!$(this).hasClass('active')){
                var default_img = $(this).data('img');
                $(this).css('background-image', "url('/resources/img/body/"+default_img+"')");
            }
        }
    });
    
    
    $('.engine_type .item').on('click', function(){
        $('.engine_type .item').removeClass('active');
        $(this).addClass('active');
        $('#engine_type').val($(this).data('id'));
        $(this).parent('div').parent('.form-line').find('.indicator').show();
        checkNext();
    });
    
    $('.transmission_type .item').on('click', function(){
        $('.transmission_type .item').removeClass('active');
        $(this).addClass('active');
        $('#transmission_type').val($(this).data('id'));
        $(this).parent('div').parent('.form-line').find('.indicator').show();
        checkNext();
    });
    
    $('.drive_type .item').on('click', function(){
        $('.drive_type .item').removeClass('active');
        $(this).addClass('active');
        $('#drive_type').val($(this).data('id'));
        $(this).parent('div').parent('.form-line').find('.indicator').show();
        checkNext();
    });
    
    $('.rudder .item').on('click', function(){
        $('.rudder .item').removeClass('active');
        $(this).addClass('active');
        $('#rudder').val($(this).data('id'));
    });
    
    $('#run').bind('change keyup input click', function(event){
        if (this.value.match(/[^0-9(\s{1})]/g)) {
            this.value = this.value.replace(/[^0-9(\s{1})]/g, '');
        }
        if (this.value.charAt(0)=='0')
            this.value = this.value.substr(1);
        if (this.value.length>7)
            this.value = this.value.substr(0, this.value.length-1);
        var str = this.value.replace(/\s+/g, '');
        this.value = str.replace(/(\d)(?=(\d\d\d)+([^\d]|$))/g, '$1 ')
        if ($('#run').val().length>5){
            $('#run').parent('.form-line').find('.indicator').show();
        } else
            $('#run').parent('.form-line').find('.indicator').hide();
        checkNext();
    });
    
    $('#engine_power').bind('change keyup input click', function(event){
        if (this.value.match(/[^0-9]/g)) {
            this.value = this.value.replace(/[^0-9]/g, '');
        }
        if (this.value.charAt(0)=='0')
            this.value = this.value.substr(1);
        if ($('#engine_power').val().length>1){
            $('#engine_power').parent('.form-line').find('.indicator').show();
        } else
            $('#engine_power').parent('.form-line').find('.indicator').hide();
        checkNext();
    });
    
    $('.page-car .next').on('click', function(){
        if (!$(this).hasClass('next-active')) return;
        $('.page-create').hide();
        $('.page-photos').show();
        $('.tab-car').removeClass('active');
        $('.tab-photos').addClass('active');
    });
    
    $('.page-photos .prev').on('click', function(){
        $('.page-car').show();
        $('.page-photos').hide();
        $('.tab-photos').removeClass('active');
        $('.tab-car').addClass('active');
        
    });
    
    $('.page-photos .next, .page-photos .pass').on('click', function(){
        $('.page-photos').hide();
        $('.page-price-and-comment').show();
        $('.tab-photos').removeClass('active');
        $('.tab-price').addClass('active');
    });
    
    
    $('#contact').bind('change keyup input', function(event){
        if (this.value.charAt(0)==' ')
            this.value = this.value.substr(1);
    });
  
    $('#phone').bind('change keyup input click', function(event){
        if ($(this).attr('readonly'))
            return;
        if (this.value.match(/[^0-9]/g)) {
            this.value = this.value.replace(/[^0-9]/g, '');
        }
        if (event.type=='keyup' && event.keyCode!=8){
            var arr = ['+','7','X','X','X','X','X','X','X','X','X','X'];
            var cur_str = this.value.replace(/\s+/g, '');
            for (var i=1;i<=cur_str.length;i++){
                var ind = arr.indexOf('X');
                if (ind!=-1){
                    arr[ind] = cur_str.charAt(i);
                }
            }
            var str = '';
            for (var i=0;i<arr.length;i++){
                if (arr[i]=='X') break;
                str+=arr[i];
            }
        } else {
            var str = this.value.substr(1);
            str = '+7'+str;
        }
        this.value = str;


        if ($('#phone').val().length==12){
            $('#phone').parent('.form-line').find('.indicator').show();
            if ($('.confirm-code-place:visible').length!=2){
                $('.page-contacts .confirm').fadeIn();
            }
        } else {
            $('#phone').parent('.form-line').find('.indicator').hide();
            $('.page-contacts .confirm').fadeOut();
        }
    /*    if ($('.page-contacts .indicator:visible').length==2){
            $('.tab-contacts .indicator').show();
            $('.page-contacts .next').addClass('next-active')
        } else {
            $('.page-contacts .next').removeClass('next-active')
        }*/
    });
    
    function avitoTimer(){
        $('#avitoru_timer .value').html(parseInt($('#avitoru_timer .value').html())-1);
        if (parseInt($('#avitoru_timer .value').html())==0) {
            clearInterval(avitoru_timer);
            $('#avitoru_timer').hide();
            $('#avitoru_code_get').html('Выслать ещё раз').show();
        }
    }
    
    function autoTimer(){
        $('#autoru_timer .value').html(parseInt($('#autoru_timer .value').html())-1);
        if (parseInt($('#autoru_timer .value').html())==0) {
            clearInterval(autoru_timer);
            $('#autoru_timer').hide();
            $('#autoru_code_get').html('Выслать ещё раз').show();
        }
    }
    
    // Повторная отправка кода
    $('#registration_repeat').on('click', function(){
        $(this).hide();
        $('#autoru_code,#avitoru_code').val('').prop('disabled', true);
        $( ".page-contacts .confirm" ).trigger( "click" );
    });
    
    
    
    $('#autoru_code').on('change keyup input', function(){
        if (this.value.match(/[^0-9]/g)) {
            this.value = this.value.replace(/[^0-9]/g, '');
        }
        $('#autoru_code_error').html('');
        if ($(this).val().length>0){
            $('#autoru_code_get').hide();
            $('#autoru_timer').hide();
            $('#autoru_code_repeat').hide();
            $('#autoru_code_confirm').show();
        } else {
            $('#autoru_code_get').show();
            $('#autoru_code_confirm').hide();
        /*    if (parseInt($('#autoru_timer .value').html())!=0){
                $('#autoru_timer').show();
            } else {
                $('#autoru_timer').hide();
            }*/
        }
    });
    
    
    $('#avitoru_code').on('change keyup input', function(){
        if (this.value.match(/[^0-9]/g)) {
            this.value = this.value.replace(/[^0-9]/g, '');
        }
        $('#avitoru_code_error').html('');
        if ($(this).val().length>0){
            $('#avitoru_code_get').hide();
            $('#avitoru_timer').hide();
            $('#avitoru_code_repeat').hide();
            $('#avitoru_code_confirm').show();
        } else {
            $('#avitoru_code_get').hide();
            $('#avitoru_code_confirm').hide();
            if (parseInt($('#avitoru_timer .value').html())!=0){
          //      $('#avitoru_timer').show();
            //    $('#avitoru_code_repeat').hide();
            } else {
         //       $('#avitoru_timer').hide();
            //    $('#avitoru_code_repeat').show();
            }
        }
    });
    
    $('#avitoru_code_get, #autoru_code_get').on('click', function(){
        var me = this;
        var params = new Object();
        params.name = $('#contact').val();
        params.phone = $('#phone').val().replace(/\s+/g, '');
        params.phone_id = $('#phone_id').val();
        if ($(this).attr('id')=='autoru_code_get') {
            params.type = "auto";
        } else {
            params.type = "avito";
        }
        $.ajax({
            type: "post",
            url: "/ajax/startpartial",
            dataType: "json",
            data: params,
            beforeSend: function() {
                if ($(me).attr('id')=='autoru_code_get') {
                    $('#autoru_code_get').hide();
                    $('#send-code-auto-ajax-loader').show();
                    $('#autoru_code_error').hide();
                } else {
                    $('#avitoru_code_get').hide();
                    $('#send-code-avito-ajax-loader').show();
                    $('#avitoru_code_error').hide();
                }
            },
            complete: function(){
                if ($(me).attr('id')=='autoru_code_get') {
                    $('#send-code-auto-ajax-loader').hide();
                } else {   
                    $('#send-code-avito-ajax-loader').hide();
                }
            },
            success: function(response){
                if (response.success){
                    if ($(me).attr('id')=='autoru_code_get') {
                        $('#autoru_code_get').show();
                        $('#autoru_code').prop('disabled', false).css('background-color', 'initial');
                    } else {
                        $('#avitoru_code_get').show();
                        $('#avitoru_code').prop('disabled', false).css('background-color', 'initial');
                    }
                } else {
                    if (response.status == 'wait') {
                        if ($(me).attr('id')=='autoru_code_get') {
                            $('#autoru_code').prop('disabled', false).css('background-color', 'initial');
                            $('#autoru_timer .value').html(response.value);
                            $('#autoru_timer').show();
                            setTimeout(function(){
                                $('#autoru_timer').fadeOut('slow', function(){
                                    $('#autoru_code_get').show();
                                });
                            }, 3000);
                        //    $('#autoru_code_get').hide();
                        } else {
                            $('#avitoru_code').prop('disabled', false).css('background-color', 'initial');
                            $('#avitoru_timer .value').html(response.value);
                            $('#avitoru_timer').show();
                            setTimeout(function(){
                                $('#avitoru_timer').fadeOut('slow', function(){
                                    $('#avitoru_code_get').show();
                                });
                            }, 3000);
                        }
                    } else if (response.status == 'used') { // Тут used может прийти только от Avito
                        $('#avitoru_code_confirm').hide();
                        $('#avitoru_code_error').html('Данный телефон уже зарегистрирован на Avito.ru!').show();
                    } else {
                        if ($(me).attr('id')=='autoru_code_get') {
                            $('#autoru_code_error').html(response.message);
                            $('#autoru_code_get').show();
                        } else {
                            $('#avitoru_code_error').html(response.message);
                            $('#avitoru_code_get').show();
                        }
                    }
                }
            }
        });
    });
    
    $('.page-contacts .confirm, .start_all').on('click', function(){
        $(this).hide();
        $('#autoru_code_get,#avitoru_code_get').html('Получить код');
        $('#avitoru_code_error,#autoru_code_error').html('');
        $('#phone').prop('readonly', true);
        $('#phone').css('background-color', '#EBEBE4');
        $('#change_phone').show();
        $('#autoru_code,#avitoru_code').val('').prop('disabled', true).css('background-color', '#EBEBE4');
        contact_phone = $('#phone').val();
        contact_name = $('#contact').val();
        $.ajax({
            type: "post",
            url: "/ajax/checkconfirmation",
            dataType: "json",
            async: false,
            data: {
                phone: $('#phone').val().replace(/\s+/g, ''),
                name: $('#contact').val()
            },
            beforeSend: function(){
            },
            complete: function(){
            },
            success: function(response){
                if (response.success){
                    confirmed_auto = response.data.confirmed_auto;
                    confirmed_avito = response.data.confirmed_avito;
                    if (confirmed_auto == 1) {
                        $('.confirm-code-auto-place .indicator').fadeIn();
                        $('#autoru_code,#autoru_code_get,#autoru_code_confirm').hide();
                    } else {
                        $('.confirm-code-auto-place .indicator').fadeOut();
                        $('#autoru_code,#autoru_code_get').show();
                    }
                    if (confirmed_avito == 1) {
                        $('.confirm-code-avito-place .indicator').fadeIn();
                        $('#avitoru_code,#avitoru_code_get,#avitoru_code_confirm').hide();
                    } else {
                        $('.confirm-code-avito-place .indicator').fadeOut();
                        $('#avitoru_code,#avitoru_code_get').show();
                    }
                    if (response.data.auto_status == 'used') {
                        $('#autoru_code_confirm,#autoru_code_get').hide();
                        $('#autoru_code_error').html('Данный телефон ранее уже был зарегистрирован на Auto.ru!').show();
                    }
                    if (response.data.avito_status == 'used') {
                        $('#avitoru_code_confirm,#avitoru_code_get').hide();
                        $('#avitoru_code_error').html('Данный телефон уже зарегистрирован на Avito.ru!').show();
                    }
                    $('#phone_id').val(response.data.phone_id);
                }
            }
        });
    });
    
    $('#autoru_code_confirm').on('click', function(){
        var me = this;
        var params = new Object();
        params.code = $('#autoru_code').val();
        params.name = $('#contact').val();
        params.phone = $('#phone').val().replace(/\s+/g, '');
        params.phone_id = $('#phone_id').val();
        $('#autoru_code_error').html('');
        $('#autoru_code_confirm').hide();
        $('#send-code-auto-ajax-loader').show();
        $('#autoru_code').prop("disabled", true).css('background-color', '#EBEBE4');
        
        $.ajax({
            type: "post",
            url: "/ajax/confirmautoru",
            dataType: "json",
            data: params,
            beforeSend: function(){
                
            },
            complete: function(){
                $('#send-code-auto-ajax-loader').hide();
                $('#autoru_code_show').hide();
            },
            success: function(response){
                if (response.success){
                    confirmed_auto = 1;
                    $('#phone_id').val(response.phone_id);
                    $('#autoru_code_confirm').hide();
                    $('.confirm-code-auto-place .indicator').show();
                    $('#autoru_code').prop('disabled', true).css('background-color', '#EBEBE4').hide();
                    $('.confirm-code-avito-place').show();
                    if ($('.page-contacts .indicator:visible').length==5){
                        $('.page-contacts .next').addClass('next-active');
                        $('#change_phone').hide();
                    } else {
                        $('.page-contacts .next').removeClass('next-active');
                        $('#change_phone').show();
                    }
                } else {
                    if (response.status == 'used'){
                        $('#autoru_code_get,#autoru_code_confirm').hide();
                    }
                    $('#autoru_code').prop('disabled', false).css('background-color', 'initial');
                    $('#autoru_code_confirm').show();
                    $('#autoru_code_error').html(response.message?response.message:response.code).show();
                }
            }
        });
    });
    
    $('#avitoru_code_confirm').on('click', function(){
        var me = this;
        var params = new Object();
        params.code = $('#avitoru_code').val();
        params.name = $('#contact').val();
        params.phone = $('#phone').val().replace(/\s+/g, '');
        params.phone_id = $('#phone_id').val();
        $('#avitoru_code_error').html('');
        $('#avitoru_code_confirm').hide();
        $('#send-code-avito-ajax-loader').show();
        $('#avitoru_code').prop("disabled", true);
        
        $.ajax({
            type: "post",
            url: "/ajax/confirmavitoru",
            dataType: "json",
            data: params,
            beforeSend: function(){
            },
            complete: function(){
                $('#send-sms-avito-ajax-loader').hide();
                $('#avitoru_code_show').hide();
            },
            success: function(response){
                $('#avitoru_code').prop("disabled", false);
                $('#send-code-avito-ajax-loader').hide();
                if (response.success){
                    confirmed_avito = 1;
                    avito_code = params.code;
                    $('#phone_id').val(response.phone_id);
               //     $('#change_phone').hide();
                    $('#avitoru_code_confirm').hide();
                    $('.confirm-code-avito-place .indicator').show();
                    $('#avitoru_code').prop('disabled', true).css('background-color', '#EBEBE4').hide();
                    if ($('.page-contacts .indicator:visible').length==6) {
                        $('.page-contacts .next').addClass('next-active');
                        $('#change_phone').hide();
                    }
                } else {
                    if (response.status == 'used'){
                        $('#avitoru_code_get,#avitoru_code_confirm').hide();
                    } else {
                        $('#avitoru_code_confirm').show();
                    }
                    $('#avitoru_code').prop('disabled', false).css('background-color', 'initial');
                    
                    $('#avitoru_code_error').html(response.message?response.message:response.code).show();
                }
            }
        });
    });
    
    
    $('#change_phone').on('click', function(){
        $('#avitoru_code_get,#autoru_code_get,#registration_repeat,#avitoru_code_confirm,#autoru_code_confirm,#registration_timer').hide();
        $('#phone').prop('readonly', false).css('background-color', 'initial');
        $('#autoru_code,#avitoru_code').prop('disabled', true).css('background-color', '#EBEBE4');
        $('.confirm-code-avito-place .indicator,.confirm-code-auto-place .indicator').hide();
        $('#autoru_code_error,#avitoru_code_error').html('');
        $('.confirm-code-place').hide();
        $('#change_phone').hide();
        //clearInterval(autoru_timer);
        $('#avitoru_code,#autoru_code').val('').prop('disabled', true);
        $('.page-contacts .confirm').show();
    });
    
 /*   $('#subway').on('change', function(){
        if ($(this).val()!=0){
            if ($('.page-contacts .indicator:visible').length==5){
                $('.tab-contacts .indicator').show();
                $('.page-contacts .next').addClass('next-active');
            } else {
                $('.page-contacts .next').removeClass('next-active');
            }
        }
    });*/
    
    $('#contact').bind('change keyup input', function(event){
        if (this.value.match(/[^A-Za-zА-Яа-я(\s{1})]/g)) {
            this.value = this.value.replace(/[^A-Za-zА-Яа-я(\s{1})]/g, '');
        }
        this.value = this.value.charAt(0).toUpperCase() + this.value.substr(1);
        if ($('#contact').val().length>0){
            $('#contact').parent('.form-line').find('.indicator').show();
        } else
            $('#contact').parent('.form-line').find('.indicator').hide();
        if ($('.page-contacts .indicator:visible').length==5){
            $('.tab-contacts .indicator').show();
            $('.page-contacts .next').addClass('next-active');
        } else {
            $('.page-contacts .next').removeClass('next-active');
        }
    });
    
    $('#price').bind('change keyup input click', function(event){
        if (this.value.match(/[^0-9(\s{1})]/g)) {
            this.value = this.value.replace(/[^0-9(\s{1})]/g, '');
        }
        var str = this.value.replace(/\s+/g, '');
        if (this.value.charAt(0)=='0')
            this.value = this.value.substr(1);
        if (this.value.length>9)
            this.value = this.value.substr(0, 9);
        this.value = str.replace(/(\d)(?=(\d\d\d)+([^\d]|$))/g, '$1 ')
        if ($('#price').val().length>5){
            $('#price').parent('.form-line').find('.indicator').show();
        } else {
            $('#price').parent('.form-line').find('.indicator').hide();
        }
        if ($('.page-price-and-comment .indicator:visible').length==2){
            $('.tab-price .indicator').show();
            $('.page-price-and-comment .next').addClass('next-active');
        } else {
            $('.page-price-and-comment .next').removeClass('next-active');
            $('.tab-price .indicator').hide();
        }
    });
    
    $('#comment').bind('change keyup input click', function(event){
        var no_space = $('#comment').val().replace(/\s+/g, '');
        if (this.value.charAt(0)==' ')
            this.value = this.value.substr(1);
        if (no_space>2000)
            this.value = this.value.substr(0, 2000);
        if (no_space.length>=25){
            $('#comment').parent('.form-line').find('.indicator').show();
            $('.tab-price .indicator').show();
        } else {
            $('#comment').parent('.form-line').find('.indicator').hide();
            $('.tab-price .indicator').hide();
        }
        if ($('.page-price-and-comment .indicator:visible').length==2){
            $('.page-price-and-comment .next').addClass('next-active');
        } else {
            $('.page-price-and-comment .next').removeClass('next-active');
        }
    });
    
    
/*    $('#inspection-place').bind('change keyup input click', function(event){
        var no_space = $('#inspection-place').val().replace(/\s+/g, '');
        if (this.value.charAt(0)==' ')
            this.value = this.value.substr(1);
        if (no_space>20)
            this.value = this.value.substr(0, 20);
        if (no_space.length>=3){
            $('#inspection-place').parent('.form-line').find('.indicator').show();
            $('.tab-price .indicator').show();
        } else {
            $('#inspection-place').parent('.form-line').find('.indicator').hide();
            $('.tab-price .indicator').hide();
        }
        if ($('.page-contacts .indicator:visible').length==5){
            $('.page-contacts .next').addClass('next-active');
        } else {
            $('.page-contacts .next').removeClass('next-active');
        }
    });*/

    $('.page-price-and-comment .prev').on('click', function(){
        $('.page-photos').show();
        $('.page-price-and-comment').hide();
        $('.tab-price').removeClass('active');
        $('.tab-photos').addClass('active');
    });
    
    $('.page-price-and-comment .next').on('click', function(){
        if (!$(this).hasClass('next-active')) return false;
        $('.page-price-and-comment').hide();
        
        if (confirmed_auto==1 || confirmed_avito==1) {
            $('#phone_id').val(phone_id);
            // Кнопка "подтвердить" не нужна
            $('.page-contacts .confirm').hide();
            // Заполняем имя и телефон, помечаем галочками
            $('#contact').val(contact_name);
            $('#phone').val(contact_phone);
            $('#phone').prop('readonly', true).css('background-color', '#EBEBE4');
            $('#contact').parent('.form-line').find('.indicator').show();
            $('#phone').parent('.form-line').find('.indicator').show();
            $('#change_phone').show(); // Кнопка для изменения номера
            // Заполняем поля с смс-кодами
            $('#autoru_code').val(auto_code);
            $('#avitoru_code').val(avito_code);
            if (confirmed_auto==1 && confirmed_avito==1) {
                $('.page-contacts .next').addClass('next-active');
            }
            
            
            
            if (confirmed_auto == 1) {
                $('.confirm-code-auto-place .indicator').fadeIn();
                $('#autoru_code,#autoru_code_get,#autoru_code_confirm').hide();
            } else {
                $('.confirm-code-auto-place .indicator').fadeOut();
                $('#autoru_code,#autoru_code_get').show();
            }
            if (confirmed_avito == 1) {
                $('.confirm-code-avito-place .indicator').fadeIn();
                $('#avitoru_code,#avitoru_code_get,#avitoru_code_confirm').hide();
            } else {
                $('.confirm-code-avito-place .indicator').fadeOut();
                $('#avitoru_code,#avitoru_code_get').show();
            }
            
            
            if (auto_status == 'used') {
                $('#autoru_code_confirm,#autoru_code_get').hide();
                $('#autoru_code_error').html('Данный телефон ранее уже был зарегистрирован на Auto.ru!');
            }
            if (avito_status == 'used') {
                $('#avitoru_code_confirm,#avitoru_code_get').hide();
                $('#avitoru_code_error').html('Данный телефон уже зарегистрирован на Avito.ru!');
            }
        }
 
        $('.page-contacts').show();
        $('.tab-price').removeClass('active');
        $('.tab-contacts').addClass('active');
    });
    
    $('.page-contacts .prev').on('click', function(){
        $('.page-price-and-comment').show();
        $('.page-contacts').hide();
        $('.tab-contacts').removeClass('active');
        $('.tab-price').addClass('active');
    });
    // Вперед на странице контактов
    $('.page-contacts .next').on('click', function(){
        if (!$(this).hasClass('next-active')) return false;
        var photos = '';
        $('.preview-container').each(function(){
            photos+='<div class="preview-container-result" style="background: url('+$(this).data('src')+') no-repeat center; background-size: cover;"></div>';
        });
        if (photos.length>0)
            $('.result-photos').show().html(photos);
        else
            $('.result-photos').hide();
        $('.page-contacts').hide();
        $('.page-end').show();
        $('.tab-contacts').removeClass('active');
        $('.tab-end').addClass('active');
        $('#check_brand').html($("#brand :selected").text());
        if ($('#another_model').val().length==0)
            $('#check_model').html($("#model :selected").text());
        else
            $('#check_model').html($('#another_model').val());
        $('#check_color').html($('.colors .active').data('text'));
        $('#check_body').html($(".body_type .active .text").text());
        $('#check_year').html($("#year :selected").text());
        $('#check_run').html(accounting.formatNumber($("#run").val(), 0, " ", "")+' км');
        $('#check_engine_type').html($('.engine_type .active').html());
        $('#check_engine_volume').html($("#engine_volume :selected").text()+' л');
        $('#check_engine_power').html($("#engine_power").val()+' л.с.');
        $('#check_drive_type').html($(".drive_type .active").html());
        if ($('.rudder .active').data('id')==1)
            $('#check_rudder').html('<span style="color: red">'+$(".rudder .active").html()+'</span>');
        else
            $('#check_rudder').html($(".rudder .active").html());
        $('#check_transmission_type').html($(".transmission_type .active").html());
        if ($("#condition").val()==1) // Если битый
            $('#check_state').html('<span style="color: red">'+$("#condition :selected").html()+'</span>');
        else
            $('#check_state').html($("#condition :selected").html());
    /*    if ($("#subway").val()!=0){
            $('#check_subway').html($("#subway :selected").html());
            $('#check_subway').parent('.form-line').show();
        } else {
            $('#check_subway').parent('.form-line').hide();
        }*/
        $('#check_price').html(accounting.formatNumber($("#price").val(), 0, " ", "")+' руб.');
        $('#check_phone').html($("#phone").val());
        $('#check_comment').html($("#comment").val());
        $('#check_contact').html($('#contact').val());
        $('#check_phone').html($('#phone').val());
    //    $('#check_city').html($("#city :selected").html());
        autoHeight();
    });
    // Назад на странице завершения
    $('.page-end .prev').on('click', function(){
        $('.page-contacts').show();
        $('.page-end').hide();
        $('.tab-end').removeClass('active');
        $('.tab-contacts').addClass('active');
    });
    // Удаление загруженной фотографии
    $('.uploaded').on('click', '.delete', function(){
        var params = new Object();
        params.photo_id = $(this).data('id'); 
        var me = this;
        $.ajax({
            type: "post",
            url: "/ajax/deletephoto",
            dataType: "json",
            data: params,
            beforeSend: function(){
            },
            complete: function(){
            },
            success: function(response){
                if (response.success){
                    $(me).parent('.preview-container').remove();
                    var number = $('.uploaded-image').length;
                    if (number<10){
                        $('.camera_container').fadeIn();
                    }
                    if (number==0){
                        $('.page-photos .next').hide();
                        $('.page-photos .pass').fadeIn();
                        $('.tab-photos .indicator').hide();
                    }
                } else
                    console.dir(response.message);
            }
        });
    });
    
    $('#createform').submit(function(){
        if (!$('.tab-end').hasClass('active'))
            return false;
        $('#phone_id').val(phone_id);
    });
    
    $('.page-car .colors .color-item').on('click', function(){
        $('.colors .color-item').removeClass("active");
        $(this).addClass('active');
        $('#color').val($(this).data('value'));
        $(this).parent('.colors').parent('.form-line').find('.indicator').show();
        checkNext();
    });
});