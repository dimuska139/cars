/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

$(document).ready(function(){
    $('#admin').parent('.form-line').find('.indicator').show();
    
    if ($('#id').length!=0){
        if ($('#login').val().length>0)
            $('#login').parent('.form-line').find('.indicator').show();
        if ($('#name').val().length>0)
            $('#name').parent('.form-line').find('.indicator').show();
        if ($('#balance').val().length>0)
            $('#balance').parent('.form-line').find('.indicator').show();
        checkSubmit();
    }
    
    function checkSubmit(){
        if ($('#id').length>0){
            if ($('.create-user-form .indicator:visible').length==4){
                $('.create-user-form .registration-button').addClass('active')
            } else {
                    $('.create-user-form .registration-button').removeClass('active')
            }
            if ($('.changepassword-user-form .indicator:visible').length==2){
                $('.changepassword-user-form .changepassword-button').addClass('active')
            } else {
                $('.changepassword-user-form .changepassword-button').removeClass('active')
            }
        } else {
            if ($('.create-user-form .indicator:visible').length==6){
                $('.create-user-form .registration-button').addClass('active')
            } else {
                $('.create-user-form .registration-button').removeClass('active')
            }
        }
    }
    
    $('#login').bind('change keyup input click', function(event){
        if (this.value.match(/[^0-9]/g)) {
            this.value = this.value.replace(/[^0-9]/g, '');
        }
        if ($(this).val().length==0){
            $(this).parent('.form-line').find('.indicator').hide();
        } else {
            var params = new Object();
            params.login = $(this).val();
            if ($('#id').length>0)
                params.id = $('#id').val();
            var me = this;
            $.ajax({
                type: "post",
                url: "/ajax/checklogin",
                dataType: "json",
                data: params,
                beforeSend: function(){
                },
                complete: function(){
                },
                success: function(response){
                    if (response.error == 0){
                        $(me).parent('.form-line').find('.indicator').show();
                        $('#login-error').html('');
                    } else {
                        $(me).parent('.form-line').find('.indicator').hide();
                        $('#login-error').html('Логин занят');
                    }
                    checkSubmit();
                }
            });
        }
    });
    
    $('#balance').bind('change keyup input click', function(event){
        if (this.value.match(/[^0-9]/g)) {
            this.value = this.value.replace(/[^0-9]/g, '');
        }
        if ($(this).val().length>0){
            $(this).parent('.form-line').find('.indicator').show();
        } else
            $(this).parent('.form-line').find('.indicator').hide();
        checkSubmit();
    });
    
    $('#name,#password').bind('change keyup input click', function(event){
        if ($(this).val().length>0){
            $(this).parent('.form-line').find('.indicator').show();
        } else
            $(this).parent('.form-line').find('.indicator').hide();
        checkSubmit();
    });
    
    
    $('#repeat-password,#password').bind('change keyup input click', function(event){
        if ($(this).val().length>0){
            $(this).parent('.form-line').find('.indicator').show();
        } else {
            $(this).parent('.form-line').find('.indicator').hide();
            return;
        }
        if ($('#password').val()!=$('#repeat-password').val()){
            $('#password-error').html("Пароли не совпадают!");
            $('#repeat-password').parent('.form-line').find('.indicator').hide();
        } else {
            $('#password-error').html("");
            $('#repeat-password').parent('.form-line').find('.indicator').show();
        }
        checkSubmit();
    });
    
    $('.create-user-form').submit(function() {
        if (!$('.create-user-form .registration-button').hasClass('active'))
            return false;
    });
    
    $('.changepassword-user-form').submit(function() {
        if (!$('.changepassword-user-form .changepassword-button').hasClass('active'))
            return false;
    });

});