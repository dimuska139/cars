$(document).ready(function(){
    $('.tasks-list .copy-description').zclip({
        path:'/resources/js/ZeroClipboard.swf',
        copy: function(){
            return $(this).parent('.value').find('input').val()
        },
        afterCopy:function(){
        }
    });
    
    $('.download-photos').on('click', function(){
        window.location.replace('/ajax/getphotoszip?id='+$(this).data('id'));
    });
});