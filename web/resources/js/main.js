/*
 * jQuery File Upload Plugin JS Example 8.9.1
 * https://github.com/blueimp/jQuery-File-Upload
 *
 * Copyright 2010, Sebastian Tschan
 * https://blueimp.net
 *
 * Licensed under the MIT license:
 * http://www.opensource.org/licenses/MIT
 */

/* global $, window */
$(document).ready(function(){
/*    setTimeout(function() {
        $.ajax({
            type: "post",
            url: "/ajax/gettime",
            dataType: "json",
            data: {},
            async: false,
            success: function(res){
                $('.top .date').html(res);
            }
        });
    }, 5000);*/
    setInterval(function() {
        $.ajax({
            type: "post",
            url: "/ajax/system",
            dataType: "json",
            data: {},
            async: false,
            success: function(res){
               // console.dir(res);
               switch (res.command) {
                   case "logout":
                       location = "/";
                       break;
               }
            }
        });
    }, 1000);
});

function autoHeight(){
    var window_height = $(window).height()-140;
    $('.page .content').css('height', 'initial');
    if ($('.page .content').height()<window_height){
        $('.page .content').height(window_height);
    }
    var rl = document.getElementById("result_list");
    if (rl){
        if (rl.scrollHeight - rl.clientHeight >= 0){
            $('.page').css('overflow', 'auto');
            $('.page .content').css('height', 'auto');
        }
    }
}

/*$(window).resize(function(){
    autoHeight();
});*/

setInterval(function(){ autoHeight(); }, 100);

$(function () {
    'use strict';
    autoHeight();
    
    if ($('#fileupload').length){
    // Initialize the jQuery File Upload widget:
        $('#fileupload').fileupload({
        // Uncomment the following to send cross-domain cookies:
        //xhrFields: {withCredentials: true},
            url: 'server/php/'
        });

        // Enable iframe cross-domain access via redirect option:
        $('#fileupload').fileupload(
            'option',
            'redirect',
            window.location.href.replace(
                /\/[^\/]*$/,
                '/cors/result.html?%s'
            )
        );

        if (window.location.hostname === 'blueimp.github.io') {
            // Demo settings:
            $('#fileupload').fileupload('option', {
                url: '//jquery-file-upload.appspot.com/',
                // Enable image resizing, except for Android and Opera,
                // which actually support image resizing, but fail to
                // send Blob objects via XHR requests:
                disableImageResize: /Android(?!.*Chrome)|Opera/
                    .test(window.navigator.userAgent),
                maxFileSize: 999000,
                acceptFileTypes: /(\.|\/)(gif|jpe?g|png)$/i
            });
            // Upload server status check for browsers with CORS support:
            if ($.support.cors) {
                $.ajax({
                    url: '//jquery-file-upload.appspot.com/',
                    type: 'HEAD'
                }).fail(function () {
                    $('<div class="alert alert-danger"/>')
                        .text('Upload server currently unavailable - ' +
                                new Date())
                        .appendTo('#fileupload');
                });
            }
        } else {
            // Load existing files:
            $('#fileupload').addClass('fileupload-processing');
            $.ajax({
                // Uncomment the following to send cross-domain cookies:
                //xhrFields: {withCredentials: true},
                url: $('#fileupload').fileupload('option', 'url'),
                dataType: 'json',
                context: $('#fileupload')[0]
            }).always(function () {
                $(this).removeClass('fileupload-processing');
            }).done(function (result) {
                $(this).fileupload('option', 'done')
                    .call(this, $.Event('done'), {result: result});
            });
        }
    }

});
