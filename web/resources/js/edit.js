/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

$(document).ready(function(){
    var photos_changed = false;
    function checkNext(){
        if ($('#run').data('success')==1 && $('#engine_power').data('success')==1 && $('#price').data('success')==1){
            var changed = false;
            $('.form-line .field').each(function(){
                if ($(this).val()!=$(this).parent('.form-line').data('old') || photos_changed){
          //          console.dir($(this).attr('id'));
                    changed = true;
                }
            });
            if (changed)
                $('.page-edit .next').show();
            else
                $('.page-edit .next').hide();
        }
            
    }
    
    $('#fileupload').fileupload({
        dataType: 'json',
        start: function(e) {
            $('.camera_container .progress').html('');        
        },
        done: function (e, data) {
            if (data.result.success){
                var number = $('.uploaded-image').length;
                if (number==10)
                    return;
                number+=1;
                if (number==10){
                    $('.camera_container').fadeOut();
                }
                $(".camera_container").before('<div id="photo-preview-container-'+data.result.id+'" class="preview-container" data-url="'+data.result.url+'" style="background: url('+data.result.url+') no-repeat center; background-size: cover;">'+
                        '<input class="uploaded-image" type="hidden" name="photos_id[]" value="'+data.result.id+'">'+
                        '<div class="number">'+number+'</div>'+
                        '<img data-id="'+data.result.id+'" class="delete" src="/resources/img/close.png"/>'+
                '</div>');
            /*    $('.uploaded').append('<div class="preview-container" data-url="'+data.result.url+'" style="background: url('+data.result.url+') no-repeat center; background-size: cover;" id="photo-preview-container-'+data.result.id+'">'+
                        '<input class="uploaded-image" type="hidden" name="photos_id[]" value="'+data.result.id+'">'+
                        '<div class="number">'+number+'</div>'+
                        '<img data-id="'+data.result.id+'" class="delete" src="/resources/img/close.png"/>'+
                '</div>');*/
                checkNext();
                $('.camera_container .progress').html(''); 
                photos_changed = true;
            }
            else {
                $('.camera_container .progress').html('');
                alert("Проверьте расширение загружаемого файла. Можно загружать только фотографии в формате jpg или png");
            }
        },
        progressall: function (e, data) {
            var progress = parseInt(data.loaded / data.total * 100, 10);
            $('.camera_container .progress').html(progress+'%');
        }
    });
    
    $('.uploaded').on('click', '.delete', function(){
        photos_changed = true;
        var id = $(this).data('id');
        $('#photo-preview-container-'+id).remove();
        var counter = 1;
        $('.preview-container .number').each(function(){
            $(this).html(counter);
            counter++;
        });
        if ($('.preview-container').length<10)
            $('.camera_container').show();
        else
            $('.camera_container').hide();
        checkNext();
    });
    
    $('.body_type .item').on('click', function(){
        $('.body_type .item').removeClass('active');
        $('.body_type .item').each(function(){
            var default_img = $(this).data('img');
            $(this).css('background-image', "url('/resources/img/body/"+default_img+"')");
        });
        var img_name = $(this).data('img');
        var img_name_arr = img_name.split('.');
        $(this).css('background-image', "url('/resources/img/body/"+img_name_arr[0]+'a.'+img_name_arr[1]+"')");
        $(this).addClass('active');
        $('#body_type').val($(this).data('id'));
        checkNext();
    });
    
    $('.body_type .item').on({
        mouseenter: function () {
            if (!$(this).hasClass('active')){
                var default_img = $(this).data('img');
                var img_name_arr = default_img.split('.');
                $(this).css('background-image', "url('/resources/img/body/"+img_name_arr[0]+'h.'+img_name_arr[1]+"')");
            }
        },
        mouseleave: function () {
            if (!$(this).hasClass('active')){
                var default_img = $(this).data('img');
                $(this).css('background-image', "url('/resources/img/body/"+default_img+"')");
            }
        }
    });
    
    $('.engine_type .item').on('click', function(){
        $('.engine_type .item').removeClass('active');
        $(this).addClass('active');
        $('#engine_type').val($(this).data('id'));
        checkNext();
    });
    
    $('.transmission_type .item').on('click', function(){
        $('.transmission_type .item').removeClass('active');
        $(this).addClass('active');
        $('#transmission_type').val($(this).data('id'));
        checkNext();
    });
    
    $('.drive_type .item').on('click', function(){
        $('.drive_type .item').removeClass('active');
        $(this).addClass('active');
        $('#drive_type').val($(this).data('id'));
        checkNext();
    });
    
    $('.rudder .item').on('click', function(){
        $('.rudder .item').removeClass('active');
        $(this).addClass('active');
        $('#rudder').val($(this).data('id'));
        checkNext();
    });
    
    $('#run').bind('change keyup input click', function(event){
        if (this.value.match(/[^0-9(\s{1})]/g)) {
            this.value = this.value.replace(/[^0-9(\s{1})]/g, '');
        }
        if (this.value.charAt(0)=='0')
            this.value = this.value.substr(1);
        if (this.value.length>7)
            this.value = this.value.substr(0, this.value.length-1);
        var str = this.value.replace(/\s+/g, '');
        this.value = str.replace(/(\d)(?=(\d\d\d)+([^\d]|$))/g, '$1 ')
        if ($('#run').val().length>5){
            $(this).data('success',1);
        } else
            $(this).data('success',0);
        checkNext();
    });
    
    $('#engine_power').bind('change keyup input click', function(event){
        if (this.value.match(/[^0-9]/g)) {
            this.value = this.value.replace(/[^0-9]/g, '');
        }
        if (this.value.charAt(0)=='0')
            this.value = this.value.substr(1);
        if ($('#engine_power').val().length>0){
            $(this).data('success',1);
        } else
            $(this).data('success',0);
        checkNext();
    });
    
    $('#phone').bind('change keyup input click', function(event){
        if (this.value.match(/[^0-9]/g)) {
            this.value = this.value.replace(/[^0-9]/g, '');
        }
        if ($('#phone').val().length>0){
            $(this).data('success',1);
        } else
            $(this).data('success',0);
        checkNext();
    });
    
    $('#contact').bind('change keyup input click', function(event){
        if ($('#contact').val().length>0){
            $(this).data('success',1);
        } else
            $(this).data('success',0);
        checkNext();
    });
    
    $('#color,#year,#condition,#engine_volume,#engine_power').on('change', function(){
        checkNext();
    });
    
    $('#comment').bind('change keyup input click', function(event){
        checkNext();
    });
    
    $('#price').bind('change keyup input click', function(event){
        if (this.value.match(/[^0-9(\s{1})]/g)) {
            this.value = this.value.replace(/[^0-9(\s{1})]/g, '');
        }
        if (this.value.charAt(0)=='0')
            this.value = this.value.substr(1);
        if (this.value.length>9)
            this.value = this.value.substr(0, this.value.length-1);
        var str = this.value.replace(/\s+/g, '');
        this.value = str.replace(/(\d)(?=(\d\d\d)+([^\d]|$))/g, '$1 ')
        if ($('#price').val().length>0){
            $(this).data('success',1);
        } else {
            $(this).data('success',0);
        }
        if ($('#run').data('success')==1 && $('#phone').data('success')==1 && $('#engine_power').data('success')==1 && $('#price').data('success')==1)
            $('.next').show();
        else
            $('.next').hide();
        checkNext();
    });
    
    $('.next').on('click', function(){
        $('.page-edit').hide();
        $('.tab-edit').removeClass('active');
        $('.page-check').show();
        $('.tab-end').addClass('active');
    
          
        $('#check_color').html($('.colors .active').data('text'));
        $('#check_body').html($(".body_type .active .text").text());
        
        $('#check_brand').html($("#brand :selected").text());
        $('#check_model').html($("#model :selected").text());
        
        
        $('#check_year').html($("#year :selected").text());
        $('#check_condition').html($("#condition :selected").text());
        $('#check_condition').data("cond", $("#condition :selected").val());
        $('#check_run').html($("#run").val());
        $('#check_engine_type').html($('.engine_type .active').html());
        $('#check_engine_volume').html($("#engine_volume :selected").text());
        $('#check_engine_power').html($("#engine_power").val());
        $('#check_drive_type').html($(".drive_type .active").html());
        $('#check_transmission_type').html($(".transmission_type .active").html());
        $('#check_rudder').html($(".rudder .active").html());
        $('#check_price').html($("#price").val());
        $('#check_phone').html($("#phone").val());
        $('#check_comment').html($("#comment").val()); 
        
        $('#check_photos').html('');
        $('.preview-container').each(function(){
            $('#check_photos').html($('#check_photos').html()+'<div class="preview-container" style="background: url('+$(this).data('url')+') no-repeat center; background-size: cover;"></div>');
        });
        // Подстветка измененных полей
        if ($('#check_color').html()!=$('#check_color').parent('div').find('input').val())
            $('#check_color').parent('div').addClass('changed');
        else
            $('#check_color').parent('div').removeClass('changed');
        if ($('#check_body').html()!=$('#check_body').parent('div').find('input').val())
            $('#check_body').parent('div').addClass('changed');
        else
            $('#check_body').parent('div').removeClass('changed');
        if ($('#check_year').html()!=$('#check_year').parent('div').find('input').val())
            $('#check_year').parent('div').addClass('changed');
        else
            $('#check_year').parent('div').removeClass('changed');
        if ($('#check_condition').data('cond')!=$('#check_condition').parent('div').find('input').val())
            $('#check_condition').parent('div').addClass('changed');
        else
            $('#check_condition').parent('div').removeClass('changed');
        if ($('#check_run').html()!=$('#check_run').parent('div').find('input').val())
            $('#check_run').parent('div').addClass('changed');
        else
            $('#check_run').parent('div').removeClass('changed');
        if ($('#check_engine_type').html()!=$('#check_engine_type').parent('div').find('input').val())
            $('#check_engine_type').parent('div').addClass('changed');
        else
            $('#check_engine_type').parent('div').removeClass('changed');
        if ($('#check_engine_volume').html()!=$('#check_engine_volume').parent('div').find('input').val())
            $('#check_engine_volume').parent('div').addClass('changed');
        else
            $('#check_engine_volume').parent('div').removeClass('changed');
        if ($('#check_engine_power').html()!=$('#check_engine_power').parent('div').find('input').val())
            $('#check_engine_power').parent('div').addClass('changed');
        else
            $('#check_engine_power').parent('div').removeClass('changed');
        if ($('#check_drive_type').html()!=$('#check_drive_type').parent('div').find('input').val())
            $('#check_drive_type').parent('div').addClass('changed');
        else
            $('#check_drive_type').parent('div').removeClass('changed');
        if ($('#check_rudder').html()!=$('#check_rudder').parent('div').find('input').val())
            $('#check_rudder').parent('div').addClass('changed');
        else
            $('#check_rudder').parent('div').removeClass('changed');
        if ($('#check_transmission_type').html()!=$('#check_transmission_type').parent('div').find('input').val())
            $('#check_transmission_type').parent('div').addClass('changed');
        else
            $('#check_transmission_type').parent('div').removeClass('changed');
        if ($('#check_price').html()!=$('#check_price').parent('div').find('input').val())
            $('#check_price').parent('div').addClass('changed');
        else
            $('#check_price').parent('div').removeClass('changed');
        if ($('#check_phone').html()!=$('#check_phone').parent('div').find('input').val())
            $('#check_phone').parent('div').addClass('changed');
        else
            $('#check_phone').parent('div').removeClass('changed');
        if ($('#check_comment').html()!=$('#check_comment').parent('div').find('input').val())
            $('#check_comment').parent('div').addClass('changed');
        else
            $('#check_comment').parent('div').removeClass('changed');
        if (photos_changed)
            $('#check_photos').parent('div').addClass('changed');
        else
            $('#check_photos').parent('div').removeClass('changed');
    });
    
    $('.prev').on('click', function(){
        $('.page-check').hide();
        $('.tab-end').removeClass('active');
        $('.page-edit').show();
        $('.tab-edit').addClass('active');
    });
    
    $('.page-edit .colors .color-item').on('click', function(){
        $('.colors .color-item').removeClass("active");
        $(this).addClass('active');
        $('#color').val($(this).data('value'));
        checkNext();
    });
});