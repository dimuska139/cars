var wait = false;
var limit = 100;
var old = true;
var old_id = false;
var new_records = [];
var first_load = true;
var lines_amount = 0;


var load_lock = false; // Блокирует выполнение ajax-запроса на получение данных, если не завершился предыдущий
$(document).ready(function(){
    $('.table-controls,.filter-state').hide(); //.filters
    $('.result_list_header').hide();
    function clearFilters(){
        $('.filters input[type="text"]').val('');
        $('#year_from option:first').attr('selected', 'selected');
        $('#year_to option:first').attr('selected', 'selected');
        $('#engine-filter option:first').attr('selected', 'selected');
        $('#volume_from option:first').attr('selected', 'selected');
        $('#volume_to option:first').attr('selected', 'selected');
        $('#kpp-filter option:first').attr('selected', 'selected');
        $('#state-filter option:first').attr('selected', 'selected');
        $('#source-filter,#body-filter,#mark-filter,#region-filter').multipleSelect('uncheckAll');
        $('.source-ids,.body-ids,.mark-ids,.region-ids').remove();
        $.removeCookie('price_from', { path: '/' });
        $.removeCookie('price_to', { path: '/' });
        $.removeCookie('run_from', { path: '/' });
        $.removeCookie('run_to', { path: '/' });
        $.removeCookie('year_from', { path: '/' });
        $.removeCookie('year_to', { path: '/' });
        $.removeCookie('volume_from', { path: '/' });
        $.removeCookie('volume_to', { path: '/' });
        $.removeCookie('body_filter', { path: '/' });
        $.removeCookie('engine', { path: '/' });
        $.removeCookie('condition', { path: '/' });
        $.removeCookie('transmission', { path: '/' });
        $.removeCookie('phone-amount', { path: '/' });
        $.removeCookie('region_filter', { path: '/' });
        $.removeCookie('source_filter', { path: '/' });
        $.removeCookie('mark_filter', { path: '/' });
        old = true;
        $('#result_list').html('');
    }
    
    // Сбросить фильтры
    $('#clear-filters').on('click', function(){ clearFilters(); });
    // Применить фильтры
    $('#accept-filters').on('click', function(){
        // Фильтры, которые являются плагинами multipleSelect
        var selected_list = $('#source-filter').multipleSelect('getSelects');  
        $('#source-filter-ids').html('');
        var to_json = [];
        for (var i=0;i<selected_list.length;i++){
            $('#source-filter-ids').html($('#source-filter-ids').html()+'<input type="hidden" class="source-ids" name="filter[source]['+i+']" value="'+selected_list[i]+'">');
            to_json.push(selected_list[i]);
        }
        var encoded = JSON.stringify(to_json);
        $.cookie('source_filter', encoded, { path: '/' });
        
        var selected_list = $('#body-filter').multipleSelect('getSelects');
        $('#body-filter-ids').html('');
        var to_json = [];
        for (var i=0;i<selected_list.length;i++){
            $('#body-filter-ids').html($('#body-filter-ids').html()+'<input type="hidden" class="body-ids" name="filter[body]['+i+']" value="'+selected_list[i]+'">');
            to_json.push(selected_list[i]);
        }
        var encoded = JSON.stringify(to_json);
        $.cookie('body_filter', encoded, { path: '/' });

        var selected_list = $('#mark-filter').multipleSelect('getSelects');
        $('#mark-filter-ids').html('');
        var to_json = [];
        for (var i=0;i<selected_list.length;i++){
            $('#mark-filter-ids').html($('#mark-filter-ids').html()+'<input type="hidden" class="mark-ids" name="filter[marka]['+i+']" value="'+selected_list[i]+'">');
            to_json.push(selected_list[i]);
        }
        var encoded = JSON.stringify(to_json);
        $.cookie('mark_filter', encoded, { path: '/' });

        var selected_list = $('#region-filter').multipleSelect('getSelects');
        $('#region-filter-ids').html('');
        var to_json = [];
        for (var i=0;i<selected_list.length;i++){
            $('#region-filter-ids').html($('#region-filter-ids').html()+'<input type="hidden" class="region-ids" name="filter[regions]['+i+']" value="'+selected_list[i]+'">');
            to_json.push(selected_list[i]);
        }
        var encoded = JSON.stringify(to_json);
        $.cookie('region_filter', encoded, { path: '/' });
        old = true;
        $('#result_list').html('');
        // Обычные фильтры. Записываем их значения в data-атрибут default, т.к. из него берется значение перед выполнением запроса к серверу
        // Также пишем значения в куки
        $('#year_from').data('default', $('#year_from').val());
        $('#year_to').data('default', $('#year_to').val());
        $('#volume_from').data('default', $('#volume_from').val());
        $('#volume_to').data('default', $('#volume_to').val());
        $('#price_from').data('default', $('#price_from').val());
        $('#price_to').data('default', $('#price_to').val());
        $('#run_from').data('default', $('#run_from').val());
        $('#run_to').data('default', $('#run_to').val());
        $('#phone-amount-filter').data('default', $('#phone-amount-filter').val());
        $('#state-filter').data('default', $('#state-filter').val());
        $('#kpp-filter').data('default', $('#kpp-filter').val());
        $('#engine-filter').data('default', $('#engine-filter').val());
        
        $.cookie('year_from', $('#year_from').val(), { path: '/' });
        $.cookie('year_to', $('#year_to').val(), { path: '/' });
        $.cookie('volume_from', $('#volume_from').val(), { path: '/' });
        $.cookie('volume_to', $('#volume_to').val(), { path: '/' });
        $.cookie('price_from', $('#price_from').val(), { path: '/' });
        $.cookie('price_to', $('#price_to').val(), { path: '/' });
        $.cookie('run_from', $('#run_from').val(), { path: '/' });
        $.cookie('run_to', $('#run_to').val(), { path: '/' });
        $.cookie('phone-amount', $('#phone-amount-filter').val(), { path: '/' });
        $.cookie('condition', $('#state-filter').val(), { path: '/' });
        $.cookie('transmission', $('#kpp-filter').val(), { path: '/' });
        $.cookie('engine', $('#engine-filter').val(), { path: '/' });
    });
    
    
    function checkPhonefindbutton(){
        if ($('#searchbyphone-form .phone-number').val().length!=11)
            $('#searchbyphone-form .find').css('color', '#969696').css('cursor', 'default').addClass('nohover');
        else
            $('#searchbyphone-form .find').css('color', '#28578B').css('cursor', 'pointer').removeClass('nohover');;
    }
    checkPhonefindbutton();
 /*   if ($('#highlight-moderation').prop("checked")){
        highlight_moderation = true;
    } else 
        highlight_moderation = false;
    if ($('#highlight-cheap').prop("checked")){
        highlight_cheap = true;
    } else 
        highlight_cheap = false;*/
    
    if ($.cookie('filter_state')==1 && enough_balance){
        $('.filters').show();
     //   $('.filter-state').html("Скрыть фильтр");
    }
    
    function littleBalance(){
        $('.warning-panel').show();
    //    $('.table-controls, .filter-state').remove(); //.filters, 
    //    $('.phone-icon').remove();
        enough_balance = false;
    }
    
    if (!enough_balance) {
        $('.warning-panel').show();
    }
   
    
    $('#page_size').on('change', function(){
        //,#volume_from,#volume_to,#year_from,#year_to,#state-filter,#engine-filter,#kpp-filter,#phone-amount-filter
        old = true;
        $('#result_list').html('');
    });
    
    $('#page_size').on('change', function(){
        $.cookie('page_size', $(this).val(), { path: '/' });
    });
    
/*    $('#volume_from').on('change', function(){
        $.cookie('volume_from', $(this).val(), { path: '/' });
    });
    
    $('#volume_to').on('change', function(){
        $.cookie('volume_to', $(this).val(), { path: '/' });
    });
    
    $('#year_from').on('change', function(){
        $.cookie('year_from', $(this).val(), { path: '/' });
    });
    
    $('#year_to').on('change', function(){
        $.cookie('year_to', $(this).val(), { path: '/' });
    });
    
    $('#state-filter').on('change', function(){
        $.cookie('condition', $(this).val(), { path: '/' });
    });
    
    $('#engine-filter').on('change', function(){
        $.cookie('engine', $(this).val(), { path: '/' });
    });
    
    $('#kpp-filter').on('change', function(){
        $.cookie('transmission', $(this).val(), { path: '/' });
    });
    
    $('#phone-amount-filter').on('change', function(){
        $.cookie('phone-amount', $(this).val(), { path: '/' });
    });*/
    
    $('#highlight-moderation').on('change', function(){
        if ($(this).prop("checked")){
            highlight_moderation = true;
            $.cookie('highlight_moderation', 1, { path: '/' });
        } else {
            highlight_moderation = false;
            $.cookie('highlight_moderation', 0, { path: '/' });
        }
        old = true;
        $('#result_list').html('');        
    });
    
    $('#show-photos').on('change', function(){
        if ($(this).prop("checked")){
            $.cookie('show_photos', 1, { path: '/' });
        } else {
            $.cookie('show_photos', 0, { path: '/' });
        }
        location.reload();
    });
    
    $('#sounds').on('change', function(){
        if ($(this).prop("checked")){
            $.cookie('sounds', 1, { path: '/' });
        } else {
            $.cookie('sounds', 0, { path: '/' });
        }
    });
    
    $('#highlight-cheap').on('change', function(){
        if ($(this).prop("checked")){
            highlight_cheap = true;
            $.cookie('highlight_cheap', 1, { path: '/' });
        } else {
            highlight_cheap = false;
            $.cookie('highlight_cheap', 0, { path: '/' });
        }
        old = true;
        $('#result_list').html('');
    });
    
    
    
    $('#price_from,#price_to').bind('change keyup input click', function(event){
        if (this.value.match(/[^0-9(\s{1})]/g)) {
            this.value = this.value.replace(/[^0-9(\s{1})]/g, '');
        }
        var str = this.value.replace(/\s+/g, '');
        if (this.value.charAt(0)=='0')
            this.value = this.value.substr(1);
        this.value = str.replace(/(\d)(?=(\d\d\d)+([^\d]|$))/g, '$1 ')
        if (this.value.length>7)
            this.value = this.value.substr(0,this.value.length-1);
    });
    
    $('#phone-amount-filter').bind('change keyup input click', function(event){
        if (this.value.match(/[^0-9(\s{1})]/g)) {
            this.value = this.value.replace(/[^0-9(\s{1})]/g, '');
        }
    });
    
  /*  $('#price_from').bind('change keyup input click', function(event){
        $.cookie('price_from', $(this).val(), { path: '/' });
    });
    $('#price_to').bind('change keyup input click', function(event){
        $.cookie('price_to', $(this).val(), { path: '/' });
    });
    
    $('#volume_from').bind('change keyup input click', function(event){
        $.cookie('volume_from', $(this).val(), { path: '/' });
    });
    $('#volume_to').bind('change keyup input click', function(event){
        $.cookie('volume_to', $(this).val(), { path: '/' });
    });
    
    $('#run_from').bind('change keyup input click', function(event){
        $.cookie('run_from', $(this).val(), { path: '/' });
    });
    $('#run_to').bind('change keyup input click', function(event){
        $.cookie('run_to', $(this).val(), { path: '/' });
    });*/
    
    
  /*  $('#price_from,#price_to').bind('change keyup', function() {
        old = true;
        $('#result_list').html('');
    });*/
    
    $('#run_from,#run_to').bind('change keyup input click', function(event){
        if (this.value.match(/[^0-9(\s{1})]/g)) {
            this.value = this.value.replace(/[^0-9(\s{1})]/g, '');
        }
        var str = this.value.replace(/\s+/g, '');
        if (this.value.charAt(0)=='0')
            this.value = this.value.substr(1);
        this.value = str.replace(/(\d)(?=(\d\d\d)+([^\d]|$))/g, '$1 ')
        if (this.value.length>6)
            this.value = this.value.substr(0,this.value.length-1);
    });
    
    $('#volume_from,#volume_to').bind('change keyup input click', function(event){
        if (this.value.match(/[^0-9(\.{1})(\s{1})]/g)) {
            this.value = this.value.replace(/[^0-9(\.{1})(\s{1})]/g, '');
        }
        var str = this.value.replace(/\s+/g, '');
        if (this.value.charAt(0)=='0')
            this.value = this.value.substr(1);
        this.value = str.replace(/(\d)(?=(\d\d\d)+([^\d]|$))/g, '$1 ')
        if (this.value.length>7)
            this.value = this.value.substr(0,this.value.length-1);
    });
    
  /*  $('#run_from,#run_to').bind('change keyup', function() {
        old = true;
        $('#result_list').html('');
    });
    
    $('#volume_from,#volume_to').bind('change keyup', function() {
        old = true;
        $('#result_list').html('');
    });
    
    $('#security').on('change', function(){
        old = true;
        $('#result_list').html('');
    });*/
    
  //  getList();
    if($('.monitor').length){	
        console.log("init monitor");
        initTasks(0);
    }
    
   /* $('#clear').on('click', function(){
       location.href='/clear_search_filters';
    });*/
	
    function initTasks(online){
        if(!online){
            setInterval(getList,1000);
        }
    }	
    
    function getAdsPhone(id){
        if ($('#id_'+id).find('.phone_find').data('focus')==1 && $('#id_'+id).find('.phone_find').find('.phonefindsubmit').val()==0)
            return;
     /*   if (!enough_balance)
            return;*/
        var params = new Object();
        params.ads_id = id;//$(me).parent('.line').data('id');
        $.ajax({
            type: "post",
            url: "/ajax/getadsphone",
            dataType: "json",
            data: params,
            beforeSend: function(){
            },
            complete: function(){
                
            },
            success: function(res){
                if (res.success){
                    var phone = res.phone;
                    $('#phone-text-'+id).html(phone);                        
                    checkbalance();
                }
            }
        });
    }
    
    /*$('#result_list').on('click', '.phone', function(e){
        getAdsPhone(this);
        return false;
    });*/
    
  /*  $('#state-filter').multipleSelect({
        selectAll: false,
        placeholder: "Состояние",
        width: 167,
        onClick: function(view) {
            var selected_list = $('#state-filter').multipleSelect('getSelects');
            $('#state-filter-ids').html('');
            //if ($.cookie('filter_state')==1)
            var to_json = [];
            for (var i=0;i<selected_list.length;i++){
                $('#state-filter-ids').html($('#state-filter-ids').html()+'<input type="hidden" class="condition-ids" name="filter[condition]['+i+']" value="'+selected_list[i]+'">');
                to_json.push(selected_list[i]);
            }
            var encoded = JSON.stringify(to_json);
            $.cookie('state_filter', encoded, { path: '/' });
            old = true;
            $('#result_list').html('');
        }
    });*/
    if ($.cookie('state_filter')){
        var parsed = JSON.parse($.cookie('state_filter'));
        $('#state-filter').multipleSelect('setSelects', parsed);
    }
    
    
    
    $('#source-filter').multipleSelect({
        selectAll: false,
        placeholder: "Источники",
        width: 210,
        onClick: function(view) {
        /*    var selected_list = $('#source-filter').multipleSelect('getSelects');
            
            $('#source-filter-ids').html('');
            var to_json = [];
            for (var i=0;i<selected_list.length;i++){
                $('#source-filter-ids').html($('#source-filter-ids').html()+'<input type="hidden" class="source-ids" name="filter[source]['+i+']" value="'+selected_list[i]+'">');
                to_json.push(selected_list[i]);
            }
            var encoded = JSON.stringify(to_json);
            $.cookie('source_filter', encoded, { path: '/' });
            old = true;
            $('#result_list').html('');*/
        }
    });
    if ($.cookie('source_filter')){
        var parsed = JSON.parse($.cookie('source_filter'));
        $('#source-filter').multipleSelect('setSelects', parsed);
    }
    
    $('#body-filter').multipleSelect({
        selectAll: false,
        placeholder: "Кузов",
        width: 193,
        onClick: function(view) {
        /*    var selected_list = $('#body-filter').multipleSelect('getSelects');
            $('#body-filter-ids').html('');
            var to_json = [];
            for (var i=0;i<selected_list.length;i++){
                $('#body-filter-ids').html($('#body-filter-ids').html()+'<input type="hidden" class="body-ids" name="filter[body]['+i+']" value="'+selected_list[i]+'">');
                to_json.push(selected_list[i]);
            }
            var encoded = JSON.stringify(to_json);
            $.cookie('body_filter', encoded, { path: '/' });
            old = true;
            $('#result_list').html('');*/
        }
    });
    if ($.cookie('body_filter')){
        var parsed = JSON.parse($.cookie('body_filter'));
        $('#body-filter').multipleSelect('setSelects', parsed);
    }
    
    
    $('#mark-filter').multipleSelect({
        selectAll: false,
        placeholder: "Марка",
        width: 283,
        onClick: function(view) {
        /*    var selected_list = $('#mark-filter').multipleSelect('getSelects');
            $('#mark-filter-ids').html('');
            var to_json = [];
            for (var i=0;i<selected_list.length;i++){
                $('#mark-filter-ids').html($('#mark-filter-ids').html()+'<input type="hidden" class="mark-ids" name="filter[marka]['+i+']" value="'+selected_list[i]+'">');
                to_json.push(selected_list[i]);
            }
            var encoded = JSON.stringify(to_json);
            $.cookie('mark_filter', encoded, { path: '/' });
            old = true;
            $('#result_list').html('');*/
        }
    });
    if ($.cookie('mark_filter')){
        var parsed = JSON.parse($.cookie('mark_filter'));
        $('#mark-filter').multipleSelect('setSelects', parsed);
    }
    
    
    
    
    
    
    
    $('#region-filter').multipleSelect({
        selectAll: false,
        placeholder: "Регион",
        width: 283,
        onClick: function(view) {
        /*    var selected_list = $('#region-filter').multipleSelect('getSelects');
            $('#region-filter-ids').html('');
            var to_json = [];
            for (var i=0;i<selected_list.length;i++){
                $('#region-filter-ids').html($('#region-filter-ids').html()+'<input type="hidden" class="region-ids" name="filter[regions]['+i+']" value="'+selected_list[i]+'">');
                to_json.push(selected_list[i]);
            }
            var encoded = JSON.stringify(to_json);
            $.cookie('region_filter', encoded, { path: '/' });
            old = true;
            $('#result_list').html('');*/
        }
    });
    if ($.cookie('region_filter')){
        var parsed = JSON.parse($.cookie('region_filter'));
        $('#region-filter').multipleSelect('setSelects', parsed);
    }
    
    
    $('#result_list').on('mousemove',"[data-minutestooltip]",function (eventObject) {
        
        $data_tooltip = $(this).attr("data-minutestooltip");
        if ($data_tooltip.length>0){
        /*    $(this).find('.minutestooltip').text($data_tooltip)
                     .show();*/
        }
        
    }).mouseout(function () {
        
     /*   $(this).find('.minutestooltip').hide()
                     .text("");*/
    });
    
    
/*    $('#result_list').on('mousemove',".line", function(){
        var icon = $(this).find('.phone .phone-icon');
        if ($(icon).data('focus')==0)
            $(icon).css("opacity", 0.25);
    }).mouseout(function () {
        var icon = $(this).find('.phone .phone-icon');
        $(icon).css("opacity", 0.0);
    });*/
    
    $('#result_list').on('mousemove',".phone_find", function(){
        $(this).data("focus",1);
        $(this).css("opacity", 1);
    });
    $('#result_list').on('mouseout',".phone_find", function() {
        $(this).data("focus",0);
    });
    
   /* $('#result_list').on('mousemove',".line [data-tooltip]",function (eventObject) {
        
        $data_tooltip = $(this).attr("data-tooltip");
        if ($data_tooltip.length>0){
            $(this).find('.tooltip').text($data_tooltip)

                     .show();
        }
        
    }).mouseout(function () {

        $(this).find('.tooltip').hide()
                     .text("");
    });*/
    $('#result_list').on('mousemove',".line",function (eventObject) {
        $(this).find('.phone_find').addClass('phone_find_hover');
    }).mouseout(function () {
        $(this).find('.phone_find').removeClass('phone_find_hover');
    });
    
  /*  $('#result_list').on('mouseover','.new', function(){
        $(this).removeClass('new');
    });*/
    
  /*  $('#result_list').on('mouseover','.moderation', function(){
        $(this).removeClass('moderation');
    });*/
    
    function checkbalance(){
        $.ajax({
            type: "post",
            url: "/ajax/checkbalance",
            dataType: "json",
        //    data: params,
            beforeSend: function(){
            },
            complete: function(){
            },
            success: function(res){
                if (res.success){
                    $('#balance').html(res.balance);
                    var b = res.balance.replace(/\s+/g, ''); // Убрать пробелы (форматирование)
                    b = parseInt(b);
                    if (b<2)
                        littleBalance();
                }
            }
        });
    }
    
    $('#result_list').on('click','a', function(){
        setTimeout(function() {
            checkbalance();
        }, 2000);
        var id = $(this).data('id');
        if ($(this).find(".moderation").length==0){
            getAdsPhone(id);
        }
        if ($(this).find(".phone_find").data('focus')==1){
            getAdsPhone(id);
        }
    });
    
    
    function in_array(value, array) 
    {
        for(var i = 0; i < array.length; i++) 
        {
            if(array[i] == value) return true;
        }
        return false;
    }
    
    $('#phone-number').bind('change keyup input click', function(event){
        if (this.value.match(/[^0-9]/g)) {
            this.value = this.value.replace(/[^0-]/g, '');
        }
        if (event.type=='keyup' && event.keyCode!=8){
            var arr = ['7','X','X','X','X','X','X','X','X','X','X'];
            var cur_str = this.value.replace(/\s+/g, '');
            for (var i=1;i<=cur_str.length;i++){
                var ind = arr.indexOf('X');
                if (ind!=-1){
                    arr[ind] = cur_str.charAt(i);
                }
            }
            var str = '';
            for (var i=0;i<arr.length;i++){
                if (arr[i]=='X') break;
                str+=arr[i];
            }
        } else {
            var str = this.value.substr(1);
            str = '7'+str;
        }
        this.value = str;
        $.cookie('phone-number', $(this).val(), { path: '/' });
        checkPhonefindbutton();
    });
    
    
    
    $('#searchbyphone-form').on('submit', function(){
        var stopsubmit = false;
        if ($('#phone-number').val().length<11)
            return false;
        var phone = $('#phone-number').val();//79867008717
        var params = new Object();
        params.phone = phone;
        $.ajax({
            type: "post",
            url: "/ajax/findbyphone",
            dataType: "json",
            data: params,
            async: false,
            beforeSend: function(){
                $('.phone-search .find').hide();
                $('#checkphone-ajax-loader').show();
                $('.transition-and-phone-search .error').fadeOut();
            },
            complete: function(){
                $('#checkphone-ajax-loader').hide();
                $('.phone-search .find').show();
            },
            success: function(res){
                if (res.success==false && res.code==4){
                    $('.transition-and-phone-search .error').fadeIn();
                    setTimeout(function(){$('.transition-and-phone-search .error').fadeOut('slow')},3000); 
                    stopsubmit = true;
                }
            }
        });
        if (stopsubmit)
            return false;
    });
    
    
    $('.transition-and-phone-search .online').on('click', function(){
        if (!$(this).hasClass('active')){
            $('.filter-state').show();
            $('.transition-and-phone-search .online').removeClass('active');
            $('.byphonesearch-table').hide();
            $('.ads-table').show();
        }
    });
    
    $('.filter-state').on('click', function(){
        if ($('.filters').is(':visible')){
            $('.filters').hide();
            $('.filter-state').html("Показать фильтр");
            $.cookie('filter_state', 0, { path: '/' });
        } else {
            $('.filters').show();
            $('.filter-state').html("Скрыть фильтр");
            $.cookie('filter_state', 1, { path: '/' });
        }
    });
	
	function getList(){   
				
		
                var params = new Object();
                params.id = $('#result_list').attr('data_id');
                params.old = (old?'1':'0');
                params.volume_from = $('#volume_from').data('default')!==undefined?$('#volume_from').data('default'):$('#volume_from').val();
                params.volume_to = $('#volume_to').data('default')!==undefined?$('#volume_to').data('default'):$('#volume_to').val();
                params.price_from = $('#price_from').data('default')!==undefined?$('#price_from').data('default').replace(/\s+/g, ''):$('#price_from').val();
                params.price_to = $('#price_to').data('default')!==undefined?$('#price_to').data('default').replace(/\s+/g, ''):$('#price_to').val();
                params.run_from = $('#run_from').data('default')!==undefined?$('#run_from').data('default').replace(/\s+/g, ''):$('#run_from').val();
                params.run_to = $('#run_to').data('default')!==undefined?$('#run_to').data('default').replace(/\s+/g, ''):$('#run_to').val();
                params.year_from = $('#year_from').data('default')!==undefined?$('#year_from').data('default'):$('#year_from').val();
                params.year_to = $('#year_to').data('default')!==undefined?$('#year_to').data('default'):$('#year_to').val();
                params.phone_amount = $('#phone-amount-filter').data('default')!==undefined?$('#phone-amount-filter').data('default'):$('#phone-amount-filter').val();
                params.page_size = search_pagesize;
                params.state = $('#state-filter').data('default')!==undefined?$('#state-filter').data('default'):$('#state-filter').val();
                params.kpp = $('#kpp-filter').data('default')!==undefined?$('#kpp-filter').data('default'):$('#kpp-filter').val();
                params.engine = $('#engine-filter').data('default')!==undefined?$('#engine-filter').data('default'):$('#engine-filter').val();
                var src = [];
            //    console.dir($('.source-ids'));
                $('.source-ids').each(function(){
                    src.push($(this).val());
                });
                params.source = JSON.stringify(src);
                
                var bodys = [];
                $('.body-ids').each(function(){
                    bodys.push(parseInt($(this).val()));
                });
                params.bodys = JSON.stringify(bodys);
                
                
                var mark = [];
                $('.mark-ids').each(function(){
                    mark.push(parseInt($(this).val()));
                });
                params.marks = JSON.stringify(mark);
                
                var regions = [];
                $('.region-ids').each(function(){
                    regions.push(parseInt($(this).val()));
                });
                params.regions = JSON.stringify(regions);
                params.security = $('#security').prop("checked")?1:0;
              /*  params.bodys = JSON.stringify($('#body-filter').multipleSelect('getSelects'));
                params.kpp = JSON.stringify($('#kpp-filter').multipleSelect('getSelects'));
                params.marks = JSON.stringify($('#mark-filter').multipleSelect('getSelects'));
                params.regions = JSON.stringify($('#region-filter').multipleSelect('getSelects'));
                */
               
                if(wait || load_lock) 
                    return;
                wait=true;
               
                if (first_load || old){
                  //  $('.table-controls').css('position','fixed').css('bottom', '108px');
                    $('.table-controls').hide();
                    $('#result_list').hide();
                    $('.legend').hide();
                    $('#table-loading-indicator').show();
                    $('.result_list_header').hide();
                    
                } else {
                  /*  if (!enough_balance)
                        return;*/
                }
                
                load_lock = true;
                $.ajax({
                    type: "post",
                    url: "/ajax/getsearchresult",
                    dataType: "json",
                    data: params,
                 //\   async: false,
                    complete: function(){
                        load_lock = false;
                        $('#table-loading-indicator').hide();
                        $('#result_list').show();
                        $('.legend').show();
                        $('.table-controls').show();
                    },
                    success: function(res){
                        /*    $('#table-loading-indicator').hide();
                            $('#result_list').show();
                            $('.legend').show();
                            $('.table-controls').show();*/

                        if(res.status=="ok"){   
                            
                            if($('#result_list .line').length==0)
                                trClass = '';                                                                                                                                                                                                                                         
                            else{ 				
                                trClass = 'newTask'; 
                            }						

                            var first_date=0;
                            if(!res.ads.length) return;
                            var sound = false;
                            
                            
                            var data = res.ads;
                            if (params.old != 1) {                        
                                data = data.reverse();                 
                            }
                            $(data).each(function(idx,obj){       
                            //    if(obj.phone){
                        //            console.dir(obj);
                                    var td = '';    
                                    td+= '<a href="/adsredirect?id='+obj.Id+'" target="_blank" class="'+(lines_amount%2==0?'even ':'odd ')+'line'+((highlight_cheap && obj.price!=0 && obj.difference_price>=50 && parseInt(obj.condition)!=1)?' difference_price':'')+''+(highlight_moderation && obj.minutes_to_publish.length>0?' moderation':'')+'" id="id_'+obj.Id+'" data-id='+obj.Id+' data-minutestooltip="'+(obj.minutes_to_publish.length>0?('Публикация через '+obj.minutes_to_publish+' мин'):'')+'">';
                                    td+= '<div class="minutestooltip"></div>';    
//       if (show_photos==1)
                             //           td+= '<td>'+(obj.photo?'<img style="max-width: 140px; max-height:100px" rel="'+obj.photo+'" src="'+obj.photo+'">':'')+'</td>';  
                                    if (!show_photos){
                                        td+= '<div style="width: 50px" class="td time"><div class="pos">'+obj.time+'</div></div>';
                                        td+= '<div style="width: 40px" class="td logo">'+(obj.marka_logo?('<img class="brand-logo" src="/resources/img/brands/'+obj.marka_logo+'">'):'')+'</div>';
                                    } else {
                                        td+= '<div style="width: 122px;height:92px;" class="td main-photo"><div class="pos">';
                                        if (obj.photo.length>0)
                                            td+= '<img src="'+obj.photo+'">';
                                        else
                                            td+= '<img src="/resources/img/nophoto.png">';
                                        td+= '</div></div>';
                                    }
                                    td+= '<div style="width: 380px" class="name td"><nobr><div class="fade"></div><div class="pos">';
                                    
                                    var eng = obj.enginevol+((obj.engine.length!=0 && obj.engine!='b')?obj.engine:'')+(obj.transmission.length!=0?(' '+obj.transmission):'');
                                    if (eng.length>2)
                                        eng = '('+eng+')';
                                    var mark = obj.marka+' '+obj.model+' '+obj.model_2;
                                    if (mark.length>25)
                                        mark = mark.substring(0,25)+'...';
                                    td+= '<span class="mark-info" style="top: 3px;">'+mark+' <span class="engine-info">'+eng+'</span></span></div>';
                                    if (!show_photos){
                                        td+= (obj.rudder==1?'<img class="rudder-item" src="/resources/img/rudder.png">':'');
                                        td+= (obj.vin==1?'<img class="vin-item" src="/resources/img/vin.png">':'');
                                 //       td+= (obj.fast==1?'<img class="fast-item" src="/resources/img/fast.png">':'');
                                 //       td+= (obj.wheel==1?'<img class="wheel-item" src="/resources/img/wheel.png">':'');
                                        td+= '<span id="phone-text-'+obj.Id+'" class="phone-text"></span>';
                                    } else 
                                        td+= (obj.rudder==1?'<img class="rudder-item" src="/resources/img/rudder.png">':'');
                                    
                                    
                                        
               
                                    
                                    td+= '</nobr>';
                                    if (show_photos){
                                        td+='<div class="show-photos-time">'+obj.time+' <span id="phone-text-'+obj.Id+'" class="phone-text"></span></div>';
                                        td+='<div class="show-photos-items">';
                                        td+= (obj.vin==1?'<img class="vin-item" src="/resources/img/vin.png">':'');
                                 //       td+= (obj.fast==1?'<img class="fast-item" src="/resources/img/fast.png">':'');
                                 /*       td+= (obj.wheel==1?'<img class="wheel-item" src="/resources/img/wheel.png">':'');*/
                                        td+='</div>';
                                    }
                                    td+= '</div>';
                                //    td+= '<div style="width: 86px" class="name_2 td"><div class="pos"'+(obj.model_2.length>14?' style="top: 4px"':'')+'>'+obj.model_2+'</div></div>'; 
                                //    td+= '<div style="width: 26px" class="phone td">';
                                    
                                 //   td+= '</div>';
                                 //   td+= '<img data-focus="0" class="phone-icon" src="/resources/img/tel.png"/>';
                                    
                                    td+= '<div style="width: 90px" class="price td"><div class="pos">'+((obj.price!=0 && obj.torg==1)?'<img class="torg-item" src="/resources/img/torg.png">':'')+(obj.price!=0?(accounting.formatNumber(obj.price, 0, " ", "")):'')+'</div></div>';
                                    if (obj.condition==0){
                                        if (obj.average_price==0)
                                            td+= '<div style="width: 70px" class="diff td"><div class="pos"></div></div>';
                                        else {
                                            if (obj.difference_price>0) // средняя минус текущая > 0, т.е. цена занижена
                                                td+= '<div style="width: 70px" class="diff td"><div class="pos'+((obj.price!=0)?' green':'')+'">'+(obj.difference_price!=0?(accounting.formatNumber(obj.difference_price, 0, " ", "")):'')+'</div></div>';
                                            else
                                                td+= '<div style="width: 70px" class="diff td"><div class="pos'+((obj.price!=0)?' red':'')+'">'+(obj.difference_price!=0?(accounting.formatNumber((-1)*obj.difference_price, 0, " ", "")):'')+'</div></div>';
                                        }
                                    } else
                                        td+= '<div style="width: 70px" class="badcondition td"><img src="/resources/img/bt.png"></div>';
                                    td+= '<div style="width: 50px" class="year td"><div class="pos">'+obj.year+'</div></div>';
                                    td+= '<div style="width: 80px" class="run td"><div class="pos">'+(obj.run!=0?accounting.formatNumber(obj.run, 0, " ", ""):'')+'</div></div>';
                                    
                               //     td+= '<div style="width: 90px" class="td'+(obj.engine.length!=0?' engine_eng':' engine')+'"><div class="pos">'+obj.enginevol+' '+obj.transmission+(obj.engine.length!=0?(', '+obj.engine):'')+'</div></div>';
                                    var body = (obj.body_img && obj.body_img.length>0?obj.body_img:obj.body);
                                    if (body==null)
                                        body = '';
                                    td+= '<div style="width: 70px" class="td body" data-tooltip="'+obj.body+'"><div class="tooltip"></div><div class="pos">'+body+'</div></div>';
                                    td+='<div style="width: 30px" class="td color">'+
                                            '<div class="pos'+(obj.dark!=1?' white':'')+'">'+obj.color+'</div>'+
                                    //        (obj.metallic==1?'<div class="metallic'+((obj.dark!=1 || obj.color_hex.length==0 || obj.color_hex=="FFFFFF")?' metallic-white':'')+'">м</div>':'')+
                                        '</div>';
                               //     td+= '<div style="width: 70px" class="td"></div>';
                                    
                                    var metr_reg = obj.region+(obj.metro.length>0?(', м. '+obj.metro):'');
                                    var ogr = show_photos?75:25;
                                    if (metr_reg.length>ogr)
                                        metr_reg = metr_reg.substring(0,ogr)+'...';
                                    td+= '<div style="'+(show_photos?'width: 233px':'width: 235px')+'" class="region td"><div class="pos">'+metr_reg+'</div></div>';
                                    if (!show_photos){
                                        if (obj.photo){
                                            td+= '<div style="width: 30px" class="td photos"><div class="pos"><img class="ph" src="/resources/img/ph.png"/></div></div>';
                                        } else {
                                            td+= '<div style="width: 30px" class="td photos"></div>'
                                        }
                                    }
                                    var phone_find_color = '#f5f5f5';
                                    if (obj.minutes_to_publish.length>0 && highlight_moderation)
                                        phone_find_color = '#ffeb00';
                                    if (obj.difference_price>=50 && obj.price!=0 && parseInt(obj.condition)!=1 && highlight_cheap)
                                        phone_find_color = '#a5f028';
                                    td+= '<div style="width: 25px; background-color: '+phone_find_color+'" class="phone_find td"><div class="pos">'+
                                          '<form id="phone-search-form-'+obj.Id+'" class="phone-search-form" action="/search" method="post" target="_blank"><input type="hidden" name="id" value="'+obj.Id+'">'+
                                          (obj.phone_find<20?
                                          '<input class="phonefindsubmit" type="submit" '+(obj.phone_find>0?'style="color:#28578b; cursor: pointer;" ':'style="color:#969696" ')+'name="send" value="'+obj.phone_find+'" />':
                                          '<input class="phonefindsubmitimg" type="submit" name="send" value="" />')+
                                          '</form>'+
                                    '</div></div>';
                                //    td+= '<td width="110px">'+obj.region+'</td>';

                                    /*td+= '<td>'+obj.body+'</td>';
                                    td+= '<td>'+obj.enginevol+'</td>';
                                    td+= '<td>'+obj.transmission+'</td>';
                                    td+= '<td>'+obj.condition+'</td>';
                                    td+= '<td>'+obj.run+'</td>';
                                    td+= '<td>'+obj.color+'</td>';
                                    td+= '<td>'+obj.wheel+'</td>';
                                    td+= '<td><a href="'+obj.url+'">'+obj.phone+'</a><sup>'+obj.phone_find+'</sup></td>';
                                    td+= '<td>'+obj.address+'<br>'+obj.region+'</td>';
                                    td+= '<td>'+obj.source+'</td>';
                                    td+= '<td>'+obj.fast+'</td>';
                                    td+= '<td>'+obj.tires+'</td>';*/
                                    td+= '</a>';  
                                    
                                    if (params.old == 1){
                                        $('.result_list_header').show();
                                        $('#result_list').append(td);
                                         if (!enough_balance){
                                            littleBalance();
                                        }
                                    } else {
                                        if (obj.Id>parseInt($('#result_list').attr('data_id'))) {
                                            $('#result_list').prepend(td);
                                            sound = true;
                                        }
                                    }
                                    $('#result_list').on('submit', '#phone-search-form-'+obj.Id, function(elem){
                                        if ($('#phone-search-form-'+obj.Id).find('.phonefindsubmit').val()==0)
                                            return false;
                                    });
                                /*    if ($('#id_'+obj.Id).hasClass('moderation') && $('#id_'+obj.Id).hasClass('difference_price'))
                                        $('#id_'+obj.Id).find('.time').css("background-color","#fff500");*/
                                    ///////////////////////////
                                   if (obj.phone != null){
                                        var me = $('#id_'+obj.Id).find('.phone');
                                        $(me).parent('.line').find('.name').css("width",316);
                                        $(me).css("width",88);


                                        $(me).find('.phone-icon').remove();
                                 //       var ph_text = $(me).find('.phone-text');
                                            $('#phone-text-'+obj.Id).html(obj.phone);
                                    }
                    
                                    ///////////////////////////
                    
                                 /*   if (params.old==0)
                                        $('#id_'+obj.Id).fadeIn(2000);*/
                                //    console.dir(new_records);
                                    if (!first_load && !in_array(obj.Id, new_records)){
                                        $('#id_'+obj.Id).addClass('new');
                                        new_records.push(obj.Id);
                                    }
                                    
                                    if (first_load)
                                        new_records.push(obj.Id);
                                    
                                    
                                    
                                    old_id = $('#result_list .line:first-child').data('id');
                                    if (parseInt(obj.Id) > parseInt($('#result_list').attr('data_id')))
                                        $('#result_list').attr('data_id',obj.Id);

                                    if($('#result_list .line').length>search_pagesize){ 
                                        $('#result_list .line:last-child').remove();
                                    }						
                            //    }
                                lines_amount++;
                            });
                            if (sound && sounds){
                                var audio = document.getElementsByTagName("audio")[0];
                                audio.play();
                            }
                            if (!show_photos){
                                $('#result_list .phone-text').css('position', 'absolute');
                            }
                            first_load = false;
			}
                        autoHeight();
                    }
                });
                
	/*	if (enough_balance)
                    $('.filter-state').show();*/
	/*	$.post('/ajax/getads', {'id':$('#ads').attr('data_id'), 'old':(old?'1':'0')}, function(res){ 
			if(res.status=="ok"){                                                                                                                                                                                                                                             
				 
				if($('#ads tr').length==0)
                    trClass = '';                                                                                                                                                                                                                                         
                else{ 				
					trClass = 'newTask'; 
				}						

				var first_date=0;
				if(!res.ads.length) return;
				
				$(res.ads).each(function(idx,obj){       
					if(obj.phone){
						var td = '';    
						td+= '<tr>';
						td+= '<td>'+(obj.photo?'<img style="max-width: 140px; max-height:100px" rel="'+obj.photo+'" src="'+obj.photo+'">':'')+'</td>';  						
						td+= '<td>'+obj.marka+' <nobr>'+obj.model+'</nobr></td>';   
						td+= '<td>'+obj.price+'</td>';   
						td+= '<td>'+obj.body+'</td>';
						td+= '<td>'+obj.enginevol+'</td>';
						td+= '<td>'+obj.transmission+'</td>';
						td+= '<td>'+obj.condition+'</td>';
						td+= '<td>'+obj.run+'</td>';
						td+= '<td>'+obj.color+'</td>';
						td+= '<td>'+obj.wheel+'</td>';
						td+= '<td><a href="'+obj.url+'">'+obj.phone+'</a><sup>'+obj.phone_find+'</sup></td>';
						td+= '<td>'+obj.address+'<br>'+obj.region+'</td>';
						td+= '<td>'+obj.source+'</td>';
						td+= '<td>'+obj.fast+'</td>';
						td+= '<td>'+obj.tires+'</td>';
						td+= '</tr>';    						
						
						$('#ads tbody').prepend(td);
						
						if (parseInt(obj.Id) > parseInt($('#ads').attr('data_id')))
							$('#ads').attr('data_id',obj.Id);
						
						if($('#ads tbody tr').length>limit){ 
							$('#ads tbody tr:last-child').remove();
						}						
					}
				});
				
				
			}			
		});	*/	
		
	//	console.log("add ads last Id: "+$('#ads').attr('data_id'));

		if(old)
                    old=false;
		
		wait=false;
	}
        
        

});/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


