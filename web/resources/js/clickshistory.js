$(document).ready(function(){
    if (!enough_balance)
        $('.warning-panel').show();
    
    $('.more-button').show();
    function checkPhonefindbutton(){
        if ($('#searchbyphone-form .phone-number').val().length!=11)
            $('#searchbyphone-form .find').css('color', '#969696').css('cursor', 'default').addClass('nohover');
        else
            $('#searchbyphone-form .find').css('color', '#28578B').css('cursor', 'pointer').removeClass('nohover');;
    }
    checkPhonefindbutton();
    var page_number = 1;
    var page_size = 25;
    $('#phone-number').bind('change keyup input click', function(event){
        if (this.value.match(/[^0-9]/g)) {
            this.value = this.value.replace(/[^0-]/g, '');
        }
        if (event.type=='keyup' && event.keyCode!=8){
            var arr = ['7','X','X','X','X','X','X','X','X','X','X'];
            var cur_str = this.value.replace(/\s+/g, '');
            for (var i=1;i<=cur_str.length;i++){
                var ind = arr.indexOf('X');
                if (ind!=-1){
                    arr[ind] = cur_str.charAt(i);
                }
            }
            var str = '';
            for (var i=0;i<arr.length;i++){
                if (arr[i]=='X') break;
                str+=arr[i];
            }
        } else {
            var str = this.value.substr(1);
            str = '7'+str;
        }
        this.value = str;
        checkPhonefindbutton();
    });
    
    $('#searchbyphone-form').on('submit', function(){
        var stopsubmit = false;
        if ($('#phone-number').val().length<11)
            return false;
        var phone = $('#phone-number').val();//79867008717
        var params = new Object();
        params.phone = phone;
        $.ajax({
            type: "post",
            url: "/ajax/findbyphone",
            dataType: "json",
            data: params,
            async: false,
            beforeSend: function(){
                $('.phone-search .find').hide();
                $('#checkphone-ajax-loader').show();
                $('.transition-and-phone-search .error').fadeOut();
            },
            complete: function(){
                $('#checkphone-ajax-loader').hide();
                $('.phone-search .find').show();
            },
            success: function(res){
                if (res.success==false && res.code==4){
                    $('.transition-and-phone-search .error').fadeIn();
                    setTimeout(function(){$('.transition-and-phone-search .error').fadeOut('medium')},3000); 
                    stopsubmit = true;
                }
            }
        });
        if (stopsubmit)
            return false;
    });
    
    // Подсчет количества страниц
    function pageAmount(count){
        if (count%page_size==0)
            return count/page_size;
        else
            return Math.floor(count/page_size)+1;
    }
    
    function loadMore(){
        var params = new Object();
        params.page = page_number;
        $('.more-button').hide();
        $('#more-loading-indicator').show();
        $.ajax({
            type: "post",
            url: "/ajax/loadmorehistory",
            dataType: "html",
            data: params,
            async: false,
            beforeSend: function(){
                
            },
            complete: function(){
                
            },
            success: function(res){
                $('#more-loading-indicator').hide();
                $('.more-button').show();
                
                $('#result_list').append(res);
                if (page_number == pageAmount(amount))
                    $('.more-button').hide();
                else
                    $('.more-button').show();
            }
        });
    }
    
    $('.more-button').on('click', function(){
        page_number+=1;
        loadMore();
    });
    
    $('#result_list').on('mousemove',".line",function (eventObject) {
        $(this).find('.phone_find').addClass('phone_find_hover');
    }).mouseout(function () {
        $(this).find('.phone_find').removeClass('phone_find_hover');
    });
});