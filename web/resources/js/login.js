$(document).ready(function(){
    var lock = false;
    if(navigator.userAgent.toLowerCase().indexOf('chrome') >= 0){
        $(window).load(function(){
            $('input:-webkit-autofill').each(function(){
                $(this).after(this.outerHTML).remove();
                $('input[name='+$(this).attr('name')+']').val($(this).val());
            });
        });
    }
    
    document.onkeyup = function (e) {
        e = e || window.event;
        if (e.keyCode === 13) {
            if ($('.login-form input[name=login]').val().length>0 && $('.login-form input[name=password]').val().length>0)
                $('.login-button').parent('form').submit();
        }
        return false;
    }

    $('.login-form').submit(function(){
        var error = false;
        if ($('.login-form .login-input').val().length==0){
            if (!$('.login-form .login-input').is(':animated'))
                $('.login-form .login-input').css('background-color', '#ffe1e1').animate({"background-color": "initial"}, 3000);
            error = true;
        }
        if ($('.login-form .password-input').val().length==0){
            if (!$('.login-form .password-input').is(':animated'))
                $('.login-form .password-input').css('background-color', '#ffe1e1').animate({"background-color": "initial"}, 3000);
            error = true;
        }
        if (error) return false;
    });
    
    $('.login-button').on('click', function(){
        $(this).parent('form').submit();
    });
    
    $('.online-table .online-body').on('mousemove',".line",function (eventObject) {
        $(this).find('.phonefind').addClass('phone_find_hover');
    }).mouseout(function () {
        $(this).find('.phonefind').removeClass('phone_find_hover');
    });
    
    setInterval(getLast,1000);
    
    function getLast(){
        if (lock || typeof last_item_id=='undefined')
            return;
        var params = new Object();
        params.last_id = last_item_id;
        $.ajax({
            type: "post",
            url: "/ajax/getlastdemo",
            dataType: "json",
            data: params,
            complete: function(){
                
            },
            success: function(res){
                if (res.html.length>0 && res.last_id!=0 && last_item_id != res.last_id){
                //    console.dir(res.last_id+" "+res.amount);
                    last_item_id = res.last_id;
                    $('.online-table .online-body').html(res.html);
                /*    $('.online-table .online-body').prepend(res.html);
                    while ($('.online-table .online-body .line').length>10){ 
                        $('.online-table .online-body .line:last-child').remove();
                        console.dir("removed");
                    }*/
                }
                lock = false;
            }
        });
    }
    
    $('#register-button').on('click', function(){
        var error = false;
        if ($('.register .username').val().length<3 || $('.register .username').val().length>12){
            if (!$('.register .username').is(':animated'))
                $('.register .username').css('background-color', '#ffe1e1').animate({"background-color": "initial"}, 3000);
            error = true;
        }
        if ($('.register .userphone').val().length==0){
            if (!$('.register .userphone').is(':animated'))
                $('.register .userphone').css('background-color', '#ffe1e1').animate({"background-color": "initial"}, 3000);
            error = true;
        }
        
        if (error) return;
        
        var phone = $('.register .userphone').val();
        phone = phone.substr(1); // Убираем "+" спереди

        
        $('#register-error').html('');
        var params = new Object();
        params.name = $('.register .username').val();
        params.phone = phone;
        $.ajax({
            type: "post",
            url: "/ajax/registration",
            dataType: "json",
            data: params,
            complete: function(){
                
            },
            success: function(res){
                if (!res.success){
                    $('#register-error').html(res.message);
                } else {
                    $('#register-button, .register .username, .register .userphone').hide();
                    $('#register-error').html('');
                    $('#register-success').show();
                }
            }
        });
    });
    
    $('.register .username').bind('change keyup input click', function(event){
        var str = "";
        for (var i=0;i<this.value.length;i++){
            if (this.value.charAt(i).match(/[а-яА-ЯёЁ]+/g))
                str+=this.value.charAt(i);
        }
        this.value = str;
    });
    
    $('.register .userphone').bind('change keyup input click', function(event){
        if (this.value.match(/[^0-9]/g)) {
            this.value = this.value.replace(/[^0-9]/g, '');
        }
        if (event.type=='keyup' && event.keyCode!=8){
            var arr = ['+','7','X','X','X','X','X','X','X','X','X','X'];
            var cur_str = this.value.replace(/\s+/g, '');
            for (var i=1;i<=cur_str.length;i++){
                var ind = arr.indexOf('X');
                if (ind!=-1){
                    arr[ind] = cur_str.charAt(i);
                }
            }
            var str = '';
            for (var i=0;i<arr.length;i++){
                if (arr[i]=='X') break;
                str+=arr[i];
            }
        } else {
            var str = this.value.substr(1);
            str = '+7'+str;
        }
        this.value = str;
    });
    
    $('#login').bind('change keyup input click', function(event){
        if (this.value.match(/[^0-9]/g)) {
            this.value = this.value.replace(/[^0-9]/g, '');
        }
        if (event.type=='keyup' && event.keyCode!=8){
            var arr = ['+','7','X','X','X','X','X','X','X','X','X','X'];
            var cur_str = this.value.replace(/\s+/g, '');
            for (var i=1;i<=cur_str.length;i++){
                var ind = arr.indexOf('X');
                if (ind!=-1){
                    arr[ind] = cur_str.charAt(i);
                }
            }
            var str = '';
            for (var i=0;i<arr.length;i++){
                if (arr[i]=='X') break;
                str+=arr[i];
            }
        } else {
            var str = this.value.substr(1);
            str = '+7'+str;
        }
        this.value = str;
    });
    
    $('.changepassword_place .question').on('click', function(){
        $(this).hide();
        $('.changepassword_place .input-place').show();
    });
    
    $('.changepassword_place .input-place .password-button ').on('click', function(){
        if ($('.changepassword_place .input-place input').val().length!=12){
            if (!$('.changepassword_place .input-place input').is(':animated'))
                $('.changepassword_place .input-place input').css('background-color', '#ffe1e1').animate({"background-color": "initial"}, 3000);
            return;
        }
        $('.changepassword_place .result').html('');
        var params = new Object();
        var login = $('.changepassword_place .input-place input').val();
        login = login.substr(1);
        params.login = login;
        $.ajax({
            type: "post",
            url: "/ajax/changepassword",
            dataType: "json",
            data: params,
            complete: function(){
                
            },
            success: function(res){
                if (!res.success){
                    $('.changepassword_place .info').html(res.message);
                } else {
                    $('.changepassword_place .info').hide();
                    $('.changepassword_place .input-place').hide();
                    $('.changepassword_place .result').html("Новый пароль успешно отправлен!");
                }
            }
        });
    });
    
    
});


