$(document).ready(function(){
/*    $('#phone').bind('change keyup input click', function(event){
        if (this.value.match(/[^0-9]/g)) {
            this.value = this.value.replace(/[^0-9]/g, '');
        }
    });*/
    
    $('.save').on('click', function(){
    /*    if ($('#name').val()==0){
            $('#name-error').html("Обязательное поле");
            return false;
        } else
            $('#name-error').html("");
        if ($('#phone').val()==0){
            $('#phone-error').html("Обязательное поле");
            return false;
        } else
            $('#phone-error').html("");*/
        if ($('#password').val()==0){
            $('#password-error').html("Обязательное поле");
            return false;
        } else
            $('#password-error').html("");
        if ($('#password').val()!=$('#repeat-password').val()){
            $('#password-error').html("Пароли не совпадают!");
            return false;
        } else
            $('#password-error').html("");
        var params = new Object();
        //params.name = $('#name').val();
        //params.phone = $('#phone').val();
        params.password = $('#password').val();
        params.search_photos = $("#search_photos").prop("checked")?1:0;
        params.search_sounds  = $("#search_sounds").prop("checked")?1:0;
        $.ajax({
            type: "post",
            url: "/ajax/updateprofile",
            dataType: "json",
            data: params,
            beforeSend: function(){
            },
            complete: function(){
            },
            success: function(response){
                if (response.error_code != 0){
                    console.dir(response.error_code+": "+response.message);
                    if (response.error_code==2)
                        $('#phone-error').html("Пользователь с таким телефоном уже существует");
                } else {
                    $('.saved').fadeIn();
                    setTimeout(function(){$('.saved').fadeOut('fast')},3000); 
                }
            }
        });
    });
    
    $('.settings-item').on('change', function(){
        var me = $(this);
        var params = {};
        params.type = $(this).data('type');
        if ($(this).attr('type') == 'checkbox') {
            params.value = $(this).prop('checked')?1:0;
        } else {
            params.value = $(this).val();
        }
        $.ajax({
            type: "post",
            url: "/ajax/updatesettings",
            dataType: "json",
            data: params,
            beforeSend: function() {
            },
            complete: function(){
            },
            success: function(response){
                if (!response.success){
                    alert('Ошибка при сохранении настроек. Пожалуйста, обратитесь к администратору');
                } else {
                    $(me).parent('.form-line').find('.success-text').show();
                    setTimeout(function(){
                        $(me).parent('.form-line').find('.success-text').fadeOut('slow');
                    }, 1000);

                    
                }
            }
        });
    });
});