$(document).ready(function(){
    $(".send").on("click", function(){
        if (!$(this).hasClass("active")) return false;
    });
    
    
    $('#sum').bind('change keyup input click', function(event){
        if (this.value.match(/[^0-9]/g)) {
            this.value = this.value.replace(/[^0-9]/g, '');
        }
        if (this.value.charAt(0)=='0')
            this.value = this.value.substr(1);
        
        if (this.value>4999)
            this.value = this.value.substr(0, this.value.length-1);
        if (this.value>=200 && this.value<=15000)
    //    if (this.value.length>0)
            $(".send").addClass("active");
        else
            $(".send").removeClass("active");
    });
    
});