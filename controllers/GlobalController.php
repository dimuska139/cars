<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\filters\VerbFilter;
use app\models\Users;

class GlobalController extends Controller
{
    public $enableCsrfValidation = false;

    public function setActivepage($item){
        $this->view->params['active_page'] = $item;
    }
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'foreColor'=>0x66534F,
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }
    
    public function actionGettime(){
        $datetime = mb_strtolower(date("w j m, G:i"), 'UTF-8');
        $datetime_array = explode(" ", $datetime);
        switch ($datetime_array[0]){
            case 0: $d='ВС'; break;
            case 1: $d='ПН'; break;
            case 2: $d='ВТ'; break;
            case 3: $d='СР'; break;
            case 4: $d='ЧТ'; break;
            case 5: $d='ПТ'; break;
            case 6: $d='СБ'; break;
        }
        $datetime_array[0] = $d." ";
        switch ($datetime_array[2]){
            case 1: $m='января'; break;
            case 2: $m='февраля'; break;
            case 3: $m='марта'; break;
            case 4: $m='апреля'; break;
            case 5: $m='мая'; break;
            case 6: $m='июня'; break;
            case 7: $m='июля'; break;
            case 8: $m='августа'; break;
            case 9: $m='сентября'; break;
            case 10: $m='октября'; break;
            case 11: $m='ноября'; break;
            case 12: $m='декабря'; break;
        }
        $datetime_array[2] = $m.",";
        $datetime = implode(" ", $datetime_array);
        return $datetime;
    }
    
    public function monthsShort(){
        return [
            'янв',
            'фев',
            'мар',
            'апр',
            'май',
            'июн',
            'июл',
            'авг',
            'сен',
            'окт',
            'ноя',
            'дек'
        ];
    }

    public function init()
    {
   /*     if (empty(Yii::$app->session->get('id'))){
            header( 'Location: /users/login', true, 302 );
            exit();
        }*/
        if (!empty(Yii::$app->session->get('id'))){
            $this->view->params['datetime'] = $this->actionGettime();
            $user = Users::findOne(['id'=>Yii::$app->session->get('id')]);
            $this->view->params['login'] = $user->login;
            $this->view->params['name'] = $user->name;
            $this->view->params['balance'] = $user->balance;
            $this->view->params['is_admin'] = $user->admin==1?true:false;
            
            $enough_balance = $user->balance>=Yii::$app->params['ads_price']?true:false;
            $login_cookie = ['PHPSESSID', '_csrf', 'highlight_moderation', 'highlight_cheap', 'sounds'];
            if (!$enough_balance){
                foreach ($_COOKIE as $key => $val){
                    if (!in_array($key, $login_cookie))
                        setcookie($key, "", time() - 3600);
                }
            }
        }
        
       
        if (!Yii::$app->request->isAjax){
            $this->view->registerJsFile('/resources/jquery-ui-1.11.4/jquery-ui.min.js', ['depends'=>'yii\web\JqueryAsset']);
            $this->view->registerJsFile('/resources/js/jquery.ui.datepicker-ru.js', ['depends'=>'yii\web\JqueryAsset']);
            $this->view->registerCssFile('/resources/jquery-ui-1.11.4/jquery-ui.min.css');
            $this->view->registerCssFile('/resources/jquery-ui-1.11.4/jquery-ui.theme.min.css');
            $this->view->registerJsFile('/resources/js/jquery.cookie.js', ['depends'=>'yii\web\JqueryAsset']);
            $this->view->registerJsFile('/resources/js/main.js', ['depends'=>'yii\web\JqueryAsset']);
        }
    }
    
    public function getSessionKey($length = 30) {
        $chars = 'abdefhiknrstyzABDEFGHKNQRSTYZ23456789';
        $numChars = strlen($chars);
        $string = '';
        for ($i = 0; $i < $length; $i++) {
            $string .= substr($chars, rand(1, $numChars) - 1, 1);
        }
        return $string;
    }
}
