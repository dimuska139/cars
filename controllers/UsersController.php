<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\filters\VerbFilter;
use app\models\Users;
use app\models\Cars;
use app\models\Ads;
use app\models\Body;
use app\models\Colors;
use app\models\Brands;

function mb_ucfirst($string, $encoding='UTF-8')
{
    $firstChar = mb_substr($string, 0, 1, $encoding);
    $then = mb_substr($string, 1, mb_strlen($string, $encoding)-1, $encoding);
    return mb_strtoupper($firstChar, $encoding) . $then;
}


class UsersController extends GlobalController
{
    public $enableCsrfValidation = false;
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'foreColor'=>0x66534F,
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    public function actionIndex()
    {
   //     $this->view->registerCssFile('resources/css/index.css');
    //    $this->view->registerCssFile('resources/css/slider.css');
    //    $this->view->registerJsFile('resources/js/index.js', ['depends'=>'yii\web\JqueryAsset']);
     /*   $new = Goods::find()
                ->where([
                    'new' => 1
                ])
                ->orderBy(['rand_sort' => SORT_DESC])
                ->limit(12)
                ->all();*/

    }
    
    public function actionProfile()
    {   
        if (empty(Yii::$app->session->get('id'))){
            header( 'Location: /', true, 302 );
            exit();
        } else {
            $user = Users::find()
                ->where([
                    'id' => Yii::$app->session->get('id')
                ])->one();
            if (empty($user)/* || strcmp($user->session_key, Yii::$app->session->get('session_key'))!=0*/) {
                header( 'Location: /', true, 302 );
                exit();
            }
        }
        $id = Yii::$app->session->get('id');
        $user = Users::find()
                    ->where([
                        'id' => $id
                    ])->one();
        $this->view->registerCssFile('/resources/css/main.css');
        $this->view->registerCssFile('/resources/css/profile.css');
        $this->view->registerJsFile('/resources/js/profile.js',['depends'=>'yii\web\JqueryAsset']);
        $this->setActivepage('settings');
        return $this->render('profile', [
            'user' => $user
        ]);
    }
    
    public function actionLogin()
    {
        if (!empty(Yii::$app->session->get('id'))){
            $user = Users::find()
                ->where([
                    'id' => Yii::$app->session->get('id')
                ])->one();
            if (!empty($user)/* && strcmp($user->session_key, Yii::$app->session->get('session_key'))==0*/) {
                header( 'Location: /active', true, 302 );
                exit();
            }
        }
        $this->layout = "login.php";
        
        $this->view->registerCssFile('/resources/css/main.css');
        $this->view->registerCssFile('/resources/css/login.css');
        $this->view->registerJsFile('/resources/js/login.js',['depends'=>'yii\web\JqueryAsset']);
        $error = false;
        $in_system = false;
        if (!empty($_POST)){
            $user = Users::find()
                    ->where([
                        'login' => $_POST['login'],
                        'state' => 1
                    ])->one();
            if (!empty($user)){
                if (password_verify($_POST['password'], $user->hash)){
                    $sessionKey = parent::getSessionKey();
                    $user->session_key = $sessionKey;
                    $user->password_changed = 0;
                    $user->save();
                    Yii::$app->session->set('id', $user->id);
                    Yii::$app->session->set('admin', $user->admin==1?true:false);
                    Yii::$app->session->set('session_key', $sessionKey);
                    header( 'Location: /', true, 302 );
                    exit();
                } else
                    $error = true;
            } else
                $error = true;
        }
        $last_list = Cars::find()
                    ->where([
                    //    'status' => 3,
                        'active' => 1
                    ])
                    ->orderBy(['createdate'=> SORT_DESC])
                    ->limit(10)
                    ->all();
        $ids = [];
        foreach ($last_list as $car){
            $ids[] = $car->id;
        }
        // Перемешивание массива индексов
        shuffle($ids);
        // Оставляем первые 4
        $ids = array_slice($ids, 0, 4);
        $last_list = Cars::find()
                ->where([
                    'id' => $ids
                ])
                ->all();
        $this->view->params['error'] = $error;
        
        define("SQL_USER",     Yii::$app->params['SQL_USER']);
	define("SQL_PASSWORD", Yii::$app->params['SQL_PASSWORD']);
	define("SQL_BASE",     Yii::$app->params['SQL_BASE']);
	define("SQL_SERVER",   Yii::$app->params['SQL_SERVER']);
	
	$sql=new \mysqli(SQL_SERVER, SQL_USER, SQL_PASSWORD, SQL_BASE);
	$sql->set_charset("utf8");
        $fields=' ads.`Id`,
        `model`,
        `model_2`,
        `price`,
        `brand_id`,
        `body_id`,
        `color_id`,
        `average_price`,
        MINUTE(TIMEDIFF((`moderation_time`+INTERVAL 30 MINUTE), NOW())) AS minutes_to_publish,
        TIMEDIFF((`moderation_time`+INTERVAL 30 MINUTE), NOW()) AS time_to_publish,
        `engine`,
        `enginevol`,
        `transmission`,
        `condition`,
        `run`,
        `rudder`,
        `phone`,
        `fio`,
        `photo`,
        `metro`,
        `address`,
        `region`,
        `url`,
        `phone_find`,
        `source`,
        `info`,
        `year`,
        `createtime`,
        `vin`';
        $sql_str = "select ".$fields." from ads order by ads.Id desc limit 0, 10";
        $rows = $sql->query($sql_str);
        $brands_list = Brands::find()->all();
        $brands_arr = [];
        foreach ($brands_list as $br){
            $brands_arr[$br->id] = $br->toArray();
        }
        
        $bodys_list = Body::find()->all();
        $bodys_arr = [];
        foreach ($bodys_list as $bod){
            $bodys_arr[$bod->id] = $bod->toArray();
        }
        
     /*   $colors_list = Colors::find()->all();
        $colors_arr = [];
        foreach ($colors_list as $color){
            $colors_arr[$color->id] = $color->toArray();
        }*/
        $result = array();
        while($rlt = $rows->fetch_array(MYSQLI_ASSOC)){
            $rlt['marka'] = $brands_arr[$rlt['brand_id']]['name'];
            $rlt['marka_logo'] = $brands_arr[$rlt['brand_id']]['img_name'];
            $rlt['body'] = !empty($bodys_arr[$rlt['body_id']])?$bodys_arr[$rlt['body_id']]['name']:'';
      //      $rlt['color'] = !empty($colors_arr[$rlt['color_id']])?$colors_arr[$rlt['color_id']]['name']:'';
            $rlt['metallic'] = !empty($colors_arr[$rlt['color_id']])?$colors_arr[$rlt['color_id']]['metallic']:0;
            $rlt['dark'] = !empty($colors_arr[$rlt['color_id']])?$colors_arr[$rlt['color_id']]['dark']:0;

            $rlt['difference_price'] = floor((int)$rlt['average_price']-(int)$rlt['price'])/1000;


            $rlt['minutes_to_publish'] = (!empty($rlt['minutes_to_publish']) && !empty($rlt['time_to_publish']) && mb_substr($rlt['time_to_publish'], 0, 1, 'UTF-8')!='-')?$rlt['minutes_to_publish']:'';

            $timestamp = strtotime($rlt['createtime']);
            $rlt['time'] = date('H:i', $timestamp);
            $rlt['metro'] = empty($rlt['metro'])?'':$rlt['metro'];
            $rlt['photos_amount'] = 0;
            if($rlt['photo']!=''){				
                $photo = explode(',', $rlt['photo']);
                $rlt['photo'] = $photo[0];
                $rlt['photos_amount'] = count($photo);
            }
            if (strcmp($rlt['year'], '0000')==0)
                $rlt['year'] = '';
            $rlt['fast'] = 0;
            $rlt['wheel'] = 0;
            $rlt['torg'] = 0;
            if ((int)$rlt['run']==0)
                $rlt['run'] = "";
            if (strcmp(mb_convert_encoding($rlt['marka'], 'utf-8'), mb_convert_encoding('Лада', 'utf-8'))==0)
                $rlt['marka'] = 'ВАЗ (Lada)';
            if (!empty($rlt['info'])){
                $info_arr = mb_split("\s", $rlt['info']);
              //  $info_arr = explode(' ', $rlt['info']);
                foreach ($info_arr as $w_num => $word) {
                    // Срочная продажа
                    if(mb_strpos(mb_strtolower($word),'срочн'))
                        $rlt['fast'] = 1;
                    // Колесо
                    if(mb_strpos(mb_strtolower($word),'резина'))
                        $rlt['wheel'] = 1;
                    if(mb_strpos(mb_strtolower($word),'колеса'))
                        $rlt['wheel'] = 1;
                    if(mb_strpos(mb_strtolower($word),'колёса'))
                        $rlt['wheel'] = 1;
                    if(mb_strpos(mb_strtolower($word),'покрышки'))
                        $rlt['wheel'] = 1;
                    // Торг
                    if (mb_strtolower($word)=='торг')
                        $rlt['torg'] = 1;
                    if (mb_strtolower(mb_eregi_replace('[!?.\)]','',$word))=='торг')
                        $rlt['torg'] = 1;
                }
            }
            if (!empty($rlt['enginevol'])){
                $rlt['enginevol'] = number_format((float)$rlt['enginevol'], 1, '.', '');
            }

            foreach ($bodys_list as $number => $type){
                if (strcmp(mb_ucfirst($rlt['body']), mb_ucfirst($type->name))==0){
                    if (!empty($type->img_name)){
                        $path = '/resources/img/'.$type->img_name;
                        $rlt['body_img'] = '<img class="body-img" src="'.$path.'" />';
                    } else {
                        $rlt['body_img'] = !empty($type->name)?$type->name:'';
                    }
                    break;
                } else
                    $rlt['body_img'] = "";
            }
            if ($rlt['body']=='null')
                $rlt['body'] = '';

            $rlt['phone_find'] = (int)$rlt['phone_find'];
            $rlt['model_2'] = !empty($rlt['model_2'])?$rlt['model_2']:'';


            if(strpos($rlt['info'],'резина')!==false || strpos($rlt['info'],'колеса')!==false){
                $rlt['tires'] = 1;//'Резина/Колеса';
            }else{
                $rlt['tires'] = '';
            }	
        /*    $color = $sql->query("SELECT `hex` FROM `colors` WHERE `name`='".trim($rlt['color']," \xC2\xA0")."'");
            $color_arr = $color->fetch_array(MYSQLI_ASSOC);
            if (!empty($color_arr)){
                $rlt['color'] = '<div class="color-preview" style="background-color: #'.$color_arr['hex'].';"></div>';
                $rlt['color_hex'] = $color_arr['hex'];
            } else {
                $rlt['color'] = '';
                $rlt['color_hex'] = '';
            }*/
            $result[] = $rlt;
        }
	
        return $this->render('main', [
                    'last_list' => $last_list,
                    'lastonline' => $result
                ]);
    }
    
    public function actionExit()
    {
        Yii::$app->session->remove('id');
        Yii::$app->session->remove('admin');
        Yii::$app->session->remove('session_key');
        header( 'Location: /', true, 302 );
        exit();
    }
    
    public function actionAdd()
    {
        die();
        if (!empty($_POST)){
            $user = new Users();
            $user->login = $_POST['login'];
            $user->hash = password_hash($_POST['password'], PASSWORD_BCRYPT);
            $user->balance = 0;
            $user->search_photos = 1;
            $user->admin = !empty($_POST['admin'])?1:0;
            $user->state = 1;
            $user->ip = ip2long($_SERVER['REMOTE_ADDR']);
            if (!$user->save())
                print_r($user->errors);
        }
        $users = Users::find()->all();
        return $this->render('addusers',['users' => $users]);
    }
}
