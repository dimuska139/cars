<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\filters\VerbFilter;
use \app\models\Cars;
use \app\models\Users;
use \app\models\Body;
use \app\models\Proxy;




class AdminController extends GlobalController
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'foreColor'=>0x66534F,
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }
    
    function statusesCmp($a, $b)
    {
        
    }

    public function actionTasks(){
        if (!Yii::$app->session->get('admin')){
            header( 'Location: /', true, 302 );
            exit();
        } else {
            $user = Users::find()
                ->where([
                    'id' => Yii::$app->session->get('id')
                ])->one();
            if (empty($user)/* || strcmp($user->session_key, Yii::$app->session->get('session_key'))!=0*/) {
                header( 'Location: /', true, 302 );
                exit();
            }
        }
        $this->setActivepage('tasks');
        $this->view->registerCssFile('/resources/css/main.css');
        $this->view->registerCssFile('/resources/css/admin-tasks.css');
        $this->view->registerJsFile('/resources/js/jquery.zclip.min.js',['depends'=>'yii\web\JqueryAsset']);
        $this->view->registerJsFile('/resources/js/admin-tasks.js',['depends'=>'yii\web\JqueryAsset']);
        $list = Cars::find()
            ->where([
                'active' => 1
            ])
            ->orderBy(['createdate'=> SORT_DESC])
            ->all();
        $statuses = Cars::getStatuses();
        
        $statuses[1]['user'] = "Размещение";
        unset($statuses[4]);
        uasort($statuses, function($a, $b){
            if ($a['order'] === $b['order'])
                return 0;
            return $a['order'] > $b['order'] ? 1 : -1;
        });
        
        return $this->render('index', [
            'list' => $list,
            'statuses' => $statuses
        ]);
    }
    
    // Добавление -> Активно
    public function actionSave(){
        if (!Yii::$app->session->get('admin')){
            header( 'Location: /', true, 302 );
            exit();
        } else {
            $user = Users::find()
                ->where([
                    'id' => Yii::$app->session->get('id')
                ])->one();
            if (empty($user)/* || strcmp($user->session_key, Yii::$app->session->get('session_key'))!=0*/) {
                header( 'Location: /', true, 302 );
                exit();
            }
        }
        $id = $_GET['id'];
        $car = Cars::findOne([
            'id' => $id,
            'status' => 1
        ]);
        $car->status = 0;
        if ($car->save()){
            header( 'Location: /admin/tasks', true, 302 );
            exit();
        } else {
            print_r($car->errors);
            die();
        }
    }
    // Редактирование -> активно
    public function actionEdit(){
        if (!Yii::$app->session->get('admin')){
            header( 'Location: /', true, 302 );
            exit();
        } else {
            $user = Users::find()
                ->where([
                    'id' => Yii::$app->session->get('id')
                ])->one();
            if (empty($user)/* || strcmp($user->session_key, Yii::$app->session->get('session_key'))!=0*/) {
                header( 'Location: /', true, 302 );
                exit();
            }
        }
        $id = $_GET['id'];
        $car = Cars::findOne([
            'id' => $id,
            'status' => 3
        ]);
        $car->status = 0;
        if ($car->save()){
            header( 'Location: /admin/tasks', true, 302 );
            exit();
        } else {
            print_r($car->errors);
            die();
        }
    }
    // Удаление -> удалено
    public function actionDelete(){
        if (!Yii::$app->session->get('admin')){
            header( 'Location: /', true, 302 );
            exit();
        } else {
            $user = Users::find()
                ->where([
                    'id' => Yii::$app->session->get('id')
                ])->one();
            if (empty($user)/* || strcmp($user->session_key, Yii::$app->session->get('session_key'))!=0*/) {
                header( 'Location: /', true, 302 );
                exit();
            }
        }
        $id = $_GET['id'];
        $car = Cars::findOne([
            'id' => $id,
            'status' => 2
        ]);
        $car->status = 5;
        if ($car->save()){
            header( 'Location: /admin/tasks', true, 302 );
            exit();
        } else {
            print_r($car->errors);
            die();
        }
    }
    
    public function actionUsers(){
        if (!Yii::$app->session->get('admin')){
            header( 'Location: /', true, 302 );
            exit();
        } else {
            $user = Users::find()
                ->where([
                    'id' => Yii::$app->session->get('id')
                ])->one();
            if (empty($user)/* || strcmp($user->session_key, Yii::$app->session->get('session_key'))!=0*/) {
                header( 'Location: /', true, 302 );
                exit();
            }
        }
        $this->setActivepage('users');
        $this->view->registerCssFile('/resources/css/main.css');
        $this->view->registerCssFile('/resources/css/admin-users.css');
        $users_list = Users::find()->orderBy(['createdate'=> SORT_DESC])->all();
        return $this->render('users', [
            'users_list' => $users_list
        ]);
    }
    
    public function actionCreateuser(){
        if (!Yii::$app->session->get('admin')){
            header( 'Location: /', true, 302 );
            exit();
        } else {
            $user = Users::find()
                ->where([
                    'id' => Yii::$app->session->get('id')
                ])->one();
            if (empty($user)/* || strcmp($user->session_key, Yii::$app->session->get('session_key'))!=0*/) {
                header( 'Location: /', true, 302 );
                exit();
            }
        }
        $this->setActivepage('users');
        $this->view->registerCssFile('/resources/css/main.css');
        $this->view->registerCssFile('/resources/css/admin-users.css');
        $this->view->registerJsFile('/resources/js/admin-users.js',['depends'=>'yii\web\JqueryAsset']);
        if (!empty($_POST)){
            $user = new Users();
            $user->login = $_POST['login'];
            $user->name = $_POST['name'];
            $user->hash = password_hash($_POST['password'], PASSWORD_BCRYPT);
            $user->balance = $_POST['balance'];
            $user->admin = !empty($_POST['admin'])?1:0;
            $user->state = 1;
            $user->description = !empty($_POST['description'])?$_POST['description']:'';
            $user->ip = ip2long($_SERVER['REMOTE_ADDR']);
            if ($user->save()){
                header( 'Location: /admin/users', true, 302 );
                exit();
            }
        } else
            return $this->render('createuser');
    }
    
    public function actionEdituser(){
        if (!Yii::$app->session->get('admin')){
            header( 'Location: /', true, 302 );
            exit();
        } else {
            $user = Users::find()
                ->where([
                    'id' => Yii::$app->session->get('id')
                ])->one();
            if (empty($user)/* || strcmp($user->session_key, Yii::$app->session->get('session_key'))!=0*/) {
                header( 'Location: /', true, 302 );
                exit();
            }
        }
        $this->setActivepage('users');
        $this->view->registerCssFile('/resources/css/main.css');
        $this->view->registerCssFile('/resources/css/admin-users.css');
        $this->view->registerJsFile('/resources/js/admin-users.js',['depends'=>'yii\web\JqueryAsset']);
        $id = (int)$_REQUEST['id'];
        $user = Users::find()
        ->where([
            'id' => $id
        ])->one();
        if (!empty($_POST)){
            $user->login = $_POST['login'];
            $user->name = $_POST['name'];
        //    $user->hash = password_hash($_POST['password'], PASSWORD_BCRYPT);
            $user->balance = $_POST['balance'];
            $user->admin = !empty($_POST['admin'])?1:0;
            $user->state = 1;
            $user->description = !empty($_POST['description'])?$_POST['description']:'';
            $user->ip = ip2long($_SERVER['REMOTE_ADDR']);
            if ($user->save()){
                header( 'Location: /admin/users', true, 302 );
                exit();
            }
        } else
            return $this->render('edituser',[
                'user' => $user
            ]);
    }
    
    public function actionChangeuserpassword(){
        if (!Yii::$app->session->get('admin')){
            header( 'Location: /', true, 302 );
            exit();
        } else {
            $user = Users::find()
                ->where([
                    'id' => Yii::$app->session->get('id')
                ])->one();
            if (empty($user)/* || strcmp($user->session_key, Yii::$app->session->get('session_key'))!=0*/) {
                header( 'Location: /', true, 302 );
                exit();
            }
        }
        $this->setActivepage('users');
        $id = (int)$_REQUEST['id'];
        $user = Users::find()
        ->where([
            'id' => $id
        ])->one();
        if (!empty($_POST)){
            $user->hash = password_hash($_POST['password'], PASSWORD_BCRYPT);
            if ($user->save()){
                header( 'Location: /admin/users', true, 302 );
                exit();
            }
        } else
            return $this->render('edituser');
    }
    
    public function actionProxy(){
        if (!Yii::$app->session->get('admin')){
            header( 'Location: /', true, 302 );
            exit();
        } else {
            $user = Users::find()
                ->where([
                    'id' => Yii::$app->session->get('id')
                ])->one();
            if (empty($user)/* || strcmp($user->session_key, Yii::$app->session->get('session_key'))!=0*/) {
                header( 'Location: /', true, 302 );
                exit();
            }
        }
        $this->setActivepage('proxy');
        $proxy = Proxy::find()->orderBy(['createdate'=> SORT_DESC])->all();
        $unusedAmount = Proxy::find()->where([
            'confirmed_auto' => 0,
            'confirmed_avito' => 0
        ])->count();
        $this->view->registerCssFile('/resources/css/main.css');
        $this->view->registerCssFile('/resources/css/admin-proxy.css');
        $this->view->registerJsFile('/resources/js/jquery.zclip.min.js',['depends'=>'yii\web\JqueryAsset']);
        $this->view->registerJsFile('/resources/js/admin-proxy.js',['depends'=>'yii\web\JqueryAsset']);
        return $this->render('proxy', [
            'proxy_list' => $proxy,
            'unused_amount' => $unusedAmount,
        ]);
    }
    
    public function actionCreateproxy(){
        if (!Yii::$app->session->get('admin')){
            header( 'Location: /', true, 302 );
            exit();
        } else {
            $user = Users::find()
                ->where([
                    'id' => Yii::$app->session->get('id')
                ])->one();
            if (empty($user)/* || strcmp($user->session_key, Yii::$app->session->get('session_key'))!=0*/) {
                header( 'Location: /', true, 302 );
                exit();
            }
        }
        $this->setActivepage('proxy');
        $this->view->registerCssFile('/resources/css/main.css');
        $this->view->registerCssFile('/resources/css/admin-proxy.css');
        $this->view->registerJsFile('/resources/js/admin-proxy.js',['depends'=>'yii\web\JqueryAsset']);
        if (!empty($_POST)){
            $proxy = new Proxy();
            $proxy->proxy = $_POST['proxy'];
            $proxy->proxy_login = $_POST['login'];
            $proxy->proxy_pass = $_POST['pass'];
            $proxy->http_port = $_POST['http_port'];
            $proxy->socks5_port = $_POST['socks5_port'];
            $proxy->user_id = 0;
            $proxy->status = 1;
            if ($proxy->save()){
                header( 'Location: /admin/proxy', true, 302 );
                exit();
            } else {
                var_dump($proxy->getErrors());
                die();
            }
        } else
            return $this->render('createproxy');
    }
}
