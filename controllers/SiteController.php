<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\filters\VerbFilter;
use app\models\Cars;
use app\models\Brands;
use app\models\Colors;
use app\models\Subwaystations;
use app\models\Cities;
use app\models\Photos;
use app\models\Models;
use app\models\Users;
use app\models\Ads;
use app\models\Body;
use app\models\Payment;
use app\models\Operations;
use app\models\Sources;
use app\models\Proxy;
use app\models\Phones;

class SiteController extends GlobalController
{
    public function actionPhpinfo(){
        die();
        phpinfo();
        die();
    }
    
    private function mb_ucfirst($string, $encoding='UTF-8')
    {
        $firstChar = mb_substr($string, 0, 1, $encoding);
        $then = mb_substr($string, 1, mb_strlen($string, $encoding)-1, $encoding);
        return mb_strtoupper($firstChar, $encoding) . $then;
    }
    
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'foreColor'=>0x66534F,
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }
    
    public function actionClearsearchfilters()
    {
        SetCookie("filter","");
        header( 'Location: /online', true, 302 );
        exit();
    }
    
    public function actionDeleted()
    {
        if (empty(Yii::$app->session->get('id'))){
            header( 'Location: /', true, 302 );
            exit();
        } else {
            $user = Users::find()
                ->where([
                    'id' => Yii::$app->session->get('id')
                ])->one();
            if (empty($user)/* || strcmp($user->session_key, Yii::$app->session->get('session_key'))!=0*/) {
                header( 'Location: /', true, 302 );
                exit();
            }
        }
        if (!empty(Yii::$app->session->get('admin'))){
            header( 'Location: /admin/tasks', true, 302 );
            exit();
        }
        $this->view->registerCssFile('/resources/css/main.css');
        $this->view->registerCssFile('/resources/css/list.css');
        $this->view->registerJsFile('/resources/js/delete.js',['depends'=>'yii\web\JqueryAsset']);
        
        
        $active = Cars::find()
                ->where([
                    'status' => 0,
                    'active' => 1,
                    'owner' => Yii::$app->session->get('id')
                ]);
        $countQuery = clone $active;
        $activeCount = $countQuery->count();
       
        $moderation = Cars::find()
                ->where([
                    'status' => [1,2,3,4],
                    'active' => 1,
                    'owner' => Yii::$app->session->get('id')
                ]);
        $countQuery = clone $moderation;
        $moderationCount = $countQuery->count();
        
        
        $ended = Cars::find()
                ->where([
                    'status' => [5,6],
                    'active' => 1,
                    'owner' => Yii::$app->session->get('id')
                ]);
        $countQuery = clone $ended;
        $endedCount = $countQuery->count();
        if ($endedCount==0){
            header( 'Location: /active', true, 302 );
            exit();
        }
        $ended_list = $ended->all();
                
        
        
    //    $this->view->registerJsFile('/resources/js/index.js',['depends'=>'yii\web\JqueryAsset']);
    //    $this->view->params['brands'] = Brands::find()->orderBy(['name'=> SORT_ASC])->all();
        $this->view->params['off_format_detection'] = true;
        $this->setActivepage('main');
        return $this->render('deleted', [
            'active_count' => $activeCount,
            'moderation_count' => $moderationCount,
            'ended' => $ended_list,
            'ended_count' => $endedCount,
            'body_types' => Body::getPublicBodys(),
            'engine_types' => Cars::getEnginetypes(),
            'transmission_types' => Cars::getTransmissiontypes(),
            'drive_types' => Cars::getDrivetypes(),
            'statuses' => Cars::getStatuses(),
            'months' => parent::monthsShort()
        ]);
    }
    
    public function actionModeration(){
        if (empty(Yii::$app->session->get('id'))){
            header( 'Location: /', true, 302 );
            exit();
        } else {
            $user = Users::find()
                ->where([
                    'id' => Yii::$app->session->get('id')
                ])->one();
            if (empty($user)/* || strcmp($user->session_key, Yii::$app->session->get('session_key'))!=0*/) {
                header( 'Location: /', true, 302 );
                exit();
            }
        }
        if (!empty(Yii::$app->session->get('admin'))){
            header( 'Location: /admin/tasks', true, 302 );
            exit();
        }
        $this->view->registerCssFile('/resources/css/main.css');
        $this->view->registerCssFile('/resources/css/list.css');
        $this->view->registerJsFile('/resources/js/index.js',['depends'=>'yii\web\JqueryAsset']);
        $active = Cars::find()
                ->where([
                    'status' => 0,
                    'active' => 1,
                    'owner' => Yii::$app->session->get('id')
                ]);
        $countQuery = clone $active;
        $activeCount = $countQuery->count();
        $active_list = $active->all();
        $moderation = Cars::find()
                ->where([
                    'status' => [1,2,3,4],
                    'active' => 1,
                    'owner' => Yii::$app->session->get('id')
                ]);
        $countQuery = clone $moderation;
        $moderationCount = $countQuery->count();
        if ($moderationCount==0){
            header( 'Location: /active', true, 302 );
            exit();
        }
        $moderation_list = $moderation->all();
        
        $ended = Cars::find()
                ->where([
                    'status' => [5,6],
                    'active' => 1,
                    'owner' => Yii::$app->session->get('id')
                ]);
        $countQuery = clone $ended;
        $endedCount = $countQuery->count();
                
        
        
    //    $this->view->registerJsFile('/resources/js/index.js',['depends'=>'yii\web\JqueryAsset']);
    //    $this->view->params['brands'] = Brands::find()->orderBy(['name'=> SORT_ASC])->all();
        $this->view->params['off_format_detection'] = true;
        $this->setActivepage('main');
        return $this->render('moderation', [
            'active_count' => $activeCount,
            'moderation' => $moderation_list,
            'moderation_count' => $moderationCount,
            'ended_count' => $endedCount,
            'body_types' => Body::getPublicBodys(),
            'engine_types' => Cars::getEnginetypes(),
            'transmission_types' => Cars::getTransmissiontypes(),
            'drive_types' => Cars::getDrivetypes(),
            'statuses' => Cars::getStatuses(),
            'months' => parent::monthsShort()
        ]);
    }
    
    public function actionActive(){
        if (empty(Yii::$app->session->get('id'))){
            header( 'Location: /', true, 302 );
            exit();
        } else {
            $user = Users::find()
                ->where([
                    'id' => Yii::$app->session->get('id')
                ])->one();
            if (empty($user)/* || strcmp($user->session_key, Yii::$app->session->get('session_key'))!=0*/) {
                header( 'Location: /', true, 302 );
                exit();
            }
        }
        if (!empty(Yii::$app->session->get('admin'))){
            header( 'Location: /admin/tasks', true, 302 );
            exit();
        }
        $this->view->registerCssFile('/resources/css/main.css');
        $this->view->registerCssFile('/resources/css/list.css');
        $this->view->registerJsFile('/resources/js/index.js',['depends'=>'yii\web\JqueryAsset']);
        
        
        $active = Cars::find()
                ->where([
                    'status' => 0,
                    'active' => 1,
                    'owner' => Yii::$app->session->get('id')
                ]);
        $countQuery = clone $active;
        $activeCount = $countQuery->count();
        $active_list = $active->all();
        $moderation = Cars::find()
                ->where([
                    'status' => [1,2,3,4],
                    'active' => 1,
                    'owner' => Yii::$app->session->get('id')
                ]);
        $countQuery = clone $moderation;
        $moderationCount = $countQuery->count();
        $moderation_list = $moderation->all();
        
        $ended = Cars::find()
                ->where([
                    'status' => [5,6],
                    'active' => 1,
                    'owner' => Yii::$app->session->get('id')
                ]);
        $countQuery = clone $ended;
        $endedCount = $countQuery->count();
                
        
        
    //    $this->view->registerJsFile('/resources/js/index.js',['depends'=>'yii\web\JqueryAsset']);
    //    $this->view->params['brands'] = Brands::find()->orderBy(['name'=> SORT_ASC])->all();
        $this->view->params['off_format_detection'] = true;
        $this->setActivepage('main');
        return $this->render('index', [
            'active' => $active_list,
            'active_count' => $activeCount,
            'moderation_count' => $moderationCount,
            'ended_count' => $endedCount,
            'body_types' => Body::getPublicBodys(),
            'engine_types' => Cars::getEnginetypes(),
            'transmission_types' => Cars::getTransmissiontypes(),
            'drive_types' => Cars::getDrivetypes(),
            'statuses' => Cars::getStatuses(),
            'months' => parent::monthsShort()
        ]);
    }

    public function actionIndex()
    {
        header( 'Location: /active', true, 302 );
        exit();
    }
    
    // Удаление объявления
    public function actionDelete(){
        if (empty(Yii::$app->session->get('id')) || empty($_REQUEST['id'])){
            header( 'Location: /', true, 302 );
            exit();
        } else {
            $user = Users::find()
                ->where([
                    'id' => Yii::$app->session->get('id')
                ])->one();
            if (empty($user)/* || strcmp($user->session_key, Yii::$app->session->get('session_key'))!=0*/) {
                header( 'Location: /', true, 302 );
                exit();
            }
        }
        $ad_id = (INT)$_REQUEST['id'];
        $old_car = Cars::findOne([
            'id' => $ad_id,
            'active' => 1,
            'status' => 0
        ]);
        $car = new Cars();
        $car->model_id = $old_car->model_id;
        $car->year = $old_car->year;
        $car->body_id = $old_car->body_id;
        $car->engine_type = $old_car->engine_type;
        $car->engine_volume = $old_car->engine_volume;
        $car->engine_power = $old_car->engine_power;
        $car->transmission_type = $old_car->transmission_type;
        $car->drive_type = $old_car->drive_type;
        $car->color_id = $old_car->color_id;
        $car->rudder = $old_car->rudder;
        $car->condition = $old_car->condition;
        $car->run = $old_car->run;
        $car->ad_type = $old_car->ad_type;
        $car->status = 3;
        $car->price = $old_car->price;
        $car->description = $old_car->description;
        $car->inspection_place = $old_car->inspection_place;
        $car->contact = $old_car->contact;
        $car->phone = $old_car->phone;
        $car->owner = $old_car->owner;
        $car->active = 1;
        if ($car->save()){
            $old_car->active = 0;
            if ($old_car->save()){
                $photos = Photos::findAll([
                    'car_id' => $old_car->id
                ]);
                foreach($photos as $photo){
                    $new_photo = new Photos();
                    $new_photo->url = $photo->url;
                    $new_photo->car_id = $car->id;
                    $new_photo->main = $photo->main;
                    $new_photo->owner = $photo->owner;
                    $new_photo->save();
                }
                header( 'Location: /', true, 302 );
                exit();
            }
        }
    }
    
    // Редактирование объявления
    public function actionEdit()
    {
        if (empty(Yii::$app->session->get('id')) || empty($_REQUEST['id'])){
            header( 'Location: /', true, 302 );
            exit();
        } else {
            $user = Users::find()
                ->where([
                    'id' => Yii::$app->session->get('id')
                ])->one();
            if (empty($user)/* || strcmp($user->session_key, Yii::$app->session->get('session_key'))!=0*/) {
                header( 'Location: /', true, 302 );
                exit();
            }
        }
   //     $this->setActivepage('main');
        $this->view->registerCssFile('/resources/css/main.css');
        $this->view->registerCssFile('/resources/css/edit.css');
        $this->view->registerJsFile('/resources/js/jquery.ui.widget.js',['depends'=>'yii\web\JqueryAsset']);
        $this->view->registerJsFile('/resources/js/jquery.iframe-transport.js',['depends'=>'yii\web\JqueryAsset']);
        $this->view->registerJsFile('/resources/js/jquery.fileupload.js',['depends'=>'yii\web\JqueryAsset']);
        $this->view->registerJsFile('/resources/js/edit.js',['depends'=>'yii\web\JqueryAsset']);
        $ad_id = (INT)$_REQUEST['id'];
        $old_car = Cars::findOne([
            'id' => $ad_id,
            'active' => 1,
            'status' => 0
        ]);
        if (empty($_POST)){
            $active = Cars::find()
                ->where([
                    'status' => 0,
                    'active' => 1,
                    'owner' => Yii::$app->session->get('id')
                ]);
            $countQuery = clone $active;
            $activeCount = $countQuery->count();

            $moderation = Cars::find()
                    ->where([
                        'status' => [1,2,3,4],
                        'active' => 1,
                        'owner' => Yii::$app->session->get('id')
                    ]);
            $countQuery = clone $moderation;
            $moderationCount = $countQuery->count();


            $ended = Cars::find()
                    ->where([
                        'status' => [5,6],
                        'active' => 1,
                        'owner' => Yii::$app->session->get('id')
                    ]);
            $countQuery = clone $ended;
            $endedCount = $countQuery->count();
            return $this->render('edit',[
                'active_count' => $activeCount,
                'moderation_count' => $moderationCount,
                'ended_count' => $endedCount,
                'old_car' => $old_car,
                'brands' => Brands::find()->orderBy(['name'=> SORT_ASC])->all(),
                'body_types' => Body::getPublicBodys(),
                'engine_types' => Cars::getEnginetypes(),
                'transmission_types' => Cars::getTransmissiontypes(),
                'drive_types' => Cars::getDrivetypes(),
                'colors' => Colors::find()->where(['published' => 1])->orderBy(['id'=> SORT_ASC])->all(),
                'rudders' => Cars::getRudders(),
                'conditions' => Cars::getConditions(),
                'subways' => Subwaystations::find()->where([
                    'city_id' => $old_car->city_id
                ])->orderBy(['name'=> SORT_ASC])->all(),
                'adtypes' => Cars::getAdtypes()
            ]);
        } else {
            $car = new Cars();
            $car->model_id = $old_car->model_id;
            $car->year = $_POST['year'];
            $car->body_id = $_POST['body_type'];
            $car->engine_type = $_POST['engine_type'];
            $car->engine_volume = $_POST['engine_volume'];
            $car->engine_power = $_POST['engine_power'];
            $car->transmission_type = $_POST['transmission_type'];
            $car->drive_type = $_POST['drive_type'];
            $car->color_id = $_POST['color'];
            $car->rudder = $_POST['rudder'];
            $car->condition = $_POST['condition'];
            $car->run = (int)str_replace(" ","",$_POST['run']);
            $car->ad_type = $old_car->ad_type;
            $car->status = 1;
            $car->price = (int)str_replace(" ","",$_POST['price']);
            $car->description = !empty($_POST['comment'])?$_POST['comment']:'';
            $car->inspection_place = $old_car->inspection_place;
            $car->contact = $old_car->contact;
            $car->phone = $old_car->phone;
            $car->owner = $old_car->owner;
            $car->active = 1;
            if ($car->save()){
                $old_car->active = 0;
                if ($old_car->save()){
                    // Если пользователь удалил или загрузил какие-либо фотографии
                    $main_photo = false;
                    if (!empty($_POST['photos_id'])){
                        $ids = [];
                        foreach ($_POST['photos_id'] as $photo_id){
                            $ids[] = (int)$photo_id;
                        }
                        $photos = Photos::find()
                                ->where(['id' => $ids])->all();
                        foreach($photos as $n => $photo){
                            if (!$main_photo)
                                $main_photo = $photo->url;
                            $photo->car_id = $car->id;
                            $photo->save();
                        }
                    }
                    $model = Models::findOne(['id' => $old_car->model_id]);
                    $model_name = $model->name;
                    $brand = Brands::findOne(['id' => $model->parent_id]);
                    $brand_name = $brand->name;
                    $brandmodel = $brand_name.' '.$model_name;
                    return $this->render('updated',[
                        'brandmodel' => $brandmodel,
                        'main_photo' => $main_photo,
                        'phone' => $old_car->phone
                    ]);
                }
            } else {
                print_r($car->errors);
                die;
            }
        }
    }
    // Редирект из поиска
    public function actionAdsredirect()
    {
        if (empty(Yii::$app->session->get('id'))){
            header( 'Location: /', true, 302 );
            exit();
        } else {
            $user = Users::find()
                ->where([
                    'id' => Yii::$app->session->get('id')
                ])->one();
            if (empty($user)/* || strcmp($user->session_key, Yii::$app->session->get('session_key'))!=0*/) {
                header( 'Location: /', true, 302 );
                exit();
            }
        }
        $id = (int)$_GET['id'];
        
        $rec = Ads::findOne([
            'Id'=>$id
        ]);
        if (!empty($rec) && !empty($rec->url)){
            $current_user = Users::findOne([
                'id' => Yii::$app->session->get('id')
            ]);
            $op = Operations::findOne([
                'user_id' => Yii::$app->session->get('id'),
                'bulletin_id' => $id,
                'type' => 'ads'
            ]);
            if (empty($op) && $current_user->balance<(int)Yii::$app->params['ads_price']){
                header( 'Location: /balance', true, 302 );
                die;
            } else {
                if (empty($op)){
                    $current_user->balance = (int)$current_user->balance-(int)Yii::$app->params['ads_price'];
                    $current_user->save();
                    $o = new Operations();
                    $o->user_id = Yii::$app->session->get('id');
                    $o->bulletin_id = $id;
                    $o->balance = $current_user->balance;
                    $o->type = "ads";
                    $o->price = (-1)*(int)Yii::$app->params['ads_price'];
                    $o->status = "completed";
                    $o->save();
                }
                header( 'Location: '.$rec->url, true, 302 );
                die;
            }
        }
    }
    // Создание объявления
    public function actionAdd()
    {
        if (empty(Yii::$app->session->get('id'))){
            header( 'Location: /', true, 302 );
            exit();
        } else {
            $user = Users::find()
                ->where([
                    'id' => Yii::$app->session->get('id')
                ])->one();
            if (empty($user)/* || strcmp($user->session_key, Yii::$app->session->get('session_key'))!=0*/) {
                header( 'Location: /', true, 302 );
                exit();
            }
        }
        $current_user = Users::findOne([
            'id' => Yii::$app->session->get('id')
        ]);
        
  //      $this->setActivepage('main');
        $this->view->registerCssFile('/resources/css/main.css');
        $this->view->registerCssFile('/resources/css/create.css');
        $this->view->registerJsFile('/resources/js/jquery.ui.widget.js',['depends'=>'yii\web\JqueryAsset']);
        $this->view->registerJsFile('/resources/js/jquery.iframe-transport.js',['depends'=>'yii\web\JqueryAsset']);
        $this->view->registerJsFile('/resources/js/jquery.fileupload.js',['depends'=>'yii\web\JqueryAsset']);
        $this->view->registerJsFile('/resources/js/accounting.min.js',['depends'=>'yii\web\JqueryAsset']);
    //    $this->view->registerJsFile('/resources/js/jquery.maskedinput.min.js',['depends'=>'yii\web\JqueryAsset']);
        $this->view->registerJsFile('/resources/js/create.js',['depends'=>'yii\web\JqueryAsset']);
        $this->setActivepage('main');
        if (empty($_POST)){
            $current_user = Users::findOne([
                'id' => Yii::$app->session->get('id')
            ]);
            $enough_balance = $current_user->balance>Yii::$app->params['price']?true:false;
            // Вдруг есть подтверждённый частично или полностью номер телефона
            $addedPhone = Phones::find()
                ->where("((auto_code IS NULL AND avito_code IS NOT NULL) OR (avito_code IS NULL AND auto_code IS NOT NULL)) AND status=1 AND user_id=".Yii::$app->session->get('id'))
                ->one();
            
            $active = Cars::find()
                ->where([
                    'status' => 0,
                    'active' => 1,
                    'owner' => Yii::$app->session->get('id')
                ]);
            $countQuery = clone $active;
            $activeCount = $countQuery->count();

            $moderation = Cars::find()
                    ->where([
                        'status' => [1,2,3,4],
                        'active' => 1,
                        'owner' => Yii::$app->session->get('id')
                    ]);
            $countQuery = clone $moderation;
            $moderationCount = $countQuery->count();


            $ended = Cars::find()
                    ->where([
                        'status' => [5,6],
                        'active' => 1,
                        'owner' => Yii::$app->session->get('id')
                    ]);
            $countQuery = clone $ended;
            $endedCount = $countQuery->count();
            
            
            return $this->render('create',[
                'active_count' => $activeCount,
                'moderation_count' => $moderationCount,
                'ended_count' => $endedCount,
                'enough_balance' => $enough_balance,
                'brands' => Brands::find()->where(['published' => 1])->orderBy(['name'=> SORT_ASC])->all(),
                'body_types' => Body::getPublicBodys(),
                'engine_types' => Cars::getEnginetypes(),
                'transmission_types' => Cars::getTransmissiontypes(),
                'drive_types' => Cars::getDrivetypes(),
                'colors' => Colors::find()->where(['published' => 1])->orderBy(['id'=> SORT_ASC])->all(),
                'rudders' => Cars::getRudders(),
                'conditions' => Cars::getConditions(),
                'cities' => Cities::find()->orderBy(['name'=> SORT_ASC])->all(),
                'added_phone' => $addedPhone,
                'subways' => Subwaystations::find()->where([
                    "city_id" => 1
                ])->orderBy(['name'=> SORT_ASC])->all(),
                'adtypes' => Cars::getAdtypes()
            ]);
        } else {
            $enough_balance = $current_user->balance>Yii::$app->params['price']?true:false;
            if ($current_user->balance<Yii::$app->params['price']){
                header( 'Location: /balance', true, 302 );
                exit();
            }
            $car = new Cars();
            if ($_POST['model']!=-1)
                $car->model_id = $_POST['model'];
            else {
                $exists_model = Models::findOne([
                    'name' => $_POST['another_model'],
                    'parent_id' => $_POST['brand']
                ]);
                if (!empty($exists_model))
                    $car->model_id = $exists_model->id;
                else {
                    $mdl = new Models();
                    $mdl->name = $_POST['another_model'];
                    $mdl->parent_id = $_POST['brand'];
                    $mdl->published = 0;
                    if ($mdl->save())
                        $car->model_id = $mdl->id;
                    else {
                        print_r($mdl->errors);
                        die();
                    }
                }
            }
            $car->year = $_POST['year'];
            $car->body_id = $_POST['body_type'];
            $car->engine_type = $_POST['engine_type'];
            $car->engine_volume = $_POST['engine_volume'];
            $car->engine_power = $_POST['engine_power'];
            $car->transmission_type = $_POST['transmission_type'];
            $car->drive_type = $_POST['drive_type'];
            $car->color_id = $_POST['color'];
            $car->rudder = $_POST['rudder'];
            $car->condition = $_POST['condition'];
            $car->run = (int)str_replace(" ","",$_POST['run']);
            $car->ad_type = $_POST['ad_type'];
            $car->status = 1;
            $car->price = (int)str_replace(" ","",$_POST['price']);
            $car->description = !empty($_POST['comment'])?$_POST['comment']:"";
            $car->inspection_place = $_POST['inspection-place'];
            $car->contact = $_POST['contact'];
            $car->phone = $_POST['phone'];
            $car->owner = Yii::$app->session->get('id');
            $car->phone_id = $_POST['phone_id'];
            $car->active = 1;
            if ($car->save()){
                if (!empty($_POST['photos_id'])){
                    $ids = [];
                    foreach ($_POST['photos_id'] as $photo_id){
                        $ids[] = (int)$photo_id;
                    }
                    $photos = Photos::find()
                        ->where(['id' => $ids])->all();
                    foreach($photos as $n => $photo){
                        $photo->car_id = $car->id;
                        $photo->save();
                    }
                }
                // Списываем средства
                $owner = Users::findOne([
                    'id' => $car->owner
                ]);
                $owner->balance -= (int)Yii::$app->params['price'];
                if ($owner->save()){
                    // Запись в историю
                    $o = new Operations();
                    $o->user_id = Yii::$app->session->get('id');
                    $o->bulletin_id = $car->id;
                    $o->balance = $owner->balance;
                    $o->type = "creation";
                    $o->price = (-1)*(int)Yii::$app->params['price'];
                    $o->status = "completed";
                    $o->save();
                    //header( 'Location: /created?id='.$car->id, true, 302 );
                    
                }
                
                // Собираем данные для отправки email
                        
                $model = Models::findOne([
                    'id' => $_POST['model']
                ]);
                
                $brand = Brands::findOne([
                    'id' => $model->parent_id
                ]);
                
                $body = Body::findOne([
                    'id' => $_POST['body_type']
                ]);
                        
                $color = Colors::findOne([
                    'id' => $_POST['color']
                ]);
                
                $engine_type = Cars::getEnginetypes()[$_POST['engine_type']];
                $transmission_type = Cars::getTransmissiontypes()[$_POST['transmission_type']];
                
                $drive_type = Cars::getDrivetypes()[$_POST['drive_type']];
                $rudder = Cars::getRudders()[$_POST['rudder']];
                $condition = Cars::getConditions()[$_POST['condition']];
                $subway = Subwaystations::findOne([
                    'id' => $_POST['subway']
                ]);
                
                $city = Cities::findOne([
                    'id' => $_POST['city']
                ]);
                
                $photos = Photos::find()
                    ->where(['car_id' => $car->id])
                    ->all();
                
                $phone = Phones::findOne([
                    'id' => $_POST['phone_id']
                ]);
                $email_html = \Yii::$app->view->renderFile('@app/views/site/email_bulletin.php', [
                    'car' => $car,
                    'brand' => $brand,
                    'model' => $model,
                    'condition' => $condition,
                    'body' => $body,
                    'color' => $color,
                    'engine_type' => $engine_type,
                    'transmission_type' => $transmission_type,
                    'drive_type' => $drive_type,
                    'rudder' => $rudder,
                    'photos' => $photos,
                    'city' => $city,
                    'subway' => $subway,
                    'phone' => $phone
                ]);
                $emails = Yii::$app->params['adminEmails'];
                foreach ($emails as $email) {
                    Yii::$app->mailer->compose()
                        ->setFrom(Yii::$app->params['notificationEmail'])
                        ->setTo($email)
                        ->setSubject('Размещение '.$brand->name.' '.$model->name.' '.$car->year)
                        ->setHtmlBody($email_html)
                        ->send();
                }
                header( 'Location: /moderation', true, 302 );
                exit();
            } else {
                print_r($car->errors);
                die;
            }
        }
    }
    
    // Создание объявления
    public function actionCreated()
    {
  //      $this->setActivepage('main');
        $car_id = $_GET['id'];
        $car = Cars::findOne([
                    'id' => $car_id
                ]);
        $model = Models::findOne(['id' => $car->model_id]);
        $model_name = $model->name;
        $brand = Brands::findOne(['id' => $model->parent_id]);
        $brand_name = $brand->name;
        $brandmodel = $brand_name.' '.$model_name;
        $main_photo = false;
        $photo = Photos::findOne(['car_id' => $car_id]);
        if ($photo)
            $main_photo = !empty($photo->thumb)?$photo->thumb:$photo->url;
        $this->view->registerCssFile('/resources/css/main.css');
        $this->view->registerCssFile('/resources/css/create.css');
        return $this->render('created',[
            'brandmodel' => $brandmodel,
            'main_photo' => $main_photo,
            'ad_id' => $car_id, // номер объявления
            'phone' => $car->phone
        ]);
    }
    
    // Редирект на QIWI-оплату
    public function actionQiwiredirect(){
        if (empty(Yii::$app->session->get('id'))){
            header( 'Location: /', true, 302 );
            exit();
        } else {
            $user = Users::find()
                ->where([
                    'id' => Yii::$app->session->get('id')
                ])->one();
            if (empty($user)/* || strcmp($user->session_key, Yii::$app->session->get('session_key'))!=0*/) {
                header( 'Location: /', true, 302 );
                exit();
            }
        }
        $sum = 0;
        if (empty($_POST['sum']) || !is_numeric($_POST['sum']))
            die();
        $sum = $_POST['sum'];
        $current_user = Users::findOne([
            'id' => Yii::$app->session->get('id')
        ]);
        $p = new Payment();
        $p->user_id = Yii::$app->session->get('id');
        $p->sum = $_POST['sum'];
        $p->completed = 0;
        $p->active = 1;
        if (!$p->save()) die();
        $o = new Operations();
        $o->user_id = Yii::$app->session->get('id');
        $o->bulletin_id = 0;
        $o->type = "payment";
        $o->price = $_POST['sum'];
        $o->status = "created";
        $o->balance = $current_user->balance;
        $o->save();
        $txn_id = $p->id;
        $url = Yii::$app->params['QIWI_CREATE_URL']."&comm=".urlencode("Пополнение баланса ").$current_user->login."&txn_id=".$txn_id.(is_numeric($current_user->login)?"&to=".$current_user->login:"")."&summ=".(int)$_POST['sum']."&lifetime=".Yii::$app->params['QIWI_LIFETIME'];
        header( 'Location: '.$url, true, 302 );
        exit();    
    }
    
    
    public function actionBalance()
    {
        if (empty(Yii::$app->session->get('id'))){
            header( 'Location: /', true, 302 );
            exit();
        } else {
            $user = Users::find()
                ->where([
                    'id' => Yii::$app->session->get('id')
                ])->one();
            if (empty($user)/* || strcmp($user->session_key, Yii::$app->session->get('session_key'))!=0*/) {
                header( 'Location: /', true, 302 );
                exit();
            }
        }
        $current_user = Users::findOne([
            'id' => Yii::$app->session->get('id')
        ]);
        $this->setActivepage('balance');
        $this->view->registerCssFile('/resources/css/main.css');
        $this->view->registerCssFile('/resources/css/balance.css');
        $this->view->registerJsFile('/resources/js/balance.js',['depends'=>'yii\web\JqueryAsset']);
        $op = Operations::find()
                ->where("user_id = :userid AND (type='payment' OR type='creation')", [':userid' => Yii::$app->session->get('id')])
                ->orderBy(['createdate'=> SORT_DESC])
                ->all();

        $statuses = [
            'completed' => '<span style="color: green">Исполнено</span>',
            'created' => 'В обработке',
            'notperformed' => '<span style="color: red">Не исполнено</span>',
        ];
        
        return $this->render('balance', [
            'name' => $current_user->name,
            'operations' => $op,
            'op_names' => Operations::getNames(),
            'statuses' => $statuses
        ]);
    }
    // Страница поиска "Онлайн"
    public function actionSearch()
    {
        if (empty(Yii::$app->session->get('id'))){
            header( 'Location: /', true, 302 );
            exit();
        } else {
            $user = Users::find()
                ->where([
                    'id' => Yii::$app->session->get('id')
                ])->one();
            if (empty($user)/* || strcmp($user->session_key, Yii::$app->session->get('session_key'))!=0*/) {
                header( 'Location: /', true, 302 );
                exit();
            }
        }
        
        $limit=100;
	define("SQL_USER", Yii::$app->params['SQL_USER']);
	define("SQL_PASSWORD", Yii::$app->params['SQL_PASSWORD']);
	define("SQL_BASE", Yii::$app->params['SQL_BASE']);
	define("SQL_SERVER", Yii::$app->params['SQL_SERVER']);
	
	$sql=new \mysqli(SQL_SERVER, SQL_USER, SQL_PASSWORD, SQL_BASE);
	$sql->set_charset("utf8");
        $this->view->registerCssFile('/resources/css/main.css');
        $this->view->registerCssFile('/resources/css/animate.css');
        $this->view->registerCssFile('/resources/css/monitor.css');
        $this->view->registerCssFile('/resources/css/multiple-select.css');
        $this->view->registerCssFile('/resources/css/md-preloader.min.css');
        $this->view->registerJsFile('/resources/js/accounting.min.js',['depends'=>'yii\web\JqueryAsset']);
        $this->view->registerJsFile('/resources/js/jquery.multiple.select.js', ['depends'=>'yii\web\JqueryAsset']);
        $this->view->registerJsFile('/resources/js/monitor.js', ['depends'=>'yii\web\JqueryAsset']);
        
        $current_user = Users::findOne([
            'id' => Yii::$app->session->get('id')
        ]);
        $enough_balance = $current_user->balance>=Yii::$app->params['ads_price']?true:false;
        $show_photos = (int)$current_user->search_photos;
        $sounds = (int)$current_user->search_sounds;

        $history_amount = Operations::find()
            ->where("type='ads' AND user_id=".Yii::$app->session->get('id'))
            ->count();
        $sources = Sources::find()
            ->where("published=1")
            ->orderBy(['source'=> SORT_ASC])
            ->all();
        $brands = Brands::find()
            ->where("published=1")
            ->orderBy(['name'=> SORT_ASC])
            ->all();
        $this->setActivepage('online');
        return $this->render('search',[
            'user' => $current_user,
            'brands' => $brands,
            'sources' => $sources,
            'sql' => $sql,
            'show_photos' => $show_photos,
            'sounds' => $sounds,
            'enough_balance' => $enough_balance,
            'history_amount' => $history_amount
        ]);
    }
    // За месяц
    public function actionAds(){
        if (empty(Yii::$app->session->get('id'))){
            header( 'Location: /', true, 302 );
            exit();
        } else {
            $user = Users::find()
                ->where([
                    'id' => Yii::$app->session->get('id')
                ])->one();
            if (empty($user)/* || strcmp($user->session_key, Yii::$app->session->get('session_key'))!=0*/) {
                header( 'Location: /', true, 302 );
                exit();
            }
        }
        $limit=100;
	define("SQL_USER", Yii::$app->params['SQL_USER']);
	define("SQL_PASSWORD", Yii::$app->params['SQL_PASSWORD']);
	define("SQL_BASE", Yii::$app->params['SQL_BASE']);
	define("SQL_SERVER", Yii::$app->params['SQL_SERVER']);
	
	$sql=new \mysqli(SQL_SERVER, SQL_USER, SQL_PASSWORD, SQL_BASE);
	$sql->set_charset("utf8");
        $this->view->registerCssFile('/resources/css/main.css');
        $this->view->registerCssFile('/resources/css/animate.css');
        $this->view->registerCssFile('/resources/css/monitor.css');
        $this->view->registerCssFile('/resources/css/multiple-select.css');
        $this->view->registerCssFile('/resources/css/md-preloader.min.css');
        $this->view->registerJsFile('/resources/js/accounting.min.js',['depends'=>'yii\web\JqueryAsset']);
        $this->view->registerJsFile('/resources/js/jquery.multiple.select.js', ['depends'=>'yii\web\JqueryAsset']);
        $this->view->registerJsFile('/resources/js/month.js', ['depends'=>'yii\web\JqueryAsset']);
        $current_user = Users::findOne([
                'id' => Yii::$app->session->get('id')
            ]);
        $enough_balance = $current_user->balance>=Yii::$app->params['ads_price']?true:false;
        $show_photos = (int)$current_user->search_photos;
        $sounds = (int)$current_user->search_sounds;

        $history_amount = Operations::find()
                ->where("type='ads' AND user_id=".Yii::$app->session->get('id'))
                ->count();
        $sources = Sources::find()
            ->where("published=1")
            ->orderBy(['source'=> SORT_ASC])
            ->all();
        $brands = Brands::find()
            ->where("published=1")
            ->orderBy(['name'=> SORT_ASC])
            ->all();
        $current_user = Users::findOne([
            'id' => Yii::$app->session->get('id')
        ]);
        $this->setActivepage('online');
        return $this->render('ads',[
            'user' => $current_user,
            'brands' => $brands,
            'sources' => $sources,
            'sql' => $sql,
            'show_photos' => $show_photos,
            'sounds' => $sounds,
            'enough_balance' => $enough_balance,
            'history_amount' => $history_amount
        ]);
    }
    
   
    // История переходов
    public function actionClicks(){
        if (empty(Yii::$app->session->get('id'))){
            header( 'Location: /', true, 302 );
            exit();
        } else {
            $user = Users::find()
                ->where([
                    'id' => Yii::$app->session->get('id')
                ])->one();
            if (empty($user)/* || strcmp($user->session_key, Yii::$app->session->get('session_key'))!=0*/) {
                header( 'Location: /', true, 302 );
                exit();
            }
        }
        // Получаем историю переходов 1 страница
        $history = Operations::find()
            ->where("type='ads' AND user_id=".Yii::$app->session->get('id'))
            ->limit(25)
            ->orderBy(['createdate'=> SORT_DESC])
            ->all();
        // Если нет истории, но юзер как-то сюда попал (например, зная url), то отправляем его на страницу "онлайн"
        if (count($history) == 0){
            header( 'Location: /online', true, 302 );
            exit();
        }
        // Получаем полную историю переходов
        $history_all = Operations::find()
            ->where("type='ads' AND user_id=".Yii::$app->session->get('id'))
            ->orderBy(['createdate'=> SORT_DESC])
            ->all();
        
        $history_ids = [];
        $history_dates = [];
        foreach ($history as $item) {
            $history_ids[] = $item->bulletin_id;
            $history_dates[] = $item->createdate;
        }
        $history_all_ids = [];
        foreach ($history_all as $item) {
            $history_all_ids[] = $item->bulletin_id;
        }
        define("SQL_USER",     Yii::$app->params['SQL_USER']);
	define("SQL_PASSWORD", Yii::$app->params['SQL_PASSWORD']);
	define("SQL_BASE",     Yii::$app->params['SQL_BASE']);
	define("SQL_SERVER",   Yii::$app->params['SQL_SERVER']);
	
	$sql=new \mysqli(SQL_SERVER, SQL_USER, SQL_PASSWORD, SQL_BASE);
	$sql->set_charset("utf8");
	
	$where=array();
        if (!empty($history_ids)){
            $where[] = "ads.`Id` IN (".implode(",", $history_ids).")";
        }
        $where_all=array();
        if (!empty($history_all_ids)){
            $where_all[] = "ads.`Id` IN (".implode(",", $history_all_ids).")";
        }
        
	$fields=' ads.`Id`,
                br.name AS `marka`,
                br.img_name AS `marka_logo`,
                `model`,
                `model_2`,
                `price`,
                `average_price`,
                MINUTE(TIMEDIFF((`moderation_time`+INTERVAL 30 MINUTE), NOW())) AS minutes_to_publish,
                TIMEDIFF((`moderation_time`+INTERVAL 30 MINUTE), NOW()) AS time_to_publish,
                ads.body_id,
                b.name AS `body`,
                `engine`,
                `enginevol`,
                `transmission`,
                `condition`,
                `run`,
                c.name AS `color`,
                c.metallic AS `metallic`,
                c.dark,
                `rudder`,
                `phone`,
                `fio`,
                `photo`,
                `metro`,
                `address`,
                `region`,
                `url`,
                `phone_find`,
                `source`,
                `info`,
                `year`,
                `createtime`,
                `vin`';
	
            $sql_str = "select ".$fields."
                        from ads LEFT JOIN `brands` br ON br.id=`brand_id` LEFT JOIN `body` b ON b.id=`body_id` LEFT JOIN `colors` c ON c.id=ads.color_id WHERE ".(count($where)?implode(" and ", $where):'')." order by ads.Id";
        $rows = $sql->query($sql_str);
        $sql_count = "SELECT COUNT(*) as amount".
        " from ads LEFT JOIN `brands` br ON br.id=`brand_id` LEFT JOIN `body` b ON b.id=`body_id` LEFT JOIN `colors` c ON c.id=ads.color_id WHERE ".(count($where_all)?implode(" and ", $where_all):'')." order by ads.Id";
        
        $rows_count = $sql->query($sql_count);
        $results = [];
        while($rlt = $rows->fetch_array(MYSQLI_ASSOC)){
            $results[] = $rlt;
        }
        
        $amount = 0;
        while($rlt = $rows_count->fetch_array(MYSQLI_ASSOC)){
            $amount = $rlt['amount'];
            break;
        }
        $sorted = [];
        for ($j=0;$j<count($history_ids);$j++){
            for($i=0;$i<count($results);$i++){
                if ($results[$i]['Id']==$history_ids[$j]){
                    $results[$i]['operation_time'] = $history_dates[$j];
                    $sorted[] = $results[$i];
                    break;
                }
            }
        }
        
        
	$result = array(); 
	$ret = array();
        $bds = Body::find()->all();
        foreach ($sorted as $rlt){
            $rlt['difference_price'] = floor((int)$rlt['average_price']-(int)$rlt['price'])/1000;

            $rlt['minutes_to_publish'] = (!empty($rlt['minutes_to_publish']) && !empty($rlt['time_to_publish']) && mb_substr($rlt['time_to_publish'], 0, 1, 'UTF-8')!='-')?$rlt['minutes_to_publish']:'';

            $rlt['time'] = $rlt['operation_time'];
         /*   if ($rlt['time']=='00:00')
                $rlt['time'] = '';*/

            $rlt['metro'] = empty($rlt['metro'])?'':$rlt['metro'];
            $rlt['photos_amount'] = 0;
            if($rlt['photo']!=''){				
                $photo=explode(',', $rlt['photo']);
                $rlt['photo'] = $photo[0];
                $rlt['photos_amount'] = count($photo);
            }
            if (strcmp($rlt['year'], '0000')==0)
                    $rlt['year'] = '';
            $rlt['fast'] = 0;
            $rlt['wheel'] = 0;
            $rlt['torg'] = 0;
            if ((int)$rlt['run']==0)
                    $rlt['run'] = "";
            if (!empty($rlt['info'])){
                $info_arr = mb_split("\s", $rlt['info']);
         //       $info_arr = explode(' ', $rlt['info']);
                foreach ($info_arr as $w_num => $word) {
                    // Срочная продажа
                    if(mb_strpos(mb_strtolower($word),'срочн'))
                        $rlt['fast'] = 1;
                    // Колесо
                    if(mb_strpos(mb_strtolower($word),'резина'))
                        $rlt['wheel'] = 1;
                    if(mb_strpos(mb_strtolower($word),'колеса'))
                        $rlt['wheel'] = 1;
                    if(mb_strpos(mb_strtolower($word),'колёса'))
                        $rlt['wheel'] = 1;
                    if(mb_strpos(mb_strtolower($word),'покрышки'))
                        $rlt['wheel'] = 1;
                    // Торг
                    if (mb_strtolower($word)=='торг')
                        $rlt['torg'] = 1;
                    if (mb_strtolower(mb_eregi_replace('[!?.\)]','',$word))=='торг')
                        $rlt['torg'] = 1;
                }
            }

            if (!empty($rlt['enginevol'])){
                $rlt['enginevol'] = number_format((float)$rlt['enginevol'], 1, '.', '');
            }
        //    if (!empty($rlt['body_id'])){
                foreach ($bds as $number => $type){
                    if (strcmp($this->mb_ucfirst($rlt['body']), $this->mb_ucfirst($type->name))==0){
                        if (!empty($type->img_name)){
                            $path = '/resources/img/'.$type->img_name;
                            $rlt['body_img'] = '<img class="body-img" src="'.$path.'" />';
                        } else {
                            $rlt['body_img'] = !empty($type->name)?$type->name:'';
                        }
                        break;
                    } else
                        $rlt['body_img'] = "";
                }
                if ($rlt['body']=='null')
                        $rlt['body'] = '';
       /*     } else 
                $rlt['body_img'] = "";*/

            $ads_h = Operations::findOne([
                'user_id' => Yii::$app->session->get('id'),
                'bulletin_id' => $rlt['Id'],
                'type' => 'ads'
            ]);

            if (empty($ads_h))
                $rlt['phone'] = null;


            $rlt['phone_find'] = (int)$rlt['phone_find'];
            $rlt['model_2'] = !empty($rlt['model_2'])?$rlt['model_2']:'';


            if(strpos($rlt['info'],'резина')!==false||strpos($rlt['info'],'колеса')!==false){
                $rlt['tires']=1;//'Резина/Колеса';
            }else{
                $rlt['tires']='';
            }	
            $color = $sql->query("SELECT `hex` FROM `colors` WHERE `name`='".trim($rlt['color']," \xC2\xA0")."'");
            $color_arr = $color->fetch_array(MYSQLI_ASSOC);
            if (!empty($color_arr)){
                $rlt['color'] = '<div class="color-preview" style="background-color: #'.$color_arr['hex'].';"></div>';
            } else {
                $rlt['color'] = '';
            }
            $result[] = $rlt;
        }
        $history = $result;

        $current_user = Users::findOne([
                'id' => Yii::$app->session->get('id')
            ]);
        $enough_balance = $current_user->balance>=Yii::$app->params['ads_price']?true:false;
        $this->view->registerCssFile('/resources/css/main.css');
        $this->view->registerCssFile('/resources/css/monitor.css');
        $this->view->registerJsFile('/resources/js/clickshistory.js', ['depends'=>'yii\web\JqueryAsset']);
        $this->view->registerCssFile('/resources/css/md-preloader.min.css');
        $this->setActivepage('online');
        return $this->render('clickshistory', [
            'enough_balance' => $enough_balance,
            'history' => $history,
            'bodys'   => Body::find()->all(),
            'amount'  => $amount
        ]);
    }
    
    public function actionSearchbyphone()
    {
        if (empty(Yii::$app->session->get('id'))){
            header( 'Location: /', true, 302 );
            exit();
        } else {
            $user = Users::find()
                ->where([
                    'id' => Yii::$app->session->get('id')
                ])->one();
            if (empty($user)/* || strcmp($user->session_key, Yii::$app->session->get('session_key'))!=0*/) {
                header( 'Location: /', true, 302 );
                exit();
            }
        }
        if (empty($_REQUEST['phone']) && empty($_REQUEST['id'])){
            header( 'Location: /search', true, 302 );
            exit();
        }
        if (!empty($_REQUEST['phone']))
            $phone = $_REQUEST['phone'];
        else {
            $ads = Ads::findOne([
                'id' => $_REQUEST['id']
            ]);
            $phone = $ads->phone;
        }
        $current_user = Users::findOne([
            'id' => Yii::$app->session->get('id')
        ]);
        if ($current_user->balance<Yii::$app->params['ads_price']){
            header( 'Location: /balance', true, 302 );
            exit();
        }
        $data_json = file_get_contents("http://crwl.ru/api/rest/latest/search/?phone=".$phone."&api_key=b28fc651b8d409fa0034a331b0b18554");
        $info = json_decode($data_json, true);
        $this->view->registerCssFile('/resources/css/main.css');
        $this->view->registerCssFile('/resources/css/animate.css');
        $this->view->registerCssFile('/resources/css/monitor.css');
        $this->view->registerCssFile('/resources/css/multiple-select.css');
        $this->view->registerJsFile('/resources/js/accounting.min.js',['depends'=>'yii\web\JqueryAsset']);
        $this->view->registerJsFile('/resources/js/jquery.multiple.select.js', ['depends'=>'yii\web\JqueryAsset']);
        $this->view->registerJsFile('/resources/js/monitor.js', ['depends'=>'yii\web\JqueryAsset']);
      //  $enough_balance = $current_user->balance>Yii::$app->params['phonesearch_price']?true:false;
        $show_photos = (int)$current_user->search_photos;
        $sounds = (int)$current_user->search_sounds;
        $this->setActivepage('online');
     /*   $current_user->balance = (int)$current_user->balance - (int)Yii::$app->params['phonesearch_price'];
        $current_user->save();*/
        // Запись в историю
     /*   $o = new Operations();
        $o->user_id = Yii::$app->session->get('id');
        $o->bulletin_id = 0;
        $o->type = "searchbyphone";
        $o->balance = $current_user->balance;
        $o->price = (-1)*(int)Yii::$app->params['phonesearch_price'];
        $o->status = "completed";
        $o->save();*/
        return $this->render('searchbyphone', [
            'info' => $info,
            'phone' => $phone,
      //      'enough_balance' => $enough_balance
        ]);
    }
    
    public function actionSms(){
        if (empty(Yii::$app->session->get('id'))){
            header( 'Location: /', true, 302 );
            exit();
        } else {
            $user = Users::find()
                ->where([
                    'id' => Yii::$app->session->get('id')
                ])->one();
            if (empty($user)/* || strcmp($user->session_key, Yii::$app->session->get('session_key'))!=0*/) {
                header( 'Location: /', true, 302 );
                exit();
            }
        }
        $this->view->registerCssFile('/resources/css/main.css');
        $this->view->registerCssFile('/resources/css/sms.css');
        $this->setActivepage('sms');
        return $this->render('sms');
    }
    
    public function actionCheck(){
        if (empty(Yii::$app->session->get('id'))){
            header( 'Location: /', true, 302 );
            exit();
        } else {
            $user = Users::find()
                ->where([
                    'id' => Yii::$app->session->get('id')
                ])->one();
            if (empty($user)/* || strcmp($user->session_key, Yii::$app->session->get('session_key'))!=0*/) {
                header( 'Location: /', true, 302 );
                exit();
            }
        }
        $this->view->registerCssFile('/resources/css/main.css');
        $this->view->registerCssFile('/resources/css/check.css');
        $this->setActivepage('check');
        return $this->render('check');
    }
    
    public function actionGetphones(){
        define("SQL_USER",     Yii::$app->params['SQL_USER']);
	define("SQL_PASSWORD", Yii::$app->params['SQL_PASSWORD']);
	define("SQL_BASE",     Yii::$app->params['SQL_BASE']);
	define("SQL_SERVER",   Yii::$app->params['SQL_SERVER']);
	$sql=new \mysqli(SQL_SERVER, SQL_USER, SQL_PASSWORD, SQL_BASE);
	$sql->set_charset("utf8");
        $sql_str = "SELECT DISTINCT `phone` FROM `ads` WHERE region_Id IN (1,2) AND phone_find>=5 AND phone_find<=30";
        $rows = $sql->query($sql_str);
        $file = './phones.csv';
        while($item = $rows->fetch_array(MYSQLI_ASSOC)) {
            file_put_contents($file, $item['phone']."\n", FILE_APPEND | LOCK_EX);
        }
        die();      
    }
}
