<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\filters\VerbFilter;
use app\models\Brands;
use app\models\Models;
use app\models\Photos;
use app\models\Users;
use app\models\Proxy;
use app\models\Phones;
use app\models\Cars;
use app\models\Ads;
use app\models\Operations;
use app\models\Body;
use app\models\Colors;
use app\models\Subwaystations;


function mb_ucfirst($string, $encoding='UTF-8')
{
    $firstChar = mb_substr($string, 0, 1, $encoding);
    $then = mb_substr($string, 1, mb_strlen($string, $encoding)-1, $encoding);
    return mb_strtoupper($firstChar, $encoding) . $then;
}

class AjaxController extends GlobalController
{
    private function getNewPassword()
    {
        // Генерация пароля
        $accepted_chars = "0123456789";
        $password = "";
        for ($i=0;$i<Yii::$app->params['PASSWORD_LENGTH'];$i++)
            $password .= $accepted_chars[rand(0, (strlen($accepted_chars)-1))];
        return $password;
    }
    
    // Системные запросы
    public function actionSystem(){
        $response = [];
        $response['command'] = "none";
        if (!empty(Yii::$app->session->get('id'))){
            $user = Users::find()
                ->where([
                    'id' => Yii::$app->session->get('id')
                ])->one();
            // strcmp($user->session_key, Yii::$app->session->get('session_key'))!=0
            if (empty($user) || $user->password_changed == 1) {
                Yii::$app->session->remove('id');
                Yii::$app->session->remove('admin');
                Yii::$app->session->remove('session_key');
                $response['command'] = "logout";
            }
        }
        echo json_encode($response);
        die;
    }
    
    public function actionChangepassword()
    {
        $response = [];
        $response['success'] = false;
        if (empty($_REQUEST['login'])){
            $response['code'] = 1;
            $response['message'] = "Не указан логин";
            echo json_encode($response);
            die;
        }
        $u = Users::findOne([
            'login' => $_REQUEST['login']
        ]);
        if (empty($u)){
            $response['code'] = 2;
            $response['message'] = "Пользователь не найден!";
            echo json_encode($response);
            die;
        }
        $new_password = $this->getNewPassword();
        $curl = curl_init();
        $message = urlencode($u->name.",\nЛогин: ".$u->login."\nНовый пароль: ".$new_password."\n\nAM97.RU");
        curl_setopt($curl, CURLOPT_URL, 'http://smsc.ru/sys/send.php?charset=utf-8&login='.Yii::$app->params['SMSCRU_LOGIN'].'&psw='.Yii::$app->params['SMSCRU_PASSWORD'].'&phones='.$u->login.'&mes='.$message);
        curl_setopt($curl, CURLOPT_POST, false);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        $result = curl_exec($curl);
        if (mb_stripos($result,'OK')!=0){
            curl_close($curl);
            $response['code'] = 3;
            $response['message'] = "Системная ошибка";
            echo json_encode($response);
            die;
        }
        $u->hash = password_hash($new_password, PASSWORD_BCRYPT);
        $u->session_key = parent::getSessionKey(); // Меняем ключ, чтобы все сессии текущего пользователя завершились
        $u->password_changed = 1;
        $u->save();
        $response['success'] = true;
        echo json_encode($response);
        die;
    }
    
    public function actionRegistration()
    {
        $response = [];
        $response['success'] = false;
        if (empty($_REQUEST['name'])){
            $response['code'] = 1;
            $response['message'] = "Не указано имя пользователя";
            echo json_encode($response);
            die;
        }
        if (empty($_REQUEST['phone'])){
            $response['code'] = 2;
            $response['message'] = "Не указано номер телефона";
            echo json_encode($response);
            die;
        }
        $u = Users::findOne([
            'login' => $_REQUEST['phone']
        ]);
        
        if (!empty($u)){
            $response['code'] = 3;
            $response['message'] = "Вы уже зарегистрированы в системе";
            echo json_encode($response);
            die;
        }
        $password = $this->getNewPassword();

        $new_user = new Users();
        $new_user->login = $_REQUEST['phone'];
        $new_user->name = $_REQUEST['name'];
        $new_user->hash = password_hash($password, PASSWORD_BCRYPT);
        $new_user->balance = Yii::$app->params['START_BALANCE'];
        $new_user->search_photos = 1;
        $new_user->admin = 0;
        $new_user->state = 1;
        $new_user->ip = ip2long($_SERVER['REMOTE_ADDR']);
        if ($new_user->save()){
            $curl = curl_init();
            $message = urlencode($_REQUEST['name'].",\nЛогин: ".$_REQUEST['phone']."\nПароль: ".$password."\n\nAM97.RU");
            curl_setopt($curl, CURLOPT_URL, 'http://smsc.ru/sys/send.php?charset=utf-8&login='.Yii::$app->params['SMSCRU_LOGIN'].'&psw='.Yii::$app->params['SMSCRU_PASSWORD'].'&phones='.$_REQUEST['phone'].'&mes='.$message);
            curl_setopt($curl, CURLOPT_POST, false);
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
            $result = curl_exec($curl);
            if (mb_strpos($result,'ERROR')){
                curl_close($curl);
                $new_user->delete();
                $response['code'] = 4;
                $response['message'] = "Системная ошибка";
                echo json_encode($response);
                die;
            }
            $res = json_decode($result);
            curl_close($curl);
        } else {
            $response['code'] = 4;
            $response['message'] = "Системная ошибка";
            echo json_encode($response);
            die;
        }
        $response['success'] = true;
        echo json_encode($response);
        die;
    }
    
    public function actionGetlastdemo()
    {
        define("SQL_USER",     Yii::$app->params['SQL_USER']);
	define("SQL_PASSWORD", Yii::$app->params['SQL_PASSWORD']);
	define("SQL_BASE",     Yii::$app->params['SQL_BASE']);
	define("SQL_SERVER",   Yii::$app->params['SQL_SERVER']);
	
        $last_item_id = (int)$_REQUEST['last_id'];
	$sql=new \mysqli(SQL_SERVER, SQL_USER, SQL_PASSWORD, SQL_BASE);
	$sql->set_charset("utf8");
        $fields=' ads.`Id`,
        `model`,
        `model_2`,
        `price`,
        `brand_id`,
        `body_id`,
        `color_id`,
        `average_price`,
        MINUTE(TIMEDIFF((`moderation_time`+INTERVAL 30 MINUTE), NOW())) AS minutes_to_publish,
        TIMEDIFF((`moderation_time`+INTERVAL 30 MINUTE), NOW()) AS time_to_publish,
        `engine`,
        `enginevol`,
        `transmission`,
        `condition`,
        `run`,
        `rudder`,
        `phone`,
        `fio`,
        `photo`,
        `metro`,
        `address`,
        `region`,
        `url`,
        `phone_find`,
        `source`,
        `info`,
        `year`,
        `createtime`,
        `vin`';
        $sql_str = "select ".$fields." from ads order by ads.Id desc LIMIT 0,10"; //Id>".$last_item_id." 
        $rows = $sql->query($sql_str);
        $brands_list = Brands::find()->all();
        $brands_arr = [];
        foreach ($brands_list as $br){
            $brands_arr[$br->id] = $br->toArray();
        }
        
        $bodys_list = Body::find()->all();
        $bodys_arr = [];
        foreach ($bodys_list as $bod){
            $bodys_arr[$bod->id] = $bod->toArray();
        }
        
        $result = array();
        $last_id = 0;
        while($rlt = $rows->fetch_array(MYSQLI_ASSOC)){
            if ($last_id == 0)
                $last_id= $rlt['Id'];
            $rlt['marka'] = $brands_arr[$rlt['brand_id']]['name'];
            $rlt['marka_logo'] = $brands_arr[$rlt['brand_id']]['img_name'];
            $rlt['body'] = !empty($bodys_arr[$rlt['body_id']])?$bodys_arr[$rlt['body_id']]['name']:'';
            $rlt['metallic'] = !empty($colors_arr[$rlt['color_id']])?$colors_arr[$rlt['color_id']]['metallic']:0;
            $rlt['dark'] = !empty($colors_arr[$rlt['color_id']])?$colors_arr[$rlt['color_id']]['dark']:0;

            $rlt['difference_price'] = floor((int)$rlt['average_price']-(int)$rlt['price'])/1000;


            $rlt['minutes_to_publish'] = (!empty($rlt['minutes_to_publish']) && !empty($rlt['time_to_publish']) && mb_substr($rlt['time_to_publish'], 0, 1, 'UTF-8')!='-')?$rlt['minutes_to_publish']:'';

            $timestamp = strtotime($rlt['createtime']);
            $rlt['time'] = date('H:i', $timestamp);
            $rlt['metro'] = empty($rlt['metro'])?'':$rlt['metro'];
            $rlt['photos_amount'] = 0;
            if($rlt['photo']!=''){				
                $photo = explode(',', $rlt['photo']);
                $rlt['photo'] = $photo[0];
                $rlt['photos_amount'] = count($photo);
            }
            if (strcmp($rlt['year'], '0000')==0)
                $rlt['year'] = '';
            $rlt['fast'] = 0;
            $rlt['wheel'] = 0;
            $rlt['torg'] = 0;
            if ((int)$rlt['run']==0)
                $rlt['run'] = "";
            if (strcmp(mb_convert_encoding($rlt['marka'], 'utf-8'), mb_convert_encoding('Лада', 'utf-8'))==0)
                $rlt['marka'] = 'ВАЗ (Lada)';
            if (!empty($rlt['info'])){
                $info_arr = mb_split("\s", $rlt['info']);
              //  $info_arr = explode(' ', $rlt['info']);
                foreach ($info_arr as $w_num => $word) {
                    // Срочная продажа
               //     if(mb_strpos(mb_strtolower($word),'срочн'))
               //         $rlt['fast'] = 1;
                    // Колесо
              /*      if(mb_strpos(mb_strtolower($word),'резина'))
                        $rlt['wheel'] = 1;
                    if(mb_strpos(mb_strtolower($word),'колеса'))
                        $rlt['wheel'] = 1;
                    if(mb_strpos(mb_strtolower($word),'колёса'))
                        $rlt['wheel'] = 1;
                    if(mb_strpos(mb_strtolower($word),'покрышки'))
                        $rlt['wheel'] = 1;*/
                    // Торг
                    if (mb_strtolower($word)=='торг')
                        $rlt['torg'] = 1;
                    if (mb_strtolower(mb_eregi_replace('[!?.\)]','',$word))=='торг')
                        $rlt['torg'] = 1;
                    if (mb_strtolower($word)=='торга')
                        $rlt['torg'] = 0;
                }
            }
            if (!empty($rlt['enginevol'])){
                $rlt['enginevol'] = number_format((float)$rlt['enginevol'], 1, '.', '');
            }

            foreach ($bodys_list as $number => $type){
                if (strcmp(mb_ucfirst($rlt['body']), mb_ucfirst($type->name))==0){
                    if (!empty($type->img_name)){
                        $path = '/resources/img/'.$type->img_name;
                        $rlt['body_img'] = '<img class="body-img" src="'.$path.'" />';
                    } else {
                        $rlt['body_img'] = !empty($type->name)?$type->name:'';
                    }
                    break;
                } else
                    $rlt['body_img'] = "";
            }
            if ($rlt['body']=='null')
                $rlt['body'] = '';
            $rlt['phone_find'] = (int)$rlt['phone_find'];
            $rlt['model_2'] = !empty($rlt['model_2'])?$rlt['model_2']:'';
            if(strpos($rlt['info'],'резина')!==false || strpos($rlt['info'],'колеса')!==false){
                $rlt['tires'] = 1;//'Резина/Колеса';
            }else{
                $rlt['tires'] = '';
            }	
            $result[] = $rlt;
        }
        echo json_encode(['last_id' => $last_id, 'amount' => count($result), 'html' => $this->renderAjax('@app/views/users/ads_list_body.php', [
                    'lastonline' => $result
                ])]);
    }
    
    private function getErrorMessage($code)
    {
        $message = '';
        if (!empty($code)){
            switch($code){
                case 1:
                    $message = 'Ошибки входных данных';
                    break;
                case 3:
                    $message = 'Телефон уже зарегистрирован на одном из сервисов';
                    break;
                case 5:
                    $message = 'Сетевая ошибка';
                    break;
                case 6:
                    $message = 'Не распознана CAPTCHA';
                    break;
                case 7:
                    $message = 'Неверный код подтверждения';
                    break;
                case 8:
                    $message = 'Отсутствуют живые прокси';
                    break;
                case 9:
                    $message = 'Ошибка сервиса';
                    break;
                case 10:
                    $message = 'Неизвестная ошибка';
                    break;
            }
        }
        return $message;
    }
    
  /*  public function actionStartavitoru()
    {
        $name = $_REQUEST['name'];
        $phone = $_REQUEST['phone'];
        $errcode = 8;
        $res = false;
        $counter = 0;
        // Поиск живых прокси
        while ($errcode==8){
            if ($counter == 0){
                $proxy = Proxy::findOne([
                    'confirmed_avito' => 0,
                    'confirmed_auto' => 1,
                    'user_id' => Yii::$app->session->get('id'),
                    'status' => 1
                ]);
            }
            else {
                $proxys = Proxy::find([
                        'confirmed_avito' => 0,
                        'confirmed_auto' => 0,
                        'user_id' => 0,
                        'status' => 1
                ])->all();
                if (empty($proxys)){
                    $response['success'] = false;
                    $response['code'] = -1;
                    $response['message'] = "Не найден прокси";
                    echo json_encode($response);
                    die;
                }
                // Получение массива из id проксей
                $ids = [];
                foreach ($proxys as $proxy){
                        $ids[] = $proxy->id;
                }
                $rand_number = mt_rand(0, count($ids) - 1); 

                $rand_proxy_id = $ids[$rand_number];
                $proxy = Proxy::findOne([
                    'id' => $rand_proxy_id
                ]);
            }
            $request_type = 'start_avitoru';
            $json = '{
                    "request_type": "'.$request_type.'",
                    "username": "'.$name.'",
                    "phone": "'.$phone.'",
                    "proxy_address": "'.$proxy->proxy.'",
                    "proxy_auth": "'.$proxy->proxy_login.':'.$proxy->proxy_pass.'",
                    "http_port": "'.$proxy->http_port.'",
                    "socks5_port": "'.$proxy->socks5_port.'"
            }';
            $curl = curl_init();
            curl_setopt($curl, CURLOPT_URL, 'http://www.am97.ru:5432/autorubot/reg');
            curl_setopt($curl, CURLOPT_TIMEOUT, Yii::$app->params['MAX_REGISTRATION_TIMEOUT']);
            curl_setopt($curl, CURLOPT_POST, 1);
            curl_setopt($curl, CURLOPT_POSTFIELDS, $json);
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
            $json = curl_exec($curl);
            
            if (mb_strpos($json,'nternal')!=false){ // Обработка 500-й ошибки ("nternal" - это Internal server error) 
                $response = [
                    'success' => false,
                    'code' => 500,
                    'message' => "Внутренняя ошибка сервиса",
                    'proxy_id' => 0
                ];
                echo json_encode($response);
                die();
            }
            $res = json_decode($json);
            curl_close($curl);
            if (!empty($res->code))
                $errcode = $res->code;
            else
                $errcode = 0;
            // Если прокси дохлая, помечаем ее статусом = 0;
            if ($errcode==8){
                $proxy = Proxy::findOne([
                    'id' => $rand_proxy_id
                ]);
                $proxy->status = 0;
                $proxy->save();
            }
            $counter++;
        }
        $success = true;
        if (!empty($res->success)){
            $proxy->avito_login = $res->email;
            $proxy->avito_pass = $res->password;
            $proxy->save();
        }
        $message = '';
        if (!empty($res->code)){
            $message = $this->getErrorMessage($res->code);
            $success = false;
        }

        $response = [
            'success' => $success,
            'ses' => !empty($res->session)?$json:0,
            'code' => !empty($res->code)?$res->code:0,
            'message' => $message,
            'proxy_id' => $success?$proxy->id:0
        ];
        echo json_encode($response);
    }*/
    
    private function avitoRegistration($phone){
        $curl = curl_init();
        $crwlRegApiKey = Yii::$app->params['CRWL_REG_API_KEY'];
        curl_setopt($curl, CURLOPT_URL, 'http://crwl.ru/api/rest/latest/reg_avito/?api_key='.$crwlRegApiKey.'&phone='.$phone.'&name='.urlencode("Александр"));
        
        curl_setopt($curl, CURLOPT_TIMEOUT, Yii::$app->params['MAX_REGISTRATION_TIMEOUT']);
        curl_setopt($curl, CURLOPT_POST, false);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        $json = curl_exec($curl);
        curl_close($curl);
        return json_decode($json);
    }
    
    private function autoRegistration($phone){
        $curl = curl_init();
        $crwlRegApiKey = Yii::$app->params['CRWL_REG_API_KEY'];
        curl_setopt($curl, CURLOPT_URL, 'http://crwl.ru/api/rest/latest/reg_auto/?api_key='.$crwlRegApiKey.'&phone='.$phone);
        
        curl_setopt($curl, CURLOPT_TIMEOUT, Yii::$app->params['MAX_REGISTRATION_TIMEOUT']);
        curl_setopt($curl, CURLOPT_POST, false);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        $json = curl_exec($curl);
        curl_close($curl);
        return json_decode($json);
    }
    
    public function actionStartpartial()
    {
        $type = $_REQUEST['type'];
        $phone = substr($_REQUEST['phone'], 2); // Убираем +7
        if ($type == 'avito')
            $result = $this->avitoRegistration($phone);
        else
            $result = $this->autoRegistration($phone);
        $response['success'] = true;
        $response['code'] = 0;
        $response['message'] = "";
        $response['status'] = $result->status;
        $lastPhone = Phones::find()
            ->where([
                'status' => 1,
       //         'user_id' => Yii::$app->session->get('id'),
                'id' => $_REQUEST['phone_id']
            ])
            ->one();
        
        if (strcmp($result->status, 'used')==0){
            $response['success'] = false;
            $response['code'] = 1;
            $response['message'] = "Этот номер телефона уже зарегистрирован";
            $lastPhone->avito_status = $result->status;
            $lastPhone->save();
            echo json_encode($response);
            die();
        }
        if (strcmp($result->status, 'wait')==0){
            $response['success'] = false;
            $response['value'] = !empty($result->value)?$result->value:180;
            $response['code'] = 2;
            $response['message'] = "Пожалуйста, подождите немного и повторите снова";
            if ($type == 'avito')
                $lastPhone->avito_status = $result->status;
            else
                $lastPhone->auto_status = $result->status;
            $lastPhone->save();
            echo json_encode($response);
            die();
        }
        
        if (strcmp($result->status, 'error')==0){
            $response['success'] = false;
            $response['code'] = 3;
            $response['message'] = "Ошибка регистрации";
            if ($type == 'avito')
                $lastPhone->avito_status = $result->status;
            else
                $lastPhone->auto_status = $result->status;
            $lastPhone->save();
            echo json_encode($response);
            die();
        }
        if (strcmp($result->status, 'sms')==0) {
            if ($type == 'avito')
                $lastPhone->avito_status = $result->status;
            else
                $lastPhone->auto_status = $result->status;
            $lastPhone->save();
            echo json_encode($response);
            die();
        }
        $response['success'] = false;
        $response['code'] = 2;
        $response['message'] = "Непредвиденная ошибка";
        echo json_encode($response);
        die();
    }
    
    public function actionCheckconfirmation()
    {
        $response = [
            'success' => true,
            'code' => 0,
            'message' => "",
            'data' => [
                'phone_id' => 0,
                'confirmed_auto' => 0,
                'confirmed_avito' => 0,
                'auto_status' => 0,
                'avito_status' => 0
            ]
        ];
        $response['success'] = true;
        $response['code'] = 0;
        $response['message'] = "";
        $phone = $_REQUEST['phone'];
        $name = $_REQUEST['name'];
        $lastPhone = Phones::find()
            ->where([
                'status' => 1,
            //    'user_id' => Yii::$app->session->get('id'),
                'phone' => (int)$phone
            ])
            ->one();
        if (empty($lastPhone)){
            $newPhone = new Phones();
            $newPhone->phone = (int)$phone;
            $newPhone->name = $name;
            $newPhone->confirmed_auto = 0;
            $newPhone->confirmed_avito = 0;
            $newPhone->user_id = Yii::$app->session->get('id');
            $newPhone->auto_status = 0;
            $newPhone->avito_status = 0;
            $newPhone->status = 1;
            $newPhone->save();
            $response['data']['phone_id'] = $newPhone->id;
        } else {
            $response['data']['phone_id'] = $lastPhone->id;
            $response['data']['confirmed_auto'] = $lastPhone->confirmed_auto;
            $response['data']['confirmed_avito'] = $lastPhone->confirmed_avito;
            $response['data']['auto_status'] = $lastPhone->auto_status;
            $response['data']['avito_status'] = $lastPhone->avito_status;
        }
        echo json_encode($response);
        die();
    }
    
  /*  public function actionStartall()
    {
        $name = $_REQUEST['name'];
        $phone = substr($_REQUEST['phone'], 1);
        $avitoResult = $this->avitoRegistration($phone, $name);
        $autoResult = $this->autoRegistration($phone);
        $response['success'] = true;
        $response['code'] = 0;
        $response['message'] = "";
        if (strcmp($avitoResult->status, 'used')==0 || strcmp($autoResult->status, 'used')==0){
            $response['success'] = false;
            $response['code'] = 1;
            $response['message'] = "Этот номер телефона уже зарегистрирован в одном из сервисов";
            echo json_encode($response);
            die();
        }
        if (strcmp($avitoResult->status, 'wait')==0 || strcmp($autoResult->status, 'wait')==0){
            $response['success'] = false;
            $response['code'] = 1;
            $response['message'] = "Пожалуйста, подождите немного и повторите снова";
            echo json_encode($response);
            die();
        }
        
        if (strcmp($avitoResult->status, 'error')==0 || strcmp($autoResult->status, 'error')==0){
            $response['success'] = false;
            $response['code'] = 1;
            $response['message'] = "Ошибка регистрации";
            echo json_encode($response);
            die();
        }
        if (strcmp($avitoResult->status, 'sms')==0 && strcmp($autoResult->status, 'sms')==0) {
            echo json_encode($response);
            die();
        }
        $response['success'] = false;
        $response['code'] = 2;
        $response['message'] = "Непредвиденная ошибка";
        echo json_encode($response);
        die();
    }*/
    
    public function actionConfirmavitoru()
    {
        $code = $_REQUEST['code'];
        $phone = $_REQUEST['phone'];
        $name = $_REQUEST['name'];
        
        $response['success'] = true;
        $response['code'] = 0;
        $response['phone_id'] = 0;
        $response['message'] = "";
        $response['status'] = "";
        
        $curl = curl_init();
        $crwlRegApiKey = Yii::$app->params['CRWL_REG_API_KEY'];
        curl_setopt($curl, CURLOPT_URL, 'http://crwl.ru/api/rest/latest/code_avito/?api_key='.$crwlRegApiKey.'&phone='.substr($phone, 2).'&code='.$code.'&name='.urlencode("Александр"));
        curl_setopt($curl, CURLOPT_TIMEOUT, Yii::$app->params['MAX_REGISTRATION_TIMEOUT']);
        curl_setopt($curl, CURLOPT_POST, false);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        $json = curl_exec($curl);
        curl_close($curl);
        $result = json_decode($json);             
        $lastPhone = Phones::find()
            ->where([
                'status' => 1,
         //       'user_id' => Yii::$app->session->get('id'),
                'id' => $_REQUEST['phone_id']
            ])
            ->one();
        if ($result == "NULL") {
            $response['success'] = false;
            $response['code'] = 1;
            $response['message'] = "Error: http://crwl.ru/api/rest/latest/code_avito/?api_key=".$crwlRegApiKey."&phone=".substr($phone, 2)."&code=".$code."&name=".urlencode("Александр");
            echo json_encode($response);
            die();
        }
        
        $lastPhone->avito_status = $result->status;
        $response['status'] = $result->status;
        if (strcmp($result->status, 'incorrect')==0) {
            $response['success'] = false;
            $response['code'] = 1;
            $response['message'] = "Неверный код подтверждения";
            $lastPhone->save();
            echo json_encode($response);
            die();
        }
        
        if (strcmp($result->status, 'ok')==0) {    
            $lastPhone->phone = $phone;
            $lastPhone->avito_code = $code;
            $lastPhone->confirmed_avito = 1;
            $lastPhone->save();
            $response['phone_id'] = $lastPhone->id;
            echo json_encode($response);
            die();
        }
        $lastPhone->save();
        $response['success'] = false;
        $response['code'] = 2;
        $response['message'] = "Непредвиденная ошибка";
        echo json_encode($response);
        die();
    }
    
    public function actionConfirmautoru()
    {
        $code = $_REQUEST['code'];
        $phone = $_REQUEST['phone'];
        $name = $_REQUEST['name'];
        
        $response['success'] = true;
        $response['code'] = 0;
        $response['message'] = "";
        $response['phone_id'] = 0;
        $response['status'] = "";
        $curl = curl_init();
        $crwlRegApiKey = Yii::$app->params['CRWL_REG_API_KEY'];
        curl_setopt($curl, CURLOPT_URL, 'http://crwl.ru/api/rest/latest/code_auto/?api_key='.$crwlRegApiKey.'&phone='.substr($phone, 2).'&code='.$code.'&name='.urlencode("Александр"));
        curl_setopt($curl, CURLOPT_TIMEOUT, Yii::$app->params['MAX_REGISTRATION_TIMEOUT']);
        curl_setopt($curl, CURLOPT_POST, false);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        $json = curl_exec($curl);
        curl_close($curl);
        $result = json_decode($json);

     /*   $result->status = 'ok';
        $result->email = 'dimuska139@mail.ru';
        $result->pwd = '12345';*/
        $response['status'] = $result->status;
          
        
        
        $lastPhone = Phones::find()
            ->where([
                'status' => 1,
         //       'user_id' => Yii::$app->session->get('id'),
                'id' => $_REQUEST['phone_id']
            ])
            ->one();
        $lastPhone->auto_status = $result->status;
        if (strcmp($result->status, 'used')==0) {
            $response['success'] = false;
            $response['code'] = 3;
            $response['message'] = "Данный телефон ранее уже был зарегистрирован на Auto.ru!";
            $lastPhone->save();
            echo json_encode($response);
            die();
        }
        
        if (strcmp($result->status, 'incorrect')==0) {
            $response['success'] = false;
            $response['code'] = 1;
            $response['message'] = "Неверный код подтверждения";
            $lastPhone->save();
            echo json_encode($response);
            die();
        }
        if (strcmp($result->status, 'ok')==0) {
            $lastPhone->phone = $phone;
            $lastPhone->auto_code = $code;
            $lastPhone->confirmed_auto = 1;
            $response['phone_id'] = $lastPhone->id;
            $lastPhone->save();
            echo json_encode($response);
            die();
        }
        $lastPhone->save();
        $response['success'] = false;
        $response['code'] = 2;
        $response['message'] = "Непредвиденная ошибка";
        echo json_encode($response);
        die();
    }
    
  /*  public function actionConfirmregistration()
    {
        $proxy_id = $_REQUEST['proxy_id'];
        $type = $_REQUEST['type'];
        $code = $_REQUEST['code'];
        $phone = $_REQUEST['phone'];
        $name = $_REQUEST['name'];
        $ses = json_decode($_REQUEST['ses']);
        $ses = $ses->session;
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, 'http://www.am97.ru:5432/autorubot/reg');
        curl_setopt($curl, CURLOPT_POST, 1);
        if (strcmp($type, 'avito')==0){
            $json = '{
                "request_type": "confirm_avitoru",
                "code": "'.$code.'",
                "session": {
                                    "avitoru": {
                                            "cookies": "'.preg_replace('#[\n\r]+#', '\\n', $ses->avitoru->cookies).'",
                                            "http_port": "'.$ses->avitoru->http_port.'",
                                            "proxy_address": "'.$ses->avitoru->proxy_address.'",
                                            "proxy_auth": "'.$ses->avitoru->proxy_auth.'"
                                    }
				}
            }';
        } else {
            $json = '{
                "request_type": "confirm_autoru",
                "code": "'.$code.'",
                "session": {
					"autoru": {
						"confirm_magic_hidden": "'.$ses->autoru->confirm_magic_hidden.'",
						"cookies": "'.preg_replace('#[\n\r]+#', '\\n', $ses->autoru->cookies).'",
						"http_port": "'.$ses->autoru->http_port.'",
						"proxy_address": "'.$ses->autoru->proxy_address.'",
						"proxy_auth": "'.$ses->autoru->proxy_auth.'"
					},
					"avitoru": {
						"cookies": "'.preg_replace('#[\n\r]+#', '\\n', $ses->avitoru->cookies).'",
						"http_port": "'.$ses->avitoru->http_port.'",
						"proxy_address": "'.$ses->avitoru->proxy_address.'",
						"proxy_auth": "'.$ses->avitoru->proxy_auth.'"
					}
				}
            }';
        }
        curl_setopt($curl, CURLOPT_POSTFIELDS, $json);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        $res = json_decode(curl_exec($curl)); 
        curl_close($curl);
        $message = '';
        if (!empty($res->code))
            $message = $this->getErrorMessage($res->code);
        $proxy = Proxy::findOne($proxy_id);
        if (strcmp($type, 'avito')==0){
            $proxy->confirmed_avito = 1;
            $proxy->phone = $phone;
            $proxy->name = $name;
        } else {
            $proxy->user_id = Yii::$app->session->get('id');
            $proxy->confirmed_auto = 1;
            $proxy->phone = $phone;
            $proxy->name = $name;
        }
        $proxy->save();
        $response = [
            'success' => $res->success,
            'code' => !empty($res->code)?$res->code:0,
            'message' => $message
        ];
        echo json_encode($response);
    }*/
    
    public function actionLoadmorehistory()
    {
        $current_user = Users::findOne([
                'id' => Yii::$app->session->get('id')
            ]);

        // Получаем историю переходов
        $history = Operations::find()
            ->where("type='ads' AND user_id=".Yii::$app->session->get('id'))
            ->orderBy(['createdate'=> SORT_DESC])
            ->offset(((int)$_REQUEST['page']-1)*25)
            ->limit(25)
            ->all();
        // Если нет истории, но юзер как-то сюда попал, то отправляем его на страницу "онлайн"
        if (count($history) == 0){
            header( 'Location: /online', true, 302 );
            exit();
        }
        $history_dates = [];
        $history_ids = [];
        foreach ($history as $item) {
            $history_ids[] = $item->bulletin_id;
            $history_dates[] = $item->createdate;
        }

        
        define("SQL_USER",     Yii::$app->params['SQL_USER']);
	define("SQL_PASSWORD", Yii::$app->params['SQL_PASSWORD']);
	define("SQL_BASE",     Yii::$app->params['SQL_BASE']);
	define("SQL_SERVER",   Yii::$app->params['SQL_SERVER']);
	
	$sql=new \mysqli(SQL_SERVER, SQL_USER, SQL_PASSWORD, SQL_BASE);
	$sql->set_charset("utf8");
	
	$where=array();
	
        if (!empty($history_ids)){
            $where[] = "ads.`Id` IN (".implode(",", $history_ids).")";
        }
        
	$fields=' ads.`Id`,
                br.name AS `marka`,
                br.img_name AS `marka_logo`,
                `model`,
                `model_2`,
                `price`,
                `brand_id`,
                `body_id`,
                `color_id`,
                `average_price`,
                MINUTE(TIMEDIFF((`moderation_time`+INTERVAL 30 MINUTE), NOW())) AS minutes_to_publish,
                TIMEDIFF((`moderation_time`+INTERVAL 30 MINUTE), NOW()) AS time_to_publish,
                b.name AS `body`,
                `engine`,
                `enginevol`,
                `transmission`,
                `condition`,
                `run`,
                c.name AS `color`,
                c.metallic AS `metallic`,
                c.dark,
                `rudder`,
                `phone`,
                `fio`,
                `photo`,
                `metro`,
                `address`,
                `region`,
                `url`,
                `phone_find`,
                `source`,
                `info`,
                `year`,
                `createtime`,
                `vin`';
	
            $sql_str = "select distinct ".$fields."
                        from ads LEFT JOIN `brands` br ON br.id=`brand_id` LEFT JOIN `body` b ON b.id=`body_id` LEFT JOIN `colors` c ON c.id=ads.color_id WHERE ".(count($where)?implode(" and ", $where):'')." order by ads.Id";

        $rows = $sql->query($sql_str);
        	
        $results = [];
        while($rlt = $rows->fetch_array(MYSQLI_ASSOC)){
            $results[] = $rlt;
        }
        
        $sorted = [];
        for ($j=0;$j<count($history_ids);$j++){
            for($i=0;$i<count($results);$i++){
                if ($results[$i]['Id']==$history_ids[$j]){
                    $results[$i]['operation_time'] = $history_dates[$j];
                    $sorted[] = $results[$i];
                    break;
                }
            }
        }

        
	$result = array(); 
	$ret = array();
        $bds = Body::find()->all();
        foreach ($sorted as $rlt){
            $rlt['difference_price'] = floor((int)$rlt['average_price']-(int)$rlt['price'])/1000;

            $rlt['minutes_to_publish'] = (!empty($rlt['minutes_to_publish']) && !empty($rlt['time_to_publish']) && mb_substr($rlt['time_to_publish'], 0, 1, 'UTF-8')!='-')?$rlt['minutes_to_publish']:'';


            $rlt['time'] = $rlt['operation_time'];

            $rlt['metro'] = empty($rlt['metro'])?'':$rlt['metro'];
            $rlt['photos_amount'] = 0;
            if($rlt['photo']!=''){				
                $photo=explode(',', $rlt['photo']);
                $rlt['photo'] = $photo[0];
                $rlt['photos_amount'] = count($photo);
            }
            if (strcmp($rlt['year'], '0000')==0)
                    $rlt['year'] = '';
            $rlt['fast'] = 0;
            $rlt['wheel'] = 0;
            $rlt['torg'] = 0;
            if (strcmp(mb_convert_encoding($rlt['marka'], 'utf-8'), mb_convert_encoding('Лада', 'utf-8'))==0)
                $rlt['marka'] = 'ВАЗ (Lada)';
            if (!empty($rlt['info'])){
             //   $info_arr = explode(' ', $rlt['info']);
                $info_arr = mb_split("\s", $rlt['info']);
                foreach ($info_arr as $w_num => $word) {
                    // Срочная продажа
                //    if(mb_strpos($word,'очн'))
                //        $rlt['fast'] = 1;
                    // Колесо
                /*    if(mb_strpos(mb_strtolower($word),'резина'))
                        $rlt['wheel'] = 1;
                    if(mb_strpos(mb_strtolower($word),'колеса'))
                        $rlt['wheel'] = 1;
                    if(mb_strpos(mb_strtolower($word),'колёса'))
                        $rlt['wheel'] = 1;*/
              //      if(mb_strpos(mb_strtolower($word),'покрышки'))
              //          $rlt['wheel'] = 1;
                    // Торг
                    if (mb_strtolower($word)=='торг')
                        $rlt['torg'] = 1;
                    if (mb_strtolower(mb_eregi_replace('[!?.\)]','',$word))=='торг')
                        $rlt['torg'] = 1;
                }
            }

            if (!empty($rlt['enginevol'])){
                $rlt['enginevol'] = number_format((float)$rlt['enginevol'], 1, '.', '');
            }

            foreach ($bds as $number => $type){
                if (strcmp(mb_ucfirst($rlt['body']), mb_ucfirst($type->name))==0){
                    if (!empty($type->img_name)){
                        $path = '/resources/img/'.$type->img_name;
                        $rlt['body_img'] = '<img class="body-img" src="'.$path.'" />';
                    } else {
                        $rlt['body_img'] = $type->name;
                    }
                    break;
                } else
                    $rlt['body_img'] = "";
            }

            $ads_h = Operations::findOne([
                'user_id' => Yii::$app->session->get('id'),
                'bulletin_id' => $rlt['Id'],
                'type' => 'ads'
            ]);

            if (empty($ads_h))
                $rlt['phone'] = null;


            $rlt['phone_find'] = (int)$rlt['phone_find'];
            $rlt['model_2'] = !empty($rlt['model_2'])?$rlt['model_2']:'';


            if(strpos($rlt['info'],'резина')!==false||strpos($rlt['info'],'колеса')!==false){
                $rlt['tires']=1;//'Резина/Колеса';
            }else{
                $rlt['tires']='';
            }	
            $color = $sql->query("SELECT `hex` FROM `colors` WHERE `name`='".trim($rlt['color']," \xC2\xA0")."'");
            $color_arr = $color->fetch_array(MYSQLI_ASSOC);
            if (!empty($color_arr)){
                $rlt['color'] = '<div class="color-preview" style="background-color: #'.$color_arr['hex'].';"></div>';
            } else {
                $rlt['color'] = '';
            }
            $result[] = $rlt;
        }
        $history = $result;
        echo $this->renderAjax('/site/clickshistory_page',array(
                'history' => $history  
            ));
    }
    
    public function actionSubwaystations()
    {
        $response = [];
        if (empty($_REQUEST['city_id'])){
            $response['success'] = false;
            $response['code'] = 1;
            $response['message'] = "Не указан ID города";
            echo json_encode($response);
            die;
        }
        $subwaystations = Subwaystations::find()->where([
            "city_id" => $_REQUEST['city_id']
        ])->orderBy(['name'=> SORT_ASC])->all();
        $subwaystations_arr = [];
        foreach ($subwaystations as $station){
            $subwaystations_arr[] = [
                "id" => $station->id,
                "name" => $station->name
            ];
        }
        $response['success'] = true;
        $response['subwaystations'] = $subwaystations_arr;
        echo json_encode($response);
        die();
    }
    
    public function actionGetadsphone()
    {
        $response = [];
        if (empty($_REQUEST['ads_id'])){
            $response['success'] = false;
            $response['code'] = 1;
            $response['message'] = "Не указан ID объявления";
            echo json_encode($response);
            die;
        }
        
        $current_user = Users::findOne([
            'id' => Yii::$app->session->get('id')
        ]);
        
        $ads = Ads::findOne([
            'id' => $_REQUEST['ads_id']
        ]);
        
        if (empty($ads)){
            $response['success'] = false;
            $response['code'] = 2;
            $response['message'] = "Не найдено объявление";
            echo json_encode($response);
            die;
        }
        
        if ($current_user->balance<(int)Yii::$app->params['ads_price']){
            $response['success'] = false;
            $response['code'] = 3;
            $response['message'] = "На Вашем счете недостаточно средств";
            echo json_encode($response);
            die;
        }
        
        $op = Operations::findOne([
            'user_id' => Yii::$app->session->get('id'),
            'bulletin_id' => $_REQUEST['ads_id'],
            'type' => 'ads'
        ]);
        
        if (empty($op)){
            $current_user->balance = (int)$current_user->balance-(int)Yii::$app->params['ads_price'];
            $current_user->save();
            $o = new Operations();
            $o->user_id = Yii::$app->session->get('id');
            $o->bulletin_id = $_REQUEST['ads_id'];
            $o->balance = $current_user->balance;
            $o->type = "ads";
            $o->price = (-1)*(int)Yii::$app->params['ads_price'];
            $o->status = "completed";
            $o->save();
        }
        
        $response['success'] = true;
        $response['phone'] = $ads->phone;
        echo json_encode($response);
        die();
    }
    
    public function actionCheckbalance()
    {
        $current_user = Users::findOne([
            'id' => Yii::$app->session->get('id')
        ]);
        
        $response['success'] = true;
        $response['balance'] = number_format($current_user->balance,0,' ',' ');  
        echo json_encode($response);
    }
    
    
    // Поиск по телефону на странице поиска
    public function actionFindbyphone()
    {
        $response = [];
        if (empty($_REQUEST['phone'])){
            $response['success'] = false;
            $response['code'] = 1;
            $response['message'] = "Не указан номер";
            echo json_encode($response);
            die;
        }
        $current_user = Users::findOne([
            'id' => Yii::$app->session->get('id')
        ]);

        
        $data_json = file_get_contents("http://crwl.ru/api/rest/latest/search/?phone=".$_REQUEST['phone']."&api_key=b28fc651b8d409fa0034a331b0b18554");
        print_r($data_json);
        if (empty($data_json)){
            $response['success'] = false;
            $response['code'] = 4;
            $response['message'] = "Объявления отсутствуют";
            echo json_encode($response);
            die;
        }
        $response['success'] = true;
        echo json_encode($response);
    }
    // Страница поиска "Онлайн" - данные таблицы
    public function actionGetsearchresult()
    {
        
        $limit=100;
	define("SQL_USER",     Yii::$app->params['SQL_USER']);
	define("SQL_PASSWORD", Yii::$app->params['SQL_PASSWORD']);
	define("SQL_BASE",     Yii::$app->params['SQL_BASE']);
	define("SQL_SERVER",   Yii::$app->params['SQL_SERVER']);
	
	$sql=new \mysqli(SQL_SERVER, SQL_USER, SQL_PASSWORD, SQL_BASE);
	$sql->set_charset("utf8");
	
	
        $current_user = Users::findOne([
                'id' => Yii::$app->session->get('id')
            ]);
        
	if(isset($_REQUEST['id']))
            $id=(int)$_REQUEST['id'];
				
	$old=false;
	if(isset($_REQUEST['old']))
            $old=((int)$_REQUEST['old']==1?true:false);	
	
	$where=array();
//	if(!empty($_COOKIE['filter'])){
//		$filter=unserialize($_COOKIE['filter']);
		
		if(!empty($_REQUEST['run_from'])){
                    $where[]='`run`>='.(int)$_REQUEST['run_from'];
		}
                
                if(!empty($_REQUEST['run_to'])){
                    $where[]='`run`<='.(int)$_REQUEST['run_to'];
		}
                
                if(!empty($_REQUEST['volume_from'])){
                    $where[]="`enginevol`+0.01>".floatval($_REQUEST['volume_from']);
		}
                if(!empty($_REQUEST['volume_to'])){
                    $where[]="`enginevol`-0.01<".floatval($_REQUEST['volume_to']);
		}
                
          /*      if(!empty($_REQUEST['volume_to'])){
                    $where[]="(`enginevol`+0.00001)<='".floatval($_REQUEST["volume_to"])."'";
		}*/
                
                if(!empty($_REQUEST['price_from'])){
                    $where[]='`price`>='.(int)$_REQUEST['price_from'];
		}
                
                if(!empty($_REQUEST['price_to'])){
                    $where[]='`price`<='.(int)$_REQUEST['price_to'];
		}
		
                if(!empty($_REQUEST['year_from'])){
                    $where[]='`year`>='.(int)$_REQUEST['year_from'];
		}
                if(!empty($_REQUEST['year_to'])){
                    $where[]='`year`<='.(int)$_REQUEST['year_to'];
		}

		
		/*if(isset($filter['phone_find']) && intval($filter['phone_find'])>0){
			$where[]='`phone_find`<'.intval($filter['phone_find']);
		}			
		
		if(isset($filter['wheel']) && intval($filter['wheel'])>0){
			switch($filter['wheel']){
				case 1:
					$where[]="`wheel`='левый'";
					break;
					
				case 2:
					$where[]="`wheel`='правый'";
					break;
			}
		}*/
                
                if(!empty($_REQUEST['security'])){
                    $where[]="`phone_find`=0";
                }
                    
                
                if(!empty($_REQUEST['kpp'])){
                    if (mb_strlen($_REQUEST['kpp'])<=2){
                        $where[] = "`transmission`='".$_REQUEST['kpp']."'";
                    }
                }
                
                if(!empty($_REQUEST['engine'])){
                    $engine = (int)$_REQUEST['engine']==1?'b':'d';
                    $where[] = "`engine`='".$engine."'";
                }
                
                if(!empty($_REQUEST['state'])){
                    if ((int)$_REQUEST['state']==1){
                        $where[]="`condition` in ('Требует ремонта', 'требует ремонта', 'Битый', 'битый', 'Битый или не на ходу')";
                    } else {
                        $where[]="`condition` not in ('Требует ремонта', 'требует ремонта', 'Битый', 'битый', 'Битый или не на ходу')";
                    }
                }
                
                if(!empty($_REQUEST['source'])){
                    $source_arr = json_decode($_REQUEST['source']);
                    // Если есть autoru, то выбираем и trucksautoru
                    if (in_array(1, $source_arr))
                        $source_arr[] = 4;
                    $src = implode(",",$source_arr);
                    if (!empty($source_arr)){
                        $where[]="`source_id` in (".$src.")";
                    }
                }
                
                if(!empty($_REQUEST['bodys'])){
                    $bodys_arr = json_decode($_REQUEST['bodys']);
                    if (!empty($bodys_arr)){
                        $bodys_str = "('".  implode("','", $bodys_arr)."')";
                        $where[] = "`body_id` in ".$bodys_str;
                    }
                }
                
                if (!empty($_REQUEST['marks'])){
                    $marks_arr = json_decode($_REQUEST['marks']);
                    if(count($marks_arr)){
                    /*    $result=$sql->query("select `marka` from `auto` where `Id` in (".implode(",", $marks_arr).")");
                        if($result->num_rows){
                            $marka_arr=array();
                            while($row=$result->fetch_array(MYSQLI_ASSOC)){
                                $marka_arr[]=$row['marka'];
                            }
                            $where[]="br.`name` in ('".implode("','", $marka_arr)."')";
                        }*/
                        $where[]="brand_id in (".implode(",", $marks_arr).")";
                    }
                }
                
                if (!empty($_REQUEST['regions'])){
                    $regions_arr = json_decode($_REQUEST['regions']);
                    if (count($regions_arr)>0)
                        $where[]="`region_Id` in (".implode(",", $regions_arr).")";
                }
                
                if (isset($_REQUEST['phone_amount']) && strlen($_REQUEST['phone_amount'])>0){
                    $where[]="`phone_find`<=".(int)$_REQUEST['phone_amount'];
                /*    if ($_REQUEST['phone_amount'] == 0){
                        $where[]="`phone_find`=0";
                    } else {
                        $where[]="`phone_find`<=".(int)$_REQUEST['phone_amount'];
                    }*/
                }
	/*		
		
		if(isset($filter['body']) && is_array($filter['body'])){
			$body=array();
			foreach($filter['body'] as $body_id){
				$body_id=intval($body_id);
				if($body_id<9999){
					$body[]=$body_id;
				}
			}
			
			if(count($body)>0){
				$result=$sql->query("select `name` from `body` where Id in (".implode(",",$body).")");
				if($result->num_rows){
					$body=array();
					while($row=$result->fetch_array(MYSQLI_ASSOC)){
						$body[]=$row['name'];
					}
					$where[]="`body` in ('".implode("','",$body)."')";
				}
			}
		}
		
		if(isset($filter['regions']) && is_array($filter['regions'])){
			$regions=array();
			foreach($filter['regions'] as $region_Id){
				$region_Id=intval($region_Id);
				if($region_Id>0 and $region_Id<=99999){
					$regions[]=$region_Id;
				}
			}
			
			if(count($regions)){
				$where[]="`region_Id` in (".implode(",", $regions).")";
			}
		}
		
		if(isset($filter['marka']) && is_array($filter['marka'])){
			$marka_arr=array();
			foreach($filter['marka'] as $marka_Id){
				$marka_Id=intval($marka_Id);
				
				if($marka_Id>0 and $marka_Id<=9999999){
					$marka_arr[]=$marka_Id;
				}
			}
			if(count($marka_arr)){
				$result=$sql->query("select `marka` from `auto` where `Id` in (".implode(",", $marka_arr).")");
				
				if($result->num_rows){
					$marka_arr=array();
					while($row=$result->fetch_array(MYSQLI_ASSOC)){
						$marka_arr[]=$row['marka'];
					}
					
					$where[]="`marka` in ('".implode("','", $marka_arr)."')";
				}
			}			
		}	*/	
		
		
	/*	if(isset($filter['sources']) && is_array($filter['sources'])){
			$sources=array();
			foreach($filter['sources'] as $src){
				$src=intval($src);
				if(array_key_exists($src, $_sources)){
					$sources[]=$_sources[$src];
				}			
			}
			
			
			if(count($sources)){
				$where[]="`source` in ('".str_replace('.','',implode("','",$sources))."')";
			}
		}*/
		
//	}
	
	$fields=' ads.`Id`,
				`model`,
                                `model_2`,
				`price`,
                                `brand_id`,
                                `body_id`,
                                `color_id`,
                                `average_price`,
                                MINUTE(TIMEDIFF((`moderation_time`+INTERVAL 30 MINUTE), NOW())) AS minutes_to_publish,
                                TIMEDIFF((`moderation_time`+INTERVAL 30 MINUTE), NOW()) AS time_to_publish,
				`engine`,
				`enginevol`,
				`transmission`,
				`condition`,
				`run`,
				`rudder`,
				`phone`,
				`fio`,
				`photo`,
				`metro`,
				`address`,
				`region`,
				`url`,
				`phone_find`,
				`source`,
				`info`,
                                `year`,
                                `createtime`,
                                `vin`';
	
	if($old){
            $sql_str = "select ".$fields."
                        from ads ".(count($where)?'where '.implode(" and ", $where):'')." order by ads.createtime desc limit 0,".(int)$current_user->search_pagesize;
      //  echo $sql_str;
            
        } else {
            $last = Ads::findOne([
                'Id' => $id
            ]);
            
            $sql_str = "select ".$fields."
                        from ads where ads.old_id>".$last->old_id." ".(count($where)?' and '.implode(" and ", $where):'')." order by ads.createtime desc limit 0,".(int)$current_user->search_pagesize;
	
            
        }
        
        $rows = $sql->query($sql_str);
        						
	$stat = $rows->num_rows?'ok':'no';
	
        $brands_list = Brands::find()->all();
        $brands_arr = [];
        foreach ($brands_list as $br){
            $brands_arr[$br->id] = $br->toArray();
        }
        
        $bodys_list = Body::find()->all();
        $bodys_arr = [];
        foreach ($bodys_list as $bod){
            $bodys_arr[$bod->id] = $bod->toArray();
        }
        
        $colors_list = Colors::find()->all();
        $colors_arr = [];
        foreach ($colors_list as $color){
            $colors_arr[$color->id] = $color->toArray();
        }
        
      //  print_r($brands_arr);
        
	$result = array(); 
	$ret = array();
	$ret['status']=$stat;
	if($stat=='ok'){
            $bds = Body::find()->all();
            while($rlt = $rows->fetch_array(MYSQLI_ASSOC)){
                $rlt['marka'] = $brands_arr[$rlt['brand_id']]['name'];
                $rlt['marka_logo'] = $brands_arr[$rlt['brand_id']]['img_name'];
                $rlt['body'] = !empty($bodys_arr[$rlt['body_id']])?$bodys_arr[$rlt['body_id']]['name']:'';
                $rlt['color'] = !empty($colors_arr[$rlt['color_id']])?$colors_arr[$rlt['color_id']]['name']:'';
                $rlt['metallic'] = !empty($colors_arr[$rlt['color_id']])?$colors_arr[$rlt['color_id']]['metallic']:0;
                $rlt['dark'] = !empty($colors_arr[$rlt['color_id']])?$colors_arr[$rlt['color_id']]['dark']:0;

                $rlt['difference_price'] = floor((int)$rlt['average_price']-(int)$rlt['price'])/1000;
         
                
                $rlt['minutes_to_publish'] = (!empty($rlt['minutes_to_publish']) && !empty($rlt['time_to_publish']) && mb_substr($rlt['time_to_publish'], 0, 1, 'UTF-8')!='-')?$rlt['minutes_to_publish']:'';
                
                $timestamp = strtotime($rlt['createtime']);
                $rlt['time'] = date('H:i', $timestamp);
                
                $rlt['metro'] = empty($rlt['metro'])?'':$rlt['metro'];
                $rlt['photos_amount'] = 0;
                if($rlt['photo']!=''){				
                    $photo = explode(',', $rlt['photo']);
                    $rlt['photo'] = $photo[0];
                    $rlt['photos_amount'] = count($photo);
                }
                if (strcmp($rlt['year'], '0000')==0)
                    $rlt['year'] = '';
                $rlt['fast'] = 0;
                $rlt['wheel'] = 0;
                $rlt['torg'] = 0;
                if ((int)$rlt['run']==0)
                    $rlt['run'] = "";
                if (strcmp(mb_convert_encoding($rlt['marka'], 'utf-8'), mb_convert_encoding('Лада', 'utf-8'))==0)
                    $rlt['marka'] = 'ВАЗ (Lada)';
                if (!empty($rlt['info'])){
                    $info_arr = mb_split("\s", $rlt['info']);
                  //  $info_arr = explode(' ', $rlt['info']);
                    foreach ($info_arr as $w_num => $word) {
                        // Срочная продажа
                        if(mb_strpos(mb_strtolower($word),'срочн'))
                            $rlt['fast'] = 1;
                        // Колесо
                        if(mb_strpos(mb_strtolower($word),'резина'))
                            $rlt['wheel'] = 1;
                        if(mb_strpos(mb_strtolower($word),'колеса'))
                            $rlt['wheel'] = 1;
                        if(mb_strpos(mb_strtolower($word),'колёса'))
                            $rlt['wheel'] = 1;
                        if(mb_strpos(mb_strtolower($word),'покрышки'))
                            $rlt['wheel'] = 1;
                        // Торг
                        if (mb_strtolower($word)=='торг')
                            $rlt['torg'] = 1;
                        if (mb_strtolower(mb_eregi_replace('[!?.\)]','',$word))=='торг')
                            $rlt['torg'] = 1;
                    }
                }
                if (!empty($rlt['enginevol'])){
                    $rlt['enginevol'] = number_format((float)$rlt['enginevol'], 1, '.', '');
                }

                foreach ($bds as $number => $type){
                    if (strcmp(mb_ucfirst($rlt['body']), mb_ucfirst($type->name))==0){
                        if (!empty($type->img_name)){
                            $path = '/resources/img/'.$type->img_name;
                            $rlt['body_img'] = '<img class="body-img" src="'.$path.'" />';
                        } else {
                            $rlt['body_img'] = !empty($type->name)?$type->name:'';
                        }
                        break;
                    } else
                        $rlt['body_img'] = "";
                }
                if ($rlt['body']=='null')
                    $rlt['body'] = '';
                    
                $ads_h = Operations::findOne([
                    'user_id' => Yii::$app->session->get('id'),
                    'bulletin_id' => $rlt['Id'],
                    'type' => 'ads'
                ]);
                
                if (empty($ads_h))
                    $rlt['phone'] = null;
                
                
                $rlt['phone_find'] = (int)$rlt['phone_find'];
                $rlt['model_2'] = !empty($rlt['model_2'])?$rlt['model_2']:'';


                if(strpos($rlt['info'],'резина')!==false || strpos($rlt['info'],'колеса')!==false){
                    $rlt['tires'] = 1;//'Резина/Колеса';
                }else{
                    $rlt['tires'] = '';
                }	
                $color = $sql->query("SELECT `hex` FROM `colors` WHERE `name`='".trim($rlt['color']," \xC2\xA0")."'");
                $color_arr = $color->fetch_array(MYSQLI_ASSOC);
                if (!empty($color_arr)){
                    $rlt['color'] = '<div class="color-preview" style="background-color: #'.$color_arr['hex'].';"></div>';
                    $rlt['color_hex'] = $color_arr['hex'];
                } else {
                    $rlt['color'] = '';
                    $rlt['color_hex'] = '';
                }
                $result[] = $rlt;
            }
            $ret['ads'] = $result;
	}

	//header("Content-Type:application/json");	
	echo json_encode($ret);
    }
    
    
    
    public function actionGetmonthresult()
    {
        $limit=100;
	define("SQL_USER",     Yii::$app->params['SQL_USER']);
	define("SQL_PASSWORD", Yii::$app->params['SQL_PASSWORD']);
	define("SQL_BASE",     Yii::$app->params['SQL_BASE']);
	define("SQL_SERVER",   Yii::$app->params['SQL_SERVER']);
	
	$sql=new \mysqli(SQL_SERVER, SQL_USER, SQL_PASSWORD, SQL_BASE);
	$sql->set_charset("utf8");
	
	

	if(isset($_REQUEST['id']))
		$id=(int)$_REQUEST['id'];
				
	$old=false;
	if(isset($_REQUEST['old']))
		$old=((int)$_REQUEST['old']==1?true:false);	
	
	$where=array();
        if ((int)$_REQUEST['last_id']!=-1)
            $where[]="ads.`Id` <=".(int)$_REQUEST['last_id'];
        $where[]="`createtime` > (NOW() - interval 1 month)";
        
        
        if(!empty($_REQUEST['date_from'])){
            $date_from = date("Y-m-d H:i:s", strtotime($_REQUEST['date_from']));
            $where[]="`createtime`>='".$date_from."'";
        }
        if(!empty($_REQUEST['date_to'])){
            $date_to = date("Y-m-d H:i:s", strtotime($_REQUEST['date_to']));
            $to_time = new \DateTime($_REQUEST['date_to'], new \DateTimeZone('Europe/Moscow'));
            $to_time->add(new \DateInterval("P1D"));
            $where[]="`createtime`<'".$to_time->format("Y-m-d H:i:s")."'";
        }
		
		if(!empty($_REQUEST['run_from'])){
                    $where[]='`run`>='.(int)$_REQUEST['run_from'];
		}
                
                if(!empty($_REQUEST['run_to'])){
                    $where[]='`run`<='.(int)$_REQUEST['run_to'];
		}
                
                if(!empty($_REQUEST['volume_from'])){
                    $where[]="`enginevol`+0.01>".floatval($_REQUEST['volume_from']);
		}
                if(!empty($_REQUEST['volume_to'])){
                    $where[]="`enginevol`-0.01<".floatval($_REQUEST['volume_to']);
		}
                
                if(!empty($_REQUEST['price_from'])){
                    $where[]='`price`>='.(int)$_REQUEST['price_from'];
		}
                
                if(!empty($_REQUEST['price_to'])){
                    $where[]='`price`<='.(int)$_REQUEST['price_to'];
		}
		
                if(!empty($_REQUEST['year_from'])){
                    $where[]='`year`>='.(int)$_REQUEST['year_from'];
		}
                if(!empty($_REQUEST['year_to'])){
                    $where[]='`year`<='.(int)$_REQUEST['year_to'];
		}
		
		/*if(isset($filter['phone_find']) && intval($filter['phone_find'])>0){
			$where[]='`phone_find`<'.intval($filter['phone_find']);
		}			
		
		if(isset($filter['wheel']) && intval($filter['wheel'])>0){
			switch($filter['wheel']){
				case 1:
					$where[]="`wheel`='левый'";
					break;
					
				case 2:
					$where[]="`wheel`='правый'";
					break;
			}
		}*/
                
                if(!empty($_REQUEST['security'])){
                    $where[]="`phone_find`=0";
                }
                    
                if(!empty($_REQUEST['kpp'])){
                    if (mb_strlen($_REQUEST['kpp'])<=2){
                        $where[] = "`transmission`='".$_REQUEST['kpp']."'";
                    }
                }
                
                if(!empty($_REQUEST['engine'])){
                    $engine = (int)$_REQUEST['engine']==1?'b':'d';
                    $where[] = "`engine`='".$engine."'";
                }
                
                if(!empty($_REQUEST['state'])){
                    if ((int)$_REQUEST['state']==1){
                        $where[]="`condition` in ('Требует ремонта', 'требует ремонта', 'Битый', 'битый', 'Битый или не на ходу')";
                    } else {
                        $where[]="`condition` not in ('Требует ремонта', 'требует ремонта', 'Битый', 'битый', 'Битый или не на ходу')";
                    }
                }
                
                if(!empty($_REQUEST['source'])){
                    $source_arr = json_decode($_REQUEST['source']);
                    // Если есть autoru, то выбираем и trucksautoru
                    if (in_array(1, $source_arr))
                        $source_arr[] = 4;
                    $src = implode(",",$source_arr);
                    if (!empty($source_arr)){
                        $where[]="`source_id` in (".$src.")";
                    }
                }
                
                if(!empty($_REQUEST['bodys'])){
                    $bodys_arr = json_decode($_REQUEST['bodys']);
                    if (!empty($bodys_arr)){
                        $bodys_str = "('".  implode("','", $bodys_arr)."')";
                        $where[] = "`body_id` in ".$bodys_str;
                    }
                }
                
                if (!empty($_REQUEST['marks'])){
                    $marks_arr = json_decode($_REQUEST['marks']);
                    if(count($marks_arr)){
                    /*    $result=$sql->query("select `marka` from `auto` where `Id` in (".implode(",", $marks_arr).")");
                        if($result->num_rows){
                            $marka_arr=array();
                            while($row=$result->fetch_array(MYSQLI_ASSOC)){
                                $marka_arr[]=$row['marka'];
                            }
                            $where[]="br.`name` in ('".implode("','", $marka_arr)."')";
                        }*/
                        $where[]="brand_id in (".implode(",", $marks_arr).")";
                    }
                }
                
                if (!empty($_REQUEST['regions'])){
                    $regions_arr = json_decode($_REQUEST['regions']);
                    if (count($regions_arr)>0)
                        $where[]="`region_Id` in (".implode(",", $regions_arr).")";
                }
                
                if (isset($_REQUEST['phone_amount']) && strlen($_REQUEST['phone_amount'])>0){
                    $where[]="`phone_find`<=".(int)$_REQUEST['phone_amount'];
                /*    if ($_REQUEST['phone_amount'] == 0){
                        $where[]="`phone_find`=0";
                    } else {
                        $where[]="`phone_find`<=".(int)$_REQUEST['phone_amount'];
                    }*/
                }
	
	$fields=' ads.`Id`,
                `model`,
                `model_2`,
                `price`,
                `brand_id`,
                `body_id`,
                `color_id`,
                `average_price`,
                MINUTE(TIMEDIFF((`moderation_time`+INTERVAL 30 MINUTE), NOW())) AS minutes_to_publish,
                TIMEDIFF((`moderation_time`+INTERVAL 30 MINUTE), NOW()) AS time_to_publish,
                `engine`,
                `enginevol`,
                `transmission`,
                `condition`,
                `run`,
                `rudder`,
                `phone`,
                `fio`,
                `photo`,
                `metro`,
                `address`,
                `region`,
                `url`,
                `phone_find`,
                `source`,
                `info`,
                `year`,
                `createtime`,
                `vin`';
	if (strcmp($_REQUEST['orderby'],'dateasc')==0)
            $orderby = "ORDER BY ads.createtime ASC";
        else
            $orderby = "ORDER BY ads.createtime DESC";
        $page = !empty($_REQUEST['page'])?($_REQUEST['page']-1)*(int)$_REQUEST['page_size']:0;
        $sql_str = "select ".$fields."
                    from ads ".(count($where)?'where '.implode(" and ", $where):'')." ".$orderby." "
                . "limit ".$page.", ".(int)$_REQUEST['page_size'];
        $rows=$sql->query($sql_str);
        $amount = 0;
        if (!empty($_REQUEST['first_load'])){
            $sql_count = "select count(*) as `amount`
                        from ads ".(count($where)?'where '.implode(" and ", $where):'');
            $count = $sql->query($sql_count);
            while($rlt = $count->fetch_array(MYSQLI_ASSOC)){
                $amount =  (int)$rlt['amount'];
                break;
            }
        }

	$stat = $rows->num_rows?'ok':'no';
	
        $brands_list = Brands::find()->all();
        $brands_arr = [];
        foreach ($brands_list as $br){
            $brands_arr[$br->id] = $br->toArray();
        }
        
        $bodys_list = Body::find()->all();
        $bodys_arr = [];
        foreach ($bodys_list as $bod){
            $bodys_arr[$bod->id] = $bod->toArray();
        }
        
        $colors_list = Colors::find()->all();
        $colors_arr = [];
        foreach ($colors_list as $color){
            $colors_arr[$color->id] = $color->toArray();
        }
        
	$result = array(); 
	$ret = array();
	$ret['status']=$stat;
	if($stat=='ok'){
            $bds = Body::find()->all();
            while($rlt = $rows->fetch_array(MYSQLI_ASSOC)){
                $rlt['marka'] = $brands_arr[$rlt['brand_id']]['name'];
                $rlt['marka_logo'] = $brands_arr[$rlt['brand_id']]['img_name'];
                $rlt['body'] = !empty($bodys_arr[$rlt['body_id']])?$bodys_arr[$rlt['body_id']]['name']:'';
                $rlt['color'] = !empty($colors_arr[$rlt['color_id']])?$colors_arr[$rlt['color_id']]['name']:'';
                $rlt['metallic'] = !empty($colors_arr[$rlt['color_id']])?$colors_arr[$rlt['color_id']]['metallic']:0;
                $rlt['dark'] = !empty($colors_arr[$rlt['color_id']])?$colors_arr[$rlt['color_id']]['dark']:0;
                
                $rlt['difference_price'] = floor((int)$rlt['average_price']-(int)$rlt['price'])/1000;
                
                $rlt['minutes_to_publish'] = (!empty($rlt['minutes_to_publish']) && !empty($rlt['time_to_publish']) && mb_substr($rlt['time_to_publish'], 0, 1, 'UTF-8')!='-')?$rlt['minutes_to_publish']:'';
                
                $timestamp = strtotime($rlt['createtime']);
                $rlt['time'] = '<span class="hi">'.date('H:i', $timestamp).'</span>'.(empty($_COOKIE['show_photos'])?'<br>':'').'<span class="dm">'.date('d.m', $timestamp).'</span>';
            /*    if ($rlt['time']=='00:00')
                    $rlt['time'] = '';*/
                
                $rlt['metro'] = empty($rlt['metro'])?'':$rlt['metro'];
                $rlt['photos_amount'] = 0;
                if($rlt['photo']!=''){				
                    $photo=explode(',', $rlt['photo']);
                    $rlt['photo'] = $photo[0];
                    $rlt['photos_amount'] = count($photo);
                }
                if (strcmp($rlt['year'], '0000')==0)
                    $rlt['year'] = '';
                $rlt['fast'] = 0;
                $rlt['wheel'] = 0;
                $rlt['torg'] = 0;
                if ((int)$rlt['run']==0)
                    $rlt['run'] = "";
                if (!empty($rlt['info'])){
                    $info_arr = mb_split("\s", $rlt['info']);
                    //$info_arr = explode(' ', $rlt['info']);
                    foreach ($info_arr as $w_num => $word) {
                        // Срочная продажа
                        if(mb_strpos(mb_strtolower($word),'срочн'))
                            $rlt['fast'] = 1;
                        // Колесо
                        if (in_array(mb_strtolower($word), ['резина', 'колеса', 'колёса', 'покрышки']))
                            $rlt['wheel'] = 1;
                        // Торг
                        if (mb_strtolower($word)=='торг')
                            $rlt['torg'] = 1;
                        if (mb_strtolower(mb_eregi_replace('[!?.\)]','',$word))=='торг')
                            $rlt['torg'] = 1;
                    }
                }
                if (!empty($rlt['enginevol'])){
                    $rlt['enginevol'] = number_format((float)$rlt['enginevol'], 1, '.', '');
                }

            //    if (!empty($rlt['body_id'])){
                    foreach ($bds as $number => $type){
                        if (strcmp(mb_ucfirst($rlt['body']), mb_ucfirst($type->name))==0){
                            if (!empty($type->img_name)){
                                $path = '/resources/img/'.$type->img_name;
                                $rlt['body_img'] = '<img class="body-img" src="'.$path.'" />';
                            } else {
                                $rlt['body_img'] = !empty($type->name)?$type->name:'';
                            }
                            break;
                        } else
                            $rlt['body_img'] = "";
                    }
                    if ($rlt['body']=='null')
                        $rlt['body'] = '';
               /* } else 
                    $rlt['body_img'] = "";*/
                $ads_h = Operations::findOne([
                    'user_id' => Yii::$app->session->get('id'),
                    'bulletin_id' => $rlt['Id'],
                    'type' => 'ads'
                ]);
                
                if (empty($ads_h))
                    $rlt['phone'] = null;
                
                
                $rlt['phone_find'] = (int)$rlt['phone_find'];
                $rlt['model_2'] = !empty($rlt['model_2'])?$rlt['model_2']:'';


                if(strpos($rlt['info'],'резина')!==false || strpos($rlt['info'],'колеса')!==false){
                    $rlt['tires'] = 1;//'Резина/Колеса';
                }else{
                    $rlt['tires'] = '';
                }	
                $color = $sql->query("SELECT `hex` FROM `colors` WHERE `name`='".trim($rlt['color']," \xC2\xA0")."'");
                $color_arr = $color->fetch_array(MYSQLI_ASSOC);
                if (!empty($color_arr)){
                    $rlt['color'] = '<div class="color-preview" style="background-color: #'.$color_arr['hex'].';"></div>';
                    $rlt['color_hex'] = $color_arr['hex'];
                } else {
                    $rlt['color'] = '';
                    $rlt['color_hex'] = '';
                }
                $result[] = $rlt;
            }
            $ret['ads'] = $result;
	}
        $ret['amount'] = $amount;
	//header("Content-Type:application/json");	
	echo json_encode($ret);
    }
    
    public function actionModels()
    {
        $brand_id = (INT)$_REQUEST['brand_id'];
        $models = Models::find()
            ->where([
                'published' => 1,
                'parent_id' => $brand_id
            ])->orderBy(['name'=> SORT_ASC])->asArray()->all();
        echo json_encode($models);
    }
    
    public function actionChecklogin(){
        $id = !empty($_POST['id'])?$_POST['id']:false;
        $login = $_POST['login'];
        $user_check = Users::find();
        if ($id)
            $user_check = $user_check->where("id<>".$id." AND login='".$login."'");
        else
            $user_check = $user_check->where("login='".$login."'");
        $user_check = $user_check->one();
        if (!empty($user_check))
            $response['error'] = 1;
        else
            $response['error'] = 0;
        echo json_encode($response);
    }
    
    public function actionUpdatesettings() {
        $response = ['success' => false, 'message' => '', 'code' => 0];
        if (empty(Yii::$app->session->get('id'))){
            $response['code'] = 1;
            $response['message'] = "Not authorized";
            echo json_encode($response);
            die;
        }
        $id = Yii::$app->session->get('id');
        $type = $_REQUEST['type'];
        $value = $_REQUEST['value'];
        $user_to_update = Users::find()
            ->where([
                'id' => $id
            ])->one();
        switch ($type) {
            case "sounds":
                $user_to_update->search_sounds = $value;
                break;
            case "markmoderation":
                $user_to_update->search_markmoderation = $value;
                break;
            case "markcheap":
                $user_to_update->search_markcheap = $value;
                break;
            case "photos":
                $user_to_update->search_photos = $value;
                break;
            case "pagesize":
                $user_to_update->search_pagesize = $value;
                break;
            default:
                $response['code'] = 2;
                $response['message'] = "Unknown type";
                echo json_encode($response);
                die;
                break;
        }
        if (!$user_to_update->save()) {
            $response['code'] = 3;
            $response['message'] = "Update error";
            echo json_encode($response);
            die;
        }
        $response['success'] = true;
        echo json_encode($response);
        die;
    }
    
    public function actionUpdateprofile() {

        $password = $_POST['password'];
        $response = ['success' => false, 'message' => '', 'error_code' => 0];
        if (empty(Yii::$app->session->get('id'))){
            $response['message'] = "Not authorized";
            echo json_encode($response);
            die;
        }
        $id = Yii::$app->session->get('id');
 /*       if (empty($_POST['name'])){
            $response['message'] = "'name' couldn't be empty";
            $response['error_code'] = 1;
            echo json_encode($response);
            die;
        }
        if (empty($_POST['phone'])){
            $response['message'] = "'phone' couldn't be empty";
            $response['error_code'] = 1;
            echo json_encode($response);
            die;
        }*/
        if (empty($_POST['password'])){
            $response['message'] = "'password' couldn't be empty";
            $response['error_code'] = 1;
            echo json_encode($response);
            die;
        }
    /*    $user = Users::find()
                ->where("id<>".$id." AND login='".$phone."'")
                ->all();
        if (!empty($user)){
            $response['message'] = "User with same phone exists";
            $response['error_code'] = 2;
            echo json_encode($response);
            die;
        }*/
        $user_to_update = Users::find()
                ->where([
                    'id' => $id
                        ])
                ->one();
        if (empty($user_to_update)){
            $response['message'] = "User not found";
            $response['error_code'] = 3;
            echo json_encode($response);
            die;
        }
   //     $user_to_update->name = $name;
   //     $user_to_update->login = $phone;
   //     if (!empty($password))
        $user_to_update->search_photos = $_POST['search_photos']==1?1:0;
        $user_to_update->search_sounds = $_POST['search_sounds']==1?1:0;
        $user_to_update->hash = password_hash($password, PASSWORD_BCRYPT);
        $sessionKey = parent::getSessionKey();
        $user_to_update->session_key = $sessionKey;
        $user_to_update->password_changed = 1;
        Yii::$app->session->set('session_key', $sessionKey);
        if (!$user_to_update->save()){
            $response['message'] = "Couldn't update user";
            $response['error_code'] = 4;
            echo json_encode($response);
            die;
        }
        $response['success'] = true;
        echo json_encode($response);
    }
    
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'foreColor'=>0x66534F,
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    public function actionIndex()
    {
/*        if (empty(Yii::$app->session->get('id'))){
            header( 'Location: /users/login', true, 302 );
            exit();
        }
        $this->view->registerCssFile('/resources/css/main.css');
    //    $this->view->registerJsFile('/resources/js/index.js',['depends'=>'yii\web\JqueryAsset']);
        $this->view->params['brands'] = Brands::find()->orderBy(['name'=> SORT_ASC])->all();
        return $this->render('index');*/
    }
    
 /*   public function actionCreate()
    {
        if (empty(Yii::$app->session->get('id'))){
            header( 'Location: /users/login', true, 302 );
            exit();
        }
        $this->view->registerCssFile('/resources/css/main.css');
        $this->view->registerJsFile('/resources/js/create.js',['depends'=>'yii\web\JqueryAsset']);
        return $this->render('create',[
            'brands' => Brands::find()->orderBy(['name'=> SORT_ASC])->all()
        ]);
    }*/
    
    public function actionPhotoupload()
    {
        if (empty($_FILES['photos']))
            return;
        $response = ['success' => false, 'url' => '', 'message' => '', 'id' => ''];
        if (empty(Yii::$app->session->get('id'))){
            $response['message'] = "Not authorized";
            echo json_encode($response);
            die;
        }
        $imageinfo = getimagesize($_FILES['photos']['tmp_name'][0]);
        $allowed_mimes = ['image/jpeg','image/png'];
        
        if (!in_array($imageinfo['mime'], $allowed_mimes)){
            $response['message'] = "File type error";
            echo json_encode($response);
            die;
        }
        $uploaddir = './photos/'.date('Ymd').'/';
        $uploadthumbdir = './photos/'.date('Ymd').'/thumb/';
        if (!file_exists($uploaddir))
            mkdir($uploaddir, 0755, true);
        if (!file_exists($uploadthumbdir))
            mkdir($uploadthumbdir, 0755, true);
        $filename_arr = explode('.', $_FILES['photos']['name'][0]);
        $f = md5(microtime() . rand(0, 9999));
        $filename = $f.'.'.$filename_arr[count($filename_arr)-1];
        $full_url = $uploaddir . $filename; 
        
        if (move_uploaded_file($_FILES['photos']['tmp_name'][0], $full_url)) {
            if (strcmp($imageinfo['mime'], 'image/jpeg')==0){
                $im = imagecreatefromjpeg($full_url);
            } else {
                $im = imagecreatefrompng($full_url);
            }
            $ox = imagesx($im);
            $oy = imagesy($im);
            $nx = Yii::$app->params['thumb_width'];
            $ny = floor($oy * ($nx / $ox));
            $nm = imagecreatetruecolor($nx, $ny);
            imagecopyresized($nm, $im, 0,0,0,0,$nx,$ny,$ox,$oy);
            $thumb_file = $uploadthumbdir.$f.'.jpg';
            imagejpeg($nm, $thumb_file);
            $photo = new Photos();
            $photo->url = substr($full_url, 2);
            $photo->thumb = substr($thumb_file, 2);
            $photo->main = 0;
            $photo->owner = Yii::$app->session->get('id');
            if ($photo->save()){
                $response['success'] = true;
                $response['url'] = $thumb_file;//substr($full_url, 1);
                $response['id'] = $photo->id;
            } else
                $response['message'] = "Save link to database error";
        }
        echo json_encode($response);
    }
    
    public function actionDeletephoto()
    {
        $response = ['success' => false, 'message' => ''];
        if (empty(Yii::$app->session->get('id'))){
            $response['message'] = "Not authorized";
            echo json_encode($response);
            die;
        }
        if (empty($_POST['photo_id'])){
            $response['message'] = "'photo_id' value empty";
            echo json_encode($response);
            die;
        }
        $photo = Photos::findOne([
            'id'=>(int)$_POST['photo_id'],
            'owner'=>Yii::$app->session->get('id') 
        // Юзер может удалять только свои фотографии
        ]);
        if (empty($photo)){
            $response['message'] = "Photo not found";
            echo json_encode($response);
            die;
        }
        $file_url = './'.$photo->url;
        if ($photo->delete()){
            if (unlink($file_url))
                $response['success'] = true;
            else {
                $response['message'] = "File not found";
                echo json_encode($response);
                die;
            }
        }
        echo json_encode($response);
    }
    
    private function downloadFile($file){
        if (ob_get_level()) {
            ob_end_clean();
        }
        header('X-Accel-Redirect: ' . $file);
        header('Content-Description: File Transfer');
        header('Content-Type: application/octet-stream');
        header('Content-Disposition: attachment; filename=' . basename($file));
        header('Content-Transfer-Encoding: binary');
        header('Expires: 0');
        header('Cache-Control: must-revalidate');
        header('Pragma: public');
        header('Content-Length: ' . filesize($file));
        readfile($file);
        exit;
    }
    
    public function actionGetphotoszip()
    {
        $response = ['success' => false, 'message' => ''];
        if (empty(Yii::$app->session->get('id'))){
            $response['message'] = "Not authorized";
            echo json_encode($response);
            die;
        }
        if (empty($_REQUEST['id'])){
            $response['message'] = "'id' value empty";
            echo json_encode($response);
            die;
        }
        $photos = Photos::findAll(['car_id' => $_REQUEST['id']]);
        $zip = new \ZipArchive;
        
        $name = './photos_zip/photos_'.$_REQUEST['id'].'.zip';
        $res = $zip->open($name, \ZipArchive::CREATE);
        if ($res === TRUE) {
            $counter = 1;
            foreach($photos as $photo){           
                if (file_exists('./'.$photo->url)){
                    $photo_name_arr = explode('/',$photo->url);
                    $name_arr = explode('.', $photo_name_arr[count($photo_name_arr)-1]);
                    $ext = $name_arr[count($name_arr)-1];
                    $zip->addFile('./'.$photo->url, $counter.'.'.$ext);
                    $counter++;
                }
            }
            $zip->close();
            if ($counter>1){
                if (file_exists($name)){
                    chmod ($name, 0755);
                    $this->downloadFile($name);
                } else {
                    $response['message'] = "Error in zip-archive creation";
                    echo json_encode($response);
                    die;
                }
            } else {
                $response['message'] = "Photos not found";
                echo json_encode($response);
                die;
            }
        } else {
            $response['message'] = "Couldn't create zip-archive";
            echo json_encode($response);
            die;
        }
            
    }
    
    public function actionChangestatus(){
        $response = ['success' => false, 'message' => ''];
        if (empty(Yii::$app->session->get('id'))){
            $response['message'] = "Not authorized";
            echo json_encode($response);
            die;
        }
        if (!Yii::$app->session->get('admin')){
            $response['message'] = "Not admin";
            echo json_encode($response);
            die;
        }
        $id = Yii::$app->request->post('id');
        $status = Yii::$app->request->post('status');
        $car = Cars::findOne([
            'id' => $id
        ]);
        if (empty($car)){
            $response['message'] = "Car not found";
            echo json_encode($response);
            die;
        }
        $car->status = $status;
        $car->save();
        $response['success'] = true;
        echo json_encode($response);
        die;
    }
    
    public function actionChangeavitourl(){
        $response = ['success' => false, 'message' => ''];
        if (empty(Yii::$app->session->get('id'))){
            $response['message'] = "Not authorized";
            echo json_encode($response);
            die;
        }
        if (!Yii::$app->session->get('admin')){
            $response['message'] = "Not admin";
            echo json_encode($response);
            die;
        }
        $id = Yii::$app->request->post('id');
        $url = Yii::$app->request->post('url');
        $car = Cars::findOne([
            'id' => $id
        ]);
        if (empty($car)){
            $response['message'] = "Car not found";
            echo json_encode($response);
            die;
        }
        $car->avitoru_url = $url;
        $car->save();
        $response['success'] = true;
        echo json_encode($response);
        die;
    }
    
    public function actionChangeautourl(){
        $response = ['success' => false, 'message' => ''];
        if (empty(Yii::$app->session->get('id'))){
            $response['message'] = "Not authorized";
            echo json_encode($response);
            die;
        }
        if (!Yii::$app->session->get('admin')){
            $response['message'] = "Not admin";
            echo json_encode($response);
            die;
        }
        $id = Yii::$app->request->post('id');
        $url = Yii::$app->request->post('url');
        $car = Cars::findOne([
            'id' => $id
        ]);
        if (empty($car)){
            $response['message'] = "Car not found";
            echo json_encode($response);
            die;
        }
        $car->autoru_url = $url;
        $car->save();
        $response['success'] = true;
        echo json_encode($response);
        die;
    }
}
