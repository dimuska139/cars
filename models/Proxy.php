<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "proxy".
 *
 * @property integer $id
 * @property integer $proxy
 * @property string $proxy_login
 * @property string $proxy_pass
 * @property string $avito_login
 * @property string $avito_pass
 * @property string $createdate
 * @property string $phone
 */
class Proxy extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'proxy';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['proxy', 'proxy_login', 'proxy_pass'], 'required'],
            [['createdate'], 'safe'],
            [['proxy_login', 'proxy_pass', 'avito_login', 'avito_pass'], 'string', 'max' => 50]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'proxy' => 'Proxy',
            'proxy_login' => 'Proxy Login',
            'proxy_pass' => 'Proxy Pass',
            'avito_login' => 'Avito Login',
            'avito_pass' => 'Avito Pass',
            'createdate' => 'Createdate',
            'phone' => 'Phone',
        ];
    }
    
    public function getUser(){
        return $this->hasOne(Users::className(), ['id' => 'user_id']);
    }
}
