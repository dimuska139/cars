<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "body".
 *
 * @property integer $Id
 * @property string $name
 * @property string $img_name
 * @property integer $show_in_creation
 * @property integer $show_order
 */
class Body extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'body';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'show_in_creation'], 'required'],
            [['show_in_creation', 'show_order'], 'integer'],
            [['name'], 'string', 'max' => 20],
            [['img_name'], 'string', 'max' => 50]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'img_name' => 'Img Name',
            'show_in_creation' => 'Show In Creation',
            'show_order' => 'Show Order',
        ];
    }
    
    public static function getPublicBodys()
    {
        return Body::find()->where(['show_in_creation' => 1])
                           ->orderBy(['show_order'=> SORT_ASC])
                           ->all();
    }
}
