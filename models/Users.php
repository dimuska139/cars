<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "users".
 *
 * @property integer $id
 * @property string $login
 * @property string $hash
 * @property integer $balance
 * @property integer $admin
 * @property integer $state
 * @property integer $ip
 * @property integer $search_photos
 * @property integer $search_sounds
 * @property string $createdate
 */
class Users extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'users';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['login', 'hash', 'balance', 'admin', 'state', 'ip'], 'required'],
            [['balance', 'admin', 'state', 'ip', 'search_photos', 'search_sounds'], 'integer'],
            [['createdate'], 'safe'],
            [['login'], 'string', 'max' => 200],
            [['hash'], 'string', 'max' => 60]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'login' => 'Login',
            'hash' => 'Hash',
            'balance' => 'Balance',
            'admin' => 'Admin',
            'state' => 'State',
            'ip' => 'Ip',
            'createdate' => 'Createdate',
        ];
    }
    
}
