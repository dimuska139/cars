<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "ads".
 *
 * @property integer $Id
 * @property integer $brand_id
 * @property string $model
 * @property integer $price
 * @property string $engine
 * @property string $enginevol
 * @property string $transmission
 * @property string $condition
 * @property integer $run
 * @property string $rudder
 * @property string $phone
 * @property string $fio
 * @property string $photo
 * @property string $metro
 * @property string $address
 * @property string $region
 * @property string $url
 * @property string $year
 * @property integer $average_price
 * @property integer $phone_find
 * @property integer $region_Id
 * @property string $source
 * @property string $info
 */
class Ads extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'ads';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['brand_id', 'price', 'run', 'average_price', 'phone_find', 'region_Id', 'rudder'], 'integer'],
            [['photo', 'info'], 'string'],
            [['year'], 'safe'],
            [['model', 'address'], 'string', 'max' => 100],
            [['engine', 'enginevol', 'fio'], 'string', 'max' => 45],
            [['transmission'], 'string', 'max' => 25],
            [['phone', 'source'], 'string', 'max' => 20],
            [['metro', 'region'], 'string', 'max' => 50],
            [['url'], 'string', 'max' => 250]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'Id' => 'ID',
            'brand_id' => 'Brand ID',
            'model' => 'Model',
            'price' => 'Price',
            'engine' => 'Engine',
            'enginevol' => 'Enginevol',
            'transmission' => 'Transmission',
            'condition' => 'Condition',
            'run' => 'Run',
            'rudder' => 'Rudder',
            'phone' => 'Phone',
            'fio' => 'Fio',
            'photo' => 'Photo',
            'metro' => 'Metro',
            'address' => 'Address',
            'region' => 'Region',
            'url' => 'Url',
            'year' => 'Year',
            'average_price' => 'Average Price',
            'phone_find' => 'Phone Find',
            'region_Id' => 'Region  ID',
            'source' => 'Source',
            'info' => 'Info',
        ];
    }
    public function getBrand(){
        return $this->hasOne(Brands::className(), ['id' => 'brand_id']);
    }
    public function getBody(){
        return $this->hasOne(Body::className(), ['id' => 'body_id']);
    }
}
