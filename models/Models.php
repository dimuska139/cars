<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "marks".
 *
 * @property integer $id
 * @property string $name
 * @property string $alias
 * @property integer $parent_id
 * @property string $start_year_production
 * @property string $end_year_production
 * @property integer $is_last
 */
class Models extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'models';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'parent_id'], 'required'],
            [['parent_id', 'is_last'], 'integer'],
            [['name', 'alias'], 'string', 'max' => 200],
            [['start_year_production', 'end_year_production'], 'string', 'max' => 20]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'alias' => 'Alias',
            'parent_id' => 'Parent ID',
            'start_year_production' => 'Start Year Production',
            'end_year_production' => 'End Year Production',
            'is_last' => 'Is Last',
        ];
    }
    
    public function getBrand(){
        return $this->hasOne(Brands::className(), ['id' => 'parent_id']);
    }
}
