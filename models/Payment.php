<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "payment".
 *
 * @property integer $id
 * @property integer $user_id
 * @property integer $sum
 * @property integer $completed
 * @property integer $active
 * @property string $createdate
 */
class Payment extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'payment';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'sum'], 'required'],
            [['user_id', 'sum', 'completed', 'active'], 'integer'],
            [['createdate'], 'safe']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'sum' => 'Sum',
            'completed' => 'Completed',
            'active' => 'Active',
            'createdate' => 'Createdate',
        ];
    }
}
