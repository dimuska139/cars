<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "operations".
 *
 * @property integer $id
 * @property integer $user_id
 * @property integer $bulletin_id
 * @property string $type
 * @property integer $price
 * @property string $status
 * @property string $createdate
 */
class Operations extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'operations';
    }

    public static function getNames()
    {
        return [
            'payment'  => 'Пополнение баланс',
            'creation' => 'Добавление объявления'
        ];
    }
    
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'bulletin_id', 'type', 'price', 'status'], 'required'],
            [['user_id', 'bulletin_id', 'price'], 'integer'],
            [['createdate'], 'safe'],
            [['type', 'status'], 'string', 'max' => 20]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'bulletin_id' => 'Bulletin ID',
            'type' => 'Type',
            'price' => 'Price',
            'status' => 'Status',
            'createdate' => 'Createdate',
        ];
    }
    
    
    public function getAds(){
        return $this->hasOne(Ads::className(), ['id' => 'bulletin_id']);
    }
}
