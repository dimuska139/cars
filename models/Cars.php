<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "cars".
 *
 * @property integer $id
 * @property integer $model_id
 * @property integer $year
 * @property integer $body_type
 * @property integer $engine_type
 * @property string $engine_volume
 * @property integer $engine_power
 * @property integer $transmission_type
 * @property integer $drive_type
 * @property integer $color_id
 * @property integer $rudder
 * @property integer $condition
 * @property integer $run
 * @property integer $ad_type
 * @property integer $status
 * @property integer $price
 * @property string $description
 * @property string $contact
 * @property integer $phone
 * @property integer $owner
 */
class Cars extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'cars';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['model_id', 'year', 'engine_type', 'engine_volume', 'engine_power', 'transmission_type', 'drive_type', 'color_id', 'rudder', 'condition', 'run', 'ad_type', 'status', 'price', 'contact', 'phone'], 'required'],
            [['model_id', 'year', 'engine_type', 'engine_power', 'transmission_type', 'drive_type', 'color_id', 'rudder', 'condition', 'run', 'ad_type', 'status', 'price', 'body_id'], 'integer'],
            [['engine_volume'], 'number'],
            [['description', 'contact'], 'string']
        ];
    }
    
    
    
    public function getBody(){
        return $this->hasOne(Body::className(), ['id' => 'body_id']);
    }
    
    public static function getEnginetypes()
    {
        return ['Бензин', 'Дизель', 'Гибрид'];
    }
    
    public static function getTransmissiontypes()
    {
        return [
            [
                'name' => 'Механическая',
                'second_name' => 'Механика',
                'shortname' => 'MT'
            ],
            [
                'name' => 'Автоматическая',
                'second_name' => 'Автомат',
                'shortname' => 'AT'
            ],
            [
                'name' => 'Роботизированная',
                'second_name' => 'Робот',
                'shortname' => 'AMT'
            ],
            [
                'name' => 'Вариатор',
                'second_name' => 'Вариатор',
                'shortname' => 'CVT'
            ]
        ];
    }
    
    public static function getDrivetypes()
    {
        return ['Передний','Задний','Полный'];
    }
    
    public static function getRudders()
    {
        return ['Левый','Правый'];
    }
    
    public static function getConditions()
    {
        return ['не битый','битый'];
    }
    
    public static function getAdtypes()
    {
        return ['обычная','турбо-продажа'];
    }
    
    public static function getStatuses()
    {
        return [
            [
                'user' => 'Активен',
                'admin' => 'Активен',
                'order' => 0
            ],
            [
                'user' => 'Размещение',
                'admin' => 'Размещение',
                'order' => 3
            ],
            [
                'user' => 'Удаление',
                'admin' => 'удаление',
                'order' => 6
            ],
            [
                'user' => 'Редактирование',
                'admin' => 'Редактирование',
                'order' => 4
            ],
            [
                'user' => 'Обновление',
                'admin' => 'Обновлен',
                'order' => 5
            ],
            [
                'user' => 'Завершён',
                'admin' => 'Завершён',
                'order' => 1
            ],
            
            [
                'user' => 'Заблокирован',
                'admin' => 'Заблокирован',
                'order' => 2
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'model_id' => 'Model ID',
            'year' => 'Year',
            'engine_type' => 'Engine Type',
            'engine_volume' => 'Engine Volume',
            'engine_power' => 'Engine Power',
            'transmission_type' => 'Transmission Type',
            'drive_type' => 'Drive Type',
            'color_id' => 'Color ID',
            'rudder' => 'Rudder',
            'condition' => 'Condition',
            'run' => 'Run',
            'ad_type' => 'Ad Type',
            'status' => 'Status',
            'price' => 'Price',
            'description' => 'Description',
            'contact' => 'Contact',
            'phone' => 'Phone',
            'owner' => 'Owner'
        ];
    }
    
    public function getColor(){
        return $this->hasOne(Colors::className(), ['id' => 'color_id']);
    }
  /*  public function getSubway(){
        return $this->hasOne(Subwaystations::className(), ['id' => 'subway_id']);
    }*/
    public function getModel(){
        return $this->hasOne(Models::className(), ['id' => 'model_id']);
    }
    public function getPhoto(){
        return $this->hasOne(Photos::className(), ['car_id' => 'id']);
    }
    public function getPhotos(){
        return $this->hasMany(Photos::className(), ['car_id' => 'id']);
    }
    
    public function getProxy(){
        return $this->hasOne(Proxy::className(), ['id' => 'proxy_id']);
    }
}