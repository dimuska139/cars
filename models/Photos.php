<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "photos".
 *
 * @property integer $id
 * @property string $url
 * @property integer $car_id
 * @property integer $main
 * @property integer $owner
 */
class Photos extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'photos';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['url', 'main', 'owner'], 'required'],
            [['car_id', 'main', 'owner'], 'integer'],
            [['url'], 'string', 'max' => 500]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'url' => 'Url',
            'car_id' => 'Car ID',
            'main' => 'Main',
            'owner' => 'Owner',
        ];
    }
}
