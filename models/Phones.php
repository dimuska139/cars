<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "phones".
 *
 * @property integer $id
 * @property integer $avito_code
 * @property integer $auto_code
 * @property string $createdate
 * @property string $name
 * @property integer $confirmed_avito
 * @property integer $confirmed_auto
 * @property integer $user_id
 * @property integer $status
 */
class Phones extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'phones';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['phone', 'name', 'user_id', 'status'], 'required'],
            [['id', 'avito_code', 'auto_code', 'confirmed_avito', 'confirmed_auto', 'user_id', 'status'], 'integer'],
            [['createdate'], 'safe'],
            [['name'], 'string', 'max' => 200]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'phone' => 'Phone',
            'avito_code' => 'Avito Code',
            'auto_code' => 'Auto Code',
            'createdate' => 'Createdate',
            'name' => 'Name',
            'confirmed_avito' => 'Confirmed Avito',
            'confirmed_auto' => 'Confirmed Auto',
            'user_id' => 'User ID',
            'status' => 'Status',
        ];
    }
}
