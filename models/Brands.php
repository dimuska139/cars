<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "brands".
 *
 * @property integer $id
 * @property string $name
 * @property string $alias
 * @property integer $folder_id
 * @property integer $is_popular
 */
class Brands extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'brands';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'alias', 'folder_id', 'is_popular'], 'required'],
            [['folder_id', 'is_popular'], 'integer'],
            [['name', 'alias'], 'string', 'max' => 25],
            [['name'], 'unique']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'alias' => 'Alias',
            'folder_id' => 'Folder ID',
            'is_popular' => 'Is Popular',
        ];
    }
}
