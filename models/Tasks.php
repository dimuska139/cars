<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "tasks".
 *
 * @property integer $id
 * @property string $name
 * @property integer $interval_sec
 * @property string $last_start_time
 * @property integer $status
 */
class Tasks extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tasks';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'interval', 'last_start_time', 'status'], 'required'],
            [['status'], 'integer'],
            [['last_start_time'], 'safe'],
            [['name'], 'string', 'max' => 100],
            [['name'], 'unique']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'interval' => 'Interval',
            'last_start_time' => 'Last Start Time',
            'status' => 'Status',
        ];
    }
}
