#!/usr/bin/env python
from mysql.connector import MySQLConnection, Error
from mysql.connector import Error

def connect():
    try:
        conn = mysql.connector.connect(host='localhost',
                                       database='cars',
                                       user='root',
                                       password='12345')
        if conn.is_connected():
            return conn
    except Error as e:
        print(e)

    finally:
        conn.close()


f = open("metro_spb.txt","r")
metro = f.readlines()


if __name__ == '__main__':
    conn = MySQLConnection(host='localhost',
                                       database='cars',
                                       user='root',
                                       password='12345')
    cursor = conn.cursor()
    sql = "INSERT INTO subwaystations(name, city_id) " \
          "VALUES(%s,2)"
    for m in metro:
        args = (m,)
	cursor.execute(sql, args)
        if cursor.lastrowid:
            print('last insert id', cursor.lastrowid)
        else:
            print('last insert id not found')
        conn.commit()
    cursor.close()
    conn.close()
