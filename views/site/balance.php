<?php $this->title="Баланс"; ?>
    <span class="balance">
        <?=$name;?>, Ваш баланс <span class="balance-value"><?=!empty($this->params['balance'])?number_format($this->params['balance'],0,' ',' '):'0';?></span> рублей!
    </span>
    <span class="balance-info">
        При пополнение баланса на сумму от 5 000 руб. – бонус 10%, 15 000 руб. – бонус 20% от суммы пополнения!<br>
        Пополнение на сумму 5 000 руб. и более осуществляется только переводом на карту клиента Сбербанка.<br>
        Подробнее можно узнать по телефону техподдержки: +79251682285.
    </span>
<div class="qiwi-panel">
    <img class="qiwi-logo" src="/resources/img/vqw.png">
    <div class="sum-place">
        <form action="/qiwiredirect" method="post" target="_blank">
            <span class="label">Сумма</span>
            <input name="sum" type="text" id="sum">
            <span class="currency">руб.</span>
            <span class="helper">мин. 200, макс. 4 999 руб.</span>
            <input type="submit" value="Пополнить" class="send">
        </form>
    </div>
</div>
<span class="info">
     Пополнение происходит автоматически. Статус счета на оплату проверяется каждую минуту!<br>
     В случае возникновения каких-либо проблем, свяжитесь с техподдержкой сайта!
</span>
<div class="requisites">
	<span class="text">Дополнительные реквизиты</span>
	<div class="req">
	<p class="req-text">Карта Сбербанка <font class="req-min">(мин. 1 000 руб.)</font></p>
	<p class="req-number">XXXX XXXX XXXX XXXX</p>
	<img class="req-logo" src="/resources/img/sberbank.png">
	</div>
	<div class="req">
	<p class="req-text">Visa QIWI Wallet <font class="req-min">(мин. 500, макс. 4 999 руб.)</font></p>
	<p class="req-number">+79251682285</p>
	<img class="req-logo" src="/resources/img/qiwi.png">
	</div>
	<div class="req">
	<p class="req-text">Карта Альфа-Банка</p>
	<p class="req-number">XXXX XXXX XXXX XXXX</p>
	<img class="req-logo" src="/resources/img/alfabank.png">
	</div>
	<div class="req">
	<p class="req-text">Яндекс.Деньги <font class="req-min">(мин. 1 000, макс. 4 999 руб.)</font></p>
	<p class="req-number">410011644523482</p>
	<img class="req-logo" src="/resources/img/yandex.png">
	</div>
</div>
<span class="info">
     После совершения перевода, необходимо связаться с техподдержкой сайта!<br>
	 Баланс будет пополнен в течение получаса с момента обращения.
</span>