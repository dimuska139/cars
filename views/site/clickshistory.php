<?php $this->title="История переходов"; ?>
<script>
    var amount = <?=$amount;?>;
    var enough_balance = <?=$enough_balance?'true':'false';?>;
</script>
<link rel="stylesheet" type="text/css" href="//fonts.googleapis.com/css?family=Open+Sans" />

<div class="search_block">
<div class="search">
            <?php
                    if(isset($_POST['filter'])){
                        setcookie("filter", serialize($_POST['filter']));
                        $filter=$_POST['filter'];
                    } else {
                        if (!empty($_COOKIE['filter']))
                            $filter=unserialize($_COOKIE['filter']);
                        else 
                            $filter = [];
                    }

            ?>
<div class="add-menu">
    <div class="tab-button tab-active">
        <div class="text">
            <a href="/online">
            Online
            </a>
        </div>
    </div>
        <div class="tab-button tab-moderation">
            <div class="text">
                <a href="/ads">
                За месяц
                </a>
            </div>
        </div>
    <div class="tab-button tab-ended tab-button-active">
            <div class="text">
                <span>
                Переходы
                </span>
            </div>
        </div>
	<div class="phone-search">
         <form id="searchbyphone-form" action="/search" method="post" target="_blank">
              <span class="label">Поиск объявлений по номеру телефона</span>
                  <input type="text" placeholder="Телефон" id="phone-number" name="phone" class="phone-number" value="">
                  <img src="/resources/img/ajax-loader.gif" id="checkphone-ajax-loader" class="ajax-loader">
                  <input type="submit" class="find" value="Найти">
         </form>
     </div>
     <span class="error">
        Объявлений не найдено!
     </span>
</div>
    <div class="warning-panel" <?=$enough_balance?'style="display: none"':'';?>>
        <img src="/resources/img/redcar.png">
        <div class="title">К сожалению, Вам недоступны платные действия!</div>
        <div class="text">Недостаточно средств на счету. Вы можете пополнить баланс в разделе "Баланс".<br>
        С уважением, администрация AM97.RU!</div>
    </div>
     <div class="ads-table">
            <div class="monitor monitor-clicks">
                <div id="result_list" class="history-table">
                    <?php echo \Yii::$app->view->renderFile('@app/views/site/clickshistory_page.php',['history' => $history]); ?>
                </div>
                <?php if ($amount>25): ?>
                <div class="more-button">
                    <div class="text">Показать ещё</div>
                </div>
                <div class="md-preloader" id="more-loading-indicator">
                    <svg xmlns="http://www.w3.org/2000/svg" version="1.1" height="55" width="55" viewbox="0 0 55 55"><circle cx="27.5" cy="27.5" r="15" stroke-width="6"/>
                    </svg>
                </div>
                <?php endif; ?>
            </div>
         </div>
    </div>
</div>