<?php 
    function mb_ucfirst($string, $encoding='UTF-8')
    {
        $firstChar = mb_substr($string, 0, 1, $encoding);
        $then = mb_substr($string, 1, mb_strlen($string, $encoding)-1, $encoding);
        return mb_strtoupper($firstChar, $encoding) . $then;
    }
?>
<?php foreach ($history as $num => $item): ?>
<a href="/adsredirect?id=<?=$item['Id'];?>" 
   target="_blank" 
   class="<?=($num%2==0?'even ':'odd ');?>line" id="id_<?=$item['Id'];?>">
    <?php 
        $timestamp = strtotime($item['time']);
        $item['time'] = '<span class="hi">'.date('H:i', $timestamp).'</span><br><span class="dm">'.date('d.m', $timestamp).'</span>';
    ?>
   <div style="width: 50px" class="td time"><div class="pos"><?=$item['time'];?></div></div>
   <div style="width: 40px" class="td logo"><?=$item['marka_logo']?('<img class="brand-logo" src="/resources/img/brands/'.$item['marka_logo'].'">'):'';?></div>
   <div style="width: 380px" class="name td"><div class="fade"></div><div class="pos"><nobr>
    <?php             
         $eng = $item['enginevol'].((strlen($item['engine'])!=0 && $item['engine']!='b')?$item['engine']:'').(!empty($item['transmission'])?(' '.$item['transmission']):"");
         if (mb_strlen($eng,'UTF-8')>2)
             $eng = '('.$eng.')';
         if (strcmp(mb_convert_encoding($item['marka'], 'utf-8'), mb_convert_encoding('Лада', 'utf-8'))==0)
                $item['marka'] = 'ВАЗ (Lada)';
         $mark = $item['marka'].' '.$item['model'].' '.$item['model_2'];
         if (mb_strlen($mark,'UTF-8')>25)
             $mark = mb_substr($item['marka'], 0, 25,'UTF-8').'...';
     ?>
     <span class="mark-info" style="top: 3px;"><?=$mark;?> <span class="engine-info"><?=$eng;?></span></span></div>

    <?php if ($item['rudder']==1): ?>
        <img class="rudder-item" src="/resources/img/rudder.png">
    <?php endif; ?>
    <?php if ($item['vin']==1): ?>
        <img class="vin-item" src="/resources/img/vin.png">
    <?php endif; ?>
    <?php if ($item['fast']==1): ?>
        <img class="fast-item" src="/resources/img/fast.png">
    <?php endif; ?>
    <?php if ($item['wheel']==1): ?>
        <img class="wheel-item" src="/resources/img/wheel.png">
    <?php endif; ?>
        
    <span class="phone-text"><?=$item['phone'];?></span>
    </nobr>
    </div>

    <div style="width: 90px" class="price td">
        <div class="pos">
            <?php if (!empty($item['price']) && $item['torg']==1): ?>
                <img class="torg-item" src="/resources/img/torg.png"><?php endif; ?><?php if (!empty($item['price'])): ?><?=number_format($item['price'],0,' ',' ');?>
            <?php endif; ?>
        </div>
    </div>
    <?php if ($item['condition']==0): ?>
        <?php if ($item['average_price']==0): ?>
            <div style="width: 70px" class="diff td"><div class="pos"></div></div>
        <?php else: ?>
            <?php if ($item['difference_price']>0): ?>
                <div style="width: 70px" class="diff td"><div class="pos green"><?=number_format($item['difference_price'],0,' ',' ');?></div></div>
            <?php else: ?>
                <div style="width: 70px" class="diff td"><div class="pos red"><?=number_format($item['difference_price'],0,' ',' ');?></div></div>
            <?php endif; ?>
        <?php endif; ?>
    <?php else: ?>
        <div style="width: 70px" class="badcondition td"><img src="/resources/img/bt.png"></div>
    <?php endif; ?>
    <div style="width: 50px" class="year td"><div class="pos"><?=$item['year'];?></div></div>
    <div style="width: 80px" class="run td"><div class="pos"><?=(!empty($item['run'])?number_format($item['run'],0,' ',' '):'');?></div></div>
    <div style="width: 70px" class="td body" data-tooltip="<?=$item['body'];?>"><div class="tooltip"></div>
        <div class="pos">
            <?php if (!empty($item['body_img'])): ?>
                <?=$item['body_img']; ?>
            <?php endif; ?>
        </div>
    </div>
    <div style="width: 30px" class="td color">
        <div class="pos<?=$item['dark']!=1?' white':'';?>"><?=$item['color']; ?></div>
    <!--    <?php if($item['metallic']==1): ?>
            <?php if($item['dark']!=1 || empty($item['color'])): ?>
                <div class="metallic metallic-white">м</div>
            <?php else: ?>
                <div class="metallic">м</div>
            <?php endif; ?>
        <?php endif; ?>-->
    </div>
    <div style="width: 235px" class="region td">
        <?php
            $metr_reg = $item['region'].(mb_strlen($item['metro'], 'UTF-8')?(", м. ".$item['metro']):"");
            if (mb_strlen($metr_reg, 'UTF-8')>25)
                $metr_reg = mb_substr($metr_reg, 0, 25, 'UTF-8').'...';
        ?>
        <div class="pos"><?=$metr_reg;?></div>
    </div>
    <?php if ($item['photo']):?>
        <div style="width: 30px" class="td photos"><div class="pos"><img class="ph" src="/resources/img/ph.png"/></div></div>
    <?php else: ?>
        <div style="width: 30px" class="td photos"></div>
    <?php endif; ?>
    <?php
        $phone_find_color = '#f5f5f5';
    ?>

    <div style="width: 25px; background-color: <?=$phone_find_color;?>" class="phone_find td">
        <div class="pos">
            <?php if ($item['phone_find']<20):?>
                <div class="phonefindsubmit" <?=$item['phone_find']>0?'style="color:#28578b; cursor: pointer;" ':'style="color:#969696" ';?>><?=$item['phone_find'];?></div>
            <?php else: ?>    
                <div class="phonefindsubmitimg"></div>
            <?php endif; ?>
        </div>
    </div>
</a>
<?php endforeach; ?>