<div>
    <label>Марка автомобиля</label>
    <div>
        <select id="brand" name="brand">
            <option value="0">выберите марку</option>
            <?php foreach($this->params['brands'] as $brand): ?>
            <option value="<?=$brand->id;?>"><?=$brand->name;?></option>
            <?php endforeach; ?>
        </select>
    </div>
</div>
<div>
    <label>Модель автомобиля</label>
    <div>
        <select id="model" name="model">
        </select>
    </div>
</div>