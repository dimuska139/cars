<?php $this->title="Размещение объявления"; ?>
<?php
if ($added_phone): ?>
    <script>
        var phone_id = <?=$added_phone->id;?>;
        var contact_phone = "<?=$added_phone->phone;?>";
        var contact_name = "<?=$added_phone->name;?>";
        var confirmed_auto = <?=$added_phone->confirmed_auto;?>;
        var confirmed_avito = <?=$added_phone->confirmed_avito;?>;
        var auto_code = <?=!empty($added_phone->auto_code)?$added_phone->auto_code:"''";?>;
        var avito_code = <?=!empty($added_phone->avito_code)?$added_phone->avito_code:"''";?>;
        var auto_status = "<?=$added_phone->auto_status;?>";
        var avito_status = "<?=$added_phone->avito_status;?>";
    </script>
<?php else: ?>
    <script>
        var phone_id = 0;
        var contact_phone = '';
        var contact_name = '';
        var confirmed_auto = 0;
        var confirmed_avito = 0;
        var auto_code = '';
        var avito_code = '';
        var auto_status = '';
        var avito_status = '';
    </script>
<?php endif; ?>
<div class="add-menu">
    <a href="/add" class="add-button-link" id="add-button-link">
        <div class="button bt_1" id="add">
            <p>Добавить объявление</p>
        </div>
    </a>
    <div class="tab-button tab-active">
        <div class="text">
            <a href="/active">
            Активные
            </a>
        </div>
        <div class="amount">
            <?=(int)$active_count; ?>
        </div>
    </div>
    <?php if ($moderation_count>0): ?>
        <div class="tab-button tab-moderation">
            <div class="text">
                <a href="/moderation">
                На модерации
                </a>
            </div>
            <div class="amount">
                <?=$moderation_count; ?>
            </div>
        </div>
    <?php endif; ?>
    <?php if ($ended_count>0): ?>
    <div class="tab-button tab-ended">
            <div class="text">
                <a href="/deleted">
                Завершённые
                </a>
            </div>
            <div class="amount">
                <?=$ended_count; ?>
            </div>
        </div>
    <?php endif; ?>
</div>
<?php if (empty($enough_balance)): ?>
<div class="warning-panel">
    <img src="/resources/img/redcar.png">
    <div class="title">К сожалению, Вы не можете добавить объявление!</div>
    <div class="text">Недостаточно средств на счету. Вы можете пополнить баланс в разделе "Баланс".<br>
    С уважением, администрация AM97.RU!</div>
</div>
<?php endif; ?>
<div class="page-menu">
    <div class="tab tab-end">
        <div class="text" style="padding-right: 0px;">Завершение</div>
        <img class="indicator" src="/resources/img/okay.svg"/>
    </div>
    <div class="tab tab-contacts">
        <div class="text">Контакты</div>
        <img class="indicator" src="/resources/img/okay.svg"/>
    </div>
    <div class="tab tab-price">
        <div class="text">Цена и комментарий</div>
        <img class="indicator" src="/resources/img/okay.svg"/>
    </div>
    <div class="tab tab-photos">
        <div class="text">Фотографии</div>
        <img class="indicator" src="/resources/img/okay.svg"/>
    </div>
    <div class="tab active tab-car">
        <div class="text">Автомобиль</div>
        <img class="indicator" src="/resources/img/okay.svg"/>
    </div>
</div>  
<?php function mb_ucfirst($string, $encoding='UTF-8')
{
    $firstChar = mb_substr($string, 0, 1, $encoding);
    $then = mb_substr($string, 1, mb_strlen($string, $encoding)-1, $encoding);
    return mb_strtoupper($firstChar, $encoding) . $then;
} ?>
<form action="/add" method="post" id="createform">
    <?php if ($added_phone): ?>
        <input type="hidden" id="phone_id" name="phone_id" value="<?=$added_phone->id;?>">
    <?php else: ?>
        <input type="hidden" id="phone_id" name="phone_id">
    <?php endif; ?>
    
    <div class="page-create page-car">
        <div class="form-line">
            <img class="indicator" src="/resources/img/okay.svg"/>
            <label>Марка</label>
            <select id="brand" name="brand">
                <option value="0"></option>
                <?php foreach($brands as $brand): ?>
                <option value="<?=$brand->id;?>"><?=$brand->name;?></option>
                <?php endforeach; ?>
            </select>
        </div>
        <div class="model-container form-line">
            <img class="indicator" src="/resources/img/okay.svg"/>
            <label>Модель</label>
            <select id="model" name="model">
            </select>
            <input type="text" name="another_model" id="another_model" placeholder="Название модели">
        </div>
        <div class="form-line">
            <img class="indicator" src="/resources/img/okay.svg"/>
            <label>Год выпуска</label>
            <select id="year" name="year">
                <option value="0"></option>
                <?php
                    $currentDate = new \DateTime();
                    $currentYear = (int)$currentDate->format('Y');
                ?>
                <?php for($i=$currentYear;$i>=1990;$i--): ?>
                <option value="<?=$i;?>"><?=$i;?></option>
                <?php endfor; ?>
            </select>
        </div>
        <div class="form-line">
            <img class="indicator" src="/resources/img/okay.svg"/>
            <label>Состояние</label>
            <select id="condition" name="condition">
                <?php foreach($conditions as $num => $condition): ?>
                <option value="<?=$num;?>"><?=mb_ucfirst($condition);?></option>
                <?php endforeach; ?>
            </select>
        </div>
        <div class="form-line">
            <img class="indicator" src="/resources/img/okay.svg"/>
            <label>Тип кузова</label>
            <div class="body_type">
                <?php $column_counter = 1; 
                foreach ($body_types as $type): ?>
                <div class="col col-<?=$column_counter; ?>">
                    <div style="background-image: url('/resources/img/body/<?=$type->img_name;?>')" data-tooltip="<?=(!empty($type->shortname)?mb_ucfirst($type->shortname):mb_ucfirst($type->name));?>" class="item" data-id="<?=$type->id;?>" data-img="<?=$type->img_name;?>">
                        <div class="text"><?=(!empty($type->shortname)?mb_ucfirst($type->shortname):mb_ucfirst($type->name));?></div>
                        <div class="tooltip"></div>
                    </div>
                </div>
                <?php
                    $column_counter == 4?$column_counter = 1:$column_counter++;
                    endforeach;
                ?>
            </div>
            <input type="hidden" id="body_type" name="body_type" value="0">
        </div>
        <div class="form-line">
            <img class="indicator" src="/resources/img/okay.svg"/>
            <label>Пробег</label>
            <input type="text" name="run" id="run" maxlength="9">км
        </div>
        <div class="form-line">
            <img class="indicator" src="/resources/img/okay.svg"/>
            <label>Цвет</label>
            <div class="colors">
                <?php foreach($colors as $color): ?>
                <div class="color-item" data-value="<?=$color->id;?>" data-text="<?=mb_ucfirst($color->name);?>">
                    <div class="color<?=$color->dark==0?" white":"";?>" style="
                        <?php
                        if (empty($color->gradient_hex_from) || empty($color->gradient_hex_to)):?>
                            background-color: #<?=$color->hex;?>
                        <?php else: ?>
                            background: linear-gradient(to bottom, #<?=$color->gradient_hex_from;?>, #<?=$color->gradient_hex_to;?>);
                        <?php endif; ?>
                    ">
                    </div>
                </div>
                <?php endforeach; ?>
            </div>
            <input id="color" type="hidden" name="color" value="0">
        </div>
        <div class="form-line">
            <img class="indicator" src="/resources/img/okay.svg"/>
            <label>Тип двигателя</label>
            <div class="engine_type">
                <?php foreach($engine_types as $num => $type): ?>
                <div class="item" data-id="<?=$num;?>"><?=$type;?></div>
                <?php endforeach; ?>
            </div>
            <input type="hidden" id="engine_type" name="engine_type" value="0">
        </div>
        <div class="form-line">
            <img class="indicator" src="/resources/img/okay.svg"/>
            <label>Объем двигателя</label>
            <select id="engine_volume" name="engine_volume">
                <option value="0"></option>
                <?php for($i=0.1;$i<=6.0;$i+=0.1): ?>
                <option value="<?=$i;?>"><?=number_format($i, 1, '.', ' ');?></option>
                <?php endfor; ?>
            </select>л
        </div>
        <div class="form-line">
            <img class="indicator" src="/resources/img/okay.svg"/>
            <label>Мощность двигателя</label>
            <input type="text" name="engine_power" id="engine_power" maxlength="3">л.с.
        </div>
        <div class="form-line">
            <img class="indicator" src="/resources/img/okay.svg"/>
            <label>Коробка передач</label>
            <div class="transmission_type">
                <?php foreach($transmission_types as $num => $type): ?>
                <div class="item" data-id="<?=$num;?>"><?=$type['second_name'];?></div>
                <?php endforeach; ?>
            </div>
            <input type="hidden" id="transmission_type" name="transmission_type" value="0">
        </div>
        <div class="form-line">
            <img class="indicator" src="/resources/img/okay.svg"/>
            <label>Привод</label>
            <div class="drive_type">
                <?php foreach($drive_types as $num => $type): ?>
                <div class="item" data-id="<?=$num;?>"><?=$type;?></div>
                <?php endforeach; ?>
            </div>
            <input type="hidden" id="drive_type" name="drive_type" value="0">
        </div>
        <div class="form-line">
            <img class="indicator" src="/resources/img/okay.svg"/>
            <label>Руль</label>
            <div class="rudder">
                <?php foreach($rudders as $num => $rudder): ?>
                <div class="item<?=$num==0?" active":"";?>" data-id="<?=$num;?>"><?=$rudder;?></div>
                <?php endforeach; ?>
            </div>
            <input type="hidden" id="rudder" name="rudder" value="0">
        </div>
        <?php if (!empty($enough_balance)): ?>
        <div class="next">Далее →</div>
        <?php endif; ?>
    </div>
    <div class="page-create page-photos">
        <div class="info">
            Доступна загрузка не более 10 фотографий. Запрещается размещение на фото контактной информации, а также ретушь с целью выделения фото среди других.<br>
			<font color="red">Не загружайте фотографии, которые были опубликованы ранее!</font> Администрация сайта не несет ответственности за блокировку объявления в результате повторной подачи!
        </div>
        <div class="uploaded">
            <div class="camera_container">
                <img src="/resources/img/camera.svg">
                <div class="progress"></div>
                <input id="fileupload" type="file" name="photos[]" data-url="/ajax/photoupload" multiple accept="image/jpeg,image/png">
            </div>
        </div>
        <div class="prev">← Назад</div>
        <div class="pass">Пропустить</div>
        <div class="next">Далее →</div>
    </div>
    <div class="page-create page-price-and-comment">
        <div class="form-line">
            <img class="indicator" src="/resources/img/okay.svg"/>
            <label>Цена</label>
            <input type="text" name="price" id="price" maxlength="9">руб.
        </div>
        <div class="form-line">
            <img class="indicator" src="/resources/img/okay.svg"/>
            <label>Комментарий</label>
            <textarea name="comment" id="comment"></textarea>
            <div class="info">
                Запрещается давать ссылки, указывать адреса электронной почты, телефоны, цену, предлагать услуги, не связанные с продажей этого авто.<br>
				<font color="red">Не копируйте комментарии с других объявлений!</font><br>
				Не менее 25 символов!
            </div>
        </div>
        <div class="prev">← Назад</div>
        <div class="next">Далее →</div>
    </div>
    <div class="page-create page-contacts">
        <div class="form-line">
            <img class="indicator" src="/resources/img/okay.svg"/>
            <label>Место осмотра</label>
			<input type="text" name="inspection-place" id="inspection-place" placeholder="Не указывать"><font color="#969696">Город, район или станция метро.</font>
        </div>
        <div class="form-line">
            <img class="indicator" src="/resources/img/okay.svg"/>
            <label>Контактное лицо</label>
            <input type="text" name="contact" id="contact">
        </div>
        <div class="form-line">
            <img class="indicator" src="/resources/img/okay.svg"/>
            <label>Телефон</label>
            <input type="text" name="phone" id="phone" maxlength="14">
            <img src="/resources/img/ajax-loader.gif" id="send-sms-ajax-loader" class="ajax-loader">
            <span id="change_phone">Изменить</span>
            <span class="confirm">Подтвердить</span>
        </div>
    <!--    <div class="form-line repeat-sms">
            <span id="repeat-sms-text">Выслать код повторно</span>
        </div> -->
        <div class="form-line phone-error"></div>
        
        
    <!--    <div id="registration_repeat">Отправить код заново</div> -->
        <div class="form-line confirm-code-auto-place">
            <img class="indicator" src="/resources/img/okay.svg"/>
            <label>Подтверждение Auto.ru</label>
            <input disabled="disabled" type="text" name="autoru_code" id="autoru_code" maxlength="4" placeholder="Код из смс">
            <img src="/resources/img/ajax-loader.gif" id="send-code-auto-ajax-loader" class="ajax-loader">
            <span id="autoru_code_error"></span>
            <span id="autoru_code_get">Получить код</span>
            <span id="autoru_code_confirm">Подтвердить</span>
            <span id="autoru_timer">Выслать еще раз через: <span class="value"></span>с</span>
        </div>
        <div class="form-line confirm-code-avito-place">
            <img class="indicator" src="/resources/img/okay.svg"/>
            <label>Подтверждение Avito.ru</label>
            <input disabled="disabled" type="text" name="avitoru_code" id="avitoru_code" maxlength="5" placeholder="Код из смс">
            <img src="/resources/img/ajax-loader.gif" id="send-code-avito-ajax-loader" class="ajax-loader">
            <span id="avitoru_code_error"></span>
            <span id="avitoru_code_get">Получить код</span>
            <span id="avitoru_code_confirm">Подтвердить</span>
            <span id="avitoru_timer">Выслать еще раз через: <span class="value"></span>с</span>
        </div>
        <div class="form-line code-error"></div>
        <div class="prev">← Назад</div>
        <div class="next">Далее →</div>
    </div>
    <div class="page-create page-end">
        <p style="margin-bottom: 10px; color: #969696;">Проверьте объявление. Если всё правильно, нажмите на кнопку "Подтвердить".</p>
        <div class="end-fields">
            <div class="form-line">
                <label>Марка</label>
                <div id="check_brand"></div>
            </div>
            <div class="form-line">
                <label>Модель</label>
                <div id="check_model"></div>
            </div>
            <div class="form-line">
                <label>Год выпуска</label>
                <div id="check_year"></div>
            </div>
            <div class="form-line">
                <label>Состояние</label>
                <div id="check_state"></div>
            </div>
            <div class="form-line">
                <label>Ти кузова</label>
                <div id="check_body"></div>
            </div>
            <div class="form-line">
                <label>Пробег</label>
                <div id="check_run"></div>
            </div>
            <div class="form-line">
                <label>Цвет</label>
                <div id="check_color"></div>
            </div>
            <div class="form-line">
                <label>Тип двигателя</label>
                <div id="check_engine_type"></div>
            </div>
            <div class="form-line">
                <label>Объем двигателя</label>
                <div id="check_engine_volume"></div>
            </div>
            <div class="form-line">
                <label>Мощность двигателя</label>
                <div id="check_engine_power"></div>
            </div>
            <div class="form-line">
                <label>Коробка передач</label>
                <div id="check_transmission_type"></div>
            </div>
            <div class="form-line">
                <label>Привод</label>
                <div id="check_drive_type"></div>
            </div>
            <div class="form-line">
                <label>Руль</label>
                <div id="check_rudder"></div>
            </div>
            
            
            
            <br>
            
            <div class="form-line">
                <label>Место осмотра</label>
                <div id="inspection-place"></div>
            </div>
            <div class="form-line">
                <label>Контактное лицо</label>
                <div id="check_contact"></div>
            </div>
            <div class="form-line">
                <label>Телефон</label>
                <div id="check_phone"></div>
            </div>
            
            <br>
            <div class="form-line">
                <label>Цена</label>
                <div id="check_price"></div>
            </div>
            <div class="form-line">
				<label>Комментарий</label>
                <div id="check_comment"></div> 
            </div>
            
            <input type="hidden" name="_csrf" value="<?=Yii::$app->request->getCsrfToken();?>">
            
        </div>
        <div class="result-photos">
        </div>
        <div class="ctrl-btns">
            <div class="prev">← Назад</div>
            <button id="final" class="next">Подтвердить</button>
        </div>
    </div>
</form>
