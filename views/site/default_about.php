<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\captcha\Captcha;
/* @var $this yii\web\View */
$this->title = 'About';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-about">
    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        This is the About page. You may modify the following file to customize its content:
    </p>

    <code><?= __FILE__ ?></code>
    <?php foreach ($sngs as $sng): ?>
        <?php echo $sng->color->create_date; ?>
    <br>Материалы:<br>
        <?php foreach ($sng->getMaterials()->all() as $material): ?>
            <?=$material->name;?><br>
        <?php endforeach; ?>
        <?php /*echo $sng->getColor()->one()->name; */?><br>
    <?php endforeach; ?>
        <?php $form = ActiveForm::begin([
                    'id' => 'review-form',
                ]); ?>
        
                <?= $form->field($review_model, 'good_id')->hiddenInput()->label(false) ?>
                <?= $form->field($review_model, 'user_name') ?>

                <?= $form->field($review_model, 'user_email')->input('email') ?>

                <?= $form->field($review_model, 'review')->passwordInput() ?>

                <?= $form->field($review_model, 'verifyCode')->widget(Captcha::className(), [
        'template' => '<div class="row"><div class="col-lg-3">{image}</div><div class="col-lg-6">{input}</div></div>',
    ]) ?>

                <?= Html::submitButton('OK', ['class' => 'btn btn-success btn-block']) ?>

            <?php ActiveForm::end(); ?>
</div>
