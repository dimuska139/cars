<?php $this->title="Редактирование объявления"; ?>
<div class="page-menu">
    <div class="tab tab-end">
        <div class="text">Завершение</div>
    </div>
    <div class="tab tab-edit">
        <div class="text">Редактирование</div>
    </div>
</div>
<div class="model-container">
    <b><?=$brandmodel;?></b> &mdash; Редактирование объявления
</div>

<?php if (!empty($main_photo)):?>
<div class="created-photo-place">
    <img src="/<?=$main_photo;?>">
</div>
<?php endif; ?>
<div class="info">
    Ваш автомобиль: <?=$brandmodel;?><br>
    Номер телефона: <?=$phone;?><br>
    Тип продажи: Обычная продажа<br>
    Успешно отредактирован и отправлен на модерацию!<br><br>
    
    Благодарим за использование нашего сервиса!<br>
    Вы можете перейти на <a href="/">главную страницу</a>.
</div>