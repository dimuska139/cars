<?php if (!empty($info)): ?>
<div class="byphonesearch_list_header">
    <div class="time">дата</div>
    <div class="car">автомобиль (двигатель кпп)</div>
    <div class="price">цена, р.</div>
    <div class="year">год</div>
    <div class="run">пробег, км</div>
    <div class="body">кузов</div>
    <div class="region">регион</div>
</div>      
<div class="byphonesearch_list_body">
<?php 
function mb_ucfirst($string, $encoding='UTF-8')
{
    $firstChar = mb_substr($string, 0, 1, $encoding);
    $then = mb_substr($string, 1, mb_strlen($string, $encoding)-1, $encoding);
    return mb_strtoupper($firstChar, $encoding) . $then;
}

?>
<?php foreach ($info as $num => $ads):?>
<div class="line">
    <div class="time"><?=date("H:i d.m.Y", strtotime($ads['dt']));?></div>
    <div class="name-engine"><span class="name">
            <?php             
                if (strcmp(mb_convert_encoding($ads['marka'], 'utf-8'), mb_convert_encoding('Лада', 'utf-8'))==0)
                       $ads['marka'] = 'ВАЗ (Lada)';
                $mark = $ads['marka'].' '.$ads['model'].' '.$ads['model_2'];
                if (strlen($mark)>25)
                    $mark = substr($mark, 0, 25).'...';
            ?>
        <?=$mark."</span> (".$ads['enginevol'].(!empty($ads['engine'])?mb_ucfirst($ads['engine'][0]):'')." ".$ads['transmission'].")";?>
    </div>
    <div class="price"><?=number_format($ads['price'],0,' ',' ');?></div>
    <div class="year"><?=$ads['year'];?></div>
    <div class="run"><?=number_format($ads['run'],0,' ',' ');?></div>
    <div class="body"><?=$ads['body'];?></div>
    <div class="region">
        <?php
            $reg = $ads['region'];
            if (strlen($reg)>25)
                $reg = mb_substr($reg, 0, 25).'...';
        ?>
        <?=$reg;?>
    </div>
    <?php if (!empty($ads['photo'])):?>
        <div class="previews">
        <?php $photos = explode(',', $ads['photo']);?>
        <?php $i=0; while ($i<10 && $i<count($photos)){
            if (mb_strpos($photos[$i], '.jpg')){
                 $check_url = get_headers($photos[$i]); 
                 if (strpos($check_url[0],'200')){
            ?>
            <a class="photo-preview-container" href="<?=$photos[$i];?>" target="_blank"><img class="photo-preview" src="<?=$photos[$i];?>"></a>
            <?php
                 } else break;
            }
            $i++;
        } ?>
        </div>
    <?php endif; ?>
</div>
<?php endforeach; ?>
<?php endif; ?>
</div>