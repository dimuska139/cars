<?php $this->title="Создание объявления"; ?>
<div class="page-menu">
    <div class="tab tab-end">
        <div class="text">Завершение</div>
        <img class="indicator_show" src="/resources/img/okay.png"/>
    </div>
    <div class="tab tab-contacts">
        <div class="text">Контакты</div>
        <img class="indicator_show" src="/resources/img/okay.png"/>
    </div>
    <div class="tab tab-price">
        <div class="text">Цена и комментарий</div>
        <img class="indicator_show" src="/resources/img/okay.png"/>
    </div>
    <div class="tab tab-photos">
        <div class="text">Фотографии</div>
        <img class="indicator_show" src="/resources/img/okay.png"/>
    </div>
    <div class="tab tab-car">
        <div class="text">Автомобиль</div>
        <img class="indicator_show" src="/resources/img/okay.png"/>
    </div>
</div>

<?php if (!empty($main_photo)):?>
<div class="created-photo-place">
    <img src="/<?=$main_photo;?>">
</div>
<?php endif; ?>
<div class="info">
    Ваш автомобиль: <?=$brandmodel;?><br>
    Номер телефона: <?=$phone;?><br>
    Тип продажи: Обычная продажа<br>
    Успешно отправлен на модерацию!<br><br>
<!--    В ближайшее время с Вами свяжется наш менеджер для подтверждения телефона.<br>
    Номер Вашего объявления: <b><?=$ad_id;?></b><br><br>
    Благодарим за использование нашего сервиса!<br>
    Вы можете перейти на <a href="/">главную страницу</a> или <a href="/announcement/create">добавить новое объявление</a>.-->
</div>