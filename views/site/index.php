<?php $this->title="Мои объявления"; ?>
<script>
    var moderation = <?php if (!empty($_GET['value']) && $_GET['value']=='moderation'): ?>true<?php else: ?>false<?php endif; ?>;
</script>
<div class="add-menu">
    <a href="/add" class="add-button-link" id="add-button-link">
        <div class="button bt_1" id="add">
            <p>Добавить объявление</p>
        </div>
    </a>
    <div class="tab-button tab-active tab-button-active">
        <div class="text">
            <span>
            Активные
            </span>
        </div>
        <div class="amount">
            <?=(int)$active_count; ?>
        </div>
    </div>
    <?php if ($moderation_count>0): ?>
        <div class="tab-button tab-moderation">
            <div class="text">
                <a href="/moderation">
                На модерации
                </a>
            </div>
            <div class="amount">
                <?=$moderation_count; ?>
            </div>
        </div>
    <?php endif; ?>
    <?php if ($ended_count>0): ?>
    <div class="tab-button tab-ended">
            <div class="text">
                <a href="/deleted">
                Завершённые
                </a>
            </div>
            <div class="amount">
                <?=$ended_count; ?>
            </div>
        </div>
    <?php endif; ?>
</div>
<?php if ($active_count>0): ?>
<div class="place active-place">
    <div class="list">
    <?php
    $column_counter = 1;
    foreach ($active as $number => $item): ?>
        <div class="col col-<?=$column_counter; ?>">
            <div class="item" data-id="<?=$item->id;?>">
                <?php
                    $photo = $item->photo;
                    $img = "/resources/img/nophoto.png";
                    if ($photo && file_exists($photo->url))
                        $img = $photo->url;
                    if ($photo && !empty($photo->thumb) && file_exists($photo->thumb))
                        $img = $photo->thumb;
                ?>
                <div class="logo" style="background: url('<?=$img; ?>') no-repeat center; background-size: cover;">
					<a href="/update?id=<?=$item->id;?>"><img data-id="<?=$item->id;?>" class="update" src="/resources/img/update.png"/></a>
                    <a href="/edit?id=<?=$item->id;?>"><img data-id="<?=$item->id;?>" class="edit" src="/resources/img/edit.png"/></a>
                    <img data-id="<?=$item->id;?>" class="delete" src="/resources/img/close.png"/>
                    <?php if (!empty($item->autoru_url)): ?>
                        <a class="autoru_url_a" target="_blank" href="<?=$item->autoru_url;?>">
                            <img class="autoru_url" src="/resources/img/autoru.svg"/>
                        </a>
                    <?php endif; ?>
                    <?php if (!empty($item->avitoru_url)): ?>
                        <a class="avitoru_url_a" target="_blank" href="<?=$item->avitoru_url;?>">
                            <img class="avitoru_url" src="/resources/img/avitoru.svg"/>
                        </a>
                    <?php endif; ?>
                </div>
                <div class="info">
                    <div class="mark-and-model">
                        <?=$item->model->brand->name;?> <?=$item->model->name;?>
                        <div class="fade"></div>
                    </div>
                    <div class="year-run"><?=$item->year;?>, <?=number_format($item->run, 0, '.', ' ');?> км</div>
                    <div class="color-bodytype"><?=$item->body->name?>, <?=$item->color->name;?>
                        <?php
                        $str = $item->body->name;
                        $enc = 'UTF-8';?>
                    </div>
                    <div class="v-box-power">
                        <?=number_format($item->engine_volume, 1, '.', ' ');?> <?=$transmission_types[$item->transmission_type]['shortname'];?> (<?=$item->engine_power;?> л.с.)
                    </div>
                    <div class="eng-type-dr-type">
                        <?=$engine_types[$item->engine_type];?>, <?=mb_strtolower($drive_types[$item->drive_type], $enc);?>
                    </div>
					<div class="price"><font style="font-weight: bold; font-size: 11pt;"><?=number_format($item->price, 0, '.', ' ');?></font> руб.</div>
                    <div class="phone">
                        <?=$item->phone; ?>, <?=$item->contact; ?>
                    </div>
                    <div class="status">
						<?php
                            $createdate = new Datetime($item->createdate);
                            $enddate = clone $createdate;
                            $enddate->add(new DateInterval("P".Yii::$app->params['MAX_BULLETIN_LIFETIME']."D"));
                            $curdate = new Datetime();
                            $ost = $curdate->diff($enddate)->format("%d");
                            $last_char = $ost{strlen($ost)-1}; // Посл. символ
                            $day = "";
                            $left = "Осталось";
                            if (in_array($last_char, ["0","5","6","7","8","9"]))
                                $day = "дней";
                            else if ($last_char=="1"){
                                $day = "день";
                                $left = "Остался";
                            } else if (in_array($last_char, ["2","3","4"]))
                                $day = "дня";
                        ?>
                        <?=$left." ".$ost." ".$day; ?>
                    </div>
                </div>
            </div>
        </div>
    <?php
    $column_counter == 5?$column_counter = 1:$column_counter++;
    endforeach;
    ?> 
    </div>
</div>
<?php else: ?>
<div class="warning-panel">
    <img src="/resources/img/graycar.png">
    <div class="title">К сожалению, у Вас нет активных объявлений!</div>
    <div class="text">Вы можете добавить новое объявление с помощью кнопки выше.<br>
    С уважением, администрация AM97.RU!</div>
</div>
<?php endif; ?>