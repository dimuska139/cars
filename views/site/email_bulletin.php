<?php use yii\helpers\Url; ?>
<table>
    <tr>
        <td>
            <b>Автомобиль</b>
        </td>
    </tr>
    <tr>
        <td>
            Марка
        </td>
        <td>
            <?=$brand->name;?>
        </td>
    </tr>
    <tr>
        <td>
            Модель
        </td>
        <td>
            <?=$model->name;?>
        </td>
    </tr>
    <tr>
        <td>
            <br />
        </td>
    </tr>
    <tr>
        <td>
            Год выпуска
        </td>
        <td>
            <?=$car->year;?>
        </td>
    </tr>
    <tr>
        <td>
            Состояние
        </td>
        <td>
            <?=$condition;?>
        </td>
    </tr>
    <tr>
        <td>
            Тип кузова
        </td>
        <td>
            <?=$body->name;?>
        </td>
    </tr>
    <tr>
        <td>
            Пробег
        </td>
        <td>
            <?=$car->run;?>
        </td>
    </tr>
    <tr>
        <td>
            Цвет
        </td>
        <td>
            <?=$color->name;?>
        </td>
    </tr>
    <tr>
        <td>
            <br />
        </td>
    </tr>
    <tr>
        <td>
            Тип двигателя
        </td>
        <td>
            <?=$engine_type;?>
        </td>
    </tr>
    <tr>
        <td>
            Объём
        </td>
        <td>
            <?=$car->engine_volume;?>
        </td>
    </tr>
    <tr>
        <td>
            Мощность
        </td>
        <td>
            <?=$car->engine_power;?>
        </td>
    </tr>
    <tr>
        <td>
            Коробка
        </td>
        <td>
            <?=$transmission_type['name'];?>
        </td>
    </tr>
    <tr>
        <td>
            Привод
        </td>
        <td>
            <?=$drive_type;?>
        </td>
    </tr>
    <tr>
        <td>
            Руль
        </td>
        <td>
            <?=$rudder;?>
        </td>
    </tr>
    <?php if (!empty($photos)): ?>
    <tr>
        <td>
            <br />
        </td>
    </tr>
    <tr>
        <td>
            <b>Фотографии</b>
        </td>
    </tr>
    <?php foreach ($photos as $photo): ?>
    <tr>
        <td colspan="2">
            <img src="http://am97.ru/<?=$photo->url;?>"/>
            <br />
            <a href="http://am97.ru/<?=$photo->url;?>">http://am97.ru/<?=$photo->url;?></a>
        </td>
    </tr>
    <?php endforeach; ?>
    <?php endif; ?>
    <tr>
        <td>
            <br />
        </td>
    </tr>
    <tr>
        <td>
            <b>Цена и комментарий</b>
        </td>
    </tr>
    <tr>
        <td>
            Цена
        </td>
        <td>
            <?=$car->price;?>
        </td>
    </tr>
    <tr>
        <td>
            Комментарий
        </td>
        <td>
            <?=$car->description;?>
        </td>
    </tr>
    <tr>
        <td>
            <br />
        </td>
    </tr>
    <tr>
        <td>
            <b>Контакты</b>
        </td>
    </tr>
    <tr>
        <td>
            Город
        </td>
        <td>
            <?=$city->name;?>
        </td>
    </tr>
    <tr>
        <td>
            Метро
        </td>
        <td>
            <?=!empty($subway->name)?$subway->name:"Не указано";?>
        </td>
    </tr>
    <tr>
        <td>
            Контактное лицо
        </td>
        <td>
            <?=$car->contact;?>
        </td>
    </tr>
    <tr>
        <td>
            Телефон
        </td>
        <td>
            <?=preg_replace('/\s/', '', $car->phone);?>
        </td>
    </tr>
    <tr>
        <td>
            <br />
        </td>
    </tr>
    <tr>
        <td>
            <b>Данные для входа Avito</b>
        </td>
    </tr>
    <?php if (!empty($phone)): ?>
    <tr>
        <td>
            Email
        </td>
        <td>
            <?=$phone->avito_login;?>
        </td>
    </tr>
    <tr>
        <td>
            Телефон
        </td>
        <td>
            <?=preg_replace('/\s/', '', $phone->phone);?>
        </td>
    </tr>
    <tr>
        <td>
            Пароль
        </td>
        <td>
            <?=$phone->avito_pass;?>
        </td>
    </tr>
    <tr>
        <td>
            <br />
        </td>
    </tr>
    <tr>
        <td>
            <b>Данные для входа Auto</b>
        </td>
    </tr>
    <tr>
        <td>
            Email
        </td>
        <td>
            <?=$phone->auto_login;?>
        </td>
    </tr>
    <tr>
        <td>
            Телефон
        </td>
        <td>
            <?=preg_replace('/\s/', '', $phone->phone);?>
        </td>
    </tr>
    <tr>
        <td>
            Пароль
        </td>
        <td>
            <?=$phone->auto_pass;?>
        </td>
    </tr>
    <?php else: ?>
        <tr>
        <td>
            Email
        </td>
        <td>
            
        </td>
    </tr>
    <tr>
        <td>
            Телефон
        </td>
        <td>
            
        </td>
    </tr>
    <tr>
        <td>
            Пароль
        </td>
        <td>
            
        </td>
    </tr>
    <tr>
        <td>
            <br />
        </td>
    </tr>
    <tr>
        <td>
            <b>Данные для входа Auto</b>
        </td>
    </tr>
    <tr>
        <td>
            Email
        </td>
        <td>
            
        </td>
    </tr>
    <tr>
        <td>
            Телефон
        </td>
        <td>
            
        </td>
    </tr>
    <tr>
        <td>
            Пароль
        </td>
        <td>
            
        </td>
    </tr>
    <?php endif; ?>
    <tr>
        <td>
            <br />
        </td>
    </tr>
</table>