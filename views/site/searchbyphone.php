<?php $this->title="Поиск объявлений по номеру телефона"; ?>
<link rel="stylesheet" type="text/css" href="//fonts.googleapis.com/css?family=Open+Sans" />
<script>
    var enough_balance = true;
</script>
<div class="search_block byphonesearch">
    <div class="search">
        <div class="transition-and-phone-search">
        <!--    <div class="online">
                <a href="/online">Online</a>
            </div>
            <div class="month">
                За месяц
            </div>
            <div class="transition">
                Переходы
            </div>-->
            <div class="phone-search">
                <form id="searchbyphone-form" action="/search" method="post">
                <span class="label">Поиск объявлений по номеру телефона</span>
                <input type="text" placeholder="7XXXXXXXXXXX" id="phone-number" class="phone-number" value="<?=$phone;?>" name="phone">
                <img src="/resources/img/ajax-loader.gif" id="checkphone-ajax-loader" class="ajax-loader">
                
                <input type="submit" class="find" value="Найти">
                </form>
            </div>
            <span class="ads_amount">Найдено объявлений: <?=count($info); ?></span>
            <span class="error">
                    Объявлений не найдено!
                </span>
        </div>
     <div class="byphonesearch-table">
        <?php echo \Yii::$app->view->renderFile('@app/views/site/byphonesearchlist.php', [
            'info' => $info
        ]); ?>
     </div>
    </div>
</div>