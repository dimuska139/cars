<?php $this->title="Поиск тс за месяц"; ?>
<script>
    var sounds = <?=$user->search_sounds==1?'true':'false';?>;
    var enough_balance = <?=$enough_balance?'true':'false';?>;
    var search_pagesize = <?=$user->search_pagesize;?>;
</script>
<audio>
    <source src="/resources/sounds/tock.wav"></source>
</audio>
<link rel="stylesheet" type="text/css" href="//fonts.googleapis.com/css?family=Open+Sans" />
<div class="search_block">
    <div class="search">
            <?php
                    if(isset($_POST['filter'])){
                        setcookie("filter", serialize($_POST['filter']));
                        $filter=$_POST['filter'];
                    } else {
                        if (!empty($_COOKIE['filter']))
                            $filter=unserialize($_COOKIE['filter']);
                        else 
                            $filter = [];
                    }

            ?>
<div class="add-menu">
    <div class="tab-button tab-active">
        <div class="text">
            <a href="/online">
            Online
            </a>
        </div>
    </div>
        <div class="tab-button tab-moderation tab-button-active">
            <div class="text">
                <span>
                За месяц
                </span>
            </div>
        </div>
	<?php if (!empty($history_amount)): ?>
    <div class="tab-button tab-ended">
            <div class="text">
                <a href="/clicks">
                Переходы
                </a>
            </div>
        </div>
	<?php endif; ?>
	<div class="phone-search">
         <form id="searchbyphone-form" action="/search" method="post" target="_blank">
              <span class="label">Поиск объявлений по номеру телефона</span>
                  <input type="text" placeholder="Телефон" id="phone-number" name="phone" class="phone-number" value="">
                  <img src="/resources/img/ajax-loader.gif" id="checkphone-ajax-loader" class="ajax-loader">
                  <input type="submit" class="find" value="Найти">
         </form>
     </div>
     <span class="error">
        Объявлений не найдено!
     </span>
<!--     <div class="filter-state">
        Показать фильтр
     </div>-->
</div>
    <div class="warning-panel" <?=$enough_balance?'style="display: none"':'';?>>
        <img src="/resources/img/redcar.png">
        <div class="title">К сожалению, Вам недоступны платные действия!</div>
        <div class="text">Недостаточно средств на счету. Вы можете пополнить баланс в разделе "Баланс".<br>
        С уважением, администрация AM97.RU!</div>
    </div>
     <div class="ads-table">
            <div class="filters month-filters">
                <div><!-- method="post" -->
                        <div class="column column-1">
                            <div class="filter-line">
                                <input id="price_from" type="text" name="filter[price_s]" placeholder="Цена от, руб." value="<?=@$_COOKIE['price_from'];  ?>">
                                <input id="price_to" type="text" name="filter[price_f]" placeholder="до" value="<?=@$_COOKIE['price_to'];  ?>">
                            </div>
                            <div class="filter-line">
                                <input id="run_from" type="text" name="filter[run_s]" placeholder="Пробег от, км" value="<?=@$_COOKIE['run_from'];  ?>">
                                <input id="run_to" type="text" name="filter[run_f]" placeholder="до" value="<?=@$_COOKIE['run_to'];  ?>">
                            </div>
                        </div>
                        <div class="column column-2">
                            <div class="filter-line">
                                <select name="year_from" id="year_from">
                                    <option<?=(isset($_COOKIE['year_from']) && (string)$_COOKIE['year_from']=='0')?' selected':''; ?> value="0">Год от</option>
                                    <?php for ($i=date('Y');$i>=1990;$i--): ?>
                                    <option<?=(isset($_COOKIE['year_from']) && (string)$i==(string)$_COOKIE['year_from'])?' selected':''; ?> value="<?=$i;?>"><?=$i;?></option>
                                    <?php endfor; ?>
                                </select>
                                <select name="year_to" id="year_to">
                                    <option<?=(isset($_COOKIE['year_to']) && (string)$_COOKIE['year_to']=='0')?' selected':''; ?> value="0">до</option>
                                    <?php for ($i=date('Y');$i>=1990;$i--): ?>
                                    <option<?=(isset($_COOKIE['year_to']) && (string)$i==(string)$_COOKIE['year_to'])?' selected':''; ?> value="<?=$i;?>"><?=$i;?></option>
                                    <?php endfor; ?>
                                </select>
                            </div>
                            
                            <div class="filter-line">
                                <select id="body-filter" multiple="multiple">
                                <?php
                                    $result=$sql->query("select `Id`, `name` from `body` where `published`=1 order by `name`");
                                    if($result->num_rows){
                                        $counter = 0;
                                        while($row=$result->fetch_array(MYSQLI_ASSOC)){
                                            echo '<option value="'.$row['Id'].'" '.(!empty($filter['body']) && in_array($row['Id'], $filter['body'])?'selected="selected"':'').'>'.$row['name'].'</option>';
                                      //      echo '<option type="checkbox" name="filter[body]['.$counter.']" value="'.$row['Id'].'" '.(isset($filter['body'][$row['Id']])?'selected="selected"':'').'>'.$row['name'].'</option>';
                                            $counter++;
                                        }
                                    }
                                ?>
                                </select>
                                <div id="body-filter-ids">
                                    <?php if (!empty($_COOKIE['body_filter'])): ?>
                                        <?php
                                            $items = json_decode($_COOKIE['body_filter']);
                                            foreach($items as $ind => $tr): ?>
                                            <input type="hidden" class="body-ids" name="filter[body][<?=$ind;?>]" value="<?=$tr;?>">
                                        <?php endforeach; ?>
                                    <?php endif; ?>
                                </div>
                            </div>
                        </div>
                    
                    <div class="column column-3">
                        <div class="filter-line">
                            <select id="engine-filter">
                                <option value="0">Двигатель</option>
                                <option value="1" <?php if (!empty($filter['engine']) && $filter['engine']==1): ?>selected="selected"<?php endif; ?>>Бензин</option>
                                <option value="2" <?php if (!empty($filter['engine']) && $filter['engine']==2): ?>selected="selected"<?php endif; ?>>Дизель</option>
                            </select>
                        </div>
                        <div class="filter-line">
                            <select name="volume_from" id="volume_from">
                                <option<?=(isset($_COOKIE['volume_from']) && (string)$_COOKIE['volume_from']=='0')?' selected':''; ?> value="0">Объём от</option>
                                <?php for ($i=0.1;$i<6.09;$i+=0.1): ?>
                                <option<?=(isset($_COOKIE['volume_from']) && (string)$i==(string)$_COOKIE['volume_from'])?' selected':''; ?> value="<?=number_format($i, 1, '.', ' ');?>"><?=number_format($i, 1, '.', ' ');?></option>
                                <?php endfor; ?>
                            </select>
                            <select name="volume_to" id="volume_to">
                                <option<?=(isset($_COOKIE['volume_to']) && (string)$_COOKIE['volume_to']=='0')?' selected':''; ?> value="0">до</option>
                                <?php for ($i=0.1;$i<6.09;$i+=0.1): ?>
                                <option<?=(isset($_COOKIE['volume_to']) && (string)$i==(string)$_COOKIE['volume_to'])?' selected':''; ?> value="<?=number_format($i, 1, '.', ' ');?>"><?=number_format($i, 1, '.', ' ');?></option>
                                <?php endfor; ?>
                            </select>
                        <!--    <input id="volume_from" type="text" name="filter[volume_from]" placeholder="Объем от" value="<?=@$filter['volume_from']?$filter['volume_from']:''  ?>">
                            <input id="volume_to" type="text" name="filter[volume_to]" placeholder="до" value="<?=@$filter['volume_to']?$filter['volume_to']:''  ?>"> -->
                        </div>
                    </div>
                    
                        <div class="column column-4">
                            <div class="filter-line">
                                <select id="kpp-filter">
                                    <option value="0">КПП</option>
                                    <option value="АТ" <?php if (!empty($filter['transmission']) && $filter['transmission']=='АТ'): ?>selected="selected"<?php endif; ?>>Автоматическая</option>
                                    <option value="МТ" <?php if (!empty($filter['transmission']) && $filter['transmission']=='МТ'): ?>selected="selected"<?php endif; ?>>Ручная</option>
                                </select>
                            </div>
                            <div class="filter-line">
                                <select id="state-filter">
                                    <option value="0">Состояние</option>
                                    <option value="2" <?php if (!empty($_COOKIE['condition']) && $_COOKIE['condition']==2): ?>selected="selected"<?php endif; ?>>Битые</option>
                                    <option value="1" <?php if (!empty($_COOKIE['condition']) && $_COOKIE['condition']==1): ?>selected="selected"<?php endif; ?>>Не битые</option>
                                </select>
                            </div>
                        </div>
                        
                        
                        <div class="column column-5">
                            <div class="filter-line">
                                <select id="mark-filter" multiple="multiple">
                                    <?php foreach ($brands as $brand): ?>
                                        <option value="<?=$brand->id;?>"><?=$brand->name;?></option>
                                    <?php endforeach; ?>
                                </select>
                                <div id="mark-filter-ids">
                                    <?php if (!empty($_COOKIE['mark_filter'])): ?>
                                        <?php
                                            $items = json_decode($_COOKIE['mark_filter']);
                                            foreach($items as $ind => $tr): ?>
                                            <input type="hidden" class="mark-ids" name="filter[mark][<?=$ind;?>]" value="<?=$tr;?>">
                                        <?php endforeach; ?>
                                    <?php endif; ?>
                                </div>
                            </div>
                            <div class="filter-line">
                                <select id="region-filter" multiple="multiple">
                                    <?php
                                        $result=$sql->query("select `Id`, `name` from `regions`");
                                        if($result->num_rows){
                                            $counter = 0;
                                            while($row=$result->fetch_array(MYSQLI_ASSOC)){
                                                echo '<option value="'.$row['Id'].'" '.(!empty($filter['regions']) && in_array($row['Id'], $filter['regions'])?'selected="selected"':'').'>'.$row['name'].'</option>';
                                                $counter++;
                                            }
                                        }
                                    ?>	
                                </select>
                                <div id="region-filter-ids">
                                    <?php if (!empty($_COOKIE['region_filter'])): ?>
                                        <?php
                                            $items = json_decode($_COOKIE['region_filter']);
                                            foreach($items as $ind => $tr): ?>
                                            <input type="hidden" class="region-ids" name="filter[region][<?=$ind;?>]" value="<?=$tr;?>">
                                        <?php endforeach; ?>
                                    <?php endif; ?>
                                </div>
                            </div>
                        </div>
						<div class="phone-amount">
                                                    <select id="source-filter" multiple="multiple">
                                                        <?php
                                                        foreach ($sources as $source):
                                                            echo '<option value="'.$source->id.'" '.(!empty($filter['sources']) && in_array($source->id, $filter['sources'])?'selected="selected"':'').'>'.$source->alias.'</option>';
                                                        endforeach;
                                                        ?>
                                                    </select>
                                                    <div id="source-filter-ids">
                                                        <?php if (!empty($_COOKIE['source_filter'])): ?>
                                                            <?php
                                                                $items = json_decode($_COOKIE['source_filter']);
                                                                foreach($items as $ind => $tr): ?>
                                                                <input type="hidden" class="body-ids" name="filter[source][<?=$ind;?>]" value="<?=$tr;?>">
                                                            <?php endforeach; ?>
                                                        <?php endif; ?>
                                                    </div>
								<input type="text" id="date-from" placeholder="Дата с 01.04.2016" value="<?=@$_COOKIE['date_from'];  ?>">
								<input type="text" id="date-to" placeholder="по 02.04.2016" value="<?=@$_COOKIE['date_to'];  ?>">
								<select name="orderby" id="orderby" style="margin-left: 0px">
								<option <?=(isset($_COOKIE["orderby"]) && $_COOKIE["orderby"]=='datedesc')?'selected':'';?> value="datedesc">Сначала новые</option>
								<option <?=(isset($_COOKIE["orderby"]) && $_COOKIE["orderby"]=='dateasc')?'selected':'';?> value="dateasc">Сначала старые</option>
								</select>
								<span>Показывать объявления, телефон которых засветился не более</span> 
                                                                <input maxlength="2" type="text" id="phone-amount-filter" value="<?=@$_COOKIE['phone-amount'];?>"><span>раз(а).</span>
								<div class="accept-filters" id="accept-filters">Применить</div>
								<div class="clear-filters" id="clear-filters">Сбросить</div>
						</div>
                    </div>
            </div>
            <div class="monitor">
                <div id="result_list" class="ads" data_id="10"></div>
                <?php if ($enough_balance): ?>
                <div class="more-button">
                    <div class="text">Показать ещё</div>
                </div>
                <?php endif; ?>
                <div class="md-preloader" id="more-loading-indicator">
                    <svg xmlns="http://www.w3.org/2000/svg" version="1.1" height="55" width="55" viewbox="0 0 55 55"><circle cx="27.5" cy="27.5" r="15" stroke-width="6"/>
                    </svg>
                </div>
            </div>
         
        <div class="md-preloader" id="table-loading-indicator">
            <svg xmlns="http://www.w3.org/2000/svg" version="1.1" height="50" width="50" viewbox="0 0 75 75"><circle cx="37.5" cy="37.5" r="33.5" stroke-width="6"/>
            </svg>
        </div>
            
         
         </div>
    </div>
</div>