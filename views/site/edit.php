<?php $this->title="Редактирование объявления"; ?>
<div class="add-menu">
    <a href="/add" id="add-button-link" class="add-button-link">
        <div class="button bt_1" id="add">
            <p>Добавить объявление</p>
        </div>
    </a>
    <div class="tab-button tab-active">
        <div class="text">
            <a href="/active">
            Активные
            </a>
        </div>
        <div class="amount">
            <?=(int)$active_count; ?>
        </div>
    </div>
    <?php if ($moderation_count>0): ?>
        <div class="tab-button tab-moderation">
            <div class="text">
                <a href="/moderation">
                На модерации
                </a>
            </div>
            <div class="amount">
                <?=$moderation_count; ?>
            </div>
        </div>
    <?php endif; ?>
    <?php if ($ended_count>0): ?>
        <div class="tab-button tab-ended">
            <div class="text">
                <span>
                Завершённые
                </span>
            </div>
            <div class="amount">
                <?=$ended_count; ?>
            </div>
        </div>
    <?php endif; ?>
</div>
<div class="model-container">
    Редактирование объявления &mdash; <b><?=$old_car->model->brand->name;?> <?=$old_car->model->name;?></b>
</div>
<?php function mb_ucfirst($string, $encoding='UTF-8')
{
    $firstChar = mb_substr($string, 0, 1, $encoding);
    $then = mb_substr($string, 1, mb_strlen($string, $encoding)-1, $encoding);
    return mb_strtoupper($firstChar, $encoding) . $then;
} ?>
<form action="/edit" method="post">
    <div class="page-edit">
        <div class="form-line" data-old="<?=$old_car->year;?>">
            <label>Год</label>
            <select class="field" id="year" name="year">
                <?php
                    $currentDate = new \DateTime();
                    $currentYear = (int)$currentDate->format('Y');
                ?>
                <?php for($i=$currentYear;$i>=1990;$i--): ?>
                <option <?=($old_car->year==$i)?"selected":'';?> value="<?=$i;?>"><?=$i;?></option>
                <?php endfor; ?>
            </select>
        </div>
        <div class="form-line" data-old="<?=$old_car->condition;?>">
            <label>Состояние</label>
            <select class="field" id="condition" name="condition">
                <?php foreach($conditions as $num => $condition): ?>
                <option <?=($old_car->condition==$num)?"selected":'';?> value="<?=$num;?>"><?=mb_ucfirst($condition);?></option>
                <?php endforeach; ?>
            </select>
        </div>
        <div class="form-line" data-old="<?=$old_car->body_id;?>">
            <label>Кузов</label>
            <div class="body_type">
                <?php $column_counter = 1; 
                foreach ($body_types as $number => $type): ?>
                <div class="col col-<?=$column_counter; ?>">
                    <div style="background-image: url('/resources/img/body/<?=$type->img_name;?>')" data-tooltip="<?=(!empty($type->shortname)?mb_ucfirst($type->shortname):mb_ucfirst($type->name));?>" class="item<?=($type->id==$old_car->body_id)?" active":"";?>" data-id="<?=$type->id;?>" data-img="<?=$type->img_name;?>">
                        <div class="text"><?=mb_ucfirst($type->name);?></div>
                    </div>
                </div>
                <?php
                    $column_counter == 4?$column_counter = 1:$column_counter++;
                    endforeach;
                ?>
            </div>
            <input class="field" type="hidden" id="body_type" name="body_type" value="<?=$old_car->body_id;?>">
        </div>
        <div class="form-line" data-old="<?=number_format($old_car->run,0,'.',' ');?>">
            <label>Пробег</label>
            <input data-success="1" class="field" data-success="1" type="text" name="run" id="run" maxlength="9" value="<?=number_format($old_car->run, 0, '.', ' ');?>"> км
        </div>
        <div class="form-line" data-old="<?=$old_car->color->id;?>">
            <label>Цвет</label>
            <div class="colors">
                <?php foreach($colors as $color): ?>
                <div class="color-item<?=($old_car->color->id==$color->id)?" active":"";?>" data-value="<?=$color->id;?>" data-text="<?=mb_ucfirst($color->name);?>">
                    <div class="color<?=$color->dark==0?" white":"";?>" style="
                        <?php
                        if (empty($color->gradient_hex_from) || empty($color->gradient_hex_to)):?>
                            background-color: #<?=$color->hex;?>
                        <?php else: ?>
                            background: linear-gradient(to bottom, #<?=$color->gradient_hex_from;?>, #<?=$color->gradient_hex_to;?>);
                        <?php endif; ?>">
                    </div>
                </div>
                <?php endforeach; ?>
            </div>
            <input class="field" id="color" type="hidden" name="color" value="<?=$old_car->color->id;?>">
        </div>
        <div class="form-line" data-old="<?=$old_car->engine_type;?>">
            <label style="margin-top: 2px">Тип двигателя</label>
            <div class="engine_type">
                <?php foreach($engine_types as $num => $type): ?>
                <div class="item<?=($num==$old_car->engine_type)?" active":"";?>" data-id="<?=$num;?>"><?=$type;?></div>
                <?php endforeach; ?>
            </div>
            <input class="field" type="hidden" id="engine_type" name="engine_type" value="<?=$old_car->engine_type;?>">
        </div>
        <div class="form-line" data-old="<?=number_format($old_car->engine_volume,1,'.',' ');?>">
            <label>Объем двигателя</label>
            <select class="field" id="engine_volume" name="engine_volume">
                <?php for($i=0.1;$i<=6.0;$i+=0.1): ?>
                <option <?=(strcmp(number_format($old_car->engine_volume, 1, '.', ' '),number_format($i, 1, '.', ' '))==0)?"selected":'';?> value="<?=number_format($i, 1, '.', ' ');?>"><?=number_format($i, 1, '.', ' ');?></option>
                <?php endfor; ?>
            </select>  л
        </div>
        <div class="form-line" data-old="<?=$old_car->engine_power;?>">
            <label>Мощность двигателя</label>
            <input data-success="1" class="field" data-success="1" value="<?=$old_car->engine_power;?>" type="text" name="engine_power" id="engine_power" maxlength="3">  л.с.
        </div>
        <div class="form-line" data-old="<?=$old_car->transmission_type;?>">
            <label style="margin-top: 2px">Коробка передач</label>
            <div class="transmission_type">
                <?php foreach($transmission_types as $num => $type): ?>
                <div class="item<?=($num==$old_car->transmission_type)?" active":"";?>" data-id="<?=$num;?>"><?=$type['second_name'];?></div>
                <?php endforeach; ?>
            </div>
            <input class="field" type="hidden" id="transmission_type" name="transmission_type" value="<?=$old_car->transmission_type;?>">
        </div>
        <div class="form-line" data-old="<?=$old_car->drive_type;?>">
            <label style="margin-top: 2px">Привод</label>
            <div class="drive_type">
                <?php foreach($drive_types as $num => $type): ?>
                <div class="item<?=($num==$old_car->drive_type)?" active":"";?>" data-id="<?=$num;?>"><?=$type;?></div>
                <?php endforeach; ?>
            </div>
            <input class="field" type="hidden" id="drive_type" name="drive_type" value="<?=$old_car->drive_type;?>">
        </div>
        <div class="form-line rudder_container" data-old="<?=$old_car->rudder;?>">
            <label style="margin-top: 2px">Руль</label>
            <div class="rudder">
                <?php foreach($rudders as $num => $rudder): ?>
                <div class="item<?=($num==$old_car->rudder)?" active":"";?>" data-id="<?=$num;?>"><?=$rudder;?></div>
                <?php endforeach; ?>
            </div>
            <input class="field" type="hidden" id="rudder" name="rudder" value="<?=$old_car->rudder;?>">
        </div>
        <div class="form-line" data-old="<?=number_format($old_car->price, 0, '.', ' ');?>">
            <label>Цена</label>
            <input data-success="1" class="field" data-success="1" type="text" name="price" id="price" maxlength="10" value="<?=number_format($old_car->price, 0, '.', ' ');?>"> руб.
        </div>
    <!--    <div class="form-line">
            <label>Мобильный телефон</label>
            <input data-success="1" readonly="true" type="text" name="phone" id="phone" value="<?=$old_car->phone;?>">
        </div>-->
        <div class="form-line comment" data-old="<?=$old_car->description;?>">
            <label style="margin-top: 3px">Комментарий</label>
            <textarea name="comment" id="comment"><?=$old_car->description;?></textarea>
            <div class="info">
                Какое установлено дополнительное оборудование, где проводилось обслуживание,
                состояние салона, состояние шин, есть ли торг?
                Запрещается давать ссылки, указывать адреса эл. почты, телефоны, цену, предлагать услуги,
                не связанные с продажей этого авто.
            </div>
        </div>
        <div class="form-line photos">
            <label>Фотографии</label>
            
            <div class="info">
               Объявления с фотографиями более привлекательны для покупателей. Доступна загрузка <b>не более 10 фотографий</b>. Запрещается размещение на фото контактной информации, а также ретушь с целью выделения фото среди других. <font color="red">Не загружайте фотографии, которые были опубликованы ранее!</font> Администрация сайта не несет ответственности за блокировку объявления!
            </div>
            
            
            
            <div class="uploaded">
                <?php $photos = $old_car->photos;?>
                <?php foreach($photos as $num=>$photo): ?>
                <div class="preview-container" id="photo-preview-container-<?=$photo->id;?>" data-url="/<?=(!empty($photo->thumb)?$photo->thumb:$photo->url);?>" style="background: url(/<?=(!empty($photo->thumb)?$photo->thumb:$photo->url);?>) no-repeat center; background-size: cover;">
                    <input class="uploaded-image" type="hidden" name="photos_id[]" value="<?=$photo->id;?>">
                    <div class="number"><?=($num+1);?></div>
                    <img data-id="<?=$photo->id;?>" class="delete" src="/resources/img/close.png"/>
                </div>
                <?php endforeach; ?>
                
                <div class="camera_container" <?=(count($photos)>=10)?'style="display:none"':'';?>>
                    <img src="/resources/img/camera.png">
                    <div class="progress"></div>
                    <input id="fileupload" type="file" name="photos[]" data-url="/ajax/photoupload" multiple accept="image/jpeg,image/png">
                </div>
            </div>
        </div>
        <div class="next">Далее</div>
    </div>
    <div class="page-check">
        <div class="head-noty">Проверьте объявление. Если всё правильно, нажмите кнопку "Подтвердить".</div>
        <div class="form-line">
            <label>Год</label>
            <span id="check_year"></span>
            <input type="hidden" value="<?=$old_car->year;?>">
        </div>
        <div class="form-line">
            <label>Состояние</label>
            <span id="check_condition"></span>
            <input type="hidden" value="<?=$old_car->condition;?>">
        </div>
        <div class="form-line">
            <label>Кузов</label>
            <span id="check_body"></span>
            <input type="hidden" value="<?=$old_car->body->name;?>">
        </div>
        <div class="form-line">
            <label>Пробег</label>
            <span id="check_run"></span>
            <input type="hidden" value="<?=number_format($old_car->run, 0, '.', ' ');?>">
        </div>
        <div class="form-line">
            <label>Цвет</label>
            <span id="check_color"></span>
            <input type="hidden" value="<?=mb_ucfirst($old_car->color->name);?>">
        </div>
        <div class="form-line">
            <label>Тип двигателя</label>
            <span id="check_engine_type"></span>
            <input type="hidden" value="<?=$engine_types[$old_car->engine_type];?>">
        </div>
        <div class="form-line">
            <label>Объем двигателя</label>
            <span id="check_engine_volume"></span> л
            <input type="hidden" value="<?=number_format($old_car->engine_volume, 1, '.', ' ');?>">
        </div>
        <div class="form-line">
            <label>Мощность двигателя</label>
            <span id="check_engine_power"></span> л.с.
            <input type="hidden" value="<?=$old_car->engine_power;?>">
        </div>
        
        <div class="form-line drive_type_container">
            <label>Привод</label>
            <span id="check_drive_type"></span>
            <input type="hidden" value="<?=$drive_types[$old_car->drive_type];?>">
        </div>
        <div class="form-line">
            <label>Коробка передач</label>
            <span id="check_transmission_type"></span>
            <input type="hidden" value="<?=$transmission_types[$old_car->transmission_type]['second_name'];?>">
        </div>
        <div class="form-line">
            <label>Руль</label>
            <span id="check_rudder"></span>
            <input type="hidden" value="<?=$rudders[$old_car->rudder];?>">
        </div>
        
        <div class="form-line">
            <label>Цена</label>
            <span id="check_price"></span> р.
            <input type="hidden" value="<?=number_format($old_car->price, 0, '.', ' ');?>">
        </div>
    <!--    <div class="form-line">
            <label>Мобильный телефон</label>
            <span id="check_phone"></span>
            <input type="hidden" value="<?=$old_car->phone;?>">
        </div>-->
        <div class="form-line">
            <label>Дополнительная информация</label>
            <span id="check_comment"></span>
            <input type="hidden" value="<?=$old_car->description;?>">
        </div>
        <div class="form-line">
            <label>Фотографии</label>
            <div id="check_photos"></div>
        </div>
        <input type="hidden" name="id" value="<?=$old_car->id;?>">
        <input type="hidden" name="_csrf" value="<?=Yii::$app->request->getCsrfToken();?>">
        <div class="prev"><img class="back" src="/resources/img/back.png">Назад</div>
        <button type="submit" class="next">Подтвердить</button>
    </div>
</form>