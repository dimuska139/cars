<?php $this->title="Завершённые объявления"; ?>
<div class="add-menu">
    <a href="/add" id="add-button-link" class="add-button-link">
        <div class="button bt_1" id="add">
            <p>Добавить объявление</p>
        </div>
    </a>
    <div class="tab-button tab-active">
        <div class="text">
            <a href="/active">
            Активные
            </a>
        </div>
        <div class="amount">
            <?=(int)$active_count; ?>
        </div>
    </div>
    <?php if ($moderation_count>0): ?>
        <div class="tab-button tab-moderation">
            <div class="text">
                <a href="/moderation">
                На модерации
                </a>
            </div>
            <div class="amount">
                <?=$moderation_count; ?>
            </div>
        </div>
    <?php endif; ?>
    <?php if ($ended_count>0): ?>
    <div class="tab-button tab-ended tab-button-active">
            <div class="text">
                <span>
                Завершённые
                </span>
            </div>
            <div class="amount">
                <?=$ended_count; ?>
            </div>
        </div>
    <?php endif; ?>
</div>
<?php if (empty($active_count) && empty($moderation_count) && empty($ended_count)):?>
<h1>У Вас пока что нет объявлений! Разместите Ваше первое объявление с помощью кнопки выше.</h1>
<?php endif; ?>
<?php if ($ended_count>0): ?>
<div class="ended-place">
    <div class="list">
    <?php
    $column_counter = 1;
    foreach ($ended as $number => $item): ?>
        <div class="col col-<?=$column_counter; ?>">
            <div class="item" data-id="<?=$item->id;?>">
                <?php
                    $photo = $item->photo;
                    $img = "/resources/img/nophoto.png";
                    if ($photo && file_exists($photo->url))
                        $img = $photo->url;
                    if ($photo && !empty($photo->thumb) && file_exists($photo->thumb))
                        $img = $photo->thumb;
                ?>
                <div class="logo" style="background: url('<?=$img; ?>') no-repeat center; background-size: cover;">

                </div>
                <div class="info">
                    <div class="mark-and-model">
                        <?=$item->model->brand->name;?> <?=$item->model->name;?>
                        <div class="fade"></div>
                    </div>
                    <div class="year-run"><?=$item->year;?>, <?=number_format($item->run, 0, '.', ' ');?> км</div>
                    <div class="color-bodytype"><?=$item->body->name?>, <?=$item->color->name;?>
                        <?php
                        $str = $item->body->name;
                        $enc = 'UTF-8';?>
                    </div>
                    <div class="v-box-power">
                        <?=number_format($item->engine_volume, 1, '.', ' ');?> <?=$transmission_types[$item->transmission_type]['shortname'];?> (<?=$item->engine_power;?> л.с.)
                    </div>
                    <div class="eng-type-dr-type">
                        <?=$engine_types[$item->engine_type];?>, <?=mb_strtolower($drive_types[$item->drive_type], $enc);?>
                    </div>
					<div class="price"><font style="font-weight: bold; font-size: 11pt;"><?=number_format($item->price, 0, '.', ' ');?></font> руб.</div>
                    <div class="phone">
                        <?=$item->phone; ?>, <?=$item->contact; ?>
                    </div>
                    <div class="status">
						 <?=$statuses[$item->status]['user'];?>
                    </div>
                </div>
            </div>
        </div>
    <?php
    $column_counter == 5?$column_counter = 1:$column_counter++;
    endforeach;
    ?> 
    </div>
</div>
<?php endif; ?>