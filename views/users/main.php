<?php $this->title = "Купля-продажа автомобилей на AM97.RU"; ?>
<div class="column-left">
    <div class="auth-panel panel">
        <div class="name">авторизация</div>
        <form class="login-form" action="/" method="post">
            <input class="login-input" required placeholder="Логин" type="text" name="login" autocomplete="off" value="<?=!empty($_POST['login'])?$_POST['login']:'';?>">
            <input class="password-input" required placeholder="Пароль" type="password" name="password" autocomplete="off">
            <input type="hidden" name="_csrf" value="<?=Yii::$app->request->getCsrfToken();?>">
            <div class="login-button">Войти</div>
            <?php if ($this->params['error']): ?>
                <div class="error">
                    Ошибка авторизации. Неправильный логин или пароль!
                </div>
            <?php endif; ?>
        </form>
        <div class="icon"></div>
        <div class="bottom">
            <div class="changepassword_place">
                <div class="question">Забыли пароль?</div>
                <div class="input-place">
                    <span class="info">Новый пароль будет отправлен Вам в СМС:</span>
                    <input maxlength="12" type="text" id="login" placeholder="Телефон">
                    <span class="password-button">Отправить</span>
                </div>
                <div class="result"></div>
            </div>
        </div>
    </div>
    <div class="check-panel panel">
        <div class="name">проверка тс</div>
        <div class="icon"></div>
        <div class="bottom">
            Получите информацию об автомобиле: нахождении<br>
            автомобиля в залоге, наличии ограничений на<br>
            регистрационные действия транспонртно средства<br>
            дополнительную информацию о тс.
        </div>
    </div>
    <div class="notify-panel panel">
        <div class="name">уведомления</div>
        <div class="icon"></div>
        <div class="bottom">
            Получайте объявления, без необходимости Вашего участия!<br>
			Создайте и настройте фильтры, по которым хотели бы<br>
			получать новые объявления в WhatsApp мгновенно.
        </div>
    </div>
    <div class="registration-panel panel">
        <div class="name">регистрация</div>
        <div class="icon"></div>
		<div class="bottom">
            Дарим 50 рублей на баланс для бесплатного тестирования!<br>
			Логин и пароль для авторизации будут отправлены Вам в СМС.
        </div>
        <div class="register block">
            <input pattern="[а-яА-ЯёЁ]+" maxlength="12" class="username" type="text" placeholder="Ваше имя">
            <input maxlength="12" class="userphone" type="text" placeholder="Телефон">
            <div id="register-error"></div>
            <div class="button bt_1" id="register-button">
                <p>Отправить</p>
            </div>
            <div id="register-success">
                Благодарим за регистрацию!
            </div>
        </div>
    </div>
</div>
<div class="column-right">
    <div class="search-panel panel">
		<div class="name">поиск тс</div>
		<div class="action-price">Переход – 2 рубля!</div>
        <div class="online-table">
            <div class="online-body">
                <?php foreach($lastonline as $num => $item): ?>
                    <?php if ($num==0): ?>
                        <script>
                            var last_item_id = <?=$item['Id'];?>;
                        </script>
                        <?php break; ?>
                    <?php endif; ?>
                <?php endforeach;?>
                <?php echo \Yii::$app->view->renderFile('@app/views/users/ads_list_body.php',['lastonline' => $lastonline]); ?>
            </div>
        </div>
        <div class="bottom">
            <div class="icon"></div>
            Получайте новые автомобильные объявления с ведущих автопорталов по всей России<br>
            в режиме реального времени. Не нужно платить за несколько тысяч переходов сразу,<br>
            платите столько — сколько считаете нужным!
        </div>
    </div>
    <?php if (!empty($last_list)): ?>
    <div class="publish-panel panel">
        <div class="name">публикация объявлений</div>
        <div class="action-price">Объявление – 399 рублей!</div>
        <div class="last block">
            <?php foreach($last_list as $item): ?>
            <div class="item" data-id="<?=$item->id;?>">
                <?php   
                    $photo = $item->photo;
                    $img = "/resources/img/nophoto.png";
                    if ($photo && file_exists($photo->url))
                        $img = '/'.$photo->url;
                    if ($photo && !empty($photo->thumb) && file_exists($photo->thumb))
                        $img = '/'.$photo->thumb;
                ?>
                <div class="photo" style="background: url('<?=$img; ?>') no-repeat center; background-size: cover;">

                </div>
            </div>
            <?php endforeach; ?>
        </div>
        <div class="bottom">
            <div class="icon"></div>
            Управляйте  объявлениями с одного личного кабинета! Удобная система публикации<br>
			объявлений, позволит Вам легко и быстро размещать объявления на Auto.ru и Avito.ru<br>
            (от частного лица), с возможностью дальнейшего редактирования и обновления их!
        </div>
    </div>
    <?php endif; ?>
</div>