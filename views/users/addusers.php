<h1>Интерфейс добавления новых пользователей</h1>
<form action="./add" method="post">
    <table>
        <tr>
            <td>
                <b>Логин</b>
            </td>
            <td>
                <input required type="text" name="login">
            </td>
        </tr>
        <tr>
            <td>
                <b>Пароль</b>
            </td>
            <td>
                <input required type="password" name="password">
            </td>
        </tr>
        <tr>
            <td>
                <b>Админ или нет</b>
            </td>
            <td>
                <input type="checkbox" name="admin">
            </td>
        </tr>      
        <tr>
            <td>
                <input type="hidden" name="_csrf" value="<?=Yii::$app->request->getCsrfToken();?>">
            </td>
            <td>
                <button type="submit">Создать</button>
            </td>    
        </tr>
    </table>
</form>
<hr>
<table border="1">
    <tr>
        <td>#</td>
        <td>Логин</td>
        <td>Администратор</td>
        <td>IP создателя</td>
    </tr>
    <?php foreach ($users as $user): ?>
    <tr>
        <td><?=$user->id;?></td>
        <td><?=$user->login;?></td>
        <td><?=$user->admin==1?"да":"нет";?></td>
        <td><?=long2ip($user->ip);?></td>
    </tr>
    <?php endforeach; ?>
</table>