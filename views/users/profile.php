<?php $this->title = "Настройки"; ?>
<div class="settings">
    <div class="profile">
        <div class="title">Профиль</div>
        <div class="form-line">
            <label>Имя</label>
            <input disabled="disabled" type="text" id="name" value="<?=$user->name;?>">
            <div class="error" id="name-error"></div>
        </div>
        <div class="form-line">
            <label>Телефон</label>
            <input disabled="disabled" type="text" id="phone" value="<?=$user->login;?>">
            <div class="error" id="phone-error"></div>
        </div>
    </div>
    <div class="site-settings">
        <div class="title">Поиск тс</div>
        <div class="form-line">
            <label>Звуки (новые объявления)</label>
            <input <?php if ($user->search_sounds==1):?>checked<?php endif;?> type="checkbox" id="search_sounds" class="settings-item" data-type="sounds">
            <div class="success-text">Изменения сохранены!</div>
        </div>
        <div class="form-line">
            <label>Выделять объявления с модерации</label>
            <input <?php if ($user->search_markmoderation==1):?>checked<?php endif;?> type="checkbox" id="search_markmoderation" class="settings-item" data-type="markmoderation">
            <div class="success-text">Изменения сохранены!</div>
        </div>
        <div class="form-line">
            <label>Выделять дешёвые объявления</label>
            <input <?php if ($user->search_markcheap==1):?>checked<?php endif;?> type="checkbox" id="search_markcheap" class="settings-item" data-type="markcheap">
            <div class="success-text">Изменения сохранены!</div>
        </div>
        <div class="form-line">
            <label>Показывать фотографии</label>
            <input <?php if ($user->search_photos==1):?>checked<?php endif;?> type="checkbox" id="search_photos" class="settings-item" data-type="photos">
            <div class="success-text">Изменения сохранены!</div>
        </div>
        <div class="form-line">
            <label>Количество объявлений на странице</label>
            <select name="page_size" id="page_size" class="settings-select settings-item" data-type="pagesize">
                <option <?=$user->search_pagesize==25?'selected':'';?> value="25">25</option>
                <option <?=$user->search_pagesize==50?'selected':'';?> value="50">50</option>
                <option <?=$user->search_pagesize==75?'selected':'';?> value="75">75</option>
                <option <?=$user->search_pagesize==100?'selected':'';?> value="100">100</option>
            </select>
            <div class="success-text">Изменения сохранены!</div>
        </div>
    </div>
    <div class="change-password">
    <div class="title">Изменить пароль</div>
    <div class="form-line">
        <label>Новый пароль</label>
        <input type="password" id="password">
        <div class="error" id="password-error"></div>
    </div>
    <div class="form-line">
        <label>Повторите пароль</label>
        <input type="password" id="repeat-password">
    </div>
    <div class="save">Изменить</div>
    <div class="saved">Пароль успешно изменен!</div>
</div>