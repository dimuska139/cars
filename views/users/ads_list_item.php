<div class="<?=($num%2==0?'even ':'odd ');?>line<?=(!empty($item['price'] && $item['difference_price']>=50 && $item['condition']!=1)?' difference_price':'');?><?=(!empty($item['minutes_to_publish'])?' moderation':'');?>">
                <div class="time">
                    <div class="pos">
                        <?=$item['time'];?>
                    </div>
                </div>
                <div class="td logo">
                    <?=$item['marka_logo']?('<img class="brand-logo" src="/resources/img/brands/'.$item['marka_logo'].'">'):'';?>
                </div>
                <div class="td car"><div class="fade"></div>
                    <div class="pos">
                        <nobr>
    <?php             
         $eng = $item['enginevol'].((mb_strlen($item['engine'],'UTF-8')!=0 && $item['engine']!='b')?$item['engine']:'').(!empty($item['transmission'])?(' '.$item['transmission']):"");
         if (mb_strlen($eng,'UTF-8')>2)
             $eng = '('.$eng.')';
         if (strcmp(mb_convert_encoding($item['marka'], 'utf-8'), mb_convert_encoding('Лада', 'utf-8'))==0)
                $item['marka'] = 'ВАЗ (Lada)';
         $mark = $item['marka'].' '.$item['model'].' '.$item['model_2'];
         if (mb_strlen($mark,'UTF-8')>22)
             $mark = mb_substr($mark, 0, 22,'UTF-8').'..';
     ?>
     <span class="mark-info"><?=$mark;?> <span class="engine-info"><?=$eng;?></span></span>
                        </nobr>
                    </div>
    </div>

    <div class="td price">
        <div class="pos">
            <?php if (!empty($item['price'])): ?><?=number_format($item['price'],0,' ',' ');?>
            <?php endif; ?>
        </div>
    </div>
    <?php if ($item['condition']==0): ?>
        <?php if ($item['average_price']==0): ?>
            <div class="diff td"><div class="pos"></div></div>
        <?php else: ?>
            <?php if ($item['difference_price']>0): ?>
                <div class="diff td"><div class="pos green"><?=number_format($item['difference_price'],0,' ',' ');?></div></div>
            <?php else: ?>
                <div class="diff td"><div class="pos red"><?=number_format((-1)*$item['difference_price'],0,' ',' ');?></div></div>
            <?php endif; ?>
        <?php endif; ?>
    <?php else: ?>
        <div class="diff badcondition td" style="padding-top: 5px"><img src="/resources/img/bt.png"></div>
    <?php endif; ?>
        
        <div class="td year">
            <div class="pos">
                <?=$item['year'];?>
            </div>
        </div>
        
        <div class="td body" data-tooltip="<?=$item['body'];?>"><div class="tooltip"></div>
            <div class="pos">
                <?php if (!empty($item['body_img'])): ?>
                    <?=$item['body_img']; ?>
                <?php endif; ?>
            </div>
        </div>
        <div class="region td">
            <?php
                $metr_reg = $item['region'];
				if (mb_strlen($metr_reg,'UTF-8')>12)
				$metr_reg = mb_substr($metr_reg, 0, 12,'UTF-8').'..';
				?>
            <div class="pos"><?=$metr_reg;?></div>
        </div>
</div>