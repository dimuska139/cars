<?php $this->title = "Задания"; ?>
<ul class="tasks-menu">
    <a href="/admin/tasks?action=add"<?php if (empty($_GET['action']) || $_GET['action']=='add'):?>class="active"<?php endif; ?>>
        <li>
            Добавление (<?=$add_count;?>)
        </li>
    </a>
    <a href="/admin/tasks?action=edit"<?php if (!empty($_GET['action']) && $_GET['action']=='edit'):?>class="active"<?php endif; ?>>
        <li>
            Редактирование (<?=$edit_count;?>)
        </li>
    </a>
    <a href="/admin/tasks?action=delete"<?php if (!empty($_GET['action']) && $_GET['action']=='delete'):?>class="active"<?php endif; ?>>
        <li>
            Удаление (<?=$delete_count;?>)
        </li>
    </a>
</ul>