<?php $this->title="Пользователи"; ?>
<div class="users-list">
    <table border="1" frame="void"  rules="rows" bordercolor="white">
        <tbody>
            <tr>
                <th width="30" style="text-align: center">
                    ID
                </th>
                <th width="200">
                    Имя
                </th>
                <th>
                    Логин
                </th>
                <th>
                    Баланс
                </th>
                <th>
                    Объявлений
                </th>
                <th>
                    Переходов
                </th>
                <th width="150" style="text-align: center">
                    Дата регистрации
                </th>
            </tr>
            <?php foreach ($users_list as $user):?>
            <tr class="line">
                <th width="30" style="text-align: center">
                    <?=$user->id; ?>
                </th>
                <th width="200">
                    <?=$user->name; ?>
                </th>
                <th>
                    <?=$user->login; ?>
                </th>
                <th>
                    <?=number_format($user->balance,0,' ',' ');?>
                </th>
                <th>
                    <?php
                        $cars_amount = \app\models\Cars::find()
                        ->where([
                            'owner' => $user->id
                        ])->count();
                    ?>
                    <?=number_format($cars_amount, 0, ' ' ,' ');?>
                </th>
                <th>
                    <?php
                        $op_amount = \app\models\Operations::find()
                            ->where([
                                'user_id' => $user->id,
                                'type' => 'ads'
                            ])->count();
                        $tomorrow = new \DateTime("now", new \DateTimeZone('Europe/Moscow'));
                        $tomorrow->add(new \DateInterval('P1D')); // Завтрашний день
                        $tomorrow = $tomorrow->format("Y-m-d");
                        
                        $today = new \DateTime("now", new \DateTimeZone('Europe/Moscow'));
                        $today = $today->format("Y-m-d");
                        
                        $op_today_amount = \app\models\Operations::find()
                            ->where([
                                'user_id' => $user->id,
                                'type' => 'ads'
                            ])
                            ->andWhere([
                                '>', 'createdate', $today
                            ])
                            ->andWhere([
                                '<', 'createdate', $tomorrow
                            ])
                            ->count();
                        
                    ?>
                    <?=number_format($op_amount, 0, ' ', ' ');?> (<?=number_format($op_today_amount, 0, ' ', ' ');?>)
                </th>
                <th width="150" style="text-align: center">
                    <?=(new \DateTime($user->createdate))->format('d.m.Y H:i'); ?>
                </th>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
</div>