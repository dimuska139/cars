<?php $this->title="Редактирование пользователя"; ?>
<p class="title">Редактирование пользователя</p>
<form action="/admin/edituser" method="post" class="create-user-form">
    <input type="hidden" name="id" id="id" value="<?=$user->id;?>">
    <div class="form-line">
        <img class="indicator" src="/resources/img/okay.png"/>
        <label>Логин (номер телефона)</label>
        <input type="text" name="login" id="login" value="<?=$user->login;?>">
        <div class="error" id="login-error"></div>
    </div>
    <div class="form-line">
        <img class="indicator" src="/resources/img/okay.png"/>
        <label>Имя</label>
        <input type="text" name="name" id="name" value="<?=$user->name;?>">
    </div>
    <div class="form-line">
        <img class="indicator" src="/resources/img/okay.png"/>
        <label>Баланс</label>
        <input type="text" name="balance" id="balance" value="<?=$user->balance;?>">
    </div>
    <div class="form-line">
        <img class="indicator" src="/resources/img/okay.png"/>
        <label>Тип</label>
        <select id="admin" name="admin">
            <option <?=$user->admin==0?'selected':'';?> value="0">обычный пользователь</option>
            <option <?=$user->admin==1?'selected':'';?> value="1">администратор</option>
        </select>
    </div>
    <div class="form-line">
        <img class="indicator" src="/resources/img/okay.png"/>
        <label>Состояние</label>
        <select id="admin" name="admin">
            <option <?=$user->state==0?'selected':'';?> value="0">удален</option>
            <option <?=$user->state==1?'selected':'';?> value="1">активен</option>
        </select>
    </div>
    <div class="form-line">
        <label>Комментарий</label>
        <textarea name="description" id="description"><?=$user->description;?></textarea>
    </div>
    <input type="submit" class="registration-button" value="Сохранить"/>
</form>
<p class="title">Смена пароля</p>
<form action="/admin/changeuserpassword" method="post" class="changepassword-user-form">
    <input type="hidden" name="id" value="<?=$user->id;?>">
    <div class="form-line">
        <img class="indicator" src="/resources/img/okay.png"/>
        <label>Пароль</label>
        <input type="password" name="password" id="password">
    </div>
    <div class="form-line">
        <img class="indicator" src="/resources/img/okay.png"/>
        <label>Повторите пароль</label>
        <input type="password" name="repeat-password" id="repeat-password">
        <div class="error" id="password-error"></div>
    </div>
    <input type="submit" class="changepassword-button" value="Изменить"/>
</form>