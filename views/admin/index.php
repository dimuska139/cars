<?php $this->title="Объявления"; ?>
<div class="cars-list">
    <table border="1" frame="void"  rules="rows" bordercolor="white">
        <tbody>
            <tr>
                <th width="30" style="text-align: center">
                    ID
                </th>
                <th width="231">
                    Автомобиль
                </th>
                <th width="200">
                    Пользователь
                </th>
                <th width="305">
                    Ссылки на Auto.ru и Avito.ru
                </th>
                <th width="150" style="text-align: center">
                    Дата добавления
                </th>
                <th width="150" style="text-align: center">
                    Статус
                </th>
            </tr>
            <?php foreach ($list as $task):?>
            <tr class="car_<?=$task->id;?> line status_<?=$task->status;?>">
                <td width="30" style="text-align: center">
                    <?=$task->id;?>
                </td>
                <td width="231">
                    <?=$task->model->brand->name;?>
                    <?=$task->model->name;?>,
                    <?=$task->year;?>
                </td>
                <td width="200">
                    <?php if (!empty($task->proxy) && !empty($task->proxy->user)):?>
                        <?=$task->proxy->user->name;?>, <?=$task->proxy->user->login;?>
                    <?php endif; ?>
                </td>
                <td width="305">
                    <input data-id="<?=$task->id;?>" type="text" placeholder="Auto.ru" class="url_input autoru_url" id="autoru_url" value="<?=$task->autoru_url;?>">
                    <input data-id="<?=$task->id;?>" type="text" placeholder="Avito.ru" class="url_input avitoru_url" id="avitoru_url" value="<?=$task->avitoru_url;?>">
                </td>
                <td width="150" style="text-align: center">
                    <?=(new \DateTime($task->createdate))->format('d.m.Y H:i'); ?>
                </td>
                <td width="150">
                    <select class="status" data-prev="<?=$task->status;?>" data-id="<?=$task->id;?>">
                        <?php foreach ($statuses as $num => $status): ?>
                            <?php if ($num==$task->status): ?>
                                <option selected value="<?=$num;?>"><?=$status['user'];?></option>
                            <?php else: ?>
                                <option value="<?=$num;?>"><?=$status['user'];?></option>
                            <?php endif; ?>
                        <?php endforeach; ?>
                    </select>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
</div>