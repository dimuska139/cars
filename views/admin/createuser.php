<?php $this->title="Создание пользователя"; ?>
<p class="title">Создание пользователя</p>
<form action="/admin/createuser" method="post" class="create-user-form">
    <div class="form-line">
        <img class="indicator" src="/resources/img/okay.png"/>
        <label>Логин (номер телефона)</label>
        <input type="text" name="login" id="login">
        <div class="error" id="login-error"></div>
    </div>
    <div class="form-line">
        <img class="indicator" src="/resources/img/okay.png"/>
        <label>Имя</label>
        <input type="text" name="name" id="name">
    </div>
    <div class="form-line">
        <img class="indicator" src="/resources/img/okay.png"/>
        <label>Пароль</label>
        <input type="password" name="password" id="password">
    </div>
    <div class="form-line">
        <img class="indicator" src="/resources/img/okay.png"/>
        <label>Повторите пароль</label>
        <input type="password" name="repeat-password" id="repeat-password">
        <div class="error" id="password-error"></div>
    </div>
    <div class="form-line">
        <img class="indicator" src="/resources/img/okay.png"/>
        <label>Баланс</label>
        <input type="text" name="balance" id="balance">
    </div>
    <div class="form-line">
        <img class="indicator" src="/resources/img/okay.png"/>
        <label>Тип</label>
        <select id="admin" name="admin">
            <option value="0">обычный пользователь</option>
            <option value="1">администратор</option>
        </select>
    </div>
    <div class="form-line">
        <label>Комментарий</label>
        <textarea name="description" id="description"></textarea>
    </div>
    <input type="submit" class="registration-button" value="Зарегистрировать"/>
</form>