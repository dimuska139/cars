<?php $this->title="Прокси"; ?>
<div class="proxy-list">
    <form action="/admin/createproxy" method="post" class="create-proxy-form">
        <div class="form-line proxy-place">
            <input required placeholder="Сервер" type="text" name="proxy" id="proxy" pattern="\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}">
        </div>
        <div class="form-line proxy-http">
            <input required placeholder="HTTP" type="text" name="http_port" id="http-port">
        </div>
        <div class="form-line proxy-socks5">
            <input required placeholder="SOCKS5" type="text" name="socks5_port" id="socks5_port">
        </div>
        <div class="form-line proxy-login">
            <input required placeholder="Логин" type="text" name="login" id="login">
        </div>
        <div class="form-line proxy-password">
            <input required placeholder="Пароль" type="text" name="pass" id="password">
        </div>
        
        
        <input type="submit" class="add-proxy-button" value="добавить"/>
    </form>
    
    <span class="unused-amount">не использовано <?=$unused_amount; ?> прокси</span>
    <table border="1" frame="void"  rules="rows" bordercolor="white">
        <tbody>
            <tr>
                <th width="30" style="text-align: center">
                    ID
                </th>
                <th width="360">
                    Сервер / HTTP / SOCKS5 / Логин / Пароль
                </th>
				<th width="110" style="text-align: center">
                    Телефон
                </th>
                <th width="60" style="text-align: center">
                    Auto.ru
                </th>
				<th width="60" style="text-align: center">
                    Avito.ru
                </th>
				<th width="80" style="text-align: center">
                    User
                </th>
				<th width="110" style="text-align: center">
					Объявление
                </td>
                <th width="120" style="text-align: center">
                    Дата добавления
                </th>
            </tr>
            <?php foreach ($proxy_list as $proxy):?>
            <?php 
                $dt = new \DateTime($proxy->createdate);
                $days = (int)($dt->diff(new \DateTime())->format('%R%a'));
            ?>
            <tr class="line <?php if ($proxy->confirmed_avito==0 && $proxy->confirmed_auto==0):?>
                    unused
                <?php elseif ($proxy->confirmed_avito==1 && $proxy->confirmed_auto==0):?>
                    used-partial
                <?php elseif ($proxy->confirmed_avito==0 && $proxy->confirmed_auto==1):?>
                    used-partial
                <?php elseif ($proxy->status==1): ?>
                    used
                <?php elseif ($proxy->status==0): ?>
                    died
                <?php else: ?>
                    used-bulletin
                <?php endif; ?> <?php if ($days>30): ?>
                     old<?php endif; ?>">
                <td width="30" style="text-align: center">
                <?=$proxy->id;?>
                </td>
                <td width="360">
                <?=$proxy->proxy;?> / <?=$proxy->http_port;?> / <?=$proxy->socks5_port;?> / <?=$proxy->proxy_login;?> / <?=$proxy->proxy_pass;?>
                </td>
                <td width="110" style="text-align: center">
				<?=$proxy->phone;?>
                </td>
				<td width="60" style="text-align: center">
				<?=$proxy->confirmed_auto;?>
                </td>
				<td width="60" style="text-align: center">
				<?=$proxy->confirmed_avito;?>
                </td>
				<td width="80" style="text-align: center">
				<?=$proxy->user_id;?>
                </td>
				<td width="110" style="text-align: center">
				12
                </td>
                <td class="data" width="120" style="text-align: center">
                    <?=(new \DateTime($proxy->createdate))->format('d.m.Y H:i'); ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
</div>