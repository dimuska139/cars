<span class="ads_amount">Найдено объявлений: <?=count($info); ?></span>
<?php if (!empty(count($info))): ?>
<div class="byphonesearch_list_header">
    <div class="time">дата</div>
    <div class="car">автомобиль (двигатель кпп)</div>
    <div class="price">цена, р.</div>
    <div class="year">год</div>
    <div class="run">пробег, км</div>
    <div class="body">кузов</div>
    <div class="region">регион</div>
</div>
<?php endif; ?>        
<div class="byphonesearch_list_body">
<?php foreach ($info as $num => $ads):?>
<div class="line">
    <div class="time"><?=date("H:i d.m.Y", strtotime($ads['dt']));?></div>
    <div class="name-engine"><span class="name"><?=$ads['marka']." ".$ads['model']."</span> (".$ads['enginevol'].$ads['transmission'].")";?></div>
    <div class="price"><?=number_format($ads['price'],0,' ',' ');?></div>
    <div class="year"><?=$ads['year'];?></div>
    <div class="run"><?=number_format($ads['run'],0,' ',' ');?></div>
    <div class="body"><?=$ads['body'];?></div>
    <div class="region"><?=$ads['region'];?></div>
</div>
<?php endforeach; ?>
</div>