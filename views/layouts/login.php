<?php
    use yii\helpers\Html;
    use app\assets\AppAsset;
    AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>"/>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta name="viewport" content="width=device-width"/>
    <meta name="viewport" content="target-densitydpi=high-dpi" />
    <meta http-equiv="Content-Language" content="ru"/>
    <meta name="format-detection" content="telephone=no"> 
	<meta http-equiv="x-rim-auto-match" content="none">
    <meta name="viewport" content="width=1200"/>
<!--    <meta name="viewport" content="width=device-width, initial-scale=1.0" /> -->
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <link rel="icon" type="image/png" href="/resources/img/favicon.png">
    <link rel="apple-touch-icon" href="/resources/img/ios.png">
    <?php $this->head() ?>
</head>

<body class="login-body">
    <div class="top">
        <div class="inner">
            <div class="logo">
                
            </div>
            <div class="information">
                <div class="second-line">– Помощник по купле-продаже автомобилей</div>
            </div>
            <div class="contacts">
                <div class="first-line">тел. техподдержки: +79251682285</div>
            </div>
        </div>
    </div>
    <div class="main-part">
        
       <!-- <div class="menu_block">
            <div class="menu">
                <div class="top">
                    <p class="information">AM97.RU: Помощник по купле-продаже автомобилей</p>
                    <div class="changepassword_place">
                        <div class="question">Забыли пароль?</div>
                        <div class="input-place">
                            <span class="info">Новый пароль будет отправлен Вам в СМС!</span>
                            <input type="text" id="login" placeholder="Телефон">
                            <span class="password-button">Отправить</span>
                        </div>
                        <div class="result"></div>
                    </div>
                </div>
                <div class="bottom">
                    <form class="login-form" action="./login" method="post">
                        <input required placeholder="Логин" type="text" name="login" autocomplete="off" value="<?=!empty($_POST['login'])?$_POST['login']:'';?>">
                        <input required placeholder="Пароль" type="password" name="password" autocomplete="off">
                        <input type="hidden" name="_csrf" value="<?=Yii::$app->request->getCsrfToken();?>">
                        <div class="login-button">Войти</div>
                        <?php if ($this->params['error']): ?>
                            <div class="error">
                                <b>Не удается войти.</b>
                                Пожалуйста, проверьте правильность написания логина и пароля.
                            </div>
                        <?php endif; ?>
                    </form>
                </div>
            </div>
        </div> -->
        <?php $this->beginBody() ?>
        <div class="page">
            <div class="content">
                <?= $content ?>
            </div>
        </div>   
        <?php $this->endBody() ?>
        
    </div>
    <div class="footer_block">
            <div class="footer">
                <p>AM97.RU: Помощник по купле-продаже автомобилей</p>
            </div>
        </div>
</body>
<?php echo \Yii::$app->view->renderFile('@app/views/site/yandex_metrika.php'); ?>
<?php echo \Yii::$app->view->renderFile('@app/views/site/google_analytics.php'); ?>
</html>
<?php $this->endPage() ?>
