<?php
    use yii\helpers\Html;
    use app\assets\AppAsset;
    AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>"/>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta name="viewport" content="width=device-width"/>
    <meta name="viewport" content="target-densitydpi=high-dpi" />
    <meta http-equiv="Content-Language" content="ru"/>
    <meta name="format-detection" content="telephone=no"> 
	<meta http-equiv="x-rim-auto-match" content="none">
    <meta name="viewport" content="width=1200">
    <?php if (!empty($this->params['off_format_detection'])):?>
        <meta name="format-detection" content="telephone=no" />
    <?php endif; ?>
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <link rel="icon" type="image/png" href="/resources/img/favicon.png">
    <link rel="apple-touch-icon" href="/resources/img/ios.png">
    <?php $this->head() ?>
</head>

<body>
    <!-- Меню -->
    <div class="main-part">
        <div class="menu_block">
            <div class="menu">
                <div class="top">
                    <p class="information">Добро пожаловать, <?=!empty($this->params['name'])?$this->params['name']:'';?>!  Баланс <span id="balance"><?=!empty($this->params['balance'])?number_format($this->params['balance'],0,' ',' '):'0';?></span> рублей!</p>
                    <p class="date"><?=!empty($this->params['datetime'])?$this->params['datetime']:'';?></p>
                    <p class="help-phone">тел. техподдержки: +79251682285</p>
                </div>
                <div class="bottom">
                    <ul>
                        <?php if (empty($this->params['is_admin'])): ?>
                        <?php if (!empty($this->params['active_page']) && $this->params['active_page']=="online"):?>
                            <li class="active">Поиск тс</li>
                        <?php else: ?>
                            <a href="/online"><li <?php if (!empty($this->params['active_page']) && $this->params['active_page']=="online"):?>class="active"<?php endif; ?>>Поиск тс</li></a>
                        <?php endif; ?>
                        <?php if (!empty($this->params['active_page']) && $this->params['active_page']=="main"):?>
                            <li class="active">Мои объявления</li>
                        <?php else: ?>
                            <a href="/active"><li <?php if (!empty($this->params['active_page']) && $this->params['active_page']=="main"):?>class="active"<?php endif; ?>>Мои объявления</li></a>
                        <?php endif; ?>
                        <?php if (!empty($this->params['active_page']) && $this->params['active_page']=="check"):?>
                            <li class="active">Проверка тс</li>
                        <?php else: ?>
                            <a href="/check"><li <?php if (!empty($this->params['active_page']) && $this->params['active_page']=="check"):?>class="active"<?php endif; ?>>Проверка тс</li></a>
                        <?php endif; ?>
                        <?php if (!empty($this->params['active_page']) && $this->params['active_page']=="sms"):?>
                            <li class="active">Уведомления</li>
                        <?php else: ?>
                            <a href="/sms"><li <?php if (!empty($this->params['active_page']) && $this->params['active_page']=="sms"):?>class="active"<?php endif; ?>>Уведомления</li></a>
                        <?php endif; ?>
                        <?php if (!empty($this->params['active_page']) && $this->params['active_page']=="balance"):?>
                            <li class="active">Баланс</li>
                        <?php else: ?>
                            <a href="/balance"><li <?php if (!empty($this->params['active_page']) && $this->params['active_page']=="balance"):?>class="active"<?php endif; ?>>Баланс</li></a>
                        <?php endif; ?>
                        <?php if (!empty($this->params['active_page']) && $this->params['active_page']=="settings"):?>
                            <li class="active">Настройки</li>
                        <?php else: ?>
                            <a href="/settings"><li <?php if (!empty($this->params['active_page']) && $this->params['active_page']=="settings"):?>class="active"<?php endif; ?>>Настройки</li></a>
                        <?php endif; ?>
                        <?php else: ?>
                            <?php if (!empty($this->params['active_page']) && $this->params['active_page']=="tasks"):?>
                                <li class="active">Объявления</li>
                            <?php else: ?>
                                <a href="/admin/tasks"><li>Задания</li></a>
                            <?php endif; ?>
                    <!--    <a href=""><li>Все объявления</li></a> -->
                            <?php if (!empty($this->params['active_page']) && $this->params['active_page']=="users"):?>
                                <li class="active">Пользователи</li>
                            <?php else: ?>
                                <a href="/admin/users"><li>Пользователи</li></a>
                            <?php endif; ?>
                            <?php if (!empty($this->params['active_page']) && $this->params['active_page']=="proxy"):?>
                                <li class="active">Прокси</li>
                            <?php else: ?>
                                <a href="/admin/proxy"><li>Прокси</li></a>
                            <?php endif; ?>
                        <?php endif; ?>
                    </ul>
                    <ul class="ul_2">
                        <a href="/users/exit"><li class="exit">Выйти</li></a>
                    </ul>
                </div>
            </div>
        </div>
        <?php $this->beginBody() ?>
        <!-- Content -->
                    <div class="page">     
                            <div class="content">
                                <?= $content ?>
                            </div>
                    </div>

        <?php $this->endBody() ?>
        <!-- Футер -->
        
    </div>
        <div class="footer_block">
            <div class="footer">
                <p>AM97.RU: Помощник по купле-продаже автомобилей</p>
            </div>
        </div>
</body>
<?php echo \Yii::$app->view->renderFile('@app/views/site/yandex_metrika.php'); ?>
<?php echo \Yii::$app->view->renderFile('@app/views/site/google_analytics.php'); ?>
</html>
<?php $this->endPage() ?>
