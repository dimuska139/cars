<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\assets;

use yii\web\AssetBundle;
use Yii;
/**
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'resources/jquery-ui-1.11.4/jquery-ui.min.css',
   //     'resources/css/main.css',
    //    'css/site.css',
     //   'resources/css/main.css'
    //    'resources/css/slider.css',
    ];
    public $js = [
      //  'resources/js/jquery-2.1.3.min.js',
        'resources/js/jquery.cookie.js',
        'resources/jquery-ui-1.11.4/jquery-ui.min.js',
     //   'resources/js/jquery.rotate2.2.min.js',
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
    ];
    
    public function init()
    {
        parent::init();
        // resetting BootstrapAsset to not load own css files
        Yii::$app->assetManager->bundles['yii\\bootstrap\\BootstrapAsset'] = [
            'css' => []
        ];
    }
}
