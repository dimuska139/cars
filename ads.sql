-- phpMyAdmin SQL Dump
-- version 4.2.12deb2
-- http://www.phpmyadmin.net
--
-- Хост: localhost
-- Время создания: Фев 28 2016 г., 14:46
-- Версия сервера: 5.5.44-0+deb8u1
-- Версия PHP: 5.6.7-1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- База данных: `cars`
--

-- --------------------------------------------------------

--
-- Структура таблицы `ads`
--

CREATE TABLE IF NOT EXISTS `ads` (
`Id` bigint(20) NOT NULL,
  `brand_id` int(11) NOT NULL,
  `model` varchar(100) NOT NULL,
  `model_2` varchar(100) DEFAULT NULL,
  `price` int(11) DEFAULT '0',
  `body` varchar(45) NOT NULL DEFAULT '',
  `engine` varchar(20) NOT NULL DEFAULT '',
  `enginevol` varchar(45) NOT NULL DEFAULT '',
  `transmission` varchar(25) NOT NULL DEFAULT '',
  `condition` tinyint(1) NOT NULL DEFAULT '0',
  `run` int(11) DEFAULT NULL,
  `rudder` varchar(20) NOT NULL DEFAULT '',
  `phone` varchar(20) NOT NULL DEFAULT '',
  `fio` varchar(45) NOT NULL DEFAULT '',
  `photo` text NOT NULL,
  `metro` varchar(50) NOT NULL DEFAULT '',
  `address` varchar(100) NOT NULL DEFAULT '',
  `region` varchar(50) NOT NULL DEFAULT '',
  `url` varchar(250) NOT NULL DEFAULT '',
  `year` year(4) NOT NULL,
  `average_price` int(11) NOT NULL,
  `phone_find` int(10) NOT NULL,
  `region_Id` int(11) NOT NULL,
  `source` varchar(20) NOT NULL,
  `source_id` int(11) NOT NULL,
  `info` text NOT NULL,
  `body_id` int(11) NOT NULL,
  `color_id` int(11) NOT NULL,
  `createtime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `moderation_time` datetime DEFAULT NULL,
  `old_id` int(11) NOT NULL,
  `hash` varchar(40) NOT NULL,
  `vin` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB AUTO_INCREMENT=501 DEFAULT CHARSET=utf8;

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `ads`
--
ALTER TABLE `ads`
 ADD PRIMARY KEY (`Id`), ADD UNIQUE KEY `old_id` (`old_id`), ADD KEY `hash` (`hash`), ADD KEY `createtime` (`createtime`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `ads`
--
ALTER TABLE `ads`
MODIFY `Id` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=501;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
