<?php
namespace app\commands;
use yii\console\Controller;
use Yii;
use app\models\Ads;
use app\models\Colors;
use app\models\Body;
use app\models\Brands;
use app\models\Sources;
use app\models\Tasks;
use app\models\Users;
use app\models\Payment;
use app\models\Operations;

// Контроллер, отвечающий за выполнение задач
class TasksController extends Controller {
    public function actionIndex()
    {
        set_time_limit(0);
        $tasks = Tasks::find()->all();
        foreach ($tasks as $i => $task) {
            $now = new \DateTime("now", new \DateTimeZone('Europe/Moscow'));
            $last_start_time = new \DateTime($task->last_start_time, new \DateTimeZone('Europe/Moscow'));
            $next_start_time = clone $last_start_time;
            $next_start_time->add(new \DateInterval($task->interval));
            if ($next_start_time<$now){
                if ($task->status == 1 && $task->name!='ads_add'){ // Если у скрипта статус "выполняется", но при этом вышел таймаут, то считаем, что он завершился с ошибкой
                    $die_time = clone $last_start_time;
                    $die_time->add(new \DateInterval(Yii::$app->params['TASK_LIFETIME']));
                    if ($die_time>$now) // Если время, до которого скрипт должен закончить работу, еще не наступило, то считаем, что скрипт еще работает
                        continue;
                }
                $task->status = 1;
                $task->last_start_time = $now->format('Y-m-d H:i:s');
                $task->save();
                $microtime = microtime(true);
                switch ($task->name){
                    case 'ads_add': // Добавление объявлений 'online'
                        $this->add_ads();
                        break;
                    case 'ads_deleteold': // Проверка на необходимость удаления "онлайн"-объявлений
                        $this->delete_ads();
                        break;
                    case 'bulletin_end': // Проверка на необходимость перевода обычных объявлений в статус "завершен"
                        $this->bulletin_end();
                        break;
                    case 'qiwi_checkpaymant': // Проверка платежа 'Qiwi'
                        $this->checkpayment();
                        break;
                    
                }
                $completed_task = Tasks::findOne([
                    'id' => $task->id
                ]);
                $completed_task->worktime = microtime(true)-$microtime;
                $completed_task->status = 0;
                $completed_task->save();
            }
        }
    }
    
    // Перевод объявления в статус "завершен"
    private function bulletin_end(){
        define("SQL_USER",     Yii::$app->params['SQL_USER']);
	define("SQL_PASSWORD", Yii::$app->params['SQL_PASSWORD']);
	define("SQL_BASE",     Yii::$app->params['SQL_BASE']);
	define("SQL_SERVER",   Yii::$app->params['SQL_SERVER']);
        $sql=new \mysqli(SQL_SERVER, SQL_USER, SQL_PASSWORD, SQL_BASE);
	$sql->set_charset("utf8");
        $sql_str = "UPDATE `cars` SET `status`=5 WHERE `createdate`<(NOW() - interval ".Yii::$app->params['MAX_BULLETIN_LIFETIME']." day)";
        if (!$sql->query($sql_str)){
            $t = Tasks::findOne([
                "name" => "bulletin_end"
            ]);
            $t->last_error = $sql->error;
            $t->update();
        }
        
    }
    
    // Удаление старых объявлений
    private function delete_ads()
    {
        define("SQL_USER",     Yii::$app->params['SQL_USER']);
	define("SQL_PASSWORD", Yii::$app->params['SQL_PASSWORD']);
	define("SQL_BASE",     Yii::$app->params['SQL_BASE']);
	define("SQL_SERVER",   Yii::$app->params['SQL_SERVER']);
        $sql=new \mysqli(SQL_SERVER, SQL_USER, SQL_PASSWORD, SQL_BASE);
	$sql->set_charset("utf8");
        $sql_str = "SELECT `Id` FROM `ads` WHERE `createtime`<(NOW() - interval ".Yii::$app->params['MAX_ADS_LIFETIME']." day)";
        $rows = $sql->query($sql_str);
        $ids_to_delete = [];
        while($item = $rows->fetch_array(MYSQLI_ASSOC)){
            $ids_to_delete[] = $item['Id'];
        }
        if (!empty($ids_to_delete)){
            $sql_str = "DELETE FROM `ads` WHERE `Id` IN (".implode(',',$ids_to_delete).")";
            if (!$sql->query($sql_str)){
                $t = Tasks::findOne([
                    "name" => "ads_deleteold"
                ]);
                $t->last_error = $sql->error;
                if (!$t->update()){
                    var_dump($t->errors);
                }
            }
            $sql_str = "DELETE FROM `operations` WHERE `bulletin_id` IN (".implode(',',$ids_to_delete).")";
            if (!$sql->query($sql_str)){
                $t = Tasks::findOne([
                    "name" => "ads_deleteold"
                ]);
                $t->last_error = $sql->error;
                if (!$t->update()){
                    var_dump($t->errors);
                }
            }
        }
        
    }
    
    // Добавление объявлений
    private function add_ads()
    {
        $time = time();
        try {
            $data_json = file_get_contents("http://crwl.ru/api/rest/latest/get_new_ads/?api_key=b28fc651b8d409fa0034a331b0b18554&last=1&avito=1");
            if(!empty($data_json)){							
                $ads = json_decode($data_json, true);
                // Цикл обработки полученных объявлений
                echo count($ads).": ";
                $counter = 1;
                
                foreach($ads as $ad){
                    
                    // Если нет марки или модели - объявление пропускаем
           //         if (empty($ad['marka']) || empty($ad['model']))
            //            continue;

                    // Допустимые источники объявлений
                 /*   if (strcmp($ad['source'],'autoru')!=0 
                        && strcmp($ad['source'],'trucksautoru')!=0 
                        && strcmp($ad['source'],'avitoru')!=0
                        && strcmp($ad['source'],'dromru')!=0
                        && strcmp($ad['source'],'amru')!=0
                        && strcmp($ad['source'],'irrru')!=0)
                        continue;
*/
                    // Переименование КПП
                    if (strcasecmp($ad['transmission'],'МКПП')==0)
                        $ad['transmission']='МТ';
                    if (strcasecmp($ad['transmission'],'АКПП')==0 ||
                        strcasecmp($ad['transmission'],'CVT')==0)
                        $ad['transmission']='АТ';

                    if (mb_strpos($ad['transmission'],'мат'))
                        $ad['transmission'] = 'AT';
                    if (mb_strpos($ad['transmission'],'бот'))
                        $ad['transmission'] = 'AT';
                    if (mb_strpos($ad['transmission'],'тор'))
                        $ad['transmission'] = 'AT';
                    if (mb_strpos($ad['transmission'],'CVT'))
                        $ad['transmission'] = 'AT';
                    if (mb_strpos($ad['transmission'],'AMT'))
                        $ad['transmission'] = 'AT';
                    if (mb_strpos($ad['transmission'],'аника'))
                        $ad['transmission'] = 'MT';
                    if (mb_strpos($ad['transmission'],'аническая'))
                        $ad['transmission'] = 'MT';


            /*        if (mb_strpos($ad['body'],'мобили'))
                        continue;
                    if (mb_strpos($ad['body'],'цвет'))
                        continue;
                    if (mb_strpos($ad['body'],'уль'))
                        continue;*/

                    // Переименование типа кузова под наши стандарты
                    if (mb_strpos($ad['body'],'иолет'))
                        $ad['body'] = 'Кабриолет';

                    if (mb_strpos($ad['body'],'иверсал'))
                        $ad['body'] = 'Универсал';

                    if (mb_strpos($ad['body'],'упе'))
                        $ad['body'] = 'Купе';

                    if (mb_strpos($ad['body'],'едан'))
                        $ad['body'] = 'Седан';

                    if (mb_strpos($ad['body'],'недорожник'))
                        $ad['body'] = 'Внедорожник';

                    if (mb_strpos($ad['body'],'роавтобус'))
                        $ad['body'] = 'Микроавтобус';

                    if (mb_strpos($ad['body'],'имузин'))
                        $ad['body'] = 'Лимузин';

                    if (mb_strpos($ad['body'],'икап'))
                        $ad['body'] = 'Пикап';

                    if (mb_strpos($ad['body'],'жип'))
                        $ad['body'] = 'Внедорожник';

                    if (mb_strpos($ad['body'],'тчб'))
                        $ad['body'] = 'Хэтчбэк';

                    if (mb_strpos($ad['body'],'инив'))
                        $ad['body'] = 'Минивэн';

                    if (mb_strpos($ad['body'],'россов'))
                        $ad['body'] = 'Кроссовер';

                    if (mb_strpos($ad['body'],'ургон'))
                        $ad['body'] = 'Фургон';

                    if (mb_strpos($ad['body'],'ифт'))
                        $ad['body'] = 'Хэтчбэк';

                    if (mb_strpos($ad['body'],'омпакт'))
                        $ad['body'] = 'Минивэн';

                    if (mb_strpos($ad['body'],'вэн'))
                        $ad['body'] = 'Минивэн';

                    if (mb_strpos($ad['body'],'идстер')) // спидстер
                        $ad['body'] = 'Купе';

                    if (mb_strpos($ad['body'],'одстер')) // родстер
                        $ad['body'] = 'Кабриолет';

                    if (strcmp($ad['source'],'trucksautoru')==0)
                        $ad['body'] = 'Фургон';

                    if (mb_strpos($ad['body'],'астб'))
                        $ad['body'] = 'Хэтчбэк';

                    if (mb_strpos($ad['body'],'орт'))
                        $ad['body'] = 'Фургон';

                    if (mb_strpos($ad['body'],'арга'))
                        $ad['body'] = 'Фургон';

                    if (mb_strpos($ad['body'],'ткрыт')) // открытый
                        $ad['body'] = 'Кабриолет';

                    if (mb_strpos($ad['body'],'асси')) // шасси
                        $ad['body'] = 'Фургон';

                    if (mb_strpos($ad['body'],'ардтоп')) // хардтоп
                        $ad['body'] = 'Кабриолет';

                    if (mb_strpos($ad['body'],'ент')) // тент
                        $ad['body'] = 'Фургон';

                    if (mb_strpos($ad['body'],'омби')) // комби
                        $ad['body'] = 'Фургон';

                    if (mb_strpos($ad['body'],'рузовик')) // грузовик
                        $ad['body'] = 'Фургон';

                    // Получение ID марки
                    $ads = new Ads();
                    if (strcmp(mb_convert_encoding($ad['marka'], 'utf-8'), mb_convert_encoding('Лада', 'utf-8'))==0)
                        $ad['marka'] = 'ВАЗ (Lada)';
                    $brand = (new \yii\db\Query())
                        ->select(['id'])
                        ->from('brands')
                        ->where(['like', 'lower(name)', strtolower($ad['marka'])])
                        ->one();
                    // Если марка найдена, берем ее id
                    if (!empty($brand))
                        $ads->brand_id = $brand['id'];
                    else { // Если не найдена - добавляем ее в базу, после чего берем id
                        $b = new Brands();
                        $b->name = $ad['marka'];
                        $b->alias = $ad['marka'];
                        $b->folder_id = 0;
                        $b->is_popular = 0;
                        $b->published = 0;
                        $b->img_name = '';
                        if ($b->save())
                            $ads->brand_id = $b->id;
                        else
                            $ads->brand_id = 0;
                    }
                    $ads->model = $ad['model'];
                    $ads->model_2 = $ad['model_2'];
                    // Преобразования для цен
                    if (!empty($ad['price'])){
                        $ad['price'] = (int)$ad['price']<1000?(int)($ad['price'].'000'):(int)$ad['price'];
                        $ad['price'] = (int)$ad['price']>9999999?'':(int)$ad['price'];
                    } else
                        $ad['price'] = 0;
                    $ads->price = $ad['price'];
                    $ads->vin = !empty($ad['vin'])?1:0;
                    // Поиск существующего кузова в базе
                    if (!empty($ad['body'])){
                        $exists_body = Body::find()->where(['name' => $ad['body']])->one();
                        if (!empty($exists_body)){
                            $ads->body_id = $exists_body->id;
                        } else {
                            $ads->body = $ad['body'];
                            $body = new Body();
                            $body->name = $ad['body'];
                            $body->show_in_creation = 0;
                            if ($body->save())
                                $ads->body_id = $body->id;
                        }
                    } else {
                        $ads_with_same_body = Ads::find()->where(['model' => $ad['model']])->one();
                        if (!empty($ads_with_same_body))
                            $ads->body_id = $ads_with_same_body->body_id;
                    }

                    // Приходит только от avito
                    $ads->moderation_time = !empty($ad['avito'])?$ad['avito']:'';

                    // От типа двигателя берем только первую букву
                    if (mb_strpos($ad['engine'],'зин') || mb_strpos($ad['fuel'],'зин'))
                        $ads->engine = 'b';
                    else if (mb_strpos($ad['engine'],'зел') || mb_strpos($ad['fuel'],'зел'))
                        $ads->engine = 'd';
                    else if (empty($ad['engine']))
                        $ads->engine = '';
                    else
                        continue; /*if (mb_strpos($ad['engine'],'брид'))
                        $ads->engine = 'Э';
                    else if (mb_strpos($ad['engine'],'ектр'))
                        $ads->engine = 'Э';
                    else if (mb_strpos($ad['engine'],'аз'))
                        $ads->engine = 'Г';
                    else
                        $ads->engine = $ad['engine'];*/

                    $ads->enginevol = $ad['enginevol'];
                    $ads->transmission = $ad['transmission'];
                    $ads->condition = ((mb_strpos($ad['condition'], "е") && !(mb_strpos($ad['condition'], "е на хо"))) || empty($ad['condition']))?0:1; // Если есть "е", то "нЕ битый"
                    $ads->run = $ad['run'];
                    // Левый (0) или правый (1) руль
                    if (mb_strpos($ad['wheel'],'ав'))
                        $ads->rudder = 1;
                    else
                        $ads->rudder = 0;
                    $ads->phone = $ad['phone'];
                    $ads->fio = $ad['fio'];
                    $ads->photo = $ad['photo'];
                    $ads->metro = $ad['metro'];
                    $ads->address = $ad['address'];
                    $ads->region = $ad['region'];
                    $ads->url = $ad['url'];
                    $ads->year = $ad['year'];
                    $ads->average_price = $ad['average_price'];
                    $ads->phone_find = $ad['phone_find'];
                    $ads->createtime = $ad['dt'];
          //          $ads->phone_find = $ad['phone_find']>99?99:$ad['phone_find'];
                    $ads->region_Id = $ad['region_Id'];
                    $exists_source = Sources::find()->where(['source' => $ad['source']])->one();
                    if (!empty($exists_source))
                        $ads->source_id = $exists_source->id;
                    else {
                        $s = new Sources();
                        $s->source = $ad['source'];
                        $s->alias = $ad['source'];
                        $s->save();
                        $ads->source_id = $s->id;
                    }
                    $ads->info = $ad['info'];
                    $ads->old_id = (int)$ad['Id'];

                    // Разбиваем на слова фразу с цветом
                    $color_arr = explode(' ', $ad['color']);
                    if (count($color_arr)<=2 && strlen($color_arr[0])>1){
                        $metallic = (count($color_arr)==2 && in_array('металлик', $color_arr))?1:0;
                        // Оставляем только кириллицу
                        $color = preg_replace('/[^а-яА-Я]/ui', '', $color_arr[0]);
                        if (mb_strpos($color, 'рный'))
                            $color = 'чёрный';
                        if (mb_strpos($color, 'лтый'))
                            $color = 'жёлтый';
                        if (mb_strpos($color, 'зелён'))
                            $color = 'зелёный';
                        if (mb_strpos($color, 'бряный'))
                            $color = 'серебряный';
                        if (mb_strpos($color, 'бристый'))
                            $color = 'серебристый';
                        if ($metallic == 0)
                            $exists_color = Colors::find()->where(['like', 'name', '%'.$color.'%', false])->one();
                        else
                            $exists_color = Colors::find()->where(['like', 'name', '%'.$color.'%', false])->andWhere(['metallic' => 1])->one();

                        if (!empty($exists_color))
                            $ads->color_id = $exists_color->id;
                        else {
                            $c = new Colors();
                            $c->name = $color;
                            $c->hex = '';
                            $c->metallic = $metallic;
                            $c->dark = 1; // Делает букву "М" на цвете (в поиске) белой (чтобы был контраст)
                            $c->published = 0;
                            if ($c->save())
                                $ads->color_id = $c->id;
                            else
                                print_r($c->errors);
                        }
                    } else
                        $ads->color_id = 0;
                    $hash = sha1($ads->model.$ads->year.$ads->enginevol.$ads->transmission.$ads->body_id.$ads->color_id);
                    $ads->hash = $hash;
                    
                    if (!$ads->save()){
                        $serialized_error = serialize($ads->errors);
                        $t = Tasks::findOne([
                            "name" => "ads_add"
                        ]);
                        $t->last_error = $serialized_error;
                        $t->save();
                    }
                        
                    else {
                    /*    $sql ="SELECT 
                                COUNT(*) AS `avg_count`
                            FROM
                                `ads` ads_count
                            WHERE
                                ads_count.Id <> ".$ads->Id."
                                    AND ads_count.price<>0
                                    AND ads_count.hash = '".$ads->hash."'";
                        $rows=$sql->query($sql_str);
                        $avg_count = false;
                        while($rlt = $rows->fetch_array(MYSQLI_ASSOC)){
                            $avg_count = $rlt['avg_count'];
                            break;
                        }*/
                    }
                  //      echo($ads->Id." ");
                    echo $counter.", ";
                    $counter++;
                }
            }
        } catch (Exception $e) {
            echo 'Выброшено исключение: ',  $e->getMessage(), "\n";
        }
    }
    
    private function checkpayment()
    {
        $payments = Payment::findAll([
            'active' => 1,
            'completed' => 0
        ]);
        foreach ($payments as $payment){
            $endtime = strtotime($payment->createdate."+".Yii::$app->params['QIWI_LIFETIME']." min");
            $currenttime = strtotime(date("Y-m-d h:i:s"));
            
            if ($endtime<$currenttime){ // Закончилось время жизни платежа
                $payment->active = 0;
                $payment->save();
                $operation = Operations::findOne([
                    'bulletin_id' => $payment->id,
                    'user_id' => $payment->user_id,
                    'type' => "notperformed"
                ]);
                $operation->status = "completed";
                $operation->save();
                continue;
            }
            $result = file_get_contents(Yii::$app->params['QIWI_CHECK_URL']."&transaction=".$payment->id);
            if (mb_strpos($result, 'уже оплачен')){
                $current_user = Users::findOne([
                    'id' => $payment->user_id
                ]);
                $sum = (int)$payment->sum;
                $sum_to_add = $sum;
                if ($sum>=5000 && $sum<15000){
                    $sum_to_add = $sum+floor($sum*0.1); // +10%
                }
                if ($sum>=15000){
                    $sum_to_add = $sum+floor($sum*0.2); // +20%
                }
                
                // Новый баланс
                $current_user->balance = (int)$current_user->balance+$sum_to_add;
                $payment->completed = 1;
                $payment->active = 1;
                if ($payment->save()){
                    $operation = Operations::findOne([
                        'bulletin_id' => $payment->id,
                        'user_id' => $payment->user_id,
                        'type' => "payment"
                    ]);
                    $operation->status = "completed";
                    $operation->save();
                    if (!$current_user->save()){
                        $serialized_error = serialize($current_user->errors);
                        $t = Tasks::findOne([
                            "name" => "qiwi_checkpayment"
                        ]);
                        $t->last_error = $serialized_error;
                        $t->save();
                    }
                }
            }
        }
    }
}