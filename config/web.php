<?php

$params = require(__DIR__ . '/params.php');

$config = [
    'id' => 'basic',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'components' => [
        'request' => [
            // !!! insert a secret key in the following (if it is empty) - this is required by cookie validation
            'cookieValidationKey' => 'aer.kbja kjljuvshdgjgfvterg3453223gdfjhb1!#',
        ],
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'user' => [
            'identityClass' => 'app\models\User',
            'enableAutoLogin' => true,
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'mailer' => [
            'class' => 'yii\swiftmailer\Mailer',
            // send all mails to a file by default. You have to set
            // 'useFileTransport' to false and configure a transport
            // for the mailer to send real emails.
            'useFileTransport' => false,
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'db' => require(__DIR__ . '/db.php'),
        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'rules' => [
                'online' => 'site/search',
                'search' => 'site/searchbyphone',
                'sms' => 'site/sms',
                'check' => 'site/check',
                'clicks' => 'site/clicks',
                'ads' => 'site/ads',
                'gettime' => 'global/gettime',
             //   'active' => 'site/active',
             //   'myads' => 'site/active',
                'deleted' => 'site/deleted',
                'balance' => 'site/balance',
                'qiwiredirect' => 'site/qiwiredirect',
                'clear_search_filters' => 'site/clearsearchfilters',
                '/' => 'users/login',
                'settings' => 'users/profile',
                '<action>'=>'site/<action>',
                '<controller>/<action>' => '<controller>/<action>',
            ]
        ],
    ],
    'params' => $params,
];

if (YII_ENV_DEV) {
    // configuration adjustments for 'dev' environment
    $config['bootstrap'][] = 'debug';
    $config['modules']['debug'] = 'yii\debug\Module';

    $config['bootstrap'][] = 'gii';
    $config['modules']['gii'] = 'yii\gii\Module';
}

return $config;
