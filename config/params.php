<?php

return [
    'adminEmails' => [
        'dimuska139@mail.ru',
        'am97.ru@mail.ru'
    ],
    'notificationEmail' => 'notification@am97.ru',
    'price' => 499, // Цена размещения объявления
    'ads_price' => 2, // Цена получения ссылки и номера со страницы поиска
 //   'phonesearch_price' => 5, // Цена поиска по номеру
    'QIWI_CREATE_URL' => "https://w.qiwi.com/order/external/create.action?currency=RUB&from=307252", // URL для QIWI (создание платежа)
    'QIWI_CHECK_URL' => "https://w.qiwi.com/order/external/main.action?shop=307252", // URL для QIWI (проверка платежа)
    'QIWI_LIFETIME' => 60, // Всемя жизни платежа в минутах
    'SQL_USER' => "root",
    "SQL_PASSWORD" => '12345',
    "SQL_BASE" => "cars",
    "SQL_SERVER" => "localhost",
    "MAX_ADS_LIFETIME" => 30, // Время хранения объявлений в днях
    "MAX_BULLETIN_LIFETIME" => 30, // Время хранения созданных у нас объявлений в днях
    "TASK_LIFETIME" => 'PT5S', // Максимальное время выполнения задачи в формате параметра interval_spec: http://php.net/manual/ru/dateinterval.construct.php
    "thumb_width" => "432", // Ширина уменьшенной копии изображения
    "SMSCRU_LOGIN" => "am97",
    "SMSCRU_PASSWORD" => "AM5559560",
    "PASSWORD_LENGTH" => 5,
    "START_BALANCE" => 50,
    "MAX_REGISTRATION_ATTEMPT" => 3,
    "MAX_REGISTRATION_TIMEOUT" => 25,
    "STATUSES" => [
        "created" => [
            "code" => 0,
            "name" => "Добавление"
        ],
        "active" => [
            "code" => 3,
            "name" => "Активный"
        ]
    ],
    "CRWL_REG_API_KEY" => "b28fc651b8d409fa0034a331b0b18554",
];
